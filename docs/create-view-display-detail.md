이 섹션에서는 사용자가 기사 제목을 클릭하면 기사에 대한 세부 정보가 디스플레이된다.

## 프로젝트 디렉토리로 이동하여 가상 환경을 활성화

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## MySQL이 실행 중인지 확인
이 프로젝트에서는 MySQL 데이터베이스를 사용하므로 MySQL 상태가 실행 중인지 확인한다.

## `blog/views.py` 파일에 메서드 만들기

### `get_object_or_404()`
<i>`get_object_or_404(klass, args, *kwargs)`

지정된 모델 관리자에서 `get()`을 호출하지만 모델의 `DoesNotExist` 예외 대신 `Http404`를 발생시킨다.

`Arguments`

`klass` - 객체를 가져올 `Model` 클래스, `Manager` 또는 `QuerySet` 인스턴스입니다.

`*args` - `Q` 객체.

`**kargs` - 조회 매개변수, 이는 `get()`와 `filter()`에서 허용하는 형식이어야 한다</i>

VSCode IDE에서 `blog/views.py` 파일을 열고 `article_details()` 메서드를 생성한다. 이 메서드는 데이터베이스에서 `PUBLISHED` 기사의 ID를 가져온다.

```python
from django.shortcuts import render, get_object_or_404

def article_details(request, id):
    try:
        article = get_object_or_404(Article, id=id, status=Article.Status.PUBLISHED)
    except Article.DoesNotExist:
        raise Http404("No article found.")
    
    return render(request, 'blog/detail.html', {'article': article})
    pass
```

![](images/create_view_display_detail/screenshot_05_01.webp)

## `blog/templates/blog` 디렉토리에 `detail.html` file 만들기
`detail.html` 파일을 생성하고 문서 세부 정보를 출력한다.

```html
{% extends "blog/base.html" %}

{% block title %}{{ article.title }}{% endblock %}

{% block content %}
    <h1>{{ article.title }}</h1>
    <p>
        Published {{ article.publish }} by {{ article.author }}
    </p>
    {{ article.body|linebreaks }}
{% endblock %}
```

![](images/create_view_display_detail/screenshot_05_02.webp)

## 기사 세부 정보를 위한 url pattern 만들기
`blog/urls.py` 파일을 열고 다음 줄을 추가한다.

```python
    path('<int:id>/', views.article_details, name='article_details'),​
```

![](images/create_view_display_detail/screenshot_05_03.webp)


## `blog/templates/blog/list.html` 파일 변경
기사 제목에 하이퍼링크를 추가하여 `title`을 클릭하면 특정 기사 세부 정보 페이지로 이동하도록 해야 한다.

```html
<h2>
    <a href="{% url 'blog:article_details' article.id %}">
        {{ article.title }}
    </a>
</h2>
```

![](images/create_view_display_detail/screenshot_05_04.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

![](images/create_view_display_detail/cli_05_01.png)

브라우저에 URL `http://127.0.0.1:8000/blog/`을 입력하면 아래와 같은 웹페이지가 디스플레이된다.

![](images/create_view_display_detail/screenshot_05_05.png)

제목을 클릭하면 아래와 같은 화면이 나타나고 URL은 `http://127.0.0.1:8000/blog/ID/`이다.

`http://127.0.0.1:8000/blog/3/`

![](images/create_view_display_detail/screenshot_05_06.png)

`http://127.0.0.1:8000/blog/2/`

![](images/create_view_display_detail/screenshot_05_07.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Created views to display list of articles and added template also"
$ git push origin main
```

![](images/create_view_display_detail/cli_05_02.png)

이 부분은 이 정도로 ...


**(주)** 위 내용은 [Create a view to display a article details — Part 5](https://medium.com/@nutanbhogendrasharma/create-a-view-to-display-a-article-details-in-django-part-5-35e75f1bc402)을 편역한 것이다. 
