*Django에서 Forms으로 작업하기*

양식(form)은 사용자의 입력을 형식에 따라 수집할 수 있는 HTML 요소이다.

데모에서 사용한 프로젝트의 코드는 [여기](https://github.com/okanyenigun/django-form-example)에 있다.

이 프로젝트에는 `home`이라는 매우 간단한 애플리케이션 하나만 있다. 화면에서 하나의 양식만 사용하여 사용자 정보를 수신하고 저장한다.

## Model 양식
이러한 양식은 관련 모델에 직접 연결되어 있다.

### Model

```python
# home/models.py

from django.db import models

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    city = models.CharField(max_length=100)
    age = models.IntegerField()
    hosting = models.BooleanField()

def __str__(self):
    return self.first_name + " " + self.last_name
```

### Form

```python
# home/forms.py

from django.forms import ModelForm
from .models import Customer

class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'
```

`ModelForm`을 사용하여 모델로부터 양식을 만들 수 있다.

내부 `class Meta`에서는 모델이 `Customer` 모델이 될 것임을 선언하고 모든 필드를 사용할 것이라고 기술한다. 이제 양식이 준비되었다. 우리는 view를 통해 HTML로 전달하면 된다.

### View

```python
# home/views.py

from django.shortcuts import render, redirect
from django.views import View
from .forms import CustomerForm

class HomeView(View):

    def get(self, request):
        form = CustomerForm()
        context= {"form": form}
        return render(request, './templates/home.html', context)
```

위에서 작성한 `CustomerForm`을 사용하여 양식을 만들었다. 그런 다음 템플릿으로 렌더링한다.

### Template

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Example</title>
</head>
<body>
    <form method="POST"> {% csrf_token %}
        {{form}}
        <button name="submit" type="submit">Save</button>
    </form>
</body>
</html>
```

폼 태그에 `{{form}}`을 사용한다. 매우 간단하다.

![ugly_form](images/django_forms/ugly_form.webp)

이제 양식을 작성하고, 입력을 보낸다. 양식으로부터 입력을 수집하려면 view 클래스를 수정해야 한다.

### View
먼저 POST 요청에서 템플릿으로부터 전송된 내용을 살펴보겠다.

```python
# home/views.py

class HomeView(View):

    def get(self, request):
        form = CustomerForm()
        context= {"form": form}
        return render(request, './templates/home.html', context)

    def post(self, request):
        print(request.POST)
        return redirect("home")
"""
<QueryDict: {'csrfmiddlewaretoken': ['Kw8D1qWs2cK8wtF05JRYxAezUD467oYd8bEulLDKko6Wd77UEd4h67repOKbskpc'], 
'first_name': ['Ronaldo'], 'last_name': ['WasRight'], 
'email': ['ronaldo_bigger_than_manu@cr7.com'], 
'city': ['Lisbon'], 'age': ['38'], 'hosting': ['on'], 'submit': ['']}>
"""
```

이제 양식을 저장한다.

```python
# home/views.py

...

def post(self, request):
      #create a new form object with the input
      form = CustomerForm(request.POST)
      #check if the form is valid
      if form.is_valid():
          #save it
          form.save()
      return redirect("home")

"""
We created the first record:
<QuerySet [<Customer: Ronaldo WasRight>]>
"""
```

지금까지 양식을 그대로 보냈다. 조금 커스터마이즈 해보겠다.

더 자세히 보려면 [codepen.io](https://codepen.io/)을 방문한다.

[이 양식 템플릿을 사용하겠다](https://codepen.io/chris__sev/pen/LYOyjY).

### Template

```html
<form method="POST"> {% csrf_token %}
  <div class="container">
      <h2>A Form<small>Customers</small></h2>
        <div class="group">      
          {{form.first_name}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>First Name</label>
        </div>
        <div class="group">      
          {{form.last_name}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Last Name</label>
        </div>
        <div class="group">      
          {{form.email}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email</label>
        </div>
        <div class="group">      
          {{form.city}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>City</label>
        </div>
        <div class="group">      
          {{form.age}}
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>AGE</label>
        </div>
        <button class="btn btn-success" name="submit" type="submit">Save</button>
    </div>
</form>
```

`{{form.first_name}}` 같은 것을 사용하여 양식의 각 속성을 개별적으로 사용할 수 있다.

![better_look](images/django_forms/better_look.webp)

## 기본 Form
모델을 고수하는 형태로 양식을 만들 필요는 없다. Django의 Form 클래스를 사용하여 양식을 원하는 형태로 만들 수 있다.

### Form

```python
# home/forms.py

from django import forms

class AnyForm(forms.Form):

    model_name = forms.CharField(label="Model Name",max_length=100)
    brand = forms.CharField(label="Brand", max_length=100)
    price = forms.IntegerField(label="Price")
```

`label` 속성을 사용하여 템플릿에서 보여 줄 레이블을 지정할 수 있다.

### View

```python
# home/views.py

from django.shortcuts import render, redirect
from django.views import View
from .forms import AnyForm
from .models import Customer

class HomeView(View):

    def get(self, request):
        form = AnyForm()
        context= {"form": form}
        return render(request, './templates/home.html', context)
```

### Template

```html
<form method="POST"> {% csrf_token %}
  {{form}}
  <button name="submit" type="submit">Save</button>
</form>
```

![any_form](images/django_forms/any_form.webp)

## Widgets
Form 필드는 특정 위젯을 갖을 수 있다. 위젯을 사용하여 커스터마이즈할 수 있다.

### Form

```python
# home/forms.py

class AnyForm(forms.Form):

    model_name = forms.CharField(label="Model Name",max_length=100, 
                                widget=forms.TextInput(
                                    attrs={
                                        'class': 'form-control',
                                        'placeholder': ('Model Name')
                                    }))
    brand = forms.CharField(label="Brand", max_length=100,
                            widget=forms.TextInput(
                                    attrs={
                                        'placeholder': ('Brand')
                                    }))
    price = forms.IntegerField(label="Price")
    status_choices = [('new', 'New'), ('used','Used')]
    status = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=status_choices)
```

위젯 덕분에 HTML 태그의 속성을 확인할 수 있었다. 클래스, 자리 표시자 등을 정의할 수 있다.

![widgets](images/django_forms/widgets.webp)

양식을 다른 HTML 태그의 템플리트로 보낼 수도 있다.

`{{form.as_div}}`: divs로 랩

`{{form.as_table}}`: table 셀

`{{form.as_p}}`: p 태그안으로

`{{form.as_ul}}`: list 태그

### Formsets
여러 양식을 동시에 작업하기 위해 양식 세트를 사용한다.

#### Form
간단한 양식을 만들어 보겠다.

```python
# home/forms.py

class PlayerForm(forms.Form):

    name = forms.CharField(max_length=100)
    team = forms.CharField(max_length=100)
```

#### View
`formset_factory` 메서드를 사용하여 양식 세트를 만든다.

```python
# home/views.py

from django.forms import formset_factory
from .forms import PlayerForm

class HomeView(View):

    def get(self, request):
        MyFormSet = formset_factory(PlayerForm)
        formset = MyFormSet()
        context = {"formset":formset}
        return render(request, './templates/home.html', context)
```

#### Template
Formset은 양식으로 구성되므로 양식 세트를 디스플레이하려면 formset을 반복해야 한다.

```html
<div class="form-frame" style="margin:50px;">
    <form action="" method="POST">{% csrf_token %}
        {% for form in formset %}
            {{form.as_table}}
        {% endfor %}
        <button class="btn btn-success" name="submit" type="submit">Submit</button>
    </form>
</div>
```

![formset](images/django_forms/formset.webp)

factory 메서드에 파라미터를 추가하면 원하는 만큼 양식을 복제할 수 있다.

```python
# home/views.py

def get(self, request):
      MyFormSet = formset_factory(PlayerForm, extra=2)
      formset = MyFormSet()
      print("MyFormSet: ",MyFormSet)
      print("formset: ",formset)
      context = {"formset":formset}
      return render(request, './templates/home.html', context)

"""
MyFormSet:  <class 'django.forms.formsets.PlayerFormFormSet'>

formset:  <input type="hidden" name="form-TOTAL_FORMS" value="2" id="id_form-TOTAL_FORMS"><input type="hidden" name="form-INITIAL_FORMS" value="0" id="id_form-INITIAL_FORMS"><input type="hidden" name="form-MIN_NUM_FORMS" value="0" id="id_form-MIN_NUM_FORMS"><input type="hidden" name="form-MAX_NUM_FORMS" value="1000" id="id_form-MAX_NUM_FORMS"><tr>
    <th><label for="id_form-0-name">Name:</label></th>
    <td>
      <input type="text" name="form-0-name" maxlength="100" id="id_form-0-name">
    </td>
  </tr>
  <tr>
    <th><label for="id_form-0-team">Team:</label></th>
    <td>
      <input type="text" name="form-0-team" maxlength="100" id="id_form-0-team">
    </td>
  </tr><tr>
    <th><label for="id_form-1-name">Name:</label></th>
    <td>
      <input type="text" name="form-1-name" maxlength="100" id="id_form-1-name">
    </td>
  </tr>
  <tr>
    <th><label for="id_form-1-team">Team:</label></th>
    <td>
      <input type="text" name="form-1-team" maxlength="100" id="id_form-1-team">
    </td>
  </tr>
"""
```

![two_forms](images/django_forms/two_forms.webp)

양식을 제출하고 `request.POST`를 인쇄했다.

```
<QueryDict: {'csrfmiddlewaretoken': ['BvGb3USKxkmoDVitIfb996okkvO5UI7TZac2nfz2PwIckzKnhJosIDBZPGuafEyS'], 
'form-0-name': ['Ronaldo'], 'form-0-team': ['Portugal'], 
'form-1-name': ['Messi'], 'form-1-team': ['Argentina'], 'submit': ['']}>
```

다음 양식 중 일부를 작성하도록 초기 매개 변수를 설정할 수 있다.

```python

...

formset = MyFormSet(initial=[{'name':'Ronaldo','team':'Portugal'}])
```

![myformset](images/django_forms/myformset.webp)

첫 번째 것은 초기 인수에서 정의한 매개변수로 이미 채워졌다. 추가 파라미터를 2로 설정했기 때문에 두 가지 양식이 추가된다.

또한 집합의 최대 양식 수를 결정할 수 있다.

```python
# home/views.py

...

MyFormSet = formset_factory(PlayerForm, max_num=1)
formset = MyFormSet(initial=[{'name':'Ronaldo','team':'Portugal'}])
```

![max_forms](images/django_forms/max_forms.webp)

Django Forms에 대한 짧은 소개였다.

**주** 이 포스팅은 [Discovering Django Forms](https://awstip.com/discovering-django-forms-dc4b4d9970d9)을 편역한 것임.
