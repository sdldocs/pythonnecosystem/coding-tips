# Python Best Practices: 좋은 크드 작성을 위하여 꼭 필요한 10 방법

이 페이지에서는 깨끗하고 유지보수 가능한 효율적인 Python 코드를 작성하기 위한 다양한 팁과 기술을 설명한다.

목표는 이해하기 쉽고 디버그 및 수정하기 쉬운 잘 작성된 코드를 만들 수 있도록 지원하고자 한다.

이러한 모범 사례를 따름으로써 코드의 가독성을 향상시킬 뿐만 아니라 더 나은 파이썬 개발자가 될 수 있다.

## 의미있도록 식별자(identifier)를 사용
클린 코드의 가장 중요한 측면 중 하나는 변수, 함수 및 클래스에 의미 있는 이름을 사용하는 것이다.

적합한 명명 규칙은 코드를 쉽게 설명하고 이해하기 쉽게 만든다. 다음은 한 예이다.

```python
# Bad naming
def calc(a, b):
    return a * b

# Good naming
def calculate_area(width, height):
    return width * height
```

## PEP 8 지침을 준수
[PEP 8](https://peps.python.org/pep-0008/)은 Python의 공식 스타일 가이드이며 포맷, 명명 규칙 및 기타 모범 사례에 대한 권장 사항을 제공하고 있다.

PEP 8을 따름으로써 읽기 쉽고 일관되고 깨끗한 코드를 만들 수 있다. `flake8` 또는 `black`과 같은 도구를 사용하여 PEP 8에 따라 코드를 자동으로 확인하고 포맷할 수 있다.

## 함수는 짧고 집중되도록 작성
함수 각각은 하나의 작업을 책임지고 완료해야 한다. 함수를 짧고 한 가능에 집중하도록 유지하면 함수를 이해, 테스트 및 유지보수 하기가 보다 쉬워진다.

```python
# Bad example
def process_data(data):
    # ...clean data
    # ...transform data
    # ...save data to the database

# Good example
def clean_data(data):
    # ...clean data
def transform_data(data):
    # ...transform data
def save_data(data):
    # ...save data to the database
```

## Python의 표준 라이브러리 활용
Python에는 수많은 내장 기능과 모듈을 제공하는 풍부한 표준 라이브러리가 있다.

개발자 자신의 코드를 작성하기 전에 동일한 기능을 수행할 수 있는 표준 라이브러리 모듈이 있는지 먼저 확인하도록 한다. 이렇게 하면 시간을 절약하고 코드를 더 효율적으로 작성할 수 있다.

```python
# Example: using the `csv` module to read a CSV file
import csv

with open('data.csv', mode='r') as file:
    reader = csv.reader(file)
    for row in reader:
        print(row)
```

## 오류 처리에 대한 예외(Exceptions) 사용
클린 코드를 위해서는 적절한 오류 처리가 필수적이다. 예외를 사용하여 오류를 처리하고 반환 코드 또는 글로벌 플래그를 사용하지 않도록 한다.

이렇게 하면 코드가 보다 강력하고 이해하기 쉬워진다.

```python
# Bad example
def divide(a, b):
    if b == 0:
        return "Error: Division by zero"
    return a / b

# Good example
def divide(a, b):
    if b == 0:
        raise ValueError("Division by zero")
    return a / b

try:
    result = divide(10, 0)
except ValueError as e:
    print(e)
```

## 디버깅을 편하게 하기 위해 Python 로깅 사용
디버깅에 `print()` 문을 사용하는 대신 내장 로깅 모듈을 사용한다. 이렇게 하면 로그 수준, 출력 형식 및 로그 대상을 보다 쉽게 구성할 수 있다.

```python
import logging

logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

def divide(a, b):
    logging.info("Dividing %s by %s", a, b)
    if b == 0:
        logging.error("Division by zero")
        raise ValueError("Division by zero")
    return a / b

result = divide(10, 5)
```

## List Comprehensions 사용
리스트 컴프리헨션(list comprehension)은 리스트을 만드는 간결한 방법을 제공하며 코드를 더 읽기 쉽고 효율적으로 만들어 준다.

```python
# Bad example
squares = []
for i in range(10):
    squares.append(i ** 2)

# Good example
squares = [i ** 2 for i in range(10)]
```

## Generators 사용
생성자(generator)는 대용량 데이터 세트를 사용할 때 메모리를 효율적으로 사용하는 방법이다. 전체 데이터 세트를 메모리에 저장할 필요 없이 반복기(iterator)를 생성하여 만들 수 있다.

```python
# Bad example
def get_even_numbers(limit):
    return [x for x in range(limit) if x % 2 == 0]

# Good example
def get_even_numbers(limit):
    for x in range(limit):
        if x % 2 == 0:
            yield x
```

## 단위 테스트(Unit Tests) 작성
단위 테스트는 코드가 예상대로 작동하는지 확인하고 더 쉽게 리팩터링하고 새 기능을 추가할 수 있도록 한다. Python의 내장된 `unittest` 모듈을 사용하여 테스트를 작성하고 실행한다.

```python
import unittest

def add(a, b):
    return a + b

class TestAddition(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(2, 3), 5)

if __name__ == "__main__":
    unittest.main()
```

## Continuous Integration 사용
CI(Continuous Integration)를 사용하면 변경이 있을 때마다 코드로부터 자동으로 빌드, 테스트 및 배포할 수 있다. 이렇게 하면 버그를 조기에 발견하고 코드를 항상 배포 가능한 상태로 유지할 수 있다.

## 맺으며
깨끗하고 유지보수 가능하며 효율적인 Python 코드를 작성하는 것은 프로젝트를 성공을 위하여 매우 중요하다. 이 페이지에서 설명한 팁과 모범 사례를 따르면 Python 코딩 기술을 향상시키고 이해하기 쉬운 코드를 작성하고 보다 쉽게 디버깅 및 수정을 할 수 있다.

**주** 이 페이지는 [Python Best Practices: 10 Key Techniques to Write Cleaner Code](https://medium.com/@Sabrina-Carpenter/python-best-practices-10-key-techniques-to-write-cleaner-code-313522c1cf52)을 편역한 것임.
