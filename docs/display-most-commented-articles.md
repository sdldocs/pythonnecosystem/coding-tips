이 파트에서는 블로그 웹사이트에서 댓글이 가장 많린 달린 기사를 디스플레이하고자 한다. 이전 블로그에서와 마찬가지로 커스텀 태그를 사용하여 사이드바에 댓글이 가장 많이 달린 글을 표시한다.

`Xampp`를 시작하고 가상 환경을 활성화한 다음 Visual Studio Code에서 프로젝트를 연다.

## 댓글이 가장 많린 달린 기사를 위한 커스텀 태그 만들기
객체 모음을 요약하거나 집계하여 파생된 값을 검색해야 하므로 Django 집계를 사용할 것이다. 가장 최근에 게시된 세 기사를 검색하고 있으며, 각각은 "total_comments" 속성으로 댓글 수를 포함하고 있다.

 `blog/templatetags/article_tags.py` 파일을 열고 inclusion 태그를 생성한다.

```python
from django.db.models import Count

@register.inclusion_tag('blog/most_commented_articles.html')
def show_most_commented_articles(count=3):
    most_commented_articles = Article.publishedArticles.annotate(
                                    total_comments=Count('comments')
                                ).order_by('-total_comments')[:count]
                                
    return {'most_commented_articles' : most_commented_articles}
    pass
```

![](images/display-most-commented-articles/screenshot_12_01.webp)

### `most_commented_articles.html` 파일 만들기

`template/blog` 디렉터리에 `most_commented_articles.html` 파일을 만들고 댓글이 가장 많이 달린 기사 객체를 반복한다.

```html
<ul>
    {% for most_commented_article in most_commented_articles %}
        <li>
            <a href="{{ most_commented_article.get_canonical_url }}">{{most_commented_article.title}}</a>
        </li>

    {% endfor %}

</ul>
```

![](images/display-most-commented-articles/screenshot_12_02.webp)

### `blog/templates/base.html`에 댓글이 가장 많이 달린 기사 디스플레이

`blog/templates/base.html`에서 `show_most_commented_article()` 함수를 호출한다.

```html
{% show_most_commented_articl
```

![](images/display-most-commented-articles/screenshot_12_03.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

콘솔에 오류가 없어야 한다. URL `http://127.0.0.1:8000/blog/`을 방문한다.

아래와 같은 Django 웹사이트를 볼 수 있다.

![](images/display-most-commented-articles/screenshot_12_04.png)

사이드바에서 댓글이 가장 많이 달린 기사를 볼 수 있다. 이제 최신 변경 사항을 GitHub 리포지토리에 푸시한다.

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Displayed the most commented articles"
$ git push origin main
```

![](images/display-most-commented-articles/cli_12_01.png)

최신 변경 사항을 GitHub 리포지토리에 푸시했다. 다음 파트에서는 블로그 웹사이트에 사이트맵을 추가할 예정이다.

**(주)** 위 내용은 [Display the most commented articles in the sidebar of the blog website — Part 12](https://medium.com/@nutanbhogendrasharma/display-the-most-commented-articles-in-the-sidebar-of-the-django-website-part-12-730063e54371)을 편역한 것이다.
