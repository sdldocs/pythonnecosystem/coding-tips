이 포스팅<sup>[1](#footnote_1)</sup>에서는 페이지 관리자, 관련 게시물과 검색 기능을 포함하여 Django 블로그 웹 사이트를 위한 몇 가지 선택적 고급 기능을 추가하려고 한다.

## Django에서 페이지내이션 생성

![paginator](images/django_beginner/paginator.webp)

블로그에 점점 더 많은 게시물을 추가헸다면, 한 페이지에 너무 많은 게시물을 디스플레이하지 않으려면 페이지 관리자(paginator)를 만드는 것이 좋다. 그러기 위해서는 뷰 함수에 코드를 추가해야 한다. `home` 뷰를 예로 들어보자. 먼저 필요한 패키지를 import해야 한다.

```python
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
```

`hone` 뷰를 변경하자.

```python
def home(request):
    site = Site.objects.first()
    categories = Category.objects.all()
    tags = Tag.objects.all()
    featured_post = Post.objects.filter(is_featured=True).first()

    # Add Paginator
    page = request.GET.get("page", "")  # Get the current page number
    posts = Post.objects.all().filter(is_published=True)
    paginator = Paginator(posts, n)  # Showing n post for every page

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(
        request,
        "home.html",
        {
            "site": site,
            "posts": posts,
            "categories": categories,
            "tags": tags,
            "featured_post":featured_post
        },
    )
```

9~14 번째 줄에서 여러분은 세 가지 다른 조건을 고려해야 한다. 페이지 번호가 정수이면 요청한 페이지를 반환하고, 페이지 번호가 정수가 아니면 첫 페이지를 반환하고, 페이지 번호가 페이지 수보다 크면 마지막 페이지를 반환한다.

다음으로, 다음과 같은 게시물 목록과 함께 페이지 관리자를 템플릿에 넣어야 한다.

#### `template/vendor/list.html

```python
<!-- Paginator -->
<nav
  class="isolate inline-flex -space-x-px rounded-md mx-auto my-5 max-h-10"
  aria-label="Pagination"
>
  {% if posts.has_previous %}
  <a
    href="?page={{ posts.previous_page_number }}"
    class="relative inline-flex items-center rounded-l-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20"
  >
    <span class="sr-only">Previous</span>
    <!-- Heroicon name: mini/chevron-left -->
    <svg
      class="h-5 w-5"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill="currentColor"
      aria-hidden="true"
    >
      <path
        fill-rule="evenodd"
        d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z"
        clip-rule="evenodd"
      />
    </svg>
  </a>
  {% endif %}
  
  {% for i in posts.paginator.page_range %}
  {% if posts.number == i %}
  <a
    href="?page={{ i }}"
    aria-current="page"
    class="relative z-10 inline-flex items-center border border-blue-500 bg-blue-50 px-4 py-2 text-sm font-medium text-blue-600 focus:z-20"
    >{{ i }}</a
  >
  {% else %}
  <a
    href="?page={{ i }}"
    aria-current="page"
    class="relative inline-flex items-center border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20"
    >{{ i }}</a
  >
  {% endif %}
  {% endfor %}
  
  {% if posts.has_next %}
  <a
    href="?page={{ posts.next_page_number }}"
    class="relative inline-flex items-center rounded-r-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20"
  >
    <span class="sr-only">Next</span>
    <!-- Heroicon name: mini/chevron-right -->
    <svg
      class="h-5 w-5"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill="currentColor"
      aria-hidden="true"
    >
      <path
        fill-rule="evenodd"
        d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z"
        clip-rule="evenodd"
      />
    </svg>
  </a>
  {% endif %}
</nav>
```

게시물 목록이 포함된 모든 페이지에 대해 동일한 작업을 수행해야 한다.

## Django에서 관련된 게시물 찾기

![related-posts](images/django_beginner/related-posts.webp)

아이디어는 같은 태그의 게시물을 얻는 것이다.

```python
def post(request, slug):
    site = Site.objects.first()
    requested_post = Post.objects.get(slug=slug)
    categories = Category.objects.all()
    tags = Tag.objects.all()

    # Related Posts
    ## Get all the tags related to this article
    post_tags = requested_post.tag.all()
    ## Filter all posts that contain tags which are related to the current post, and exclude the current post
    related_posts_ids = (
        Post.objects.all()
        .filter(tag__in=post_tags)
        .exclude(id=requested_post.id)
        .values_list("id")
    )

    related_posts = Post.objects.filter(pk__in=related_posts_ids)

    return render(
        request,
        "post.html",
        {
            "site": site,
            "post": requested_post,
            "categories": categories,
            "tags": tags,
            "related_posts": related_posts,
        },
    )
```

이 코드는 조금 이해하기 어렵지만, 걱정 말고 한 줄씩 분석해 보자.

세째 줄, `slug`를 이용해 요청한 게시물을 받아라.

9째 줄, 요청한 게시물에 속한 모든 태그를 가져오시요.

11 ~ 16째 줄까지, 여기가 일이 까다로워지는 곳이다. 먼저 `Post.objects.all()`은 데이터베이스에서 모든 게시물을 검색한다. 그런 다음 `filter(tag__in=post_tag)`는 현재 게시물과 관련된 태그가 있는 모든 게시물을 검색한다.

하지만, 우리는 두 가지 문제를 가지고 있다. 첫째는, 현재 게시물도 쿼리 집합에 포함되므로 `exclude(id=include_post.id)`를 사용하여 현재 게시물을 제외한다.

그러나 두 번째 문제는 이해하기 쉽지 않다. 이 시나리오를 고려해 본다. 여기 세 게시물과 세 태그가 있다.

| Tag ID | Tag Name |
|--------|----------|
| 1 | Tag 1 |
| 2 | Tag 2 |
| 3 | Tag 3 |

| Post ID | Post Nmae |
|---------|-----------|
| 1 | Post 1 |
| 2 | Post 2 |
| 3 | Post 3 |

그리고 게시물과 태그는 서로 many-to-many 관계가 있다.

! Tag ID | Post ID |
|--------|---------|
| 1 | 2 |
| 1 | 3 |
| 1 | 1 |
| 2 | 1 |
| 2 | 2 |
| 2 | 3 |
| 3 | 2 |

! Post ID | Tag ID |
|--------|---------|
| 1 | 1 |
| 1 | 2 |
| 2 | 1 |
| 2 | 2 |
| 2 | 3 |
| 3 | 1 |
| 3 | 2 |

현재 게시물이 포스트 `2`라고 가정하면 관련 태그는 `1`, `2`과 `3`이 된다. 이제 `filter(tag__in=post_tag)`를 사용하고 있다면 Django는 먼저 태그 `1`로 이동하여 태그 `1`의 관련된 포스트 `2`, `3`과 `1`의 관련 게시물을 찾은 후 태그 `2`로 이동하여 태그 `2`의 관련 게시물을 찾은 후 마지막으로 태그 `3`으로 이동한다.

즉, `filter(tag__in=post_tags)`는 결국 `[2,3,1,1,2,3,2]`를 반환한다. `exclude()` 메서드 뒤에는 `[3,1,1,3]`이 반환된다. 이는 여전히 원하는 것이 아니다, 중복된 것들을 없앨 방법을 찾아야 한다.

따라서 `values_list('id')`를 사용하여 포스트 `id`들을 `related_posts_ids` 변수로 전달한 다음 그 변수를 사용하여 관련 게시물을 검색해야 한다. 이렇게 하면 중복 항목이 제거된다.

마지막으로 관련 게시물을 해당 템플릿에 표시할 수 있다.

```html
<!-- Related posts -->

    <div class="grid grid-cols-3 gap-4 my-5">
      {% for post in related_posts %}
      <!-- post -->
      <div class="mb-4 ring-1 ring-slate-200 rounded-md h-fit hover:shadow-md">
        <a href="{% url 'post' post.slug %}"
          ><img
            class="rounded-t-md object-cover h-60 w-full"
            src="{{ post.featured_image.url }}"
            alt="..."
        /></a>
        <div class="m-4 grid gap-2">
          <div class="text-sm text-gray-500">
            {{ post.created_at|date:"F j, o" }}
          </div>
          <h2 class="text-lg font-bold">{{ post.title }}</h2>
          <p class="text-base">
            {{ post.content|striptags|truncatewords:30 }}
          </p>
          <a
            class="bg-blue-500 hover:bg-blue-700 rounded-md p-2 text-white uppercase text-sm font-semibold font-sans w-fit focus:ring"
            href="{% url 'post' post.slug %}"
            >Read more →</a
          >
        </div>
      </div>
      {% endfor %}
    </div>
```

## Django에서 검색 기능 구현

다음, 앱에 대한 검색 기능을 추가할 수 있다. 검색 기능을 만들려면 프런트 엔드에 검색 양식이 필요하다. 이 양식은 검색 쿼리를 뷰로 보내고 뷰 함수는 데이터베이스에서 정규화된 레코드를 검색한 다음 결과를 표시하는 검색 페이지를 반환한다.

### 검색 양식
먼저, 사이드 바에 검색 양식을 추가하자.

#### `templates/vendor/sidebar.html`

```html
<div class="col-span-1">
  <div class="border rounded-md mb-4">
    <div class="bg-slate-200 p-4">Search</div>
    <div class="p-4">
      <form action="{% url 'search' %}" method="POST" class="grid grid-cols-4 gap-2">
        {% csrf_token %}
        <input
          type="text"
          name="q"
          id="search"
          class="border rounded-md w-full focus:ring p-2 col-span-3"
          placeholder="Search something..."
        />
        <button
          type="submit"
          class="bg-blue-500 hover:bg-blue-700 rounded-md p-2 text-white uppercase font-semibold font-sans w-full focus:ring col-span-1"
        >
          Search
        </button>
      </form>
    </div>
  </div>
  . . .
</div>
```

7-13째 줄, `input` 필드의 `name` 애트리뷰트에 주목하시오. 여기서 입력을 `q`라고 부를 것이다. 사용자 입력은 변수 `q`에 연결되어 백엔드로 전송된다.

5째 줄, 버튼을 클릭하면 이름 `search`와 함께 URL로 사용자가 라우팅되므로 해당 URL 패턴을 등록해야 한다.

```python
path('search', views.search, name='search'),
```

### search 뷰

```python
def search(request):
    site = Site.objects.first()
    categories = Category.objects.all()
    tags = Tag.objects.all()

    query = request.POST.get("q", "")
    if query:
        posts = Post.objects.filter(is_published=True).filter(title__icontains=query)
    else:
        posts = []
    return render(
        request,
        "search.html",
        {
            "site": site,
            "categories": categories,
            "tags": tags,
            "posts": posts,
            "query": query,
        },
    )
```

### search 템플릿

#### `templates/search.html`

```html
{% extends 'layout.html' %}

{% block title %}
<title>Page Title</title>
{% endblock %}

{% block content %}
<div class="grid grid-cols-4 gap-4 py-10">
  <div class="col-span-3 grid grid-cols-1">
    
    {% include "vendor/list.html" %}
    
  </div>
  {% include "vendor/sidebar.html" %}
</div>
{% endblock %}
```

이제 검색 양식에서 무언가를 검색하려고 하면, 여러분이이 요청하는 게시물만 반환되어야 한다.

---
<a name="footnote_1">1</a>: 이 포스팅은 [Django for Beginners #5](https://www.ericsdevblog.com/posts/django-for-beginners-5/)를 편역한 것이다.
