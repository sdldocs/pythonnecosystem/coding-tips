<font size="3"> <span style="color:grey">네임스페이스(namespace)와 Python이 네임스페이스를 사용하여 코드를 개선하는 방법에 대해 살펴본다.</style></font>

## Namespcaing
많은 프로그래밍 언어와 마찬가지로 Python은 *네임스페이스*라는 개념을 통해 코드를 분리한다. 프로그램이 실행되는 동안 알려진 모든 네임스페이스와 해당 네임스페이스에서 가용한 정보를 추적한다.

네임스페이스는 다음 몇 가지 면에서 유용하다.

- 소프트웨어가 커짐에 따라 다수의 개념이 유사하거나 동일한 이름이 필요하게 된다.
- 네임스페이스는 충돌을 최소화하여 각 이름이 어떤 개념을 가리키는지 명확하게 해준다.
- 소프트웨어가 커짐에 따라 코드베이스에 어떤 코드가 이미 있는지 파악하기가 기하급수적으로 어려워진다. 네임스페이스는 코드가 존재한다면 그 코드가 어디에 있을지에 대한 근거 있는 추측을 할 수 있도록 도와준다.
- 대규모 코드베이스에 새 코드를 추가할 때 기존 네임스페이스는 새 코드가 어디에 위치해야 하는지 안내할 수도 있다. 만약 명하게 선택할 수 없다면, 새로운 네임스페이스가 적절할 수도 있다.

네임스페이스는 Zen of Python의 마지막 문장일 정도로 중요하다(Zen of Python을 모른다면 Python 인터프리터를 실행한 후 `import this`를 입력한다).

> “Namespaces are one honking great idea — let’s do more of those!” — The Zen of Python

Python에서 사용하는 모든 변수, 함수와 클래스는 네임스페이스에 있는 *이름*이다. 이름은 `x`, `total` 또는 `EssentialBusinessDomainObject`와 같이 어떤 대상에 대한 참조이다.

Python 코드에서 `x = 3`이라고 하면 "x라는 이름에 값 3을 할당하라"는 의미이며, 코드에서 `x`를 참조할 수 있다.

"변수"라는 단어는 값을 참조하는 이름과 같은 의미로 사용되지만, Python에서 이름은 함수, 클래스 등을 참조할 수 있다.

## 네임스페이스와 `import` 문
Python 인터프리터를 처음 수행하면 내장 네임스페이스가 Python에 내장된 모든 항목으로 채워진다. Python 내장 네임스페이스에는 `print()`ㅇ하 `open()`과 같은 내장 함수가 포함되어 있다.

이러한 내장 함수에는 접두사가 없으며 이를 사용하기 위해 특별한 작업이 필요하지 않다. Python 코드의 어느 곳에서든 사용할 수 있다. 그렇기 때문에 Python에서 `print('Hello world!')`가 쉽게 작동하는 것으로 유명하다.

다른 언어와 달리 Python 코드에서 네임스페이스를 명시적으로 생성하지는 않지만, 코드 구조가 생성되는 네임스페이스와 상호 작용 방식에 영향을 준다.

예를 들어 Python 모듈을 만들면 해당 모듈에 대한 네임스페이스가 자동으로 추가로 생성된다. 가장 간단하게 설명하면 Python 모듈은 코드가 포함된 `.py` 파일이다.

예를 들어 `sales_tax.py`라는 파일은 "`sales_tax` 모듈"이다.

```python
# sales_tax.py
  
 def add_sales_tax(total, tax_rate):
     return total * tax_rate
```

각 모듈에는 모듈의 코드가 자유롭게 액세스할 수 있는 *전역* 네임스페이스가 있다. 내부에 중첩되지 않은 함수, 클래스와 변수는 모듈의 전역 네임스페이스에 있다.

```python
# sales_tax.py
  
 TAX_RATES_BY_STATE = {          ❶
     'MI': 1.06,
     # ...
 }
  
 def add_sales_tax(total, state):
     return total * TAX_RATES_BY_STATE[state]  ❷
```

❶ `TAX_RATES_BY_STATE`는 모듈의 전역 네임스페이스에 있다.

❷ 모듈의 코드는 문제 없이 `TAX_RATES_BY_STATE`를 사용할 수 있다.

모듈의 함수와 클래스에는 해당 함수에서만 액세스할 수 있는 *로컬* 네임스페이스가 있다.

```python
# sales_tax.py
  
 TAX_RATES_BY_STATE = {
     'MI': 1.06,
     ...
 }
  
 def add_sales_tax(total, state):
     tax_rate = TAX_RATES_BY_STATE[state]  ❶
     return total * tax_rate               ❷
```

❶ `tax_rate`는 `add_sales_tax()`의 로컬 범위에 있다.

❷ `add_sales_tax()` 내의 코드는 아무런 문제 없이 `tax_rate`를 사용할 수 있다.

다른 모듈의 변수, 함수 또는 클래스를 사용하려는 모듈은 해당 모듈의 전역 네임스페이스로 가져와야(import) 한다. 임포트는 다른 곳에서 원하는 네임스페이스로 이름을 가져오는 방법이다.

```python
# receipt.py
  
from sales_tax import add_sales_tax  ❶


def print_receipt():
    total = ...
    state = ...
    print(f'TOTAL: {total}')
    print(f'AFTER TAX: {add_sales_tax(total, state)}')  ❷
```

❶ `receipt.py` 글로벌 네임스페이스에 `add_sales_tax` 함수가 추가되었다.

❷ `add_sales_tax`는 여전히 자체 네임스페이스에 `TAX_RATES_BY_STATE`와 `tax_rate`에 대해 알고 있다.

Python에서 변수, 함수 또는 클래스를 참조하려면 다음 중 하나를 만족해야 한다.

1. 이름이 Python 내장 네임스페이스에 있다.
2. 이름이 현재 모듈의 전역 네임스페이스에 있다.
3. 이름이 현재 코드의 로컬 네임스페이스에 있다.

이름에서 충돌이 생기면 우선순위는 반대 순서로 작동하며, 로컬 이름이 내장 이름을 재정의하는 전역 이름보다 우선한다.

일반적으로 현재 코드에 가장 구체적인 정의가 사용되기 때문에 이 점을 기억하면 된다. 이는 아래 그림에 나타나 있다.

![](images/namespacing_python.png)

Python을 사용중 `NameError: name 'my_var' is not defined`를 본 적이 있을 것이다. 이는 해당 코드에 알려진 네임스페이스에서 `my_var`라는 이름을 찾을 수 없다는 뜻이다.

이는 일반적으로 `my_var`에 값을 할당하지 않았거나 다른 곳에서 할당했으므로 가져와야 함을 의미한다.

모듈은 코드 분할하는 좋은 방법이다. 서로 관련이 없는 함수가 여러 개 포함된 긴 스크립트 파일이 하나 있는 경우 해당 함수를 모듈로 분할하는 것을 고려해 보아야 한다.

## 다양한 마스크(mask) 가져오기
Python에서 `import` 문은 언뜻 간단해 보이지만 몇 가지 방법이 있으며 각 방법에 따라 네임스페이스에 가져오는 정보에 미묘한 차이가 있다.

앞서 `sales_tax` 모듈에서 `receipt` 모듈로 `add_sales_tax()` 함수를 가져왔다.

```python
# receipt.py
  
from sales_tax import add_sales_tax
```

이렇게 하면 `receipt` 모듈의 전역 네임스페이스에 `add_sales_tax()` 함수가 추가된다.

이 모든 것이 훌륭하지만 `sales_tax`에 10개의 함수를 더 추가하고 영수증에서 모두 사용하려고 한다고 가정해 보겠다. 같은 경로를 계속 따라가면 다음과 같은 결과가 나타난다.

```python
# receipt.py
  
from sales_tax import add_sales_tax, add_state_tax, add_city_tax, add_local_millage_tax, ...
```

이 구문을 약간 개선한 대체 구문이다.

```python
# receipt.py
  
from sales_tax import (
    add_sales_tax, 
    add_state_tax, 
    add_city_tax, 
    add_local_millage_tax, 
    ...
)
```

여전히 별로 이다. 다른 모듈의 다양한 함수가 필요한 경우 대신 해당 모듈을 전체적으로 가져올 수 있다.

```python
# receipt.py
  
import sales_tax
```

이렇게 하면 `sales_tax` 모듈 전체가 현재 네임스페이스에 추가되며 해당 함수는 `sales_tax.` 접두사를 사용하여 참조할 수 있다.

```python
# receipt.py
  
import sales_tax
  
  
def print_receipt():
    total = ...
    locale = ...
    ...
    print(f'AFTER MILLAGE: {sales_tax.add_local_millage_tax(total, locale)}')
```

이렇게 하면 긴 `import` 문을 피할 수 있고 접두사를 사용하면 네임스페이스 충돌을 피할 수 있다는 이점이 있다.

## 와일드카드 가져오기
파이썬에서는 `from themodule import *`를 사용하여 모듈의 모든 이름을 약어로 가져올 수 있다. 코드 전체에서 해당 이름 앞에 themodule. 접두사를 붙이는 대신 이 방법을 사용하고 싶을 수도 있지만, 사용하지 말자!

와일드카드 가져오기는 가져오는 명시적 이름을 볼 수 없기 때문에 이름 충돌을 더 많이 일으킬 수 있으며, 문제를 디버깅하기 어렵게 만들 수 있다. 명시적 임포트를 사용하자!

**(주)** 위 내용은 [Namespacing in Python](https://betterprogramming.pub/namespacing-with-python-79574d125564)을 편역한 것이다. 