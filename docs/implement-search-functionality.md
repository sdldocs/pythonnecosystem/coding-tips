이 파트에서는 Django 웹사이트에서 전체 텍스트에 대한 검색 기능을 구현하려고 한다.

## MySQL FULLTEXT SEARCH (FTS)
풀텍스트 검색은 문서에서 검색어와 완벽하게 일치하지 않을 수 있는 모든 단어를 검사하는 데 사용되는 검색 기법이다. MySQL은 텍스트에 대한 인덱싱과 검색을 지원한다.

풀텍스트 검색 기법은 Google, Mozilla 또는 Bing과 같은 검색 엔진에서 사용된다. 이러한 모든 검색 엔진은 웹사이트의 데이터를 데이터베이스로 수집하고 키워드를 기반으로 검색을 수행한다. 블로그, 뉴스, 전자상거래 등과 같은 웹사이트에서 강력한 검색 결과를 제공한다.

**MySQL은 풀텍스트에 대한 인덱싱과 검색을 지원한다.**

1. MySQL의 풀텍스트 인덱스는 텍스트 타입 데이터에 대한 인덱스이다.

2. 풀텍스트 인덱스는 InnoDB 또는 MyISAM 테이블에서만 사용할 수 있으며 `CHAR`, `VARCHAR` 또는 `TEXT` 열에 대해서만 생성할 수 있다.

3. MySQL은 중국어, 일본어, 한국어(CJK)를 지원하는 내장형 full-text ngram parser와 설치 가능한 일본어용 MeCab full-text parser 플러그인을 제공한다. 구문 분석의 차이점은 섹션 12.9.8, "ngram Full-Text Parser"와 섹션 12.9.9, "MeCab Full-Text Parser Plugin"에 설명되어 있다.

4. 풀텍스트 인덱스 정의는 테이블을 생성할 때 `CREATE TABLE` 문에 지정하거나 나중에 `ALTER TABLE` 또는 `CREATE INDEX`를 사용하여 추가할 수 있다.

5. 대규모 데이터 세트의 경우, 기존 전체 텍스트에 대한 인덱스가 있는 테이블에 데이터를 로드하는 것보다 텍스트에 대한 인덱스가 없는 테이블에 데이터를 로드한 다음 인덱스를 생성하는 것이 훨씬 빠르다.

**전체 텍스트 검색은 `MATCH()` `AGAINST()` 구문을 사용하여 수행된다. `MATCH()`는 검색할 열의 이름을 쉼표로 구분한 목록을 받는다. `AGAINST`는 검색할 문자열과 수행할 검색 유형을 나타내는 선택적 수정자를 받는다. 검색 문자열은 쿼리 수행 중에 상수인 문자열 값이어야 한다.**

**풀텍스트 검색에는 세 가지 유형이 있다.**

1. 자연어 검색은 검색 문자열을 자연스러운 인간 언어의 구문으로 해석합니다. 큰따옴표(") 문자를 제외하고는 특별한 연산자가 없다. 금지어(stopwords) 목록이 적용된다. 풀텍스트 검색은 `IN NATURAL LANGUAGE MODE` 수정자가 지정되거나 수정자가 지정되지 않은 경우 자연어 검색이다.

2. 부울 검색은 특수 쿼리 언어의 규칙을 사용하여 검색 문자열을 해석한다. 문자열에는 검색할 단어가 포함된다. 또한 일치하는 행에 단어가 있어야 하거나 없어야 하거나 가중치가 평소보다 높거나 낮아야 하는 등의 요구 사항을 지정하는 연산자를 포함할 수 있다. 특정 일반 단어(중지어)는 검색 색인에서 생략되며 검색 문자열에 있는 경우 일치하지 않는다. `IN BOOLEAN MODE` 수정자는 부울 검색을 지정한다.

3. 쿼리 확장 검색은 자연어 검색을 수정한 것이다. 검색 문자열은 자연어 검색을 수행하는 데 사용된다. 그런 다음 검색에서 반환된 가장 관련성이 높은 행의 단어가 검색 문자열에 추가되고 검색이 다시 수행된다. 쿼리는 두 번째 검색의 행을 반환한다. `IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION` 또는 `WITH QUERY EXPANSION`이 있는 수정자는 쿼리 확장 검색을 지정한다.

## MySQL 시작과 가상 환경 활성화

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## `blog_article` table에 풀텍스트 검색 추가
이미 `blog_article` 테이블을 만들었다. 따라서 `blog_article` 테이블을 변경하고 풀텍스트를 추가한다. 검색을 위해 제목과 본문 열을 추가하고 싶다.

MySQL용 `phpmyadmin` 사용자 인터페이스를 사용한다. MySQL 콘솔에서도 테이블을 변경할 수 있다. `phpmyadmin`을 열고 블로그 웹사이트의 데이터베이스를 선택한 다음 `blog_aticle` 테이블을 클릭한다. 그런 다음 SQL을 클릭하고 다음 명령을 입력한다.

```sql
ALTER TABLE blog_article ADD FULLTEXT (title, body) ;
```

![](images/implement-search-functionality/screenshot_14_01.png)

아래와 같은 다음 화면이 디스플레이된다.

![](images/implement-search-functionality/screenshot_14_02.png)

### MySQL의 풀텍스트 검색 기능 확인

여기서는 'Linux'라는 단어를 확인하여 본다.

```sql
SELECT * FROM blog_article WHERE MATCH (title, body) AGAINST ('linux');
```

![](images/implement-search-functionality/screenshot_14_03.png)

몇 가지 결과가 표시되는 것을 볼 수 있다. 이제 프로젝트에 검색 기능을 구현해야 한다.

## 검색 기능 구현

### `blog/views.py`에 메서드 생성
Visual Studio 코드 IDE에서 프로젝트를 연다. 그런 다음 `blog/views.py` 파일을 열고 `article_search()`라는 메서드를 만든다.

먼저 여기서 SQL 쿼리가 작동하는지 여부를 확인하기 위해 `HttpResponse()`만 반환하고 나중에 HTML 템플릿을 추가하도록 한다.

```python
def article_search(request):
    query = 'linux'
    results = []
    
    results = Article.objects.raw("SELECT * FROM blog_article WHERE MATCH (title, body) AGAINST (%s)", [query])
    for result in results:
        print(result)
        pass
    
    return HttpResponse("Working")

    pass
```

![](images/implement-search-functionality/screenshot_14_04.webp)

### 기사 검색을 위한 url 패턴 생성
`blog/urls.py` 파일에 기사 검색을 위한 URL 패턴을 만든다.

```python
from django.urls import path
from . import views

app_name = 'blog'


urlpatterns = [
    path('', views.list_of_articles, name='list_of_articles'),
    #path('<int:id>/', views.article_details, name='article_details'),
    path('<int:year>/<int:month>/<int:day>/<slug:article>/', views.article_details, name='article_details'),
    path('<int:article_id>/comment/', views.comment_for_article, name='comment_for_article'),
    path('tag/<slug:tag_slug>/', views.list_of_articles, name='list_of_articles_by_tag'),
    path('search/', views.article_search, name='article_search')
]
```

![](images/implement-search-functionality/screenshot_14_05.webp)

### 개발 서버 실행

```bash
$ python manage.py runserver
```

url `http://127.0.0.1:8000/blog/search/`를 확인한다.

![](images/implement-search-functionality/screenshot_14_10.png)

터미널에서 결과를 확인한다.

![](images/implement-search-functionality/cli_14_01.png)

검색이 제대로 작동하고 있음을 알 수 있다. 사용자가 텍스트를 검색할 수 있도록 검색용 양식을 추가해 보자.

### SearchForm 생성
`blog/forms.py` 파일을 열고 `SearchForm`이라는 클래스를 만든다.

```python
class SearchForm(forms.form):
    query = forms.CharField()
    pass
```

![](images/implement-search-functionality/screenshot_14_06.webp)

### `blog/views.py` 파일에 SearchForm 가져오기 

```python
from .forms import CommentForm, SearchForm
```

![](images/implement-search-functionality/screenshot_14_07.webp)

### `views.py`의 `article_search()` 메서드 수정 

```python
def article_search(request):
    form = SearchForm()
    query = None
    results = []

    if 'query' in request.GET:
        
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            results = Article.objects.raw("SELECT * FROM blog_article WHERE MATCH (title, body) AGAINST (%s)", [query])
            pass
        pass
    
    return render(request,
            'blog/search.html',
            {'form': form, 'query': query,'results': results}
        )

    pass
```

![](images/implement-search-functionality/screenshot_14_08.webp)

### `search.html` 파일 만들기

```html
{% extends "blog/base.html" %}
{% load article_tags %}

{% block title %}Search{% endblock %}

{% block content %}
    {% if query %}
        <h1>Article containing "{{ query }}"</h1>
        <h3>
            {% with results.count as total_results %}
                Found {{ total_results }} result{{ total_results|pluralize }}
            {% endwith %}
        </h3>
    
        {% for article in results %}
            <h4>
            <a href="{{ article.get_absolute_url }}">
                {{ article.title }}
            </a>
            </h4>
            {{ article.body|truncatewords:50 }}
            {% empty %}
            <p>No results found.</p>
        {% endfor %}

        <p><a href="{% url "blog:article_search" %}">Search again</a></p>
        
    {% else %}

        <h1>Search for articles</h1>
        <form method="get">
            {{ form.as_p }}
            <input type="submit" value="Search">
        </form>

    {% endif %}
{% endblock %}
```

![](images/implement-search-functionality/screenshot_14_09.webp)

Check the url` http://127.0.0.1:8000/blog/search/`을 확인한다.

![](images/implement-search-functionality/screenshot_14_11.png)

검색 양식이 디스플레이된다. 텍스트를 입력하고 검색을 클릭한다.

![](images/implement-search-functionality/screenshot_14_12.png)

다음 화면에는 검색어가 포함된 기사가 보여진다.

![](images/implement-search-functionality/screenshot_14_13.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Added search functionality to the website"
$ git push origin main
```

![](images/implement-search-functionality/cli_14_02.png)

이 시리즈는 여기까지이다. 수고하셨습니다. 도움이 되기를 기대합니다. 

**(주)** 위 내용은 [Implement Search Functionality to the Django Blog Website — Part 14](https://medium.com/@nutanbhogendrasharma/implement-search-functionality-to-the-django-blog-website-part-14-dd451844a38c)을 편역한 것이다.
