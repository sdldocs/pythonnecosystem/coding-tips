이 파트에서는 기사(article)라는 Django 어플리케이션과 모델을 만들어 보겠다. 또한 관리자(admin) 섹션에 모델을 추가하여 추가/삭제/수정할 수 있도록 하려고 한다. 다음 기사 모델을 커스터마이즈하고 필터를 추가하는 등의 작업을 수행할 것이다.

## 가상 환경 활성화
가상 환경이 활성화되어 있지 않은 경우 활성화한다.

```bash
$ cd myproject/bog_app/
$ source ./my_blog_env/bin/activate
```

## MySQL 시작하기
시스템에서 XAMPP가 실행 중인지 확인하여 실행 중이 아니라면 XAMPP를 시작한다.

![](images/migrate-sqlite2mysql/screenshot_02_01.png)

## Django 어플리케이션 생성
우리가 Django로 작성하는 각 어플리케이션은 특정 규칙을 따르는 Python 패키지로 구성된다. Django에는 앱의 기본 디렉토리 구조를 자동으로 생성하는 유틸리티를 함께 제공한다.

이 예에서는 Django 어플리케이션 "blog"를 만든다. 요구 사항에 따라 이름을 지정할 수 있다.

```bash
!python manage.py startapp DJANGO_APP_NAME

$ python manage.py startapp blog
```

Django 프로젝트 내에 `blog` 폴더가 생성된 것을 볼 수 있다.

![](images/create_django_app_model/cli_03_01.png)

Django 애플리케이션의 기본 구조

![](images/create_django_app_model/cli_03_02.png)

Django가 아래 파일을 자동으로 생성한다.

`__init__.py`: Python이 `blog` 디렉터리를 Python 모듈로 취급하도록 지시하는 빈 파일이다.`

`admin.py`: Django 관리에 포함할 모델을 이 파일에 등록할 수 있다. Django 관리에서 모델을 관리할 수 있다.

`apps.py`: Django 애플리케이션의 주요 구성이다.

`migrations`: 이 디렉토리에는 어플리케이션의 데이터베이스 마이그레이션 파일과 `__ini__.py` 파일이 포함되어 있다. 마이그레이션 명령은 모델 변경 사항을 추적하고 그에 따라 데이터베이스를 동기화한다.

`models.py`: 이 파일에는 어플리케이션의 모델이 정의되어 있다.

`tests.py`: 어플리케이션의 테스트 케이스를 작성할 수 있는 파일이다.

`views.py`: `views.py`에서는 HTTP 요청에 대한 클래스나 메서드를 작성한다. 그리고 응답을 반환한다.

## 새롭게 만든 Django 어플리케이션 `settings.py`에 등록
`settings.py` 파일을 열고 `INSTALLED_APPS` 섹션으로 이동하여 다음 줄을 추가한다.

```
'blog.apps.BlogConfig',
```

![](images/create_django_app_model/screenshot_03_01.webp)

## 기사 모델 생성
### 기사(Article)라는 클래스 생성
`blog/models.py` 파일을 열고 `Article`이라는 클래스를 생성한다. 그 부모 클래스는 `models.Model`이다. 또한 Django 관리 사이트에서 객체의 제목을 표시하는 `__str__()` 메서드를 추가한다.

**기사 모델에 대한 필드를 선언한다.**

- `title`: 기사 제목을 위한 필드이다. 이 필드는 `CharField` 필드이며 최대 길이는 250이다.

- `slug`: 이는 SQL 데이터베이스에서 `VARCHAR`로 변환되는 `SlugField` 필드이다. slug는 문자, 숫자, 밑줄 또는 하이픈만 포함하는 짧은 레이블이다.

- `body`: 기사 텍스트를 위한 필드이다. SQL 데이터베이스에서 `TEXT`로 변환되는 `TextField` 필드이다.

- `publish`: SQL 데이터베이스에서 `DATETIME`으로 변환되는 `DateTimeField` 필드이다. 이 필드를 사용하여 기사가 게시된 날짜와 시간을 저장한다. 필드의 기본값으로 Django의 `timezone.now` 메서드를 사용한다.

- `created`: `DateTimeField` 필드이다. 이 필드를 사용하여 게시물이 생성된 날짜와 시간을 저장한다. `auto_now_add`를 사용하면 객체를 만들 때 날짜가 자동으로 저장된다.

- `updated`: `DateTimeField` 필드이다. 이 필드를 사용하여 기사가 업데이트된 마지막 날짜와 시간을 저장한다. `auto_now`를 사용하면 객체를 저장할 때 날짜가 자동으로 업데이트된다.

```python
from django.db import models
from django.utils import timezone

class Article(models.Model):
   
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
        pass

    pass
```

### 정렬 순서 추가하기
일반적으로 블로그 웹사이트는 항상 최신 콘텐츠부터 오래된 콘텐츠까지 표시한다. 따라서 필드 이름 앞에 하이픈을 사용하여 내림차순을 추가하려면 `-publish`를 입력한다.

```python
from django.db import models
from django.utils import timezone

class Article(models.Model):
   
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ['-publish']
        pass

    def __str__(self):
        return self.title
        pass

    pass
```

### 문서 모델에 대한 인덱스 추가
기본적으로 인덱스는 각 열에 대해 오름차순으로 생성된다. 열에 대해 내림차순으로 인덱스를 정의하려면 필드 이름 앞에 하이픈(`-`)을 추가한다.

```python
from django.db import models
from django.utils import timezone

class Article(models.Model):
   
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]
        pass

    def __str__(self):
        return self.title
        pass

    pass
```

![](images/create_django_app_model/screenshot_03_02.webp)

### 기사 상태 추가
기사 상태는 "DRAFT"과 "PUBLISHED"의 두 가지가 있어야 한다.

```python
from django.db import models
from django.utils import timezone

# Create your models here.
class Article(models.Model):

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'

    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2,
                                choices=Status.choices,
                                default=Status.DRAFT
                            )


    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]
        pass

    def __str__(self):
        return self.title
        pass

    pass
```

![](images/create_django_app_model/screenshot_03_03.webp)

### 기사 모델에 저자 추가

```python
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Article(models.Model):

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'

    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_articles')
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2,
                                choices=Status.choices,
                                default=Status.DRAFT
                            )


    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]
        pass

    def __str__(self):
        return self.title
        pass

    pass
```

![](images/create_django_app_model/screenshot_03_04.webp)

## `makemigrations`과 `migrate` 명령 실행
기사 모델을 만들었다. 이제 마이그레이션을 만들고 마이그레이션을 적용해야 한다.

```bash
$ python manage.py makemigrations blog
```

![](images/create_django_app_model/cli_03_03.png)

```python
$ python manage.py migrate
```

![](images/create_django_app_model/cli_03_04.png)

MySQL 데이터베이스에 생성된 테이블을 볼 수 있다

![](images/create_django_app_model/screenshot_03_05.png)


## 개발 서버 수행

```bash
$ python manage.py runserver
```

브라우저를 열고 아래 URL을 입력한다.

http://127.0.0.1:8000/admin/

로그인 후 아래와 같은 화면이 나타난다.

![](images/create_django_app_model/screenshot_03_06.png)

여기에서 기사를 볼 수 없다. 따라서 `admin.py` 파일에 기사를 추가하자.

### `blog/admin.py' 파일에 기사 모델 추가하기

```python
# blog/admin.py

from django.contrib import admin
from .models import Article

# Register your models here.
admin.site.register(Article)
```

![](images/create_django_app_model/screenshot_03_07.webp)

이제 브라우저를 새로 고치면 관리자 섹션에 기사가 보인다.

![](images/create_django_app_model/screenshot_03_08.png)

## Django 관리를 사용하여 기사 추가
`"+ADD"`를 클릭하면 아래와 같은 화면이 나타난다.

![](images/create_django_app_model/screenshot_03_09.png)

저자, 제목, slug, 본문, 게시 날짜와 상태를 입력한다. 그런 다음 저장을 클릭한다.

![](images/create_django_app_model/screenshot_03_10.png)

저장 후 아래와 같은 화면이 보인다.

![](images/create_django_app_model/screenshot_03_11.png)

기사 제목만 표시되는 것을 볼 수 있다. 목록 출력을 커스터마이즈해 보겠다.

## 관리자 섹션에서 기사 모델 커스터마이즈
어플리케이션에서 모델은 관리자 인터페이스에서 편집할 수 있어야 한다. `ModelAdmin`에 설명된 대로 각 모델을 관리자가 등록할 수 있다.

**`class ModelAdmin`**:

`ModelAdmin` 클래스는 관리자 인터페이스에서 모델을 표현하는 클래스이다. 일반적으로 이러한 클래스는 어플리케이션의 `admin.py` 파일에 저장된다.

**`The register decorator`**:

*`register(*models, site=django.contrib.admin.sites.site)`*

`ModelAdmin` 클래스를 등록할 수 있는 데코레이터도 있다.

```python
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
pass
```

**`ModelAdmin.list_display`**

`list_display`를 설정하여 관리자의 변경 목록 페이지에 표시되는 필드를 제어한다.

*`list_display = [‘title’, ‘slug’, ‘author’, ‘publish’, ‘status’]`*

`blog/admin.py` 파일을 열고 다음과 같이 코드를 작성한다. 출력 목록을 커스터마이징하므로 이 줄에 `admin.site.register(Article)`를 주석 처리한다.

```python
from django.contrib import admin
from .models import Article

# Register your models here.
#admin.site.register(Article)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'author', 'publish', 'status']
    pass
```

![](images/create_django_app_model/screenshot_03_12.webp)

### 브라우저 새로 고치기

아래와 같은 화면을 출력한다.

![](images/create_django_app_model/screenshot_03_13.png)

이제 제목, slug, 작성자, 게시일과 상태를 출력하였다.

### 리스트 필터 추가

**`ModelAdmin List Filters`**

`ModelAdmin` 클래스는 관리자의 목록 변경 페이지 오른쪽 사이드바에 표시되는 목록 필터를 정의할 수 있다.

`blog/admin.py` 파일에 다음 줄을 추가한다.

*`list_filter = ['status', 'created', 'publish', 'author']`*

```python
from django.contrib import admin
from .models import Article

# Register your models here.
#admin.site.register(Article)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'author', 'publish', 'status']
    list_filter = ['status', 'created', 'publish', 'author']
    pass
```

![](images/create_django_app_model/screenshot_03_14.webp)

파일을 저장하고 브라우저를 새로 고친다.

![](images/create_django_app_model/screenshot_03_15.png)

### 검색 필터 추가
자동 완성 검색이 이를 사용하기 때문에 관련 객체의 `ModelAdmin`에 `search_fields`를 정의할 수 있다.

```python
from django.contrib import admin
from .models import Article

# Register your models here.
#admin.site.register(Article)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'author', 'publish', 'status']
    list_filter = ['status', 'created', 'publish', 'author']
    search_fields = ['title', 'body']
    pass
```

![](images/create_django_app_model/screenshot_03_16.webp)

브라우저를 새로 고치면 검색 기능이 추가되었다.

![](images/create_django_app_model/screenshot_03_17.png)

### 제목으로 slug 설정

**`ModelAdmin.prepopulated_fields`**
`prepopulated_fields`를 필드 이름을 미리 채워야 하는 필드로 매핑하는 사전을 설정한다.

*`prepopulated_fields = {'슬러그': ('title',)}`*

설정하면 약간의 JavaScript를 사용하여 할당된 필드에 지정된 필드가 채워진다. 이 기능의 주요 용도는 하나 이상의 다른 필드에서 `SlugField` 필드에 대한 값을 자동으로 생성하는 것이다. 생성된 값은 소스 필드의 값과 연결한 다음 그 결과를 유효한 slug로 변환(예: 대시를 공백으로 대체하고 ASCII 문자를 소문자로 대체)하여 생성된다.

미리 채워진 필드는 값이 저장된 후에는 JavaScript에 의해 수정되지 않는다. 일반적으로 slug가 변경되는 것은 바람직하지 않다(slug가 사용된 경우 객체의 URL이 변경될 수 있음).

`prepopulated_fields`는 `DateTimeField`, `ForeignKey`, `OneToOneField`와 `ManyToManyField` 에는 사용할 수 없다.

### `raw_id_fields` 설정
**`ModelAdmin.raw_id_fields`**

기본적으로 Django의 관리자는 외래 키(Foreign Key)인 필드에 대해 선택 상자 인터페이스(`<select>`)를 사용한다. 드롭다운에 표시할 모든 관련 인스턴스를 선택해야 하는 오버헤드가 발생하는 것을 원하지 않을 때가 있다.

`raw_id_fields`는 외래키 또는 다대다 필드에 대한 입력을 위젯으로 변경하려는 필드의 목록이다.

```pyhon
class ArticleAdmin(admin.ModelAdmin):
    raw_id_fields = ("newspaper",)
```

`raw_id_fields` 입력 위젯에는 필드가 `ForeignKey`인 경우 기본 키가 포함되어야 하고, 필드가 `ManyToManyField`인 경우 쉼표로 구분된 값 목록이 포함되어야 한다. `raw_id_fields` 위젯은 필드 옆에 돋보기 버튼을 표시하여 사용자가 값을 검색하고 선택할 수 있도록 한다.

### `date_hierarchy` 설정

**`ModelAdmin.date_hierarchy`**

`date_hierarchy`를 모델의 `DateField` 또는 `DateTimeField` 이름으로 설정하면 변경 목록 페이지에 해당 필드별로 날짜 기반 드릴다운 탐색이 포함된다.

### 객체 목록을 표시할 순서를 설정
**`ModelAdmin.ordering`**

순서를 설정하여 Django 관리자 보기에서 객체 목록을 정렬하는 방법을 지정한다. 모델의 순서 지정 매개변수와 동일한 형식의 목록 또는 튜플이어야 한다.

이 매개 변수가 제공되지 않으면 Django 관리자는 모델의 기본 순서를 사용한다.

동적 순서를 지정해야 하는 경우(예: 사용자 또는 언어에 따라) get_ordering() 메서드를 구현하여 지정할수 있다.

```python
from django.contrib import admin
from .models import Article

# Register your models here.
#admin.site.register(Article)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'author', 'publish', 'status']
    list_filter = ['status', 'created', 'publish', 'author']
    search_fields = ['title', 'body']
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ['author']
    date_hierarchy = 'publish'
    ordering = ['status', 'publish']
    pass
```

![](images/create_django_app_model/screenshot_03_18.webp)

브라우저를 새로 고침하고 변경 사항을 확인한다.

![](images/create_django_app_model/screenshot_03_19.png)

## 몇몇 기사 추가하기 
장고 관리를 통해 몇 가지 기사를 더 추가했다.

![](images/create_django_app_model/screenshot_03_20.png)

## 최신 변경을 Github 레포지터리에 푸시
모든 것이 잘 작동하고 있다. 최신 변경 사항을 git 리포지토리에 푸시하자.

```bash
$ git add --all
$ git commit -m "Changed database sqllite to mysql"
$ git push origin main
```

![](images/create_django_app_model/cli_03_05.png)

![](images/create_django_app_model/cli_03_06.png)

여기까지이다. 다음 파트에서는 기사 목록을 표시하는 기사에 대한 보기를 만들겠다.

**(주)** 위 내용은 [Create a Django application and model — Part 3](https://medium.com/@nutanbhogendrasharma/create-a-django-application-and-model-part-3-7f35b19c0ebd)을 편역한 것이다. 
