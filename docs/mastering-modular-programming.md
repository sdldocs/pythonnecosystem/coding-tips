저자는 최근에 Python 클래스에 대한 글을 썼는데, Python에서 클래스가 모듈로 사용되는 경우가 매우 많다고 했다.
이 글에서는 모듈이 무엇인지, 모듈을 사용하는 방법과 Python에서 모듈을 사용해야 하는 이유에 대해 설명하려고 한다.

이 포스팅의 마지막 부분에서 모듈을 사용해 본 적이 없다면 생각을 바꾸게 될 것이다. 가능할 때마다 프로그래밍 방식을 변경하고 모듈을 사용할 것이다. 그건 확실하다. 여러분을 이해할 수 있다. 저자가 모듈과 모듈의 힘을 발견했을 때, 할 수 있을 때마다 그것들을 사용하기 시작했다.

저자와 여러분의 유일한 차이점은 Python 모듈에 대해 알아야 할 모든 것을 다루는 완전하고 가치 있는 리소스를 찾지 못했다는 것이다. 이것이 저자가 이 글을 쓰는 이유이다. 여기저기서 파편화된 자원을 발견했고, 이것들 덕분에 그리고 시니어 개발자들의 조언 덕분에, 며칠 후, 저자는 Python 모듈에 대한 완전한 그림을 갖게 되었다.

이 글을 읽은 다음, 이 주제를 이해하기 위해 더 이상 검색할 필요가 없을 것이다. 따라서 Python 초보자이거나 "탐색" Pythonista이지만 모듈을 사용하여 개발한 적이 없는 사용자라면 이 글은 분명히 그들을 위한 것이다.

## Python 코드를 모듈화해야 하는 이유
먼저 Python에서 모듈을 사용해야 하는 6 가지 이유를 가지고 논의를 시작한다. 

1. **코드 구성 개선**. 모듈을 통해 개발자는 코드를 재사용 가능한 단위로 구성할 수 있다. 다른 프로그램에서 모듈을 가져와 사용할 수 있다. 이는 여러분이 모든 코드를 개발하는 전체 프로그램으로서 파이썬 어플리케이션에 대해 더 이상 생각하지 말아야 한다는 것을 의미한다. 모듈을 로드하고 사용하는 메인 파일이 있다. 여러분은 다음 문단에서 실용적인 방법으로 그 아이디어를 볼 것이다.

2. **코드 재사용성**. 이는 모듈의 장점 중 하나이다. 다른 프로그램에서 가져와서 재사용할 수 있는 독립적인 프로그램을 만들 수 있다. 약간의 변경이 있을 수도 있지만 종종 그대로 사용할 수도 있다. 이는 여러분이 많은 시간과 작성된 코드의 양을 줄이는 데 도움을 준다. 비슷한 코드를 작성하는 것은 당신이 생각하는 것보다 더 자주 발생한다.

3. **네임스페이스 관리**. 만약 여러분이 어떤 언어로 코딩을 해본 적이 있다면, 여러분은 변수에 이름을 붙여야 하는 몇 번의 순간에 직면하게 될 것이다. 코드가 커짐에 따라 유사한 이름의 변수를 작성해야 할 수도 있다. 모듈화를 통해 이름을 수정할 수 있으므로 모듈에서 생성된 이름을 메인 파일에서도 사용하기 때문에 매번 휠을 재생성할 필요가 없다.

4. **협업**. 기업에서 작업하거나 분산 프로젝트(특히 Git 사용)에서 동료와 협업할 때 모듈화를 통해 모듈에서 독립적으로 작업할 수 있다. 이렇게 하면 동일한 파일에서 겹치는 문제를 방지할 수 있다.

5. **더 쉬운 디버깅**. 200줄짜리 어플리케이션을 디버깅했다면 얼마나 고통스러운지 느낄 수 있다. 수십 줄의 코드에 있는 오류를 찾는 데는 며칠이 걸릴 수 있다. 모듈화는 모듈이 서로 독립적이기 때문에 이러한 모든 문제를 방지한다. 이들은 작은 프로그램을 나타내므로 모노리스 어플리케이션보다 쉽게 디버깅할 수 있다.

6. **성능 최적화**. 우리가 개발 중인 특정 응용 프로그램에서 필요한 코드만 가져올 수 있기 때문에 모듈은 여러분의 컴퓨터 성능을 최적화할 수 있다.

## Python의 모듈이란?
[`If __name__ == “__main__”` for Python Developers(비디오)](https://www.youtube.com/watch?v=NB5LGzmSiCs)에서 다음과 같이 말한다. "Python에서 모듈은 패키지와 라이브러리를 의도하지만 다른 코드와 분리할 수 있고 자체적으로 작동할 수 있는 모든 코드 조각도 의도한다."

기본적으로, 모든 Python 프로그램은 모듈이다. 자체적으로 작동할 수 있기 때문이다. 그렇다고 모든 모듈을 가져올 수 있는 것은 아니다. 또한 모듈을 다른 파일로 가져오는 것이 항상 의미가 있는 것은 아니다.

예를 들어 세 개의 폴더가 있는지 확인하는 Python 프로그램을 만들고자 한다. 그렇지 않다면, 그것들을 만든다. 다음과 같이 될 수 있다.

```python
import os

# Define folders name
folders = ["fold_1", "fold_2", "fold_3"]

# Check if folders exist. If not, create folders
for folder in folders:
  if not os.path.isdir(folder):
    os.makedirs(folder)
```

이 파일은 `folders.py` 으로 저장하여 실행할 수 있다. 그것은 스스로 작동하며 모듈도 마찬가지이다. 하지만 의문이 하나 있다. 다른 Python 파일로 가져오는 것이 가능할까?

잠시 읽기를 멈추고 이 질문에 답하여 보자.

답은 '아니오'이다. 단순한 이유 때문이죠. 너무 구체적이다. 이 세 개의 폴더를 다른 폴더로 다시 만들어야 하는 경우 파일을 새 폴더로 이동하여 실행할 수 있다.

따라서 모듈을 가져올 때 우리가 원하는 것은 일반성이다. 이것이 우리가 클래스와 함수를 모듈로 만드는 이유이다. 왜냐하면 그것들은 일반적이기 때문이다. 위 모듈을 다른 파일에서 재사용하려면 일반화해야 한다. 예를 들어 다음과 같은 함수를 만들 수 있다.

```python
import os

def create_folders(fold_1, fold_2, fold_3):
  # Define folders name
  folders = [fold_1, fold_2, fold_3]

  # Check if folders exist. If not, create folders
  for folder in folders:
    if not os.path.isdir(folder):
      os.makedirs(folder)
```

따라서 이 함수는 특정 폴더에 일반 폴더 세 개가 있는지 확인하는 데 유용하다. 그러나 이번에는 함수의 인수로 전달되기 때문에 그 이름을 임의로 정할 수 있다. 그런 다음, 그들이 존재하지 않으면 생성되다. 예를 들어, 다음과 같이 호출할 수 있다.

```python
# Invoke the function
create_folders("audio", "image", "documents")
```

그런 다음 함수는 "audio", "image", "documents" 폴더가 Python 파일이 있는 디렉터리에 존재하는지 확인한다. 그렇지 않다면, 그것들을 만든다.

이 코드는 일반적이기 때문에 다른 Python 파일로 가져오는 것이 가능하다. 여러분이 세 개의 폴더를 확인해야 할 필요가 있을 때, 그것을 사용할 수 있다. 우리는 단지 세 개의 폴더 이름을 선언하기만 하면 된다.

## Python에서 모듈 사용법
기본적으로 모듈은 다음과 같이 작동한다.

![](images/mastering_modular_programming/pic_01.webp)

우리는 일반적으로 `main.py` 이라는 "주" Python 파일을 가지고 있다. 그 Python 파일에서 우리는 두 개의 모듈을 가져온다.

여러분이 눈치채지 못했을 수도 있지만, 위의 코드에서 우리는 디렉터리를 만들고, 관리하고, 제거하는 데 도움이 되는 Python 모듈인 `os`를 가져왔다. 즉, 디렉터리를 관리할 때 커스텀 함수(또는 클래스)을 만들 필요가 없다. `os` 모듈을 사용하여 특정 사례에 적용할 수 있다. 예를 들어 폴더를 만드는 `os.makedirs`를 사용했다.

그러나 알려진 모듈과 패키지 외에도 일상적인 프로그래밍에서 Python 프로그램의 모듈화를 어떻게 적용할 수 있을까? 모듈화의 힘을 사용해 본 적이 없다면, 여러분은 마음을 "바꿔야" 할 것이다. 그것은 충분한 가치가 있다!

모듈을 사용하지 않은 Python 프로그램은 다음 그림과 같다.

![](images/mastering_modular_programming/pic_02.webp)

한 파일에서 다음과 같이 처리할 수 있다.

- 함수와 클래스를 만든다.
- 변수 또는 필요한 방법으로 함수와 클래스를 호출한다.

대신 모듈을 사용하면 Python 프로그램은 다음과 같이 됩니다:

![](images/mastering_modular_programming/pic_03.webp)

이렇게 하면 두 개의 함수와 두 개의 클래스를 별도의 Python 파일로 생성할 수 있다. 즉 모듈이다. 그런 다음 `main` 파일로 가져온다. 이 시점에서 폴더 구성은 다음과 같다.

```
project_name

├── main.py
│   └── packages
│       └── __init__.py
│       └── module_1.py
│       └── module_2.py
│       └── module_3.py
└──     └── module_4.py
```

물론 필요만큼 패키지 하위 폴더를 생성할 수 있다. 중요한 것은 각 하위 폴더에 `__init_.py` 파일이 있어야 한다는 것이다.

그것이 무엇인지, 그리고 그것을 어떻게 사용하는지 살펴보자.

## Python에서 실용적인 예
그럼, Python의 실용적인 예를 들어보겠다. `my_project`라는 프로젝트를 다음과 같이 만든다. 

- 메인 파일은 `main.py` 파일이다.
- `operations`라는 하위 폴더에서 `__init__.py`, `addition.py `, `subtraction.py` 세 개의 Python 파일을 생성한다.

프로젝트의 구조는 다음과 같다.

```
my_project

├── main.py
│   └── operations
│       └── __init__.py
│       └── addition.py
└──     └── subtraction.py
```

우리는 두 정수의 합과 차이를 만드는 간단한 모듈 두 개를 만들고자 한다. 입력이 정수가 아닌 경우 프로그램에서 오류를 반환한다.

따라서 두 모듈은 다음과 같다.

```python
def sum(x: int, y: int) -> int:
        """ this function sums two integers.
            
        Args:
            param 1: integer                         
            param 2: integer

        Returns:
            an integer that is the sum of the two arguments
        """
        if type(x) and type(y) == int:
            print(f"the sum of these two numbers is: {x+y}")
        else:
            print("arguments are not integers!")

if __name__ == "__main__":
     sum()
```

그리고

```python
def diff(x: int, y: int) -> int:
        """ this function subtracts two integers.
            
        Args:
            param 1: integer                         
            param 2: integer

        Returns:
            an integer that is the difference of the two arguments
        """
        if type(x) and type(y) == int:
            print(f"the difference of these two numbers is: {x-y}")
        else:
            print("arguments are not integers!")

if __name__ == "__main__":
     diff()
```

> **NOTE**:
>
> 만약 당신이 타입 힌트를 사용하여 Python을 쓰는 방법에 낯설다면, 그리고 만약 `if __name__ == "__main__"`이 어떻게 작동하는지 모른다면 [이 글](python-oop-definitive-guide.md)을 읽기를 추천한다.

이제 이 두 모듈을 메인 파일로 가져오려고 한다. 그러기 위해서는 `__init__.py`을 다음과 같이 작성하여야 한다.

```
__all__ = ["sum", "diff"]
```

따라서 `__init__.py`는 `operations` 폴더에 가져올 패키지가 있음을 메인 파일에 알려주기 때문에 모듈이 가져와 작업을 할 때 사용해야 하는 Python 파일이다. 따라서 `__init__.py`에서는 위와 같이 패키지에 있는 모든 함수를 선언해야 한다.

이제 `main.py`을 다음과 같이 프로그래밍할 수 있다.

```python
from operations.addition import sum
from operations.subtraction import diff

# Sum two numbers
a = sum(1, 2)
# Subtract two numbers
b = diff(3,2)

# Print results
print(a)
print(b)
```

메인 파일에 대한 몇 가지 고려 사항이 있다.

1. 모듈에는 각각 하나의 함수만 있기 때문에 `operations.addition import *`처럼 쓸 수 있다. 어쨌든 모듈에서 사용하는 함수와 클래스만 가져오는 것이 좋다.
2. 여기서 볼 수 있듯이, 메인 파일은 매우 깔끔하다. 가져오기만 있을 뿐이다. 즉 가져온 함수을 사용한 변수 선언과 결과 인쇄만 있다.

## 최종 팁
선배님들의 조언을 빌리자면, 모듈별로 클래스를 만드는 것이 더 관리하기 쉽기 때문이다. 그러나 이것이 필수는 아니다. 코드화하는 내용에 따라 다르다.

위의 예에서, 저자는 각각 하나의 함수를 별도 모듈로 만들었다. 한 가지 대안은 예를 들어 Operations라는 클래스를 만들고 두 함수를 메서드로 코딩하는 것이다.

이것은 우리가 하나의 파일에 "유사한 함수"를 만들고 메인 파일에서 하나의 가져오기로 만들 수 있기 때문에 도움이 될 것이다.

따라서 항상 조언을 필수로 받아들이지 말고, 필요한 최적화를 염두에 두고 무엇을 만들어야 하는지에 대해 생각해 보도록 하여야 한다.

## 마치며
저자는 모듈에 대한 생각을 명확히 했을 때, 그것들을 일상 프로그램에 적용하기 시작했고, 더 나아졌다.

그래서, 저자는 이 주제를 여러분이 확실히 이해하였기를 바란다.


**(주)** 위 내용은 [Mastering Modular Programming: How to Take Your Python Skills to the Next Level](https://towardsdatascience.com/mastering-modular-programming-how-to-take-your-python-skills-to-the-next-level-ba14339e8429)을 편역한 것이다. 
