이 포스팅은 장고 앱의 기본 동작을 변경하기 위하여 필수 개념을 알고 있는 주로 Django 내부자를 위한 것이다.

### 1. 먼저 문장을 이해하는 것으로 시작한다

- 장고 어플리케이션은 항상 **"수행하기 전에는 데이터베이스 작업을 실행하지 않는다"**라는 문구를 들었다.
- 모델을 만들고 이 문장을 이해해 보도록 하자.

```python
class Items(models.Model):
   name = models.CharField(max_length=20)
   price = models.IntegerField()
```

아래와 같이 레코드들을 `Item` 모델에 삽입했다.

![items_table](images/django-dev-know_1/items_table.png)

지연(laziness)을 이해하기 위해 다음의 예를 살펴보자.

```
>> items = Items.objects.filter(id__lte=3)
>> item_mango = items.filter(name = "Mango")
>> mango_list= list(item_mango)
```

위 코드에서 데이터베이스를 두 번 액세스 하는 것처럼 보이지만, 사실 마지막 줄(`mango_list = list(item_list)`)에서 데이터베이스를 한 번만 액세스한다.

`SQL` 쿼리가 실제로 실행되고 있는 것을 확인하려고 시도했을 때 아래와 같은 것을 발견했기 때문이다.

```
>> print(item_mango.query)
```

```
SELECT "api_items"."id", "api_items"."name", "api_items"."price" FROM "api_items" WHERE ("api_items"."id" <= 3 AND "api_items"."name" = Mango)
```

그리고 위의 쿼리로부터, 마지막에만 데이터베이스를 액세스하고 있는 것을 분명히 알 수 있다.

### 2. `AbstractUser`와 `AbstractBaseUser`의 차이
Django의 기본 `User` 모델은 `username`을 사용하여 인증 중 사용자를 고유하게 식별한다. 대신 이메일 주소를 사용하려면 `AbstractUser` 또는 `AbstractBaseUser`의 서브 클래스로 커스텀 `User` 모델을 만들어야 한다.

옵션:

1. `AbstractUser`: 기존 사용자 모델 필드에 만족하고 username 필드를 제거하려면 이 옵션을 선택한다.
2. `AbstractBaseUser`: 완전히 새로운 `User` 모델을 만들어 처음부터 다시 시작하려면 이 옵션을 사용한다.

### 3. Django의 `filter()`와 `get()` 메서드의 차이
- 학생 테이블를 만들고 그 테이블에 레코드를 삽입했다고 가정한다.

```python
mymodel=Students.objects.get(name='rajan')
# OR
mymodel=Students.objects.filter(name='rajan')
```

Case 1: 이름이 `rajan`인 레코드가 하나만 있다고 가정하자.

- `get()`은 단일 개체를 반환하며, `filter()`는 단일 객체를 갖는 리스트를 반환한다.

Case 2: 이름이 `rajan`인 레코드가 둘 이상 있다고 가정하자.

- `get()`은 `MultipleObjectsReturned` 예외를 일으키는 반면 `filter()`는 객체 리스트를 반환한다.

Case 3: 이름이 `rajan`인 레코드가 없다고 가정하자.

- `get()`은 `DoesNotExist` 예외를 일으키는 반면 `filter()`는 빈(empty) 리스트를 반환한다.

### 4. 정적 파일을 S3 또는 다른 클라우드 스토리지에 직접 업로드하는 방법
시연을 위해, AWS 클라우드를 사용할 것이다.

Django의 ImageField 또는 FileField의 파일을 클라우드 스토리지에 직접 저장하려고 할 때 `django-storage`와 같은 클라우드 스토리지를 지원하는 스토리지 백엔드를 사용할 수 있다.

다음은 아마존 S3에서 `django-storage`를 사용하는 방법이다.

먼저 `django-storage` 패키지를 설치해야 한다.

```bash
$ pip install django-storages
```

그런 다음 설정 파일에 다음을 추가한다.

```
EFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
AWS_ACCESS_KEY_ID = 'your_access_key_id'
AWS_SECRET_ACCESS_KEY = 'your_secret_access_key'
AWS_STORAGE_BUCKET_NAME = 'your_bucket_name'
```

모델에서 upload_to 매개 변수를 S3 버킷의 서브디렉터리로 설정한다.

```python
from django.db import models

class MyModel(models.Model):
    image_field = models.ImageField(upload_to='my_subdirectory/')
```

모델의 새로운 인스턴스를 만들고 저장하면 Django는 자동으로 파일을 S3 버킷의 지정된 하위 디렉터리에 업로드한다.

```python
mymodel = MyModel.objects.create(image_field=my_uploaded_file)
```

- `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`와 `AWS_STORY_BUCKET_NAME` 값을 사용자 고유의 값으로 바꿔야 한다.
- 설정 파일에서 `DEFAULT_FILE_STORAGE` 값을 변경하여 Google Cloud Storage 또는 Microsoft Azure Storage를 `django-storage`에서 지원하는 다른 스토리지 백엔드로 사용할 수도 있다.

---

**이 포스팅은 여기서 끝나지 않고 Part 2를 개시할 것이고 여기서 Django의 가장 숨겨진 특징에 대해 설명할 것이다.**

**주** 이 포스팅은 [Django Developer Should Know(Part-1)](https://rajansahu713.medium.com/django-developer-should-know-part-1-89f66290625c)을 편역한 것임.
