Django로 로그인, 로그인, 로그아웃 및 인증 앱을 만들려면 다음 단계를 수행해야 한다<sup>[1](#footnote_1)</sup>.

Django를 설치하고 새 프로젝트를 만든다.

```bash
$ pip install django
$ django-admin startproject auth_app
```

프로젝트에 새 앱 만들기

```bash
$ python manage.py startapp accounts
```

`accounts/models.py`에 사용자 계정을 저장하는 모델을 만든다. Django에서 제공하는 모델인 `AbstractUser`를 기본으로 사용하거나 필요한 필드를 갖는 자체 모델을 만들 수 있다.

```python
# models.py

from django.contrib.auth.models import AbstractUser
from django.db import models
class User(AbstractUser):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
```

데이터베이스 마이그레이션을 실행하여 사용자 계정에 대한 데이터베이스 테이블을 만든다.

```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

`accounts/forms.py` 에서 등록과 로그인 페이지에 대한 양식을 만든다. Django에서 제공하는 사용자 작성 양식과 인증 양식을 사용하거나, 필요한 필드를 사용하여 커스터마이즈된 양식을 작성할 수 있다.

```python
# forms.py

from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import User

class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class LoginForm(AuthenticationForm):
    pass
```

`accounts/views.py` 에서 등록, 로그인과 로그아웃 페이지에 대한 view 함수를 만든다. Django에서 제공하는 로그인과 로그아웃 기능을 사용하거나 자신만의 view를 만들 수 있다.**

```python
# views.py

from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from .forms import SignupForm, LoginForm

def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = SignupForm()
    context = {'form': form}
    return render(request, 'accounts/signup.html', context)

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = LoginForm()
    context = {'form': form}
    return render(request, 'accounts/login.html', context)

def logout(request):
    logout(request)
    return redirect('home')
```

이러한 view 함수은 등록, 로그인과 로그아웃 양식의 POST 요청을 처리하고 Django에서 제공하는 `login`과 `logout` 함수를 사용하여 사용자의 로그인과 로그아웃을 수행한다. `accounts/templates/accounts` 디렉토리에 로그인, 로그인과 로그아웃 페이지에 대한 템플릿을 만든다. 다음 코드를 시작점으로 사용할 수 있다.

```html
<!-- sign_up -->

<h1>Sign Up</h1>
<form method="POST" action="{% url 'signup' %}">
    {% csrf_token %}
    {{ form.as_p }}
    <input type="submit" value="Sign Up">
</form>
```

```html
<!-- login.html -->

<h1>Log In</h1>
<form method="POST" action="{% url 'login' %}">
    {% csrf_token %}
    {{ form.as_p }}
    <input type="submit" value="Log In">
</form>
```

```html
<!-- logout.html  -->

<h1>Log Out</h1>
<form method="POST" action="{% url 'logout' %}">
    {% csrf_token %}
    <input type="submit" value="Log Out">
</form>
```

위의 템플릿은 각각 등록, 로그인과 로그아웃 페이지의 양식을 정의한다.

**주** 이 포스팅은 [Create Signup, Login, & Logout Authentication View With Django](https://medium.com/@muhammadsameer.css/create-signup-login-logout-authentication-view-with-django-ad065b25fdae)를 편역한 것임.

<a name="footnote_1">1</a>: 앱을 만들기에는 생략된 부분이 많다. 보다 구체적인 내용이 필요하면 [초보를 위한 Django 1 - 시작하기](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-1/)를 참고하세요.
