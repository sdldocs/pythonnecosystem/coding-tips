*Python을 위한 정적 타입 검사*

이전 [Python 모범 사례](https://medium.com/towards-data-science/best-practices-for-python-development-bf74c2880f87)에 대한 포스팅에서 mypy를 필수 항목으로 언급했었다. 여기서는 mypy에 대해 더 자세히 소개하고자 한다.

문서에서 설명하듯이 [mypy](https://mypy.readthedocs.io/en/stable/)는 "Python용 정적 유형 검사기"이다. 즉, 설계에 의해 동적으로 타입이 결정되는 언어 Python에 타입 주석과 검사를 추가합니다(예: C++와 반대로 타입은 런타임에 추론됨). 이렇게 하면 컴파일 때 코드에서 버그를 찾을 수 있다. 이는 버그를 찾는 데 큰 도움이 되며 이전 게시물에서 설명한 것처럼 세미프로 파이썬 프로젝트의 필수 요소이다.

이 포스팅에서 몇 가지 예를 이용하여 mypy를 소개하려 한다. 고지 사항: 이 포스팅에서 mypy 모든 기능을 소개하지 않는다. 대신에, 여러분이 원하는 거의 모든 코드를 작성할 수 있는 충분한 세부 정보와 바닥부터 확실한 이해를 위해 가파른 학습 곡선을 생성하는 것 사이에서 균형을 잡도록 노력할 것이다. 자세한 내용은 공식 문서나 기타 훌륭한 튜토리얼을 참조하도록 한다.

## 설치
다음과 같이 간단히 실행하여 mypy를 설치한다.

```bash
$ pip install mypy
```

하지만, [시](https://medium.com/towards-data-science/dependency-management-with-poetry-f1d598591161)와 같은 어떤 형태의 의존성 관리 시스템을 사용하는 것을 추천한다. [여기](https://medium.com/towards-data-science/best-practices-for-python-development-bf74c2880f87)에서 이것과 mypy를 더 큰 소프트웨어 프로젝트에 포함시키는 방법을 설명하고 있다.

### 첫 걸음
mypy의 사용에 동기를 부여하기 위해 첫 번째 예로 다음 코드를 살펴보자.

```python
def multiply(num_1, num_2):
    return num_1 * num_2


print(multiply(3, 10))
print(multiply("a", "b"))
```

`multiply`는 두 개의 숫자를 기대하고 그 곱을 반환한다. 따라서 `multiply(3, 10)`은 잘 작동하고 원하는 결과를 반환한다. 그러나 두 번째 문장은 실패하고 실행을 중단한다. 문자열을 곱할 수 없기 때문이다. Python에서는 타입이 동적으로 결정되기 때문에 그 문장을 코딩한 다음 실행하는 것을 무엇도 막지 못했고, 실행할 때만 문제가 있다는 것을 발견했다.

여기, mypy가 구조하러 왔다. 이제 인수와 함수의 반환 타입을 주석으로 달 수 있다.

```python
def multiply(num_1: int, num_2: int) -> int:
    return num_1 * num_2


print(multiply(3, 10))
print(multiply("a", "b"))
```

이 주석은 실행에 영향을 주지 않으며, 특히 이 결함이 있는 프로그램을 계속 실행할 수 있다. 하지만, 프로그램을 빼포하기 전에, 다음과 같이 mypy를 실행하여 가능한 오류를 확인할 수 있다. 

```bash
mypy .
```

이 명령을 실행하면 실패하고 문자열을 `multiply`할 수 없음을 올바르게 지적한다. 위의 명령은 어플리케이션의 wn 폴더에서 실행된다. `.`는 현재 폴더와 하위 디렉터리에 있는 모든 파일을 확인한다. 그러나 `mypy file_to_check.py`를 통해 특정 파일을 확인할 수도 있다.

이 예가 mypy의 필요성과 사용에 동기를 부여했기를 바라며다, 이제 더 깊이 들어가 보자.

### mypy 설정
mypy는 다양한 방법으로 설정할 수 있다. 자세한 내용을 설명하지 않고 "mypy" 섹션이 있는 하나의 설정 파일(mypy.ini, pyproject.toml 등)만 찾으면 된다. 여기서는 프로젝트의 기본 폴더에 있어야 하는 기본 파일 `mypy.ini`를 생성한다.

이제 가능한 설정 옵션에 대해 살펴보겠다. 이를 위해 처음 예제로 돌아가자.

```python
def multiply(num_1, num_2):
    return num_1 * num_2


print(multiply(3, 10))
print(multiply("a", "b"))
```

단순히 mypy를 실행하는 것만으로는 실제로 오류가 발생하지 않는다! 즉, 타입 힌트는 기본적으로 선택 사항이며 mypy는 주석이 있는 타입만 확인한다.  `-- disallow-untyped-defs`를 통해 이를 비활성화할 수 있다. 또한 사용할 수 있는 다른 플래그도 다수 있다([여기](https://mypy.readthedocs.io/en/stable/command_line.html#miscellaneous-strictness-flags) 참조). 하지만, 이 포스팅의 일반적인 형식에 따라, 모든 것에 대해 자세히 설명하지 않고 대신 엄격한 모드를 제시할 것이다. 이 모드는 기본적으로 모든 선택적 검사를 활성화 한다. 필자의 경험에 비추어 볼 때, mypy를 사용하는 가장 좋은 방법은 가능한 한 가장 엄격한 검사를 요청한 다음 발생한 문제를 해결(또는 선택적으로 무시)하는 것이다.

그렇게 하려면 `mypy.ini` 파일을 다음과 같이 작성한다.

```
[mypy]
strict = true
```

섹션 헤더 `[mypy]`는 mypy 관련 설정에 필요하며, 다음 줄은 매우 자명하다.

지금 mypy를 정상적으로 실행하면 누락된 타입 주석에 대한 오류가 발생한다. 주석은 모든 항목을 입력하고 잘못된 문자열 호출을 제거한 후에만 나타나지 않는다.

이제 mypy로 주석을 다는 방법에 대해 자세히 알아보겠다.

## mypy로 주석 달기
여기서는 가장 일반적인 타입의 주석과 mypy 키워드에 대해 설명한다.

### 기본 타입
단순히 Python 타입(예: `bool`, `int`, `float`, `str`, ...)을 사용하여 기본 타입에 주석을 달 수 있다.

```python
def negate(value: bool) -> bool:
    return not value


def multiply(multiplicand: int, multiplier: int) -> int:
    return multiplicand * multiplier


def divide(dividend: float, divisor: float) -> float:
    return dividend / divisor


def concat(str_a: str, str_b: str) -> str:
    return str_a + " " + str_b


print(negate(True))
print(multiply(3, 10))
print(divide(10, 3))
print(concat("Hello", "world"))
```

### 컬렉션 타입
Python 3.9 이상부터는 내장 컬렉션 타입을 타입 주석으로 사용할 수 있다. 즉, `list`, `set`, `dict`, ….

```python
def add_numbers(numbers: list[int]) -> int:
    return sum(numbers)


def cardinality(numbers: set[int]) -> int:
    return len(numbers)


def concat_values(value_dict: dict[str, float]) -> list[float]:
    return [val for _, val in value_dict.items()]


print(add_numbers([1, 2, 3, 4]))
print(cardinality({1, 2, 3}))
print(concat_values({"a": 1.5, "b": 10}))
```

보다시피, 컨테이너의 내용물(예: `int`)을 명시해야 한다. 혼합 타입은 다음을 참조한다.

### 이전 버전의 Python
이전 Python 버전에서는 `typing` 모듈의 레거시 타입을 사용해야 했다.

```python
from typing import Dict, List, Set

def add_numbers(numbers: List[int]) -> int:
    return sum(numbers)


def cardinality(numbers: Set[int]) -> int:
    return len(numbers)


def concat_values(value_dict: Dict[str, float]) -> list[float]:
    return [val for _, val in value_dict.items()]


print(add_numbers([1, 2, 3, 4]))
print(cardinality({1, 2, 3}))
print(concat_values({"a": 1.5, "b": 10}))
```

### 콘텐츠 믹싱
위에서 언급한 것처럼 서로 다른 데이터 타입을 포함하는 컨테이너를 만들고 싶을 수도 있다. 이를 위해 `Union` 키워드를 사용하면 타입을 여러 타입들의 조합으로 주석을 달 수 있다.

```python
from typing import Union

def scan_list(elements: list[Union[str | int]]) -> None:
    for el in elements:
        if isinstance(el, str):
            print(f"I got a string! ({el})")
        elif isinstance(el, int):
            print(f"I got an int! ({el})")
        else:
            # NOTE: we don't reach here because of mypy!
            raise ValueError(f"Unexpected element type {el}")


scan_list([1, "hello", "world", 100])
```

Python 3.9에서 수행된 단순화와 유사하게, Python 3.10(구체적으로 [PEP 604](https://peps.python.org/pep-0604/))은 논리 or 연산자(`|`)를 사용하여 `Union` 타입의 약어 표기법을 도입하였다.

```python
def scan_list(elements: list[str | int]) -> None:
    for el in elements:
        if isinstance(el, str):
            print(f"I got a string! ({el})")
        elif isinstance(el, int):
            print(f"I got an int! ({el})")
        else:
            # NOTE: we don't reach here because of mypy!
            raise ValueError(f"Unexpected element type {el}")


scan_list([1, "hello", "world", 100])
```

## 기타 mypy 키워드
여기에서는 더 많은 필수 타입과 키워드를 소개한다.

### `None`
`None`은 "보통" Python에서와 마찬가지로 `None` 값을 나타낸다. 반환 타입없이 함수에 주석을 달 때 가장 많이 사용된다.

```python
def print_foo() -> None:
    print("Foo")


print_foo()
```

### `Optional`
종종 매개 변수 값을 전달했는지 여부에 따라 분기 코드를 구현하려는 상황이 발생할 수 있으며 매개 변수 값이 없음을 나타내기 위해 `None`을 사용하는 경우가 많다. 이를 위해, `typing.Optional[X]`을 사용할 수 있다. 정확히 이를 의미한다. 타입 `X`에 주석을 달지만 `None`도 허용한다.

```python
from typing import Optional

def square_number(x: Optional[int]) -> Optional[int]:
    return x**2 if x is not None else None


print(square_number(14))
```

PEP 604가 도입된 Python 3.10 이상에서는 `Optional`을 `X | None`으로 다시 단축할 수 있다.

```python
def square_number(x: int | None) -> int | None:
    return x**2 if x is not None else None


print(square_number(14))
```

이는 종종 혼동되는 [필수 또는 선택적 매개변수](https://levelup.gitconnected.com/untangling-required-optional-positional-and-keyword-arguments-f7792320a40d)와 일치하지 않는다! 옵션 매개 변수는 함수를 호출할 때 지정할 필요가 없는 매개 변수이며, mypy의 옵션은 특정 유형일 수 있지만 없음도 나타내는 매개 변수이다. 선택적 매개 변수의 공통 기본값이 없음일 수는 있다.

### `Any`
이름에서 알 수 있듯이(이 문장을 계속 반복하는 것 같다...) 모든 타입을 허용하므로 타입 검사를 결과적으로 해제할 수 있다. 따라서, 가능할 때마다 이를 피하도록 한다.

```python
from typing import Any


def print_everything(val: Any) -> None:
    print(val)


print_everything(0)
print_everything(True)
print_everything("hello")
```

### 변수 주석 달기
지금까지 mypy를 사용하여 함수 매개 변수와 반환 타입에 주석을 달았다. 이것을 모든 종류의 변수로 확장하는 것이 자연스러울 것이다.

```python
int_var: int = 0
float_var: float = 1.5
str_var: str = "hello"
```

그러나 변수의 타입이 컨텍스트에서 대부분 명확하기 때문에 이는 덜 사용된다(또한 mypy의 엄격한 버전조차에서도 적용되지도 않는다). 일반적으로 코드가 비교적 모호하고 읽기 어려운 경우에만 이를 적용한다.

## 복합 타입(Complex Types) 주석 달기
여기에서는 클래스에 주석을 다는 것뿐만 아니라 사용자 자신의 클래스와 기타 복합 클래스에 주석을 다는 것에 대해서도 설명한다.

### 클래스에 주석 달기
클래스에 주석을 다는 것은 매우 빠르게 처리될 수 있다. 클래스 함수에 다른 함수처럼 주석을 달기만 하면 되지만 생성자에서 `self` 인수에는 주석을 달지 않는다.

```python
class SampleClass:
    def __init__(self, x: int) -> None:
        self.x = x

    def get_x(self) -> int:
        return self.x


sample_class = SampleClass(5)
print(sample_class.get_x())
```

### 커스텀 또는 복합 클래스에 주석 달기
클래스를 정의하면 클래스 이름을 타입 주석으로 사용할 수 있다.

```python
sample_class: SampleClass = SampleClass(5)
```

사실, mypy는 대부분의 클래스와 타입을 즉시 사용할 수 있다.

```python
import pathlib


def read_file(path: pathlib.Path) -> str:
    with open(path, "r") as file:
        return file.read()


print(read_file(pathlib.Path("mypy.ini")))
```

## mypy 문제 해결
여기서는 `numpy`와 `matplotlib`이 관련된 조금 더 복잡한 예를 바탕으로 타이핑을 지원하지 않는 외부 라이브러리를 처리하고 문제를 일으키는 특정 줄에 대한 타입 검사를 선택적으로 비활성화하는 방법에 대해 알아본다.

코드의 첫 번째 버전부터 시작하겠다.

```python
import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt


def calc_np_sin(x: npt.NDArray[np.float32]) -> npt.NDArray[np.float32]:
    return np.sin(x)


x = np.linspace(0, 10, 100)
y = calc_np_sin(x)

plt.plot(x, y)
plt.savefig("plot.png")
```

열기/닫기 아이콘
numpy 배열의 축을 계산하는 간단한 함수를 정의하고 범위 [0, 10]상의 입력 값 `x`에 적용한다. 그런 다음 `matplotlib`을 사용하여 축곡선을 그린다.

이 코드에서는 `numpy.typing`을 사용하여 numpy 배열을 바르게 주석을 다는 것도 볼 수 있다.

하지만, 만약 이것에 대해 mypy를 실행한다면, 두 개의 오류를 얻는다. 첫 번째는 다음과 같다.

> *error: Returning Any from function declared to return “ndarray[Any, dtype[floating[_32Bit]]]”*

이것은 mypy에서 비교적 흔한 패턴입니다. 실제로 코드에는 아무 잘못도 없지만, mypy는 좀 더 명확하게 하기를 원한다. 그리고 여기서 뿐만 아니라 다른 상황에서도, mypy가 코드를 받아들이도록 "강제"해야 한다. 예를 들어 올바른 타입의 프록시 변수를 도입하여 이 작업을 수행할 수 있다.

```python
def calc_np_sin(x: npt.NDArray[np.float32]) -> npt.NDArray[np.float32]:
    y: npt.NDArray[np.float32] = np.sin(x)
    return y
```

다음 오류는

> *error: Skipping analyzing “matplotlib”: found module but no type hints or library stubs*

이는 `matplotlib`가 아직 타입되지 않았기 때문이다. 따라서 검사에서 제외하기 위해 mypy에게 알려줘야 한다. 이 작업은 `mypy.ini` 파일에 다음을 추가하여 수행한다.

```python
[mypy-matplotlib.*]
ignore_missing_imports = True
ignore_errors = True
```

마지막으로 문장에 `#type: ignore`를 추가하여 코드 행을 선택적으로 무시할 수도 있다. mypy에 해결할 수 없는 문제가 있거나 알려진 경고/오류를 무시하려면 이 작업을 수행한다. 다음과 같이 위의 첫 번째 오류를 숨길 수도 있다.

```python
def calc_np_sin(x: npt.NDArray[np.float32]) -> npt.NDArray[np.float32]:
    return np.sin(x)  # type: ignore
```

## 마치며
이 포스팅에서 Pythobn용 정적 타입 체커인 mypy를 소개했다. mypy를 사용하여 변수 타입, 매개 변수와 반환 값에 주석을 달 수 있으며, 이를 통해 컴파일 시 프로그램의 온전성을 검사할 수 있다. mypy는 매우 광범위하며, 모든 준대형 소프트웨어 프로젝트에 추천한다.

mypy를 설치하고 설정하는 것으로 시작했다. 그런 다음 기번 타입과 리스트, 딕트 또는 세트같은 복잡한 타입에 주석을 다는 방법을 소개했다. 다음으로 `Union`, `Optional`, `None`과 `Any` 같은 다른 중요한 주석자에 대해 논의했다. 결국, mypy가 커스텀 클래스와 같은 다양하고 복잡한 타입을 지원한다는 것을 보여주었다. mypy 오류를 디버깅하고 수정하는 방법을 보여주며 이를 마친다.

**주** 이 페이지는 [Introduction to mypy](https://towardsdatascience.com/introduction-to-mypy-3d32fc96db75)을 편역한 것임.
