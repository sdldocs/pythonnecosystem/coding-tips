먼저 MTV 설계 패턴을 따르는 풀스택 Python 기반 웹 프레임워크인 Django를 사용하여 웹 어플리케이션을 만드는 방법에 대해 설명했다. 프론트엔드와 백엔드를 모두 만들 수 있기 때문에 풀 스택이라고 한다.

그러나 이 방법에는 한 가지 작은 결점이 있다. 최종 사용자가 웹 페이지를 요청하면 페이지를 백엔드로 렌더링한 다음 렌더링된 HTML 페이지가 사용자에게 전송된다. 여러분이 상상할 수 있듯이, 많은 사용자가 있을 때, 그것은 여러분의 서버에 많은 부담을 줄 수 있다.

이 문제를 해결하기 위해 개발자들은 보통 어플리케이션을 백엔드와 프런트엔드 두 부분으로 나눈다. 이렇게 하면 사용자가 웹 페이지를 요청할 때 웹 페이지를 렌더링하는 대신 백엔드는 필요한 데이터만 수집하여 프런트 엔드로 전송한다. 일반적으로 과도한 컴퓨팅 능력을 가진 클라이언트의 기계는 데이터를 사용하여 브라우저 내부의 웹 페이지를 직접 렌더링하여 서버에 대한 부담을 덜어준다.

본 포스팅<sup>[1](footnote_1)</sup>에서는 Django를 백엔드로, Vue를 프런트엔드로, GraphQL을 함께 백엔드와 프런트엔드를 연결하는 API 조작 언어로 사용하여 현대적인 단일 페이지 애플리케이션을 만드는 방법에 대해 논의하고자 한다.

시작하기 전에 Django와 Vue.js 프레임워크를 모두 알고 있어야 한다. 그렇지 않은 경우 먼저 아래 포스팅을 읽고 참조하시오.

- [초보를 위한 Django](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-1/)
- [초보를 위한 Vue.js](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)

## Django에 대한 간략한 검토
먼저 Django 프레임워크에 대한 간략한 검토부터 시작하겠다. Django는 MTV 아키텍처를 따르는 Python 기반 웹 프레임워크이다.

- 모델(M)은 레코드 검색, 생성, 업데이트 또는 삭제 같이 데이터베이스와 상호 작용할 수 있는 인터페이스이다.
- 템플릿(T)은 프레임워크의 프런트 엔드 부분으로, 최종 사용자에게 보이게 될 부분이다.
- 뷰(V)는 어플리케이션의 백엔드 로직으로, 모델을 사용하여 사용자가 필요로 하는 데이터를 검색하는 등 데이터베이스와 상호 작용한다. 그런 다음 뷰는 어떤 방식으로든 데이터를 조작하여 (일반적으로 사용자 정의 템플릿으로) 결과를 사용자에게 반환한다.

이 포스팅에서는 백엔드에 Django만 사용한다. 즉, Django의 템플릿이나 뷰를 사용하지 않고 Vue.js 및 GraphQL로 대체한다.

Django 쪽을 설정하는 것으로 시작한다.

## Django 프로젝트 생성
개인적으로 `backend` 디렉터리와 `frontend` 디렉터리로 분리하는 것을 좋아한다. 아래와 같이 프로젝트 구조를 만들었다.

```
blog
├── backend
└── frontend
```

백엔드 폴더로 이동하여 새 Python 가상 환경을 생성한다. Python 가상 환경은 사용자 지정 패키지가 없는 새로운 Python이 설치된 격리된 환경이다. 이 환경 내에 패키지를 설치할 때 Linux 또는 macOS를 사용하는 경우 매우 중요한 시스템의 Python 환경에 영향을 미치지 않으며 이를 방해하고 싶지 않기 때문이다.

```bash
$ cd backend
```

```bash
$ python3 -m venv env
```

이 명령을 실행하면 `env`라는 새 디렉토리가 생성되고 가상 환경이 내부에 생성된다. 이 가상 환경을 활성화하려면 다음 명령을 사용한다.

```bash
$ source env/bin/activate
```

Windows를 사용하는 경우 아래 명령을 대신 사용한다. 이는 개인적인 선호도에 따라 다르지만 Windows를 사용하는 경우 [WSL](https://learn.microsoft.com/en-us/windows/wsl/install)을 설정하는 것이 좋다.

```
env/Scripts/activate
```

가상 환경이 활성화된 후에는 터미널에 다음과 같이 표시된다. 프롬프트 앞에 있는 `(env)`를 볼 수 있다. 현재 가상 환경에서 작업 중임을 나타내는 것이다.

![virtual-env](images/modern-app/virtual-env.webp)

다음으로, 당신이 새로운 Django 프로젝트를 만들 시간이다. 이 과정을 숙지해야 한다. 그렇지 않은 경우 이전에 링크된 문서에서 자세한 내용을 확인하시오.

```bash
(env) $ python -m pip install Django
```

```bash
(env) $ django-admin startproject backend
```

새로운 어플리케이션을 생성한다.

```bash
(env) $ python manage.py startapp blog
```

마치면, 프로젝트 구조는 다음과 같다.

```
.
├── backend
│   ├── backend
│   ├── blog
│   ├── manage.py
│   └── requirements.txt
└── frontend
```

## 모델 생성
모델은 데이터베이스와 상호 작용하는 데 사용할 수 있는 인터페이스이다. Django의 가장 큰 특징 중 하나는 모델에 대한 변경 사항을 자동으로 감지하고 해당 마이그레이션 파일을 생성하여 데이터베이스 구조를 변경하는 데 사용할 수 있다는 것이다.

### `Site` 모델
웹 사이트의 기본 정보를 저장하는 `Site` 모델부터 시작하겠다.

```python
class Site(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    logo = models.ImageField(upload_to='site/logo/')

    class Meta:
        verbose_name = 'site'
        verbose_name_plural = '1. Site'

    def __str__(self):
        return self.name
```

4째 줄에는 Django에게 `'site/logo/'` 디렉토리에 이미지를 업로드하라고 알리는 `ImageField`가 있다. 이 작업을 수행하려면 두 가지 작업을 수행해야 한다.

먼저 `Pillow` 패키지를 설치해야 한다. Django는 이미지를 처리하기 위해 필요하다.

```bash
(env) $ python -m pip install Pillow
```

둘째, `settings.py` 에 새로운 설정이 필요하다. Django에게 이 미디어 파일들을 어디에 저장할 것인지, 이 파일들에 접근할 때 어떤 URL을 사용할 것인지 알려주어야 한다.

```python
import os


# Media Files
MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')
MEDIA_URL = '/media/'
```

이 설정은 미디어 파일이 `/mediafiles` 디렉토리에 저장됨을 의미하며, URL 접두사 `/media/`를 사용하여 액세스해야 한다. 예를 들어 URL `http://localhost:3000/media/example.png`은 이미지 `/mediafiles/example.png`을 검색한다.

### `User` 모델
다음은 `User` 모델이다. Django에는 기본 권한과 권한 부여 기능을 제공하는 `User` 모델이 내장되어 있다. 하지만, 이 프로젝트를 위해, 좀 더 복잡한 것을 시도해 보겠다. 프로필 아바타, 바이오와 다른 정보를 추가할 수 있다. 이 작업을 수행하려면 `AbstractUser` 클래스로 확장되는 새 `User` 모델을 만들어야 한다.

```python
from django.contrib.auth.models import AbstractUser


# New user model
class User(AbstractUser):
    avatar = models.ImageField(
        upload_to='users/avatars/%Y/%m/%d/',
        default='users/avatars/default.jpg'
    )
    bio = models.TextField(max_length=500, null=True)
    location = models.CharField(max_length=30, null=True)
    website = models.CharField(max_length=100, null=True)
    joined_date = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.username
```

Django의 `AbstractUser`는 아래와 같다.

```python
class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
```

보다시피 `first_name`, `last_name` 등 기본 필드를 제공한다.

다음으로, Django가 이 새 사용자 모델을 기본 사용자 모델로 사용하고 있는지 확인해야 한다. 그렇지 않으면 인증이 작동하지 않는다. `settings.py` 으로 이동하여 다음 을 추가한다.

```python
# Change Default User Model
AUTH_USER_MODEL = 'blog.User'
```

## `Category`, `Tag`와 `Post` 모델

```python
class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    description = models.TextField()

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name
```

```python
class Tag(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    description = models.TextField()

    class Meta:
        verbose_name = 'tag'
        verbose_name_plural = '4. Tags'

    def __str__(self):
        return self.name
```

```python
class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField()
    content = RichTextField()
    featured_image = models.ImageField(
        upload_to='posts/featured_images/%Y/%m/%d/')
    is_published = models.BooleanField(default=False)
    is_featured = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    modified_at = models.DateField(auto_now=True)

    # Each post can receive likes from multiple users, and each user can like multiple posts
    likes = models.ManyToManyField(User, related_name='post_like')

    # Each post belong to one user and one category.
    # Each post has many tags, and each tag has many posts.
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True)
    tag = models.ManyToManyField(Tag)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title

    def get_number_of_likes(self):
        return self.likes.count()
```

13째 줄에서 유사한 시스템이 어떻게 구현되는지 주목하시오. 단순한 `IntegerField`가 아니라 태그처럼 작동한다. 그리고 `get_number_of_like()` 메서드를 사용하여 각 게시물에 대한 '좋아요' 수를 가져올 수 있다.

### `Comment` 모델
이번에는 한 걸음 더 나아가 이 어플리케이션 프로그램에 대한 댓글 섹션을 만들어 보자.

```python
class Comment(models.Model):
    content = models.TextField(max_length=1000)
    created_at = models.DateField(auto_now_add=True)
    is_approved = models.BooleanField(default=False)

    # Each comment can receive likes from multiple users, and each user can like multiple comments
    likes = models.ManyToManyField(User, related_name='comment_like')

    # Each comment belongs to one user and one post
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    post = models.ForeignKey(Post, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'comment'
        verbose_name_plural = 'Comments'

    def __str__(self):
        if len(self.content) > 50:
            comment = self.content[:50] + '...'
        else:
            comment = self.content
        return comment

    def get_number_of_likes(self):
        return self.likes.count()
```

## Django admin 패널 설정
마지막으로 Django admin를 설정한다. `admin.py` 파일을 연다.

```python
from django.contrib import admin
from .models import *

# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'date_joined')

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'is_published', 'is_featured', 'created_at')

class CommentAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'is_approved', 'created_at')


admin.site.register(Site)
admin.site.register(User, UserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
```

`CommentAdmin`의 경우 `__str__`은 `Comment` 모델에서 `__str__()` 메서드를 참조한다. 그러면 "`...`"와 연결된 처음 50자가 반환된다.

이제 개발 서버를 시작하고 모든 것이 작동하는지 확인하자.

![admin](images/modern-app/admin.png)

다음 단계로 이동하기 전에 블로그에 대한 유사 정보를 추가해야 한다.

## Vue.js에 대한 간략한 검토
이제 백엔드 작업을 마쳤으므로 프런트엔드에 집중할 시간이다. 이 포스팅의 두 번째 부분에서는 `Vue.js`를 사용하여 프런트엔드 어플리케이션을 생성해 보겠다. 다시, 간단한 검토부터 시작한다. 프레임워크를 한 번도 사용해 본 적이 없는 경우, 먼저 이전에 연결된 포스팅들을 읽으시오.

Vue.js는 대화형 사용자 인터페이스를 만들 수 있는 간단한 구성 요소 기반 시스템을 제공하는 프런트엔드 JavaScript 프레임워크이다. 구성 요소 기반이란 루트 구성 요소(`App.vue`)가 다른 구성 요소(확장자가 `.vue`인 파일)를 가져올 수 있으며 이러한 구성 요소는 더 많은 구성 요소를 가져올 수 있으므로 매우 복잡한 시스템을 생성할 수 있다.

일반적인 `.vue` 파일에는 세 섹션이 포함되어 있으며, `<template>` 섹션에는 HTML 코드가, `<script>` 섹션에는 JavaScript 코드가, `<style>` 섹션에는 CSS 코드가 포함되어 있다.

`<script>` 섹션에서는 `data()` 모델 내에서 새 바인딩을 선언할 수 있다. 그런 다음 이중 중괄호 구문(`{{ binding }}`)을 사용하여 이러한 바인딩을 `<템플릿>` 섹션 내에 디스플레이할 수 있다. `data()` 메서드 내에서 선언된 바인딩은 Vue의 반응성 시스템 내에서 자동으로 래핑된다. 바인딩 값이 변경되면 페이지를 새로 고칠 필요 없이 해당 구성요소가 자동으로 렌더링된다.

`<script>` 섹션에는 `computed`, `props`, `methods` 등 `data()` 이외의 메서드도 포함될 수 있다. 또한 `<template>`를 사용하면 `v-bind`, `v-on`과 `v-model`를 사용하여 데이터를 바인딩할 수 있다.

## 새로운 Vue.js 프로젝트 생성
[초보를 위한 Vue.js](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)에서는 Vue 명령 도구를 사용하여 Vue 앱을 설치하고 만들었다. 이번에는, 다른 방식으로 할 것이다. Vue.js를 만든 동일한 작성자가 만든 Vite(fast를 뜻하는 프랑스어 단어 "veet"로 발음됨)라는 프론트엔드 빌드 도구를 사용할 것이다.

`frontend` 폴더로 이동하여 다음 명령을 실행한다.

```bash
$ npm init vue@latest
```

여러 옵션을 보여준다. 이 프로젝트의 경우 Vue Router만 추가하면 된다.

```
✔ Project name: … <your_project_name>
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit testing? … No / Yes
✔ Add Cypress for both Unit and End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formating? … No / Yes

Scaffolding project in ./<your_project_name>. . .
Done.
```

강력한 타입 언어를 원하는 경우 [TypeScript](https://www.typescriptlang.org/)를 설치하도록 선택할 수 있다. 코드에 대한 자동 수정과 자동 포맷이 필요한 경우 ESlint와 Pretty도 설치할 수 있다. 이 설치 프로세스는 필요한 패키지와 해당 버전을 저장하는 프로젝트 디렉토리내에  `package.json` 파일을 생성한다. 이러한 패키지는 프로젝트 내에 설치해야 한다.

```bash
$ cd <your_project_name>
```

```bash
$ npm install
```

```bash
$ npm run dev
```

프런트엔드 앱을 만들기 전에 한 가지 더 있다. 이 프로젝트에서 [TailwindCSS](https://tailwindcss.com/)라는 CSS 프레임워크를 사용할 것이다. 설치하려면 다음 명령을 실행한다.

```bash
$ npm install - D tailwindcss postcss autoprefixer
```

```bash
$ npx tailwindcss init -p
```

이렇게 하면 `tailwind.config.js`와 `postcss.config.js`라는 두 개 파일이 생성된다. 이 포스팅은 CSS나 Tailwind에 대한 튜토리얼이 아니기 때문에 이미 그것들을 사용하는 방법을 알고 있다고 가정한다. 그렇지 않다면 [Tailwind의 공식 문서](https://tailwindcss.com/docs/editor-setup)를 읽어보기 바란다.

`tailwindcss.config.js`로 이동하고, 모든 템플릿 파일의 경로를 추가한다.

```js
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
};
```

`./src/index.css` 파일을 만들고 Tailwind의 각 레이어에 대한 `@tailwind` 지시어를 추가한다.

```js

@tailwind base;
@tailwind components;
@tailwind utilities;
```

새로 만든 `./src/index.css` 파일을 `./src/main.js` 파일에서 import한다.

```js
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "./index.css";

const app = createApp(App);

app.use(router);

app.mount("#app");
```

이제 .vue 파일 내에서 Tailwind를 사용할 수 있다. 테스트해 보자.

```html
<template>
  <header>
    . . .
    <div class="wrapper">
      <HelloWorld msg="You did it!" />
      <h1 class="text-3xl font-bold underline">Hello world!</h1>
      . . .
    </div>
  </header>
  . . .
</template>
```

`<Hello World>` 뒤에 `<h1>` 헤딩을 추가했고, 헤딩은 Tailwind 클래스를 사용하고 있다.

![vue-welcome](images/modern-app/vue-welcome.png)

### Vue 라우터
이번에는 프로젝트 디렉토리가 조금 다르다.

![vue-router](images/modern-app/vue-router.png)

`src` 디렉터리 안에는 `router`와 `views` 폴더가 있다. `router` 디렉토리에는 `index.js` 파일이 들어 있다. 여기서 다른 경로를 정의할 수 있다. 각 경로는 `views` 디렉토리 내의 view 구성요소를 가리키며, view는 `components` 디렉토리 내의 다른 구성요소로 확장될 수 있다. Vue는 이미 `index.js`의 예를 제공하고 있다.

```js
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
```

정의된 라우터를 호출하려면 `App.vue` 파일 내부를 확인한다. `<a>` 태그 대신 `vue-router` 패키지로 부터 import된 `<RouterLink>`를 사용한다.

```js
<script setup>
import { RouterLink, RouterView } from "vue-router";
. . .
</script>

<template>
  <header>
    . . .
    <div class="wrapper">
      . . .
      <nav>
        <RouterLink to="/">Home</RouterLink>
        <RouterLink to="/about">About</RouterLink>
      </nav>
    </div>
  </header>

  <RouterView />
</template>
```

페이지가 렌더링되면 `<RouterView />` 태그가 해당 뷰로 바뀐다. 이러한 구성 요소를 import하지 않으려면 대신 `<discovery-link to="">`와 `<discovery-view>` 태그를 사용하시오. 개인적으로, 저자는 항상 그것들을 가져오는 것을 잊어버리기 때문에 이 방법을 선호한다.

## Vue 라우터로 라우트 생성
블로그 애플리케이션을 위해서는 최소 6페이지를 작성해야 한다. 최근 페이지 목록을 표시하는 홈 페이지, 모든 카테고리/태그를 표시하는 카테고리/태그 페이지, 카테고리/태그에 속하는 게시물 목록을 표시하는 카테고리/태그 페이지, 마지막으로 게시물 내용과 댓글을 표시하는 포스트 페이지가 필요하다.

아래는 라우터이다. `@`는 `src` 디렉터리에 매핑된다.

```js
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/views/main/Home.vue";
import PostView from "@/views/main/Post.vue";
import CategoryView from "@/views/main/Category.vue";
import TagView from "@/views/main/Tag.vue";
import AllCategoriesView from "@/views/main/AllCategories.vue";
import AllTagsView from "@/views/main/AllTags.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/category",
    name: "Category",
    component: CategoryView,
  },
  {
    path: "/tag",
    name: "Tag",
    component: TagView,
  },
  {
    path: "/post",
    name: "Post",
    component: PostView,
  },
  {
    path: "/categories",
    name: "Categories",
    component: AllCategoriesView,
  },
  {
    path: "/tags",
    name: "Tags",
    component: AllTagsView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
```

이 포스팅에서는 프런트엔드 인터페이스만 만들고 있으며 아직 데이터 전송은 다루지 않고 있으므로 정확한 게시물/카테고리/태그를 찾는 방법에 대해 걱정하지 마시오.

## 뷰, 페이지와 구성 요소 생성
[이것](https://github.com/ericsdevblog/django-vue-starter-blog/tree/main/frontend/src/views)은 이 프로젝트를 위해 만든 프런트 엔드 UI이다. 여러분은 이 코드를 직접 사용하거나 마음에 들지 않으면 Vue.js에 대한 [초보를 위한 Vue.js](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)을 따라 여러분만의 UI를 만들 수 있다.

#### Home 페이지

![homepage](images/modern-app/homepage.webp)

#### All Categories

![all-categories](images/modern-app/all-categories.webp)

#### All Tags

![all-tags](images/modern-app/all-tags.webp)

#### Sign In 페이지

![signin](images/modern-app/signin.webp)

#### Sign Up 페이지

![signup](images/modern-app/signup.webp)

#### Post 페이지

![post](images/modern-app/post.webp)

#### Comment 섹션

![comments](images/modern-app/comments.webp)

#### User Profile 페이지

![profile](images/modern-app/profile.webp)

---
<a name="footnote_1">1</a>: 이 포스팅은 [Create a Modern Application with Django and Vue #1 ](https://www.ericsdevblog.com/posts/create-a-modern-application-with-django-and-vue-1/)를 편역한 것이다.
