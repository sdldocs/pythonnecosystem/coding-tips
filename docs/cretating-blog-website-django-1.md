이 글에서는 Django 프레임워크를 사용하여 모든 기능을 갖춘 블로그 웹사이트를 만들어 보도록 한다.

## 사전 요구 

1. Python 설치

    Python이 시스템에 설치되어 있지 않은 경우 [이 링크](https://www.python.org/downloads)로 이동하여 다운로드한 다음 설치할 수 있다.

2. PIP 설치
3. Git 설치
4. Xampp 설치
5. VS Code IDE

*이 글은 여러 파트로 이루어져 있다.*

1. git 리포지터리와 Django 프로젝트 생성 - Part 1
2. SQLite 데이터베이스를 MySQL 데이터베이스로 마이그레이션 - Part 2
3. Django 어플리케이션과 model 만들기 - Part 3
4. 게시물 목록을 디스플레이하는 view 만들기 - Part 4
5. 게시물을 상세하게 디스플레이하는 view 만들기 - Part 5
6. 게시물을 위한 검색엔진 친화적 URL 만들기 - Part 6
7. 게시물 목록 view에 페이지내이션(pagination) 매김 추가 - Part 7
8. 게시물에 댓글 시스템 만들기 - Part 8
9. 게시물에 태깅 기능 추가 - Part 9
10. 유사한 게시물 찾기 - Part 10
11. 블로그 웹사이트의 사이드바에 최신 게시물을 표시 - Part 11
12. 블로그 웹사이트의 사이드바에 댓글이 가장 많이 달린 게시물을 표시 - Part 12
13. 블로그 웹사이트에 사이트맵 추가 - Part 13
14. 블로그 웹사이트에 검색 기능 구현 - Part 14

**Django로 블로그 웹사이트 만들기 시작**

## GitHub에 리포지터리 생성
버전 관리가 필요없는 프로젝트에 경우 이 단계가 필요하지 않다. 블로그 앱의 버전 관리를 원하면 GitHub에 리포지토리를 만들도록 한다.

![](images/creating-blog-website-django/github_screen.webp)

빈 GitHub 리포지터리를 생성한다. 리포지터리를 복제할 때 필요하므로 리포지터리 URL의 사본을 보관해야 한다. 이제 이 리포지터리를 여러분의 로컬 시스템에 복제해야 한다.

## 로컬 시스템에 리포지터리 클론하기
프로젝트를 저장하려는 프로젝트 디렉토리로 이동한다. 여기서 프로젝트 디렉터리는 `~MyProject/`이다. 원하는 대로 선택할 수 있다. 그 후 아래 명령을 입력하고 `Enter`를 입력한다.

```
$ git clone "YOUR_GITHUB_REPOSITORY_URL"

$ git clone https://github.com/YoonJoon/blog_app.git
```

![](images/creating-blog-website-django/cli_01.png)

이제 blog_app 리포지터리가 로컬 시스템에 복제되었다. 프로젝트 디렉토리로 이동하여 복제된 리포지터리를 확인할 수 있다.

![](images/creating-blog-website-django/ls_01.png)

## 가상 환경 만들기

### 프로젝트 디렉토리로 이동하여 가상 환경 만들기

`E:\django-projects`에 리포지터리를 복제했으므로 명령 프롬프트를 열고 해당 디렉터리로 이동한다. 다음 가상 환경을 생성한다. 이 예에서 가상 환경 이름은 `my_blog_env`이다. 가상 환경 이름은 원하는 대로 설정할 수 있다.

```
$ cd ~MyProject/blog_app/

$ python3 -m venv my_blog_env
```

![](images/creating-blog-website-django/cli_02.png)

## 가상 환경 활성화
Windows 컴퓨터에서 작업 중이므로 아래와 같이 활성화 명령을 입력한다.

```
YOUR_VIRTUAL_ENV_NAME/bin/activate

$ source my_blog_env/bin/activate
```

![](images/creating-blog-website-django/cli_03.png)

이제 가상 환경이 활성화된 것을 볼 수 있다.

## 활성화된 환경에서 Django 설치
다음으로, 활성화된 가상 환경에 Django를 설치해야 한다. 이를 위해 다음 명령을 수행한다.

```bash
$ pip install Django
```

![](images/creating-blog-website-django/cli_04.png)

## 홯성화된 환경에 Django 프로젝트 생성

**`django-admin startproject name[directory]`**

현재 또는 지정된 디렉토리에 지정된 프로젝트 이름으로 Django 프로젝트 디렉토리 구조를 만든다.

기본적으로 새 디렉터리에는 `manage.py`와 프로젝트 패키지(`settings.py`와 기타 파일)를 포함하고 있다.

프로젝트 이름만 지정하면 프로젝트 디렉터리와 프로젝트 패키지의 이름이 모두 지정되고 프로젝트 디렉터리는 현재 작업 디렉터리에 생성된다.

선택 사항으로 대상 디렉터리를 지정하면 Django는 해당 디렉터리를 프로젝트 디렉터리로 사용하고 그 안에 `manage.py`와 프로젝트 패키지를 만든다. `.`를 사용하여 현재 작업 디렉터리를 나타낸다.

이 예에서는 `.`(현재 디렉터리)에 `my_blog`라는 이름의 Django 프로젝트를 만든다.

```bash
$ django-admin startproject my_blog .
```

![](images/creating-blog-website-django/cli_05.png)

위 명령어를 실행하면 현재 디렉터리에 `my_blog` 폴더(또는 디렉터리)가 생성되고 `manage.py` 파일도 생성된다.

![](images/creating-blog-website-django/cli_06.png)

![](images/creating-blog-website-django/cli_07.png)

**이 파일은**

**`manage.py`**: 이 Django 프로젝트와 다양한 방식으로 상호 작용할 수 있는 명령어 유틸리티이다. `manage.py`에 대한 자세한 내용은 `django-admin`과 `manage.py`에서 확인할 수 있다.

내부의 **`my_blog/`** 디렉터리는 프로젝트의 실제 Python 패키지이다. 이 디렉터리의 이름은 Python 패키지 이름이며, 그 안에 있는 모든 것을 가져올 때 사용해야 한다.

**`my_blog/__init__.py`**: 이 디렉터리를 Python 패키지로 간주해야 한다고 Python에 알려주는 빈 파일입니다.

**`my_blog/settings.py`**: 이 Django 프로젝트에 대한 설정 또는 구성 파일이다. Django `settings.py`는 설정이 어떻게 작동하는 지에 대한 모든 것을 알려준다.

**`my_blog/urls.py`**: 이 Django 프로젝트에 대한 URL 선언이다, 즉 Django 기반 사이트의 "목차"라 할 수 있다.

**`my_blog/asgi.py`**: 프로젝트에 서비스를 제공하기 위한 ASGI 호환 웹 서버의 진입점이다.

**`my_blog/wsgi.py`**: 프로젝트를 서비스하기 위한 WSGI 호환 웹 서버의 진입점이다.

## 초기 데이터베이스 이전(migrate)

```bash
$ python manage.py makemigrations

$ python manage.py migrate
```

![](images/creating-blog-website-django/cli_08.png)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

![](images/creating-blog-website-django/cli_09.png)

브라우저 firefox 또는 chrome을 열고 http://127.0.0.1:8000/ 을 입력하면 아래와 같은 환영 페이지가 출력된다.

![](images/creating-blog-website-django/screenshot_01.png)

## super user 생성
개발 서버를 중지하고 관리 사이트에 슈퍼 유저를 생성한다. 개발 서버를 중지하려면 `Ctrl+C`를 함께 누른다.

```bash
$ python manage.py createsuperuser
```

![](images/creating-blog-website-django/cli_10.png)

슈퍼 사용자 세부 정보는 다음과 같다.

이름: yjlee

비밀번호: xxxxxx

이제 슈퍼 유저가 생성되었으니 개발 서버를 다시 실행하여 관리자 부분을 확인해 보자.

## 개발 서버를 실행하여 admin 섹션 보기

```python
$ python manage.py runserver
```

개발 서버가 실행 중이며, 관리자 섹션을 보려면 아래 URL을 입력한다.

`http://127.0.0.1:8000/admin/`

![](images/creating-blog-website-django/screenshot_02.png)

로그인 후 화면은 아래와 같다.

![](images/creating-blog-website-django/screenshot_03.png)

개발 서버를 중지하고 가상 환경을 비활성화한다.

개발 서버를 중지하려면 `Ctrl+c`를 누른다. 가상 환경을 비활성화하려면 명령 프롬프트에 `deactivate`를 입력한다.

![](images/creating-blog-website-django/cli_11.png)

## git 리포지터리에 최신 파일 반영(push)

### 루트 폴더에 `.gitignore` 파일 만들기
루트 폴더에 `.gitignore` 파일을 추가한다. `.gitignore` 파일에 푸시하지 않으려는 폴더/파일 이름을 추가해야 한다. 가상 환경 폴더는 푸시하지 않아야 하므로 여기에 추가한다.

![](images/creating-blog-website-django/cli_12.png)

`.gitingore` 파일을 열고 그 안에 `my_blog_env/` 폴더를 포함시킨다.

### 파일을 github 리포지터리에 push하기
다음 아래 명령을 입력한다.

```bash
$ git add --all
$ git commit -m "virtual environment and django peoject created"
$ git push origin main
```

> **Notes**
>
> 아래와 같은 오류 발생하면,
>
> ![](images/creating-blog-website-django/cli_13.png)
> 
> [How to Fix “Support for password authentication was removed” error in GitHub](https://collabnix.com/how-to-fix-support-for-password-authentication-was-removed-error-in-github/)를 참고하여 해결
>

파일을 github 리포지토리에 처음 푸시할 때 아래와 같이 인증을 요청한다.

![](images/creating-blog-website-django/screenshot_04.webp)

github 계정에 로그인한다.

![](images/creating-blog-website-django/screenshot_05.webp)

인증에 성공하면 탭을 닫아야 한다.

![](images/creating-blog-website-django/screenshot_06.webp)

명령 프롬프트에서 메인 브랜치에 푸시된 파일을 확인할 수 있다.

또한 github 계정에 푸시된 최신 파일도 확인할 수 있다.

![](images/creating-blog-website-django/screenshot_07.webp)

Part 1은 여기까지 이다. 다음 파트에서는 SQLite를 MySQL 데이터베이스로 마이그레이션할 것이다.

**(주)** 위 내용은 [Creating a Blog Website in Django — Part 1](https://medium.com/@nutanbhogendrasharma/creating-a-blog-website-in-django-part-1-e7354bfd6684)을 편역한 것이다. 

**to do** screenshot_04~07까지 실제 screenshot으로 교체 필요하며 또한 cli_14~16도 추가되어야 함.

