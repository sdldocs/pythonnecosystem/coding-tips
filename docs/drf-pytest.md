### 누구를 위하여?

### 작성한 이유

## 왜 PyTest인가?

## `tests` 구조

## PyTest 설정

## 테스트 유형

## 테스트를 하는 이유

## 적합한 테스트 플로우

## 단위 테스트에서 무엇을 테스트해야 하는가?

## 공통 테스팅 유틸리티

### Markers

### Mocking

### PyTest 명령

### Addopts

#### Pinpoint 테스트

### Factories

## Test를 구성하는 방법

## Test 예

### E2E 테스트

### Utls

### Signals

### Serialzers


**주** 이 페이지는 [PyTest with Django REST Framework: From Zero to Hero](https://dev.to/sherlockcodes/pytest-with-django-rest-framework-from-zero-to-hero-8c4)을 편역한 것임.