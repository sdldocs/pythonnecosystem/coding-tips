많은 Python 개발자들이 여러 가지 이유로 클래스에 대하여 어려움을 겪고 있다. 첫째, 저자의 생각에는 객체 지향 프로그래밍의 개념이 항상 명확한 것은 아니기 때문이다. 둘째로, 클래스와 객체 지향 프로그래밍(OOP)에 대한 아이디어가 많고 우리가 여기저기서 찾을 수 있는 설명(주로 온라인)이 피상적일 수 있기 때문이다.

이 글에서, Python 클래스 뒤에 있는 가장 중요한 개념과 그것을 사용하는 방법(코딩 예제)을 다루고 싶다.

그러나 먼저, 우리는 객체 지향 프로그래밍에 대해 논의하면서 시작할 것이다.

## 객체 지향 프로그래밍
[Python Object-Oriented Programming: Build robust and maintainable object-oriented Python applications and libraries](https://www.amazon.com/Python-Object-Oriented-Programming-maintainable-object-oriented/dp/1801077266)을 인용하고 다르게 표현한다면, 인간으로서 우리는 객체가 무엇인지 완벽하게 알고 있다고 말할 수 있다. 그것들은 우리의 감각으로 느낄 수 있고 조작될 수 있는 모든 것이다. 성장하는 동안, 우리는 추상화의 힘을 배워 객체는 모델이 된다. 다시 말해서, 우리는 수년간의 경험을 통해 개발된 상상력을 사용하여 간단한 개념을 사용하여 복잡한 문제를 단순화합니다. 이것이 모델을 사용하는 추상화 과정이다.

예를 들어, 우리는 지구를 (구가 아니더라도!) 구체로 모델링하여 (예를 들어 궤도를 결정하기 위해) 몇 가지 계산을 수행할 수 있다.

이는 우리가 소프트웨어 개발에서 하는 것과 크게 다르지 않다. 사실, 우리 개발자들에게, 객체는 우리가 만지거나 느낄 수 있는 것이 아니라, 그것은 어떤 소프트웨어 작업을 수행하는 것의 모델이다. 더 정확히 말하면, 우리는 "**객체는 관련된 동작이 포함된 데이터의 집합**"이라고 할 수 있다.

개념을 명확히 하기 위해 예를 들어 보자. 우리는 "Apple"이라는 객체를 만들고 싶다. 이 경우 관련 데이터는 색상(예: 빨간색)일 수 있다. 그것과 관련된 동작은 우리가 사과를 바구니에 넣고 싶어한다는 것일 수 있다.

![](images/python-oop/fig-01.webp)

이제 만약 우리가 녹색 사과를 만나게 된다면 어떨까? 그것은 여전히 사과이지만, 다른 색깔을 가지고 있다: 우리가 이 개념을 사용할 수 있을까? 여기서 클래스가 도움이 된다!

클래스는 객체의 구조를 정의하는 "템플릿"이다. 이는 객체가 특정 클래스에 대해 고유한 속성 집합을 가지고 있음을 의미한다.

사과의 예를 들면, 사과가 가질 수 있는 클래스는 다음과 같다.

-  데이터 -> 색상
-  동작 -> 사과를 넣는 장소

이 템플릿을 사용하면 객체 원하는 만큼 만들 수 있다. 예를 들어, 우리는 `color=green` 데이터로 클래스 `Apple`을 사용하여 테이블 위의 바구니에 있는 사과를 놓는 `Apple_green` 객체를 만들 수 있다.

우리는 또한 `color=red`데이터로 클래스 `Apple`을 사용하여 사과를 다시 선반에 놓는 `Apple_red`라는 객체를 가질 수 있다.

![](images/python-oop/fig-02.webp)

사과의 예를 통해 이해할 수 있듯이, 우리는 데이터가 특정 물체의 특정한 특성을 나타낸다고 말할 수 있다.
반면 동작은 행동을 나타낸다. 이러한 행동은 Python에서 클래스의 메소드로 표현된다. 그리고, 우리가 나중에 보게 될 것처럼, 클래스의 메소드는 우리가 함수를 정의하는 것과 같은 방식으로 정의된다.

객체 지향 프로그래밍이란 무엇인가?

OOP는 "모델링 객체를 향해 코드를 작성하는 프로그래밍 방법"이다.
즉, 데이터와 동작을 통해 상호 작용하는 일련의 객체를 만드는 것을 의미한다.

이것이 우리가 Python에서 프로그래밍할 때 항상 클래스를 만들어야 한다는 것을 의미하지는 않는다. 여기서 우리는 간단한 개념을 이해해야 한다. Python의 힘은 이 프로그래밍 언어의 모든 것이 객체라는 사실에 기인한다. 사실 우리가 볼 수 없어도 Python에서는 후드 아래에 가려져 있는 객체와 클래스로 작업을 하고 있다.

예를 들어 변수 `a = 3`을 정의한다고 가정하자. 만약 우리가 그것의 타입을 보고 싶다면 우리는 다음과 같은 코드를 작성할 수 있다.

```python
a = 3

type(a)

>>>

  int
```

우리는 변수 `a`의 속성을 출력하기 위해 내장 함수 `type()`을 사용하여 우리에게 `int`(즉, 정수)임을 알려주었다. 그러나 `int`는 Python 내장(built-in) 클래스이다. 이는 Python에서 실제 클래스를 만들지 않고 정수 변수를 정의할 수 있음을 의미한다.

우리가 Python으로 프로그래밍을 할 때마다, 우리는 객체와 클래스를 명시적으로 선언하지 않더라도 항상 처리해야 한다. 왜냐하면 Python은 후드 아래에서 내장 클래스를 호출하는 작업을 하기 때문이다.

## Python에서 클래스
Python에서는 클래스를 대문자로 정의해야 한다. 단순 클래스를 다음과 같이 정의할 수 있다.

```python
class MyFirstClass:
  a = 15 # this is the data of the class
  pass
```

`a=15`를 설정하는 간단한 클래스를 만들었다.

이제 메소드를 추가하여 실제로 유용한 기능을 수행하는 클래스를 만들어 보자.

```python
class Point:

  def origin(self): # this is a method 
    self.x = 0
    self.y = 0
```

그래서 우리는 `origin`이라고 하는 메소드로 `Point`라는 클래스를 만들었다. 이는 2D에서 `x=0`과 `y=0`으로 점(원점)을 얻는 간단한 일을 한다.

이제, 우리가 볼 수 있듯이, 그 메소드는 `self`라는 하나의 인수를 보낸다. 우리가 원하는 대로 이를 호출할 수 있지만, Python 개발자 커뮤니티에서는 표준으로 이를 `self`라 부릅니다. 어떻게 작동하는지에 대한 자세한 내용은 나중에 설명하겠다.

이제 클래스를 호출하려면 점 `p`를 정의하고 다음을 수행한다.

```python
  # Invoke the class
  p = Point()
```

이제 다음과 같이 작성하여 클래스에 정의된 메서드를 액세스할 수 있다.

```python
  # Access the method
  p.origin()
```

이제 우리의 욧점을 원점으로 돌, 확실하게 다음과 같이 수행할 수 있다.

```python
  # Print the coordinates
  print(p.x, p.y)
```

```
>>>

  0 0
```

### `self` 인수
이제 앞에서 소개한 'self' 인수가 무엇인지, 그리고 그것을 어떻게 사용하는지 설명해 보겠다.

`self` 인수는 메소드를 호출하는 역할을 수행 한다. 그리하여 우리가 `self`를 표준이라고 부르는 이유인 것이다. 왜냐하면 우리가 그 메소드를 호충하기 때문이다. 우리가 Python에게 "헤이! 이 메서드는 스스로 호출되"라고 알려주는 것과 같다. 

메소드에 여러 개의 인수를 전달해야 할 때는 `self`가 첫 번째 인수여야 한다. 그렇지 않으면 다른 매개 변수로 메서드를 호출한다(앛서 말했듯이, `self`를 부를 필요가 없다. 이 이름은 개발자 간의 컨벤션일 뿐이다).

`self`를 사용하는 예를 보고 그것을 사용하지 않는 다른 예와 비교해 보자.

```python
class Test:

  def printing(self):
    print("this is a test")
```

그래서 우리는 메소드 `printing`을 실행할 때 "this is a testing"라고 인쇄하는 간단한 클래스를 만들었다.

```python
  # Invoke the class
  test = Test()

  # Call the method
  test.printing()
```

```
>>>

  this is a test
```

이제 `self`가 없는 예를 보자.

```python
class Test2:

  def printing2():
    print("this is a test")
```

```python
  # Invoke the class
  test2 = Test2()

  # Call the method
  test2.printing2()
```

```
>>>

  Type Error: printing2() takes 0 positional arguments but 1 was given
```

이 경우, Python은 간단한 것을 알려주는 오류를 반환한다. 메소드에 인수가 없기 때문에 메서드를 호출할 수 없다.
이것이 우리가 `self`를 필요로 하는 이유이다.

이제 여러 개의 인수가 있는 함수를 만들어 보자.

```python
class Point:

  def move(self, x, y):
    self.x = x
    self.y = y
  
  def origin(self):
    self.move(0,0)
    return(self.x, self.y)
```

다음과 같은 클래스를 만든다.

1. 두 좌표(`x`과 `y`)로 이동한다. 볼 수 있듯이, `self`는 메소드를 호출한다.
2. 원점에 대한 좌표를 가져온다. `origin` 메소드를 호출하면 실제로 두 좌표(인수는 `self` 뿐임)를 원점(`self.move(0, 0)`)으로 모두 이동하고 이 좌표를 반환한다.

따라서 클래스를 호출하고 좌표를 원점으로 이동할 수 있다.

```python
  #Invoke the class
  p = Point

  # Call the method
  p.origin()
```

```
>>>

  0, 0
```

그리고 좌표는 원점이다.

이제 몇 줄을 다시 보면, `move` 메소드가 좌표를 이동한다고 말했지만, 이는 사실이 아니고, 메소드 `origin`이 실제 이동을 수행하였다.
실제로 `move` 메소드는 필요한 값을 호출하는 간단한 작업만 수행한다. 이것이 클래스의 첫 번째 방법이 특별한 명명법을 갖는 이유인 것이다.

### `__init__` 메소드
지금까지 점(`x`와 `y`)의 좌표를 원점으로 이동하는 클래스를 만들었다. 하지만 우리가 원하는 특정 위치로 포인트를 이동하려면 어떻게 해야 할까?

그러기 위해서 우리는 다른 클래스를 만들어야 한다. 첫 번째 시도에서는 이전에 사용한 것과 동일한 논리로 생성해 보겠다. 다음과 같이 될 수 있다.

```python
class Point:

    def move(self, x, y):
        self.x = x
        self.y = y
  
    def position(self):
        print(self.x, self.y)
```

이제 클래스와 메소드를 호출하여 보자.

```python
# Invoke the class
p = Point() 

# Get to position
p.position(5,6)

>>>

  TypeError: Point() takes no arguments
```

오류가 발생했다! 그리고 그것은 우리에게 `Point` 클래스가 인수를 갖지 않음을 알려준다. 어떻게 그것이 가능한가? `self`, `x`, `y`를 전달하도록 지정했다. 따라서 메소드를 호출하는 `self`를 제외하고 클래스는 두 개의 값을 갖어야 한다!

첫 번째 메소드에 오류가 있습니다. 앞에서 언급했듯이 첫 번째 메소드은 필요한 값을 호출하는 한 가지만 수행한다. 점이 특정 위치로 이동하지 않는다.

이것이 클래스의 첫 번째 메서드가 `__init__`라는 특정 메서드여야 하는 이유이다. 따라서 필요한 값을 올바르게 호출하려면 다음과 같이 `Point` 클래스를 만들어야 한다.

```python
class Point2:

    def __init__(self, x, y):
        self.x = x
        self.y = y
  
    def position2(self):
        print(self.x, self.y)
```

이제 호출해 보자.

```python
  # Invoke the class
  p = Point2(5,6) 

  # Get to position
  p.position2()
```

```
>>>

  5 6
```

위와 같은 결과를 얻을 수 있다.

> **Note**
>
> 심지어 __init__ 메서드를 사용하지 않는 클래스를 만들 수 있는 가능성도 있다. 이러한 경우 변수는 다른 방법으로 호출될 수 있다.
>
> 어쨌든: 우리는 여기서 다른 방법에 대해 설명하지 않을 것이다. 왜냐하면 우리는 이것은
Python 소프트웨어를 개발하기 위한 Python 방식이 아니기 때문이다. (또한, 조금 더 길고 덜 읽기 쉽지 않은 코드가 된다.)

### `If __name__ == "__main__"`
이제, 일부 클래스의 끝에 `if __name__` == `"__main__"`가 나타나는 경우가 있다. 만약 그것이 무엇인지 모른다면, 여기에 그에 대한 설명이 있다.

> `__name__`은 우리가 가져온(import) 모듈과 그렇지 않은 모듈을 구별할 수 있게 해주기 때문에 상호 작용 가능한 모듈을 만들 때 사용해야 하는 특별한 내장 변수이다.
>
> Python에서는 모듈별은 패키지와 라이브러리를 뜻하지만, 다른 코드와 분리 가능하고 자체적으로 작동할 수 있는 코드 도 으미한다. 클래스(함수 또는 Python 파일도 포함)도 자체적으로 작동할 수 있다는 점을 고려하면 모듈로 간주할 수 있다.
>
> 그러나 `__main__`은 스크립트나 프로그램에서 최상위 코드의 이름을 나타내는 특수 모듈이다.
> 
> 따라서 기본적으로 `if __name__ = "__main__"`를 실행하면 실행 중인 코드가 최상위 수준인지를 확인한다.

그렇다면, 최상위 코드는 무엇일까?

StackOverflow의 이 스레드에서 매우 잘 설명된 예를 살펴보자.

```python
import sys         # top-level

3 + 4              # top-level

x = 0              # top-level


def f():           # top-level
    import os      # not top-level!
    return 3       # not top-level

if x:              # top-level
    print 3        # not top-level
else:
    print 4        # not top-level, but executes as part of an if statement
                   # that is top-level


class TopLevel(object): # top-level
    x = 3          # not top-level, but executes as part of the class statement
    def foo(self): # not top-level, but executes as part of the class statement
        print 5    # not top-level
```

보다시피, 이 문제는 파이썬 인터프리터와 관련이 있다. 설명을 해 보면,

모듈이 자체적으로 실행되면 문제가 없다. **모듈을 다른 Python 파일로 가져올 때** 문제가 발생한다. 위의 스크립트를 다른 파일로 가져온다고 가정하면, 위의 코드의 주석에서 알 수 있듯이, 우리가 스크립트를 가져올 때, 그것은 모든 최상위 코드와 `if` 또는 클래스 문에 포함된 모든 코드를 즉시 실행한다.

예를 들어, 좀 더 자세히 살펴보면, 다음과 같이 `package_1.py`라는 Python 파일을 생성한다.

```python
# Define a function to print "Hello"
def print_hello():

    print("hello")


#Invoke the function
print_hello()
```

터미널에서 이를 실행하면 다음과 같은 출력을 얻을 수 있다.

```bash
$ python package_1.py 
hello
$
```

완벽하게 작동할 것이다.

이제 `main.py`라는 Python 파일을 생성하고 `package_1.py` 파일을 가져온 다음 `main.py`를 실행하여 보자.

```bash
$ python main.py 
hello
$
```

보다시피, `main.py`가 실행되면 모듈 `package_1.py`가 즉시 실행된다! 이것은 단순한 이유 때문에 우리가 원하지 않는 것이다. 우리는 실제로 코드를 호출할 때 모듈에서 가져온 코드를 사용하기를 원한다. 즉, 전체 모듈이 코드를 가져온 직후에 코드를 실행하면 전혀 유용하지 않다.

이를 방지하기 위해 `package_1.py`에서 `if __name__ = "__main__"`을 사용한 다음 `print_hello()` 함수를 호출한다.

```bash
$ python main.py 
$
```

> `if __name__ = "__main__"`은 모듈이 실행될 때 최상위 코드의 자동 실행을 방지 한다.

또한 이전에 `package_1.py` 파일에서 `print_hello()` 함수를 호출한 적이 있는데, 이 함수가 프로그램을 실행할 때 실제로 작동하기를 원했기 때문이다. 이제 `if __name__ = "__main__"` 뒤에 `print_hello`를 호출하면 `package_1.py`가 자체적으로 실행될 때 함수가 호출된다.

따라서 이 예의 사용을 정리하고 명확히 하면, `package_1.py`에서 `if __name__ = "_main__"` 다음 `print_hello`를 호출한다면 

- `package_1.py`이 자체적으로 실행될 때(즉, 터미널을 통해) `print_hello()`가 실행된다.
- `main.py`를 실행할 때 `print_hello()`는 실행되지 않는다.

> **NOTE:**
>
> 우리는 간단한 예들을 사용하여 것을 이해할 수 있었지만, 일반적으로 클래스를 기반으로 모듈을 만든다. 따라서 이 글에서 이 주제를 다룬 이유이다.

## 타입 힌트
Python 3부터 "[PEP 484 - 타입 힌트](https://peps.python.org/pep-0484/)"는 Python에 타입 힌트를 도입했다.

타입을 표시하는 것은 함수(또는 클래스의 경우 메서드)에 전달할 타입을 제안하는 것을 의미한다. 우리 주석과 docstring이 그 역할을 수행해야 한다는 것을 알고 있지만, 힌트를 입력하는 것은 기능에서 무엇을 기대해야 하는지 이해하는 데 도움이 된다.

> **NOTE:**
>
> 여기서 클래스가 아닌 단순함을 위하여 함수에 대해 다룰 것이다. 본 바와 같이, 사실, 클래스의 메소드는 정확히 함수로 즉 클래스에 일반화 된 것으로 정의된다. 

간단한 예를 들어 보겠다. 값을 반환하는 함수를 생성한다.

```python
def any_call(variable: any) -> any:
    return variable
```

그래서 하나의 인수(`variable`)를 전달하는 함수를 만들고 타입 힌트는 우리에게 다음과 같이 알려준다.

- `variable`의 타입은 임의일 수 있다. 문자열, 정수 등...
- 함수는 임의의 타입을 반환한다 (실제로는 `variale`을 반환한다)

유용하지 않은가? 흠, 저는 몇 주 전에 그것을 발견했고 그것이 놀랍다는 것을 발견했다! 특히, 이것은 더 복잡한 예를 들수록 흥미로워진다.

예를 들어, 인수로 리스트를 전달받고 리스트에 있는 요소의 갯수를 반환하는 함수를 원한다고 가정하다. 그 함수는 다음과 같다.

```python
def count_values(element: list[any]) -> int:
    return sum(1 for elements in element if elements)
```

```
count_values([1,2,3,4])

 >>>

     4
```

```
count_values(["hello", "stranger", "who", "are", "you", "?"])

>>>

   6
```

여기서 함수는 `element`를 유일한 인수로 전달한다. 그리고 그것이 타입 힌트 덕분에 임의 타입의 리스트일 수 있음을 알고 있다. 그런 다음 `int` 타입으로 반환한다. 실제로는 리스트의 요소 수를 계산한다.

그래서, 우리는 요점이 분명하다고 생각한다. 이것은 우리 코드의 가독성을 향상시키기 위한 매우 좋은 방법이다.

## Docstrings (과 호출 방법)
문서화는 단순한 이유로 모든 소프트웨어 프로젝트에서 가장 중요한 부분이다. 두 달 이후 우리는 한 일과 그 이유를 거의 기억하지 못할 것이다.

이러한 이유로, 우리의 코드에 대한 메모와 설명을 쓰는 것은 매우 중요하다. 유감스럽게도 코멘트는 매우 짧아야 하기 때문에 충분하지 않다.

그래서 우리가 할 수 있는 것은 docstring을 사용하는 것이며, [PEP 257](https://peps.python.org/pep-0257/)에서 다음을 지정하고 있다.

> docstring은 모듈, 함수, 클래스 또는 메서드 정의의 첫 번째 문으로 나타나는 문자열 리터럴이다. 이러한 docstring은 해당 객체의 특수 속성 `__doc__` 이 된다.
> 
> 모든 모듈에는 일반적으로 docstring이 있어야 하며 모듈에서 내보내는(export) 모든 함수와 클래스에도 docstring이 있어야 한다. `__init__` 생성자를 포함한 공용(public) 메서드에도 docstring이 있어야 한다. 패키지는 패키지 디렉터리에 있는 `__init__.py` 파일에 docstring으로 모듈을 문서화할 수 있다.

즉, 소프트웨어 개발에서 docstring은 코드 자체를 문서화하는 코드의 문자열 타입이다. 주석과 달리, docstring은 런타임에 사용할 수 있으므로 코드 검사를 단순화하고 실행 중에 도움말 또는 메타데이터를 제공할 수 있다.

Python 클래스에서 docstring을 사용하는 방법을 살펴보자.

```python
class Point:
    """
    this class moves a point in 2D, passing its coordinates
    as argument of the method 'position'
    """
    def __init__(self, x: int, y: int) -> None: 
        self.x = x
        self.y = y
  
    def position(self) -> int: 
        print(self.x, self.y)
```

문서(docstring)를 액세스하려면 다음과 같이 입력한다.

```python
Point.__doc__
```

```
>>>
  
    " this class moves a point in 2D, passing its coordinates as
    argument of the function 'position' "
```

이렇게 클래스와 관련된 docstring을 액세스할 수 있다. 어떻게 클래스의 특정 메서드의 docstring을 액세스할 수 있을까? 예를 들면,

```python
class Point:
    """ this class moves a point in 2D, passing its coordinates
    as argument of the function 'position'
    """
    def __init__(self, x: int, y: int) -> None: 
        self.x = x
        self.y = y
  
    def position(self) -> int: 
        """ this actually moves the point"""
        print(self.x, self.y)
```

`position` 메소드의 문서를 액세스하려면 다음과 같이 한다.

```python
Point.position.__doc__
```

```
>>>
    'this actually moves the point'
```

여기까지, 그런데 우리는 실제로 더 잘할 수 있다. 

```python
class Point:
    """ this class moves a point in 2D, passing its coordinates
    as argument of the function 'position'

    Args:
        param 1 (int): the coordinate x of the point in 2D
        param 2 (int): the coordinate y of the point in 2D
    """
    def __init__(self, x: int, y: int) -> None: 
        self.x = x
        self.y = y

     def position(self) -> int: 
        """ This method actually moves the point.
      
      Returns:
        int: prints the integers that represent the coordinates x and y
        of the point in 2D
      """
        print(self.x, self.y)
```

타입 힌트를 사용했으므로 중복된 것처럼 보일 수 있지만 다음과 같은 이유로 그렇지 않다.

1. 첫 번째 시도에서 클래스의 문서를 호출해야 할 수도 있다(다른 파일의 모듈로 가져왔기 때문일 수도 있다).
2. HTML로 문서를 작성하기 위해  [Sphinx](https://www.sphinx-doc.org/en/master/)와 같은 도구를 사용할 수 있으며 이러한 도구는 직접 docstring을 사용한다.

이는 잘 설명된 docstring을 사용하는 것이 매우 중요한 이유이다.

## 상속
여기서 우리는 Python의 마법 상속에 대해 이야기하고자 한다.

우리가 전에 말했듯이, OOP는 객체들 사이에서 상호작용하는 코드를 개발하는 방법론이다. 이는 Python에서 클래스를 만들 때 종종 상호 작용하도록 만들어야 한다는 것을 의미한다.

상호 작용할 때 클래스는 다른 클래스의 속성과 함수를 상속한다. 그것은 마치 당신의 연세가 많으신 이모가 210억원을 당신에게 남겨주기로 결정한 것과 같다.

먼저 우리가 만드는 모든 클래스는 상속을 사용한다고 말해야 한다. 이는 앞서 말했듯이 Python이 내장 클래스를 사용하기 때문이다. 따라서 내장 클래스를 호출할 때 코드는 호출된 내장 클래스 또는 객체의 속성을 상속한다.

이제 실질적인 예를 들어 상속이 실행되는 것을 살펴보겠다. 일부 연락처의 이름과 성을 저장하여 빈 리스트을 채우는 클래스를 만들고자 한다. 우리는 다음과 같이 만들 수 있다.

```python
class Contact:
    """
    This class saves the name and the surnames
    of some contacts in a list
    """

    # Create empty list
    all_contacts = [] 
        
    def __init__(self, name: str, surname: str) -> None:
        """ This method initializes the arguments and appends
        the arguments (name and surname)into the empty list.
        
        Returns:
            nothing.
        """
        self.name = name
        self.surname = surname
        Contact.all_contacts.append(self) # Append to list
        
    def __repr__(self) -> str:
        """
        The built-in __repr__ method provides a string representation
        of an object.
        
        Returns:
            the compiled list with name and surname of the contacts
        """
        return (
            f"{self.__class__.__name__}("
        f"{self.name!r}, {self.surname!r}"
            f")"
         )
```

시도해 보자.

```python
# Define a contact
contact_1 = Contact("Federico", "Trotta")

# Show the contacts in the list
Contact.all_contacts
```

```
>>>
  
  [Contact('Federico', 'Trotta')]
```

이제 연락처의 전자 메일을 수집하지만 어떤 이유로든 별도의 클래스를 만들고 싶다고 가정해 보겠다. 다음과 같은 클래스를 만들 수 있다.

```python
class Email(Contact): # Email is inerithing from Contact

    def get_mail(self, mail:"mail") -> None:
        return mail
```

다음과 같은 이메일을 추가할 수 있다.

```python
Email("federico trotta", "federico@example.com")
```

`Contact.all_contacts`를 호출하면,

```python
[Email('federico trotta', 'federico@example.com'), Contact('Federico', 'Trotta')]
```

따라서 `Email` 클래스는 `Contact` 클래스를 인수로 사용하므로 **`Contact` 클래스의 속성을 상속**한다. 예를 들어 `Email` 클래스에 전달된 인수에 클래스 `Contact`를 추가한다.

또한 이 클래스는 `__init()__` 메서드에 두 개의 인수가 필요하다는 사실을 상속받니다. 사실, 이것은 단 하나의 인수로 얻을 수 있는 것이다.

```python
mail_2 = Email("Jhon@example.com")
```

```
>>>
  
  TypeError: __init__() missing 1 required positional argument: 'surname'
```

잠깐!!! 우리는 이메일 클래스에서 `__init()__` 메서드를 정의하지 않았는데, 어떤 일인가?

`Email` 클래스가 `__init()__` 메서드를 상속받았으므로 다시 정의할 필요가 없다!

하위 클래스(`Email`은 하위 클래스)에서 다른 `__init()__` 메서드를 사용하려면 다음과 같이 조정해야 한다.

```python
class General(Contact):

    def __init__(self, name: str, email: str) -> None:
        super().__init__(name, email)
        self.email = email
```

그리고 이전 것과 똑같이 작동한다.

```python
# Create a contact
general_contact = General("Federico Trotta", "federico@example.com")

# Write the contact into the list
Contact.all_contacts
```

```
>>>

  [General('Federico Trotta', 'federico@example.com')
```

따라서 `Email`과 `General` 클래스는 정확하게 동일한 방식으로 작동하며 동일한 결과를 제공하지만 상속의 힘은 `Email`에서 사용한 것처럼 `__init()__` 메소드를 사용하지 않으면 코드가 단순화된다는 것을 보이고 있다.

또한, 위에서 볼 수 있듯이, `General` 클래스의 경우, 우리는 `super.__init()__` 메소드를 사용하였다. 부모 클래스(`Contact`)로부터 상속된 속성을 초기화해야 하므로 자식 클래스의 경우 반드시 사용해야 한다.

## Python 클래스 사용 팁
만약 여러분이 클래스을 이해하는 데 어려움을 겪었기 때문에 이 글을 접했다면, 여러분은 왜 클래스 사용해야 하는 지에 대해 명확하게 알지 못했을 수 있다. 그렇다면 클럽에 오신 것을 환영한다. 필자도 같은 어려움을 겪었다.

제 말은, 클래스(그리고 물론, 함수)가 우리의 코드를 자동화하는 데 도움이 된다는 것을 이해했지만, 우리가 그것들을 호출해야 한다는 사실이 저에게 어려움을 주었다.

제가 데이터 사이언스용 Python을 공부하기 시작했기 때문에 이런 일이 일어났다. (그리고 여기 제대로 공부하는 방법에 대한 팁이 있다.) 사실, 많은 경우, 우리가 데이터 사이언스을 위하여 Python을 사용할 때 클래스를 사용할 이유가 없다.

그래서 클래스의 필요성을 제대로 이해하기 위한 나의 조언은 클래스를 모듈로 다루라는 것이다. 이는 당신이 새로운 Python 프로젝트를 시작하고 싶을 때, 당신이 필요한 모든 클래스를 호출하는 `main.py` 파일을 만드는 것이 매우 좋은 조언이라는 것을 의미한다. 어쨌든, 이러한 클래스는 `main.py`에 만들어져서는 안 된다. 그것들은 별도의 Python 파일에 생성되어야 하며(일반적으로, 우리는 각 클래스에 대해 하나의 Python 파일을 생성해야 한다), 그리고 우리가 그것들을 사용하는 `main.py`로 가져와야 한다.

지금까지 모듈에 대해 이야기했다. 다음은 이 주제를 다루기 위해 작성한 글이다.

- [모듈러 프로그래밍 마스터하기 - 당신의 Python 스킬을 향상시키는 방법](mastering-modular-programming.md)

## 마치며
이 들에서, 클래스에 대한 포괄적인 가이드를 보았고, 그것에 대한 주요 주제를 명확하게 하기를 바란다.

이제 여러분이 해야 할 일은 이 가이드가 필요할 때 도움이 되기를 바라며 클래스로 연습을 많이 하는 것이다.


**(주)** 위 내용은 [Python Classes Made Easy: The Definitive Guide to Object-Oriented Programming](https://towardsdatascience.com/python-classes-made-easy-the-definitive-guide-to-object-oriented-programming-881ed609fb6)을 편역한 것이다. 