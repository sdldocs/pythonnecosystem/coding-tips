**PyTest를 사용한 Python 단위 테스트**

테스트를 통해 코드가 여러분이 바라는 대로 작동하는지 확인할 수 있다. 단위 테스트는 시스템 단위인 코드 조각이 바르게 작동하는지 확인하는 소프트웨어 테스트 방법이다. 아무리 작은 코드라도 테스트해야 하며 테스트 없이 코드를 릴리스하지 않는 것이 바람직하다.

프로그램의 테스트 프로세스는 다양한 수준으로 구성된다. 각 수준에서 고유한 역할을 수행한다.

![테스트 Pyramid](images/unittest/test-pyramid.webp)

가장 기본적인 수준은 단위 테스트이다. 위에서 언급한 것처럼 모든 단위를 테스트한다. 이 단위는 메소드 또는 클래스 등이 될 수 있다. 더 높은 수준에서는 여러 컴포넌트을 함께 테스트한다. 여기서는 통합된 단위의 상호 운용성을 테스트하는 것이다. 최상위에서는 UI 테스트(시스템 테스트 또는 엔드 투 엔드 테스트라고도 함)를 수행한다. 이 수준에서 전체 시스템의 사용 사례를 테스트하는 것이다. 여기서 테스트하는 것은 사용 사례를 충족하는지 여부인 것이다. 이러한 단계 이후에는 사용자 수용 테스트 단계가 있다. 이 부분은 사용자가 수행한다.

하위 수준에서 더 상세하고 더 많은 테스트를 수행할수록 상위 수준에서 테스트 강도는 낮아진다. 즉, 하위 레벨에서의 성공은 상위의 작업을 용이하게 한다.

낮은 수준에서는 테스트가 더 빠르다 (더 빨라야 함). 하위 수준에서는 테스트 수가 많기 때문에 이러한 테스트가 느리다면 일이 밀릴 수 있다. 상위로 갈수록 테스트 횟수는 감소하지만 지속 시간은 증가한다. 이와 동시에, 더 높은 수준의 테스트 비용은 더 높아진다.

하위 수준의 테스트는 버그를 방지하기 위한 것이다. 즉 하위 테스트는 버그를 탐지한다.

**단위(unit)가 무엇인가?** 객체 지향 프로그래밍의 관점에서, 단위는 클래스이다. 함수형 프로그래밍의 경우 **함수**이다. 클래스에 여러 메서드를 정의할 수 있다. OOP의 경우, 클래스를 단위로 할지 아니면 클래스의 각각 메서드를 단위로 할지는 실제로 여러분의 결정에 달려 있다. 모두 가능하다.

잘 작성된 테스트는 코드의 해부학적 구조를 명확하게 보여준다.

> "*단위 테스트를 작성하는 행위는 검증(verification)이라기보다는 설계 행위에 가깝다. 또한 검증이라기보다는 문서화 행위이다. 단위 테스트를 작성하는 행위는 놀라운 수의 피드백 루프로 이루어지는며, 그 중 가장 적은 루프는 함수 검증과 관련된 루프이다.*" Robert Martin.

- **테스트 커버리지**는 프로그램이 테스트된 정도를 나타낸다. 물론, 이 비율이 높기를 바란다. 낮은 비율은은 품질이 나쁘다는 것을 의미하지만 높은 비율이 모든 것이 순조롭다는 것을 직접적으로 의미하지 않는다.
- **SUT**: 시스템을 테스트 중입니다. 시험 중인 클래스가 SUT이다.
- **DOC**: 구성 요소에 대한 종속성이다. 테스트할 구성 요소가 다른 구성 요소(의존성(dependency))에 종속됨.
- **모킹(mocking)**: 테스트하는 구성 요소를 종속성으로부터 분리해야 한다. 즉, SUT에서 DOC들을 제거해야 한다. 이렇게 하면 버그는 SUT에만 속한다.

![mocking](images/unittest/mocking.webp)

종속성 대신에, 그것을 모방하도록 한다.

- **테스트 사례(Test Case)**: 사례를 테스트
- **테스트 수이트(Test Suite)**: 여러 테스트 사례들의 모음
- **테스트 메소드와 클래스**: 테스트 사례에는 테스트 메소드가 필요한 경우가 종종 있으며, 테스트 수이트에는 테스트 클래스가 필요한 경우가 종종 있다. 
- **AAA**: 준비(Arrange), 행동(Act)와 어썰트(Assert)이며, 단위 테스트의 단계이다. 준비: 테스트를 준비하여 테스트 실행 준비 상태로 만들며 그리고 실행을 설정한다. Act: 테스트를 실행한다. Assert: 결과를 확인한다.

![AAA](images/unittest/aaa.gif)

- **화이트 박스(White Box)와 블랙 박스(Black Box)**: 단위 테스트는 화이트 박스 테스트이다. 내부 구조가 보인다. 반대로, 시스템 테스트는 블랙박스 테스트이다. 시스템의 내부 구조는 보이지 않으며, 시스템을 API를 통해 테스트한다.
- **회귀 테스트(Regression Test)**: 코드를 변경하면 시스템에 오류가 있는지 여부를 검증하는 테스트이다.

## PyTest
PyTest는 Python의 단위 테스트 프레임워크이다. Python에 사용할 수 있는 다양한 단위 테스트 라이브러리가 있다. 예를 들어 unittest는 내장 라이브러리이다. 그러나 PyTest는 많은 기능으로 인해 가장 인기 있는 단위 테스트 라이브러리 중 하나이다.

```bash
pip install pytest
```

`pytest -h` 명령을 입력하면 사용할 수 있는 모든 메소드을 나열한다.

![pytest_help](images/unittest/pytest_help.png)

간단하게 시작하겠다. 다음의 코드를 테스트하려고 한다.

```python
def foo(a:int, b:int = 1) -> int:
    return a + (2*b)

def boo(a:int, b:int = 1) -> float:
    return a / (2*b)
```

이를 테스트하기 위해 `test_unitest_example.py`라는 파일을 만든다. Pytest는 'test' 접두사가 있는 파일을 자동으로 감지하기 때문에 이름을 지정할 때 `test_` 접두사를 사용했다. 따라서, 테스트 메소드를 명명할 때 그렇게 한다.

`food`와 `boo`의 테스트 함수를 작성한다. 이러한 함수는 `test_` 접두사로 시작된다. 테스트 내부에 어썰션 문을 사용할 것이다. 잠깐만, 계속하기 전에 아래 어썰션을 기억하자.

```python
import unittest_examples as ue

def test_foo():
    assert ue.foo(5, 2) == 9
    assert ue.foo(5) == 7

def test_boo():
    assert ue.boo(12, 2) == 3
    assert ue.boo(12) == 6
```

테스트를 실행하려면 테스트 스크립트가 있는 폴더에서 쉘 명령을 실행한다.

```bash
$ pytest test_unittest_example.py
```

또는

```bash
$ py.test
#this will detect the test py files automatically in the folder, that's why I named the test py files and functions with the 'test' prefix.
```

두 테스트가 모두 통과되면 아래 그림과 같은 출력이 디스플레이된다. 또한 테스트의 커버리지율(100%)과 테스트를 완료하는 데 걸린 시간의 출력을 확인할 수 있다.

![test_unittest_examples](images/unittest/test_unittest_examples.png)

오류를 만들어 어떤 출력이 발생할지 알아보자.

```python
#instead of
#assert ue.boo(12) == 4
assert ue.boo(12) == 8
```

출력으로 불합격 하나와 합격 하나의 테스트 결과를 볼 수 있다. 불합격 테스트에 대하여는 어썰션 실패에 대한 정보를 제공한다.

![test_fail_1](images/unittest/test_fail_1.png)

### 옵션
쉘 명령에 플래그를 사용하여 PyTest의 다른 기능을 사용할 수 있다.

**v** : **Verbose**, 추가 정보를 출력한다.

```python
$ pytest test_unittest_example.py -v
```

or

```python
$ py.test -v
```

![verbose](images/unittest/verbose.png)

또한 어떤 테스트들이 통과되었고 어떤 테스트들이 실패되었는 지에 대한 세부사항을 볼 수 있다.

합계를 연산하는 `foo` 함수에 대한 테스트를 하나 더 추가한다. Python의 특성상 두 문자열을 연결할 수 있다. 그런 일이 일어나지 않기를 바란다. 그럼 그것을 확인하는 테스트를 따로 작성하겠다.

```python
def test_foo_string_input():
    value = ue.foo("What", "Now")
    assert type(value) is not str
```

실패했다.

![test_fail_2](images/unittest/test_fail_2.png)

**-q**는 간단한 정보를 출력하다.

```python
pytest -q
```

![short_info](images/unittest/short_info.png)

`::` 테스트 파일에 선언된 테스트만 실행한다.

```bash
$ pytest test_unittest_example.py::test_foo
```

![declared_test](images/unittest/declared_test.png)

`-k " "`는 지정한 구문이 테스트 함수 이름에 포함된 테스트만 실행한다.

```bash
pytest test_unittest_example.py -v -k "boo or input"
```

![named_test](images/unittest/named_test.png)

`-m` 마크 표현식. 마찬가지로, pytest 데코레이터로 테스트를 표시하여 이들만 실행할 수도 있다.

```python
import unittest_examples as ue
import pytest

@pytest.mark.num
def test_foo():
    assert ue.foo(5, 2) == 9
    assert ue.foo(5) == 7

@pytest.mark.string
def test_foo_string_input():
    value = ue.foo("What", "Now")
    assert type(value) is not str

@pytest.mark.num
def test_boo():
    assert ue.boo(12, 2) == 3
    assert ue.boo(12) == 6
```

```bash
pytest -v -m num
```

![decoratort_test](images/unittest/decorator_test.png)

`-x` 조기 정지. 첫 번째 실패가 발생했을 때 pytest는 프로세스를 종료한다.

![early_stop](images/unittest/early_stop.png)

`--maxfail=n`: n번 실패한 후 pytest는 프로세스를 종료한다.

![max_fails](images/unittest/max_fails.png)

`-- tb=no` 실패의 세부 정보를 표시하지 않는다.

```bash
pytest -v -x --tb=no
```

![tb_no](images/unittest/tb_no.png)

`mark.skip`과 `-rsx`는 특정 테스트를 건너뛰는 이유를 정의할 수 있다. 따라서, 우리는 목적을 위해 시험 과정을 다양하게 구성할 수 있다.

```python
@pytest.mark.skip(reason="don't run boos")
def test_boo():
    assert ue.boo(12, 2) == 2
    assert us.boo(12) == 4
```

```bash
pytest -v -rsx
```

![mark_skip](images/unittest/mark_skip.png)

`mark.skipif` 건너뛸 조건을 추가할 수 있다. 아래는 Python 버전을 확인하고 조건이 충족되면 테스트를 실행한다.

```python
@pytest.mark.skipif(sys.version_info < (3, 3)>, reason="don't run boo")
def test_boo():
    assert ue.boo(12, 2) == 2
    assert ue.boo(12) == 4
```

`-s` 또는 `--capture=no`를 사용하여 테스트 함수의 인쇄 문을 디스플레이한다.

```bash
pytest -v -s
```

또는

```bash
pytest -v --capture=no
```

![capture](images/unittest/capture.png)

### 매개변수화
Pytest의 파라메타이즈 데코레이터를 사용하여 테스트 사례를 파라메터화할 수 있다. 이러한 방식을 적용하면 동일한 테스트에 대해 여러 어썰션이나 함수를 작성할 필요없다.

```python
@pytest.mark.parametrize('a, b, val`,
                         [
                            (5, 2, 9)
                            (5, 1, 7)
                            (3, 3, 9)
                         ])
def test_foo(a, b, val):
    assert ue.foo(a, b) == val
```

![parametrize](images/unittest/parametrize.png)

### 픽스처(Fixtures)
연결할 데이터베이스에 테스트 사례가 저장되어 있다고 가정하자. 데이터에 연결하고 이를 가져오는 데 항상 시간이 필요하다. 다음과 같은 단순 클래스 구조로 데이터베이스를 나타내도록 한다.

```python
import time

class Database:

    def __init__(self):
        time.sleep(3)

    def get_data(self, query):
        time.sleep(1)
        return query

    def close_connection(self):
        print("Closing")
```

그에 따라 다른 두 테스트 사례를 작성해 보겠다.

```python
def test_db_case_euro():
    db = ue.Database()
    val = db.get_data("euro")
    assert val == "euro"

def test_db_case_dollar():
    db = ue.Database()
    val = db.get_data("dollar")
    assert val == "dollar"
```

보시다시피, 여기에는 반복적인 과정이 있다. 두 테스트 모두 처음부터 데이터베이스에 연결한다. 대신 테스트 프로세스의 시작과 끝에서 공통 작업을 식별하여 중복을 제거할 수 있다.

![fixtures_01](images/unittest/fixture_01.png)

`setup_module` 함수는 테스트 모듈의 첫 번째 수행에서 실행된다. `teardown_module` 함수는 테스트 모듈 끝에서 실행된다. Pytest이 자동으로 감지한다.

```python
db = None

def setup_module(module):
    print("Inside setup")
    global db
    db - us.Database()

def tear_down_module(module):
    print("Inside teardown")
    db.close_connection()

def test_db_case_euro():
    val = db.get_data("euro")
    assert val == "euro"

def test_db_case_dollar():
    val = db.get_data("dollar")
    assert val == "dollar"
```

이렇게 하면 한번으로 데이터베이스에 연결하고 닫을 수 있다.

![fixtures_02](images/unittest/fixture_02.png)

이를 처리하는 또 다른 방법은 Pytest의 픽스처 데토레이터를 사용하는 것이다. 설정과 해제 부분을 모두 포함하는 함수를 정의한다. `yield` 키워드를 사용하여 `db` 객체를 반환한다. `yield` 이후, 나머지는 해제 부분이다.

```python
@pytest.fixture(scope='module')
def db():
    print("Inside fixture")
    db = ue.Database()
    yield db
    print("Now teardown session")
    db.close_connection()

def test_db_case_euro(db):
    db = ue.Database()
    val = db.get_data("euro")
    assert val == "euro"

def test_db_case_dollar(db):
    db = ue.Database()
    val = db.get_data("dollar")
    assert val == "dollar"
```

![fixtures_03](images/unittest/fixture_03.png)

다른 예를 들어 보겠다. 아래 예제에는 항공편 클래스(`Flight`)와 고객 데이터베이스 클래스(`CustomerDb`)를 정의한다. 우리는 비행기 클래스에 승객을 추가하고, 승객 목록을 얻고, 총 승객 수를 구하고, 총 매출을 계산하고자 한다. `CustomerDb`는 고객의 멤버십 상태에 따라 할인율을 결정한다.

```python
from typing import List

class CustomerDb:

    def __init__(self):
        print("Connected to db")

    def get_customer_membership(self, name: str) -> int:
        if name == "Cristiano Ronaldo":
            return 2
        elif name == "Ten Haag":
            return 0
        return 1

    def close(self):
        print("Connection Closed.")

class Flight:

    def __init__(self, capacity: int, unit_price: int) -> None:
        self.capacity: int = capacity
        self.unit_price: int = unit_price
        self.passengers: List[str] = []

    def add_passenger(self, name: str) -> None:
        """insert passenger into passenger list"""
        booked = self.number_of_passengers() 
        if booked == self.capacity:
            raise OverflowError("Flight is fully booked!")
        self.passengers.append(name)
        return

    def number_of_passengers(self) -> int:
        return len(self.passengers)

    def get_passenger_list(self) -> List[str]:
        return self.passengers

    def calculate_total_gross(self, customer_db_object) -> float:
        gross = 0
        for name in self.passengers:
            gross += round(self.unit_price / customer_db_object.get_customer_membership(name),1)
        return gross
```

위와 같이 설계된 어플리케이션 프로그램에 대해 다양한 상황을 테스트해 보자.

- 원활하게 승객을 추가할 수 있는가?
- 추가한 승객이 승객 명단에 있는가?
- 항공편이 만석일 때 신규 승객을 추가할 수 없어야 한다. 이에 대한 방법이 있는가?
- 총 메출을 정확하게 계산할 수 있는가?

이 테스트들을 하나씩 살펴보도록 하겠다.

```python
import pytest
from unittest.mock import Mock
from unittest_examples import Flight, CustomerDb

@pytest.fixture
def app():
    plane = Flight(5, 10)
    plane.add_passenger("Cristiano Ronaldo")
    plane.add_passenger("Ten Hag")
    plane.add_passenger("Harry Maguire")
    d = CustomerDb()
    yield plane, d
    d.close()

def test_adding_a_passenger(app):
    app[0].add_passenger("Luke Shaw")
    assert app[0].number_of_passengers() == 4

def test_plane_contains_booked_passenger(app):
    app[0].add_passenger("Bruno Fernandes")
    assert "Bruno Fernandes" in app[0].get_passenger_list()

def test_overbooking_not_allowed(app):
    for _ in range(2):
        app[0].add_passenger("Harry Maguire")

    with pytest.raises(OverflowError):
        app[0].add_passenger("Harry Maguire")


def test_total_gross_wo_mock(app):
    gross = app[0].calculate_total_gross(app[1])
    assert gross == 25

def test_total_gross_mocking_constant(app):
    d = app[1]
    d.get_customer_membership = Mock(return_value=1) 
    gross = app[0].calculate_total_gross(d)
    assert gross == 30

def test_total_gross_mocking_w_se(app):
    """with side effect"""
    def mock_members(name: str) -> int:
        if name == "Cristiano Ronaldo":
            return 2
        elif name == "Ten Haag":
            return 0
        return 1

    d = app[1]
    d.get_customer_membership = Mock(side_effect=mock_members) 
    gross = app[0].calculate_total_gross(d)
    assert gross == 25
```

시작에서 픽스처의 도움으로 반복되는 것을 방지하는 방식으로 테스트를 설정한다.

```python
@pytest.fixture
def app():
    plane = Flight(5, 10)
    plane.add_passenger("Cristiano Ronaldo")
    plane.add_passenger("Ten Hag")
    plane.add_passenger("Harry Maguire")
    d = CustomerDb()
    yield plane, d
    d.close()
```

픽스처에서 두 변수를 반환하기 때문에 결과로 튜플을 얻는다. 따라서 `Flight` 객체에 접근하기 위해서는 `0`번째 인덱스를 사용해야 한다. 이미 픽스처에 승객 3명을 추가했다. `Luke`와 `Bruno`는 네 번째와 다섯 번째 승객이 되었다.

### 던져진 오류 잡기(Catch Thrown Errors)
만석일때 승객을 추가하려는 경우 `OverflowError`를 발생시킨다는 것을 상기하자.

```python
if booked == self.capacity:
    raise OverflowError("Flight is fully booked!")
```

테스트에서 발생된 오류에 따라 조정할 수 있다. 예를 들어, 아래의 테스트에서 `OverflowError`는 예상하는 오류이기 때문에, 그러한 상황이 발생해도 오류가 발생하지 않도록 보장해야 한다. `with` 키워드 사용한 문장 다음 원하는 명령을 실행할 수 있었다.
그러나 만약 2명의 승객이 아닌 3명의 승객을 추가하려고 한다면, 이 과정은 `with`문 앞에 있을 것이기 때문에 테스트를 통과하지 못할 것이다.

```python
def test_overbooking_not_allowed(app):
    for _ in range(2):
        app[0].add_passenger("Harry Maguire")

    with pytest.raises(OverflowError):
        app[0].add_passenger("Harry Maguire")
```

### Mocking
기억하겠지만, 모킹 과정은 의존성을 제거하는 것이었다. 첫 번째 테스트는 모킹되지 않았다. 이 테스트는 데이터베이스에 직접 수행된다. 테스트를 데이터베이스와 독립적으로 수행하려면(즉 데이터베이스 변경사항이 테스트에 영향을 미치지 않도록 함) 모킹을 적용할 수 있다.

```python
from unittest.mock import Mock
```

```python
def test_total_gross_wo_mock(app):
    gross = app[0].calculate_total_gross(app[1])
    assert gross == 25

def test_total_gross_mocking_constant(app):
    d = app[1]
    d.get_customer_membership = Mock(return_value=1) 
    gross = app[0].calculate_total_gross(d)
    assert gross == 30
```

데이터베이스 객체의 `get_customer_membership` 메서드가 호출될 때마다 반환 값은 상수(이 경우 1)이다.

그 외에도 부작용을 이용한 함수로 `mock` 연산을 정의할 수도 있다.

```python
def test_total_gross_mocking_w_se(app):
    """with side effect"""
    def mock_members(name: str) -> int:
        if name == "Cristiano Ronaldo":
            return 2
        elif name == "Ten Haag":
            return 0
        return 1

    d = app[1]
    d.get_customer_membership = Mock(side_effect=mock_members) 
    gross = app[0].calculate_total_gross(d)
    assert gross == 25
```

쉘에서 `pytest` 명령을 실행하여 테스트 결과를 확인한다.

```bash
pytest -v
```

![mocking](images/unittest/mocking.png)

### 톹합 테스트 (REST API)
마지막으로 REST API를 통한 통합 테스트 사례 연구를 수행한다. 이를 위해 https://todo.pixegami.io/ 사이트를 사용한다. REST API 요청을 처리할 수 있는 간단한 애플리케이션이다.

설명서는 [여기](https://todo.pixegami.io/docs)에 있다.

`TaskUtility` 클래스에는 ENDPOINT 메서드가 정의되어 있다. 여기서는 정적 메서드를 사용하여 API 작업을 수행한다. `new_task_payload`는 새 레코드를 위하여 데이터를 생성한다. `uuid` 라이브러리 덕분에 `user_id`와 `contents`를 랜덤으로 생성한다.

```python
import uuid
import requests

ENDPOINT = "https://todo.pixegami.io"

class TaskUtility:

    @staticmethod
    def new_task_payload():
        user_id = f"test_user_{uuid.uuid4().hex}"
        content = f"test_content_{uuid.uuid4().hex}"
        return {
            "content":content,
            "user_id": user_id,
            "is_done":False,
        }

    @staticmethod
    def create(payload):
        return requests.put(ENDPOINT + "/create-task", json=payload)

    @staticmethod
    def update(payload):
        return requests.put(ENDPOINT + "/update-task", json=payload)

    @staticmethod
    def get(task_id):
        return requests.get(ENDPOINT + f"/get-task/{task_id}")

    @staticmethod
    def lists(user_id):
        return requests.get(ENDPOINT + f"/list-tasks/{user_id}")

    @staticmethod
    def delete(task_id):
        return requests.delete(ENDPOINT + f"/delete-task/{task_id}")



def test_reach_endpoint():
    response = requests.get(ENDPOINT)
    assert response.status_code == 200

def test_new_task_creation():
    #create a new task record
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    assert new_task_response.status_code == 200

    #check if the response data and payload is the same
    data = new_task_response.json()
    fetched_task_response = TaskUtility.get(data["task"]["task_id"])
    assert fetched_task_response.status_code == 200
    fetched_task_data = fetched_task_response.json()
    assert fetched_task_data["content"] == payload["content"]
    assert fetched_task_data["user_id"] == payload["user_id"]

def test_update_task():
    #create a new task
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    task_id = new_task_response.json()["task"]["task_id"]
    #update the task
    new_payload =  {
        "content": "new content",
        "user_id": payload["user_id"],
        "task_id": task_id,
        "is_done": True,
    }
    updated_task_response = TaskUtility.update(new_payload)
    assert updated_task_response.status_code == 200
    #check if the updated repsonse and new payload is same
    fetched_task_data = TaskUtility.get(task_id)
    assert fetched_task_data.status_code == 200
    fetched_task_data = fetched_task_data.json()
    assert fetched_task_data["content"] == new_payload["content"]
    assert fetched_task_data["user_id"] == new_payload["user_id"]


def test_list_tasks():
    n = 5
    #create n task record
    payload = TaskUtility.new_task_payload()
    for _ in range(n):
        new_task_response = TaskUtility.create(payload)
        assert new_task_response.status_code == 200
    #list all tasks
    fetched_task_data = TaskUtility.lists(payload["user_id"])
    assert fetched_task_data.status_code == 200
    data = fetched_task_data.json()
    #check if the number of tasks are same
    assert len(data["tasks"]) == n

def test_delete_task():
    #create a new task record
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    assert new_task_response.status_code == 200
    task_id = new_task_response.json()["task"]["task_id"]
    #delete that record
    deleted_task_response = TaskUtility.delete(task_id)
    assert deleted_task_response.status_code == 200
    #try to get deleted record, it should throw 404 error
    fetched_task_data = TaskUtility.get(task_id)
    assert fetched_task_data.status_code == 404
```

이제 테스트 사례를 살펴보겠다.

```python
def test_reach_endpoint():
    response = requests.get(ENDPOINT)
    assert response.status_code == 200
```

요청이 성공하면 상태 코드로 200을 받아야 한다.

두 번째 테스트에서는 첫 번째로 새로운 작업 레코드를 생성하고 응답의 상태 코드에서 성공적이었는지 확인한다. 그런 다음 작업 ID 번호를 사용하여 새로 생성된 데이터를 `get` 메서드로 새 레코드를 처음에 만들 때 사용한 내용과 동일한지 확인한다.

```python
def test_new_task_creation():
    #create a new task record
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    assert new_task_response.status_code == 200

    #check if the response data and payload is the same
    data = new_task_response.json()
    fetched_task_response = TaskUtility.get(data["task"]["task_id"])
    assert fetched_task_response.status_code == 200
    fetched_task_data = fetched_task_response.json()
    assert fetched_task_data["content"] == payload["content"]
    assert fetched_task_data["user_id"] == payload["user_id"]
```

업데이트 프로세스를 의하여 아래와 같이 유사한 단계를 수행한다.

```python
def test_update_task():
    #create a new task
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    task_id = new_task_response.json()["task"]["task_id"]
    #update the task
    new_payload =  {
        "content": "new content",
        "user_id": payload["user_id"],
        "task_id": task_id,
        "is_done": True,
    }
    updated_task_response = TaskUtility.update(new_payload)
    assert updated_task_response.status_code == 200
    #check if the updated repsonse and new payload is same
    fetched_task_data = TaskUtility.get(task_id)
    assert fetched_task_data.status_code == 200
    fetched_task_data = fetched_task_data.json()
    assert fetched_task_data["content"] == new_payload["content"]
    assert fetched_task_data["user_id"] == new_payload["user_id"]
```

새로운 기록 `n`개를 생성한다. 그런 다음 `user_id`에 따라 모든 레코드를 가져온다. 숫자를 비교한다.

```python
def test_list_tasks():
    n = 5
    #create n task record
    payload = TaskUtility.new_task_payload()
    for _ in range(n):
        new_task_response = TaskUtility.create(payload)
        assert new_task_response.status_code == 200
    #list all tasks
    fetched_task_data = TaskUtility.lists(payload["user_id"])
    assert fetched_task_data.status_code == 200
    data = fetched_task_data.json()
    #check if the number of tasks are same
    assert len(data["tasks"]) == n
```

유사한 작업으로 삭제한 레코드가 실제로 삭제되었는지 404 코드로 확인한다.

```python
def test_delete_task():
    #create a new task record
    payload = TaskUtility.new_task_payload()
    new_task_response = TaskUtility.create(payload)
    assert new_task_response.status_code == 200
    task_id = new_task_response.json()["task"]["task_id"]
    #delete that record
    deleted_task_response = TaskUtility.delete(task_id)
    assert deleted_task_response.status_code == 200
    #try to get deleted record, it should throw 404 error
    fetched_task_data = TaskUtility.get(task_id)
    assert fetched_task_data.status_code == 404
```

테스트 프로세스는 코드와 시스템 품질에 매우 중요하다. 빠른 개발 환경, 시간 제약 및 리소스 부족으로 무시하거나 필요한 주의를 기울이지 않을 수 있다. 그러나 특히 TDD를 고려할 때 테스트 프로세스를 건너뛰지 말아야 한다.

Python용으로 작성된 많은 테스트 라이브러리(내장된 유닛 테스트 라이브러리 포함)가 있다. Pytest는 업계에서 널리 사용되고 널리 인정받는 라이브러리이며 많은 기능을 가지고 있다. 여기서 몇 가지 중요한 문제에 대해 간단히 알아보았다. 

### 더 읽을 거리
- [Test-Driven Development in Python](https://python.plainenglish.io/test-driven-development-in-python-49fa22cb95d4)
- [RESTful Django — Django REST Framework](https://levelup.gitconnected.com/restful-django-django-rest-framework-8b62bed31dd8)
- [Defensive Programming in Python](https://python.plainenglish.io/defensive-programming-in-python-af0266e65dfd)

**(주)** 위 내용은 [Unit Test with PyTest](https://levelup.gitconnected.com/unit-test-with-pytest-d6f53919a19a)을 편역한 것이다.
