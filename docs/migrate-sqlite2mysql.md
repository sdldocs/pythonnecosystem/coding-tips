두 번째 파트에서는 기본 데이터베이스 SQLite를 MySQL로 마이그레이션하려고 한다. 기본적으로 Django는 SQLite 데이터베이스를 사용하지만, 이를 MySQL로 변경하려고 한다.

SQLite를 MySQL 데이터베이스로 마이그레이션을 시작해 보자.

## Xampp 설치
컴퓨터(여기서는 MacOS)에 이미 XAMPP를 설치했다. 컴퓨터에 XAMPP가 설치되어 있지 않은 경우 아래 링크를 사용하여 XAMPP를 설치할 수 있다.

[다운로드](https://www.apachefriends.org/download.html)

> **Note**
>
> [Download and Install XAMPP on MacOS — How to Download and Install XAMPP on MAC OS?](https://medium.com/analytics-vidhya/download-and-install-xampp-on-mac-oshow-to-download-and-install-xampp-on-mac-os-97705974080d)

## 가상 환경 활성화

```bash
$ cd myproject/bog_app/
$ source ./my_blog_env/bin/activate
```

## sqlite 데이터베이스 백업

### dumpdata

**`django-admin dumpdata [app_label[.ModelName] [app_label[.ModelName] ...]]`**

어플리케이션과 관련된 데이터베이스의 모든 데이터를 표준 출력으로 출력한다.

어플리케이션 이름이 제공되지 않으면 설치된 모든 애플리케이션이 덤프된다.

`dumpdata`의 출력은 `loaddata`의 입력으로 사용할 수 있습니다.

`dumpdata`는 덤프할 레코드를 선택할 때 모델의 기본 관리자를 사용한다는 점을 유의하여야 한다. 커스텀 관리자를 기본 관리자로 사용하는 경우 사용 가능한 레코드 중 일부를 필터링하면 모든 개체가 덤프되지 않는다.

SQLite 데이터베이스를 덤프하려면 다음 명령을 실행해야 한다.

```bash
$ python manage.py dumpdata > backup.json
```

![](images/migrate-sqlite2mysql/cli_02_01.png)

프로젝트 폴더에 생성된 `backup.json` 파일이 생성되었다.

![](images/migrate-sqlite2mysql/cli_02_02.png)

## Apache와 MySQL 시작하기
시스템에서 Apache 및 MySQL이 실행 중인지 확인한다.

![](images/migrate-sqlite2mysql/screenshot_02_01.png)

## 블로그 웹사이트를 위한 데이터베이스 생성
### `phpmyadmin` 사용자 인터페이스에 로그인
Apache와 MySql을 시작한 후 브라우저로 이동하여 다음 URL을 입력한다.

http://localhost/phpmyadmin/

로그인 후 아래와 같은 화면을 볼 수 있다.

![](images/migrate-sqlite2mysql/screenshot_02_02.png)

### 데이터베이스 만들기
데이터베이스 이름은 "blogdb"로 유지한다. 변경하기를 원한다면, 원하는 데이터베이스 이름을 선택할 수 있다.

![](images/migrate-sqlite2mysql/screenshot_02_03.png)

## 활성화된 환경에서 `mysqlclient` 설치

```bash
$ pip install mysqlclient
```

![](images/migrate-sqlite2mysql/cli_02_03.png)

> **Note**
>
> 만약 error가 발생하면, [[Python] Mac에서 mysqlclient 설치 에러](https://cpdev.tistory.com/195)를 참고 하시오.

## `settings.py`에 데이터베이스 세부사항 변경
`settings.py` 파일을 열고 데이터베이스 설정을 변경한다. SQLite 엔진과 이름에 주석을 달았다. 그리고 MySQL 데이터베이스 세부 정보를 추가했다.

```python
DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': BASE_DIR / 'db.sqlite3',
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'init_command': 'SET default_storage_engine=INNODB',
        },
        'NAME': 'blogdb',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
```

![](images/migrate-sqlite2mysql/screenshot_02_04.webp)

## 데이터베이스 마이그레이트
데이터베이스를 마이그레이션해야 한다. 따라서 필요한 변경을 위해 다음 명령 `python manage.py migrate - run-syncdb`를 실행해야 한다. 기본적으로 모든 테이블을 생성한다.

## 마이그레이트

**`django-admin migrate [app_label] [migration_name]`**

데이터베이스 상태를 현재 모델과 마이그레이션 집합으로 동기화한다. 마이그레이션과 앱과의 관계 등은 마이그레이션 문서에서 자세히 다루고 있다.

이 명령의 동작은 제공된 인수에 따라 달라진다.

인수가 없다면 모든 앱에 대하여 마이그레이션이 실행된다.

<`app_label`>: 지정한 앱에 가장 최근 마이그레이션까지 마이그레이션이 실행된다. 종속성으로 인해 다른 앱의 마이그레이션도 실행될 수 있다.

<`app_label`> <`migration_name`>: 데이터베이스 스키마를 명명된 마이그레이션이 적용된  상태로 만들어 주며, 이후 동일한 앱에 마이그레이션이 적용되지 않는다. 명명된 마이그레이션으로 이전에 마이그레이션한 경우 마이그레이션이 적용되지 않았을 수 있다. 마이그레이션 이름의 접두사(예: 0001)는 지정된 앱 이름이 고유한 경우에만 사용할 수 있다. 마이그레이션을 완전히 되돌리려면 `zero`라는 이름을 사용한다. 즉, 앱에 적용된 모든 마이그레이션을 되돌리려면 `zero`를 사용한다.

`--database` DATABASE: 마이그레이션할 데이터베이스를 지정한다. 기본값은 `default`이다.

`--fake`: 위의 규칙에 따라 마이그레이션 대상에 적용된 것으로 표시하지만 실제로 SQL을 실행하여 데이터베이스 스키마를 변경하지는 않는다.

이는 고급 사용자가 수동으로 변경 사항을 적용하는 경우 현재 마이그레이션 상태를 직접 조작하기 위한 것으로, `fake`를 사용하면 마이그레이션을 올바르게 실행하기 위해 수동 복구가 필요한 상태로 마이그레이션 상태 테이블을 만들 위험이 있다는 점에 유의하여야 한다.

`--fake-initial`: 해당 마이그레이션의 모든 `CreateModel` 작업에 의해 생성된 모든 모델의 이름을 가진 모든 데이터베이스 테이블이 이미 존재하는 경우 Django가 앱의 초기 마이그레이션을 건너뛸 수 있도록 한다. 이 옵션은 마이그레이션을 사용하기 전에 이미 존재했던 데이터베이스에 대해 마이그레이션을 처음 실행할 때 사용하기 위한 것이다. 그러나 이 옵션은 테이블 이름이 일치하는 것 외에 데이터베이스 스키마가 일치하는지 확인하지 않으므로 기존 스키마가 초기 마이그레이션에 기록된 것과 일치한다고 확신하는 경우에만 사용하는 것이 안전하다.

`--plan`: 지정된 마이그레이션 명령을 수행할 마이그레이션 작업을 보인다.

`--run-syncdb`: 마이그레이션 없이 앱용 테이블을 만들 수 있다. 권장하지는 않지만, 수백 개의 모델이 있는 대규모 프로젝트에서는 마이그레이션 프레임워크가 너무 느려지는 경우가 있다.

`--noinput, — no-input`: 모든 사용자 프롬프트를 표시하지 않는다. 오래된 콘텐츠 타입의 제거에 대해 묻는 메시지가 그 예이다.

`--check`: 적용되지 않은 마이그레이션이 감지되면 마이그레이션을 0이 아닌 상태로 종료한다.

`--prune`: `django_migrations` 테이블에서 존재하지 않는 마이그레이션을 삭제한다. 이 옵션은 스쿼시 마이그레이션으로 대체된 마이그레이션 파일이 제거된 경우에 유용하다. 자세한 내용은 마이그레이션 스쿼시하기를 참조하시오.

```python
$ python manage.py migrate --run-syncdb
```

![](images/migrate-sqlite2mysql/cli_02_04.png)

`phpmyadmin` 사용자 인터페이스로 이동하여 생성된 모든 테이블을 확인할 수 있다.

![](images/migrate-sqlite2mysql/screenshot_02_05.png)

## json 파일 로드
json 형식의 SQLite 데이터베이스 백업을 로드할 것이다.

```bash
$ python manage.py loaddata backup.json
```

![](images/migrate-sqlite2mysql/cli_02_05.png)

테이블에 삽입된 데이터를 볼 수 있다.

![](images/migrate-sqlite2mysql/screenshot_02_06.png)

## 개발 서버 수행

```bash
$ python manage.py runserver
```

개발 서버를 실행한 후 다음 URL을 모두 확인한다.

http://127.0.0.1:8000/

http://127.0.0.1:8000/admin/

![](images/migrate-sqlite2mysql/screenshot_02_07.png)

![](images/migrate-sqlite2mysql/screenshot_02_08.png)

## 최신 변경을 Github 레포지터리에 푸시
모든 것이 잘 작동하고 있다. 최신 변경 사항을 git 리포지토리에 푸시하자.

```bash
$ git add --all
$ git commit -m "Changed database sqllite to mysql"
$ git push origin main
```

![](images/migrate-sqlite2mysql/cli_02_06.png)

여기까지이다. 다음 파트에서는 Django 어플리케이션과 기사 모델을 만들어 보도록 하자.

**(주)** 위 내용은 [Migrate SQLite database to MySQL database - Part 2](https://medium.com/@nutanbhogendrasharma/migrate-sqlite-to-mysql-database-in-django-part-2-5c43fa055580)을 편역한 것이다. 
