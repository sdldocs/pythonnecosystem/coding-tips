이 페이지에서 Vue.js 2.x 및 Django 2.0.2를 사용하여 간단한 CRUD 어플리케이션을 개발하는 방법을 설명한다. Vue.js는 사용자 인터페이스를 구축하기 위한 진보적인 프레임워크인 반면, Django는 빠른 개발을 장려하는 고급 Python 개발 프레임워크이다. (본 페이지는 주로 Django를 위주로 편역하였다.)

Vue.js와 Django를 사용하여 CRUD 앱을 만들려면 다음 단계를 수행한다.

- Django 설치
- Django 프로젝트와 앱 생성
- 모델 생성과 마이그레이션
- Django-rest-framework 설치
- Serializar, Viewset과 라우터 생성
- Django를 사용하여 Vue.js 구성

### Django 설치
pc에 python 3, pip, virtualenv가 설치되어 있는지 확인한다, Django를 시스템에 이미 설치한 경우 create-project까지 단계를 건너뛸 수 있다.

프로젝트 폴더를 만들고 폴더 내부에서 터미널로 명령을 실행한다.

```bash
$ virtualenv venv -p $(which python3)
```

`virtualenv`는 글로벌 패키지 버전을 손상시키지 않도록 python 패키지들을 위한 가상 환경을 제공한다.

다음 생성한 가상 환경을 활성화한다.

```bash
$ source venv/bin/activate
```

가상 환경이 활성화되면 `(venv)`가 명령어 시작 부분에 표시된다. 장고를 설치한다.

```bash
(venv) $ pip install Django
```

### Django 프로젝트와 앱 생성
이제 가상 환경에 최신 버전의 장고를 설치했다. 동일한 폴더에 프로젝트를 생성한다.

```bash
(venv) $ djnago-admin startproject myproject
```

`manage.py` 파일과 프로젝트의 모든 설정을 포함한 `settings.py` 파일을 갖는 `myproject` 폴더가 생성된다. Django에서는 모든 프로그램이 작은 앱으로 분해된다. 그리하여 우리는 앱을 만들 것이다. `manage.py`가 있는 `myproject` 폴더 안으로 이동한 후, 다음 명령을 실행한다.

```bash
(venv) $ python manage.py startapp article
```

이렇게 하면 `models.py`, `admin.py`, `tests.py`, `views.py`, `admin.py`, `apps.py` 파일과 마이그레이션 폴더가 있는 `article` 폴더가 생성된다.

`models.py`에는 데이터베이스 모델이 포함되어 있고, `admin.py`에는 Django에 의해 생성된 admin 인터페이스의 설정이 포함되어 있으며, `tests`는 앱에 대한 테스트를 작성하기 위한 것이며, `views`에는 템플릿과 모델의 상호 작용을 컨트롤러하는 함수가 포함되어 있으며, 마이그레이션 폴더에는 모델에 의해 생성된 데이터베이스 마이그레이션이 포함되어 있다. 자세히 설명하기 전에 `/myproject/myproject/settings.py` 파일의 `INSTALLED_APPS` 섹션에서 앱 이름을 언급하여 우리 프로젝트에서 앱을 액세스할 수 있도록 한다.

```python
# settings.py

INSTALLED_APPS = [
 ‘django.contrib.admin’,
 ‘django.contrib.auth’,
 ‘django.contrib.contenttypes’,
 ‘django.contrib.sessions’,
 ‘django.contrib.messages’,
 ‘django.contrib.staticfiles’,
 ‘article’
]
```

### 모델 생성과 마이그레이션
모델은 데이터베이스 모델이거나 데이터베이스 필드라고 할 수 있다. Django ORM은 고도로 맞춤화되어 있어 모델에서 데이터베이스 구조를 생성하고, 데이터베이스 마이그레이션을 수행할 수 있다.

`article` 앱 내 `models.py` 에 Article 모델을 생성한다.

```python
# models.py

# Create your models here.
class Article(models.Model):
    article_id = models.AutoField(primary_key=True)
    article_heading = models.CharField(max_length=250)
    article_body = models.TextField()
```
> Django model에 대한 자세한 정보를 보려면 이 링크 https://docs.djangoproject.com/en/2.0/topics/db/models/ 를 따라간다.

`manage.py` 이 있는 myproject 베이스 폴더로 이동하여 모델에 대한 마이그레이션을 실행한다.

```bash
(venv) $ python manage.py makemigrations
```

마이그레이션이 마이그레이션 폴더에 생성되었음을 나타내는 다음과 같은 출력이 디스플레이된다.

```
Migrations for ‘article’:
 article/migrations/0001_initial.py
 — Create model Article
```

이제 이 파일을 마이그레이션하여 해당 데이터베이스 구조를 생성한다.

```bash
(venv) $ python manage.py migrate
```

실행 후 많은 마이그레이션이 진행되는 것을 볼 수 있다. 앱에서 사용할 수 있는 사용자 시스템 (django에서 기본으로 제공) 도 포함되어 있다. 목록에서 위의 마이그레이션도 확인할 수 있다.

### Django-rest-framework 설치
이제 Django rest framework를 설치한다. Django rest framework는 Django 위에 구축된 라이브러리로 REST API를 만들어 준다. 여러분은 Django를 사용하여 커스텀 함수를 사용할 수 있는 api를 만들지만 일부 보안 예외, 상태 또는 기본 문제를 놓칠 수 있다. Django rest framework는 이미 이러한 문제를 알고 있어 사용하기 전에 걱정을 하지 않아도 된다. Django에 초보이라면, [best Django tutorials](https://blog.coursesity.com/best-django-tutorials/) 목록은 Django에 대해 배울 수 있는 좋은 방법일 것이다.

> Django rest framework에 대한 보다 자세한 정보는 http://www.django-rest-framework.org/ 에서 얻을 수 있다.

Django-rest-framework를 설치한다.

```bash
(venv) $ pip install djangorestframework
```

framework를 위하여 `settings.py` 파일을 업데이트한다.

```python
# settings.py

INSTALLED_APPS = [
 ‘django.contrib.admin’,
 ‘django.contrib.auth’,
 ‘django.contrib.contenttypes’,
 ‘django.contrib.sessions’,
 ‘django.contrib.messages’,
 ‘django.contrib.staticfiles’,
 ‘article’,
 ‘rest_framework’
]
```

드디어 api를 만들기 위한 70%의 작업을 마쳤다.이제 viewsets과 라우터를 만들어야 한다.

### Serializer, Viewset과 라우터 생성
article에 `serializers.py` 파일을 만든다. api용 직렬화 프로그램(serializer)이 이 파일을 구성한다. 직렬화 도구를 사용하면 queryset과 모델 인스턴스같은 복잡한 데이터를 네이티브 Python 데이터 타입으로 변환하여 `JSON`, `XML` 또는 기타 컨텐츠 타입으로 쉽게 렌더링할 수 있다. 직렬화 프로그램을 작성하여 보자.

```python
# serializers.py

from rest_framework import serializers
from .models import Articleclass ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
```

우선, rest framework 라이브러리에서 `serializer` 클래스를 import한 다음 구조화해야 하는 데이터의 모델을 가져온다. rest 프레임워크 serializer가 베이스 클래스인 `serializer`를 위한 클래스를 정의한다. 메타에서 기술로 모델과 그 필드를 언급한다.

> 직렬화 프로그램에 대한 자세한 내용을 http://www.django-rest-framework.org/api-guide/serializers/ 에서 얻을 수 있다.

이제 Viewset을 만들겠다. 동일한 폴더 내에 `viewsets.py` 를 생성한다.

Django rest framework를 사용하면 `ViewSet`이라는 단일 클래스에서 관련 view 집합에 대한 논리를 결합할 수 있다. 다른 프레임워크에서도 'Resource' 또는 'Controller'같은 개념적으로 유사한 구현을 찾아 볼 수 있다.

```python
# viewsets.py

from rest_framework import viewsets
from .models import Article
from .serializers import ArticleSerializer

class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
```

먼저 `viewsets` 클래스를 import한 다음 모델과 직렬화 프로그램(바로 위에서 작성)도 import 한다. 이제 `viewsets` 클래스인 `ArtcleViewSet`을 정의한다. 클래스는 쿼리 결과 데이터를 정의할 `queryset`과 해당 데이터를 직렬화할 `serializer_class`를 갖는다.

> viewset에 대한 자세한 내용을 https://www.django-rest-framework.org/api-guide/viewsets/ 에서 얻을 수 있다.

이제 Django에서 rest api를 만드는 것보다 한 단계 늦은 라우터를 생성한다. Rails와 같은 일부 웹 프레임워크는 수신 요청을 처리하는 논리에 응용 프로그램의 URL을 매핑하는 방법을 자동으로 결정하는 기능을 제공한다. 라우터는 자동으로 자신의 이러한 요청을 생성한다. 또한 다양한 앱이 API를 쉽게 처리할 수 있도록 모든 라우터에 공통 파일을 만든다.

`settings.py`와 `urls.py` 파일이 있는 `myproject` 폴더 안에 `routers.py` 파일을 작성한다.

```python
# routers.py

from rest_framework import routers
from article.viewsets import ArticleViewSet

router = routers.DefaultRouter()
router.register(r’article’, ArticleViewSet)
```

먼저 `rest_framework`에서 `routers`를 import한 다음 `ArticleViewSet`을 import하고, 다양한 URL에 대한 viewset을 입력하는 함수 `router`를 정의하면 이제 api는 `ArticleViewSet`에 연결된 `/article`과 다소 유사한 것이 된다.

> router에 대한 자세한 내용을 https://www.django-rest-framework.org/api-guide/routers/ 에서 얻을 수 있다.

이제 앱의 모든 url 경로가 포함된 이 라우터 파일을 `urls.py` 내로 import한다.

```python
# urls.py

from django.contrib import admin
from django.urls import path, include
from .routers import router

urlpatterns = [
    path(‘admin/’, admin.site.urls),
    path(‘api/’, include(router.urls))
]
```

`routers.py` 파일 내에 내장된 모든 URL을 포함하기 위하여 라우터 파일을 import했다. 이제 `/api/article`로 호출될 api URL을 구분하기 위해 `api/` 키워드를 추가했다.

이제 api 부분를 완료했다.

다음과 같은 api를 얻었다.

- `GET: /api/article/`는 모든 article을 제공한다.
- `POST: /api/articl`는 새로운 article을 추가한다.
- `GET: /api/article/{article_id}/`는 특정 article을 반환한다.
- `PUT: /api/article/{article_id}/`는 특정 article의 모든 필드를 갱신한다.
- `PATCH: /api/article/{article_id}/`는 특정 article 내에 패치를 붙인다.

### Configuring Vue.js with Django
이제 템플릿 내부에 api를 통합해 보겠다. `article` 폴더 내부에 `templates` 폴더를 만들고, `templates` 폴더 내부에 `index.html` 파일을 만든다.

```html
<!DOCTYPE html>
<html lang=”en”>
    <head>
        <meta charset=”utf-8">
        <title>Vue-js | Django | Crud App</title>
        <meta name=”viewport” content=”width=device-width, initial-scale=1.0">
        <meta name=”description” content=”A simple crud app made with the vue js and django”><meta name=”keywords” content=”vuejs, django, crudapp, restapi”>
        <!-- bootstap -->
        <link rel=”stylesheet” href=”https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity=”sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm” crossorigin=”anonymous”>
        <!-- boostrap css -->
    </head>
    <body>
        <!-- bootrtap js files -->
        <script src=”https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity=”sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN” crossorigin=”anonymous”></script>
        <script src=”https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity=”sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q” crossorigin=”anonymous”></script>
        <script src=”https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity=”sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl” crossorigin=”anonymous”></script>
        
        <!-- vue.js files -->
        <script src=”https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
        <script src=”https://cdn.jsdelivr.net/npm/vue-resource@1.3.5"></script>
    </body>
</html>
```

파일 안에 디자인을 위한 부트스트랩 `css`와 `jscdn` 링크가 추가된 것을 볼 수 있다. 그런 다음 `vue.js` 링크를 추가했다. `vue.js`는 메인 **`vue js`** 라이브러리이고 rest api를 호출하기 위한 **`vue-resource`** 라이브러리가 있다.

`urls.py` 파일도 업데이트한다.

```python
# urls.py

from django.contrib import admin
from django.urls import path, include
from .routers import router
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('article', TemplateView.as_view(template_name='index.html')),
]
```

아래에서 스크립트 태그 내부에 `vue js` 인스턴스를 만들 것이다.

```html
<script type=”text/javascript”>
    new Vue({
        el: ‘#starting’,
        delimiters: [‘${‘,’}’],
        data: {
            articles: [],
            loading: false,
            currentArticle: {},
            message: null,
            newArticle: { ‘article_heading’: null, ‘article_body’: null },
        },
        mounted: function() {},
        methods: {}
    });
</script>
```

이제 이 vue 인스턴스를 이해해 보자, `el`은 divison의 id 또는 클래스이며(vue js 인스턴스가 실행될 body에 있는 div), `delimeters`는 html 파일의 데이터를 디스플레이하기 위하여 vue js 변수 주위에 적용하는 태그이고. `data`는 vue js 라이브러리 내의 모든 데이터를 포함하며, `mounted`는 vue js 인스턴스를 마운트하기 전에 실행되는 함수이다. 그 이전에 vue 인스턴스에서 실행될 모든 함수는 `methods`에 포함되어 있다.

우선 다음과 같은 메서드를 추가하겠다.

- `getArticles`은 모든 article을 가져온다
- `getArticle`은 특정 article을 가져온다
- `addArticle`은 새로운 article을 추가한다
- `updateArticle`은 특정 article을 업데이트한다
- `deleteArticle`은 기사를 특정 article을 삭제한다

```js
mounted: function() {
    this.getArticles();
},
methods: {
    getArticles: function() {
        this.loading = true;
        this.$http.get(‘/api/article/’)
            .then((response) => {
                this.articles = response.data;
                this.loading = false;
            })
            .catch((err) => {
            this.loading = false;
            console.log(err);
            })
    },
    getArticle: function(id) {
        this.loading = true;
        this.$http.get(`/api/article/${id}/`)
            .then((response) => {
                this.currentArticle = response.data;
                this.loading = false;
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
            })
    },
    addArticle: function() {
        this.loading = true;
        this.$http.post(‘/api/article/’,this.newArticle)
            .then((response) => {
                this.loading = false;
                this.getArticles();
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
            })
    },
    updateArticle: function() {
        this.loading = true;
        this.$http.put(`/api/article/${this.currentArticle.article_id}/`,     this.currentArticle)
            .then((response) => {
                this.loading = false;
                this.currentArticle = response.data;
                this.getArticles();
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
            })
    },
    deleteArticle: function(id) {
        this.loading = true;
        this.$http.delete(`/api/article/${id}/` )
            .then((response) => {
                this.loading = false;
                this.getArticles();
            })
            .catch((err) => {
                this.loading = false;
                console.log(err);
            })
    }
```

mounted 함수에서 페이지가 로드될 때 article을 모두 가져오도록 하는 메소드를 실행했다. 그 후 `getArticles`에 `loading` 변수가 있으며, 이는 api를 로딩할 때 페이지 로딩을 표시하는 데 도움을 준다. 그런 다음 api를 호출하고 응답을 처리할 수 있는 vue 리소스 코드가 있다. 이는 거의 모든 함수에 대해 동일하게 나타난다.

```js
this.$http.request_type(‘api_url’,payload)
    .then((response) => {
      // code if the api worked successfully
    })
    .catch((err) => {
      // code if the api show some error
    })
```

이제 html 파일에서 이러한 함수들을 구현해 보자.

```html
<body>
    <div id="starting">
      <div class="container">
        <div class="row">
          <h1>List of Articles
            <button class="btn btn-success">ADD ARTICLE</button>
          </h1>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Heading</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="article in articles">
                <th scope="row">${article.article_id}</th>
                <td>${article.article_heading}</td>
                <td>
                  <button class="btn btn-info" v-on:click="getArticle(article.article_id)">Edit</button>
                  <button class="btn btn-danger" v-on:click="deleteArticle(article.article_id)">Delete</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="loading" v-if="loading===true">Loading&#8230;</div>
    </div>
```

이는 HTML body의 기본 구조이다. article 배열을 순환하는 `v-for` 태그를 사용하여 article을 디스플레이했다. 그런 다음 vue.js 데이터를 디스플레이했다. vue.js 데이터는 `${}` 태그에 표시되어 있다. 이는 vue 인스턴스에 정의된 delimeter이다. `div`도 마찬가지이다. vue 인스턴스에서 언급한 것과 동일한 `id="starting"`을 볼 수 있다.이제 세부적인 작업을 더 진행한다.

먼저 Article 팝업을 추가하자. index.html 파일을 다음과 같이 변경한다.

```html
<button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#addArticleModal">ADD ARTICLE</button>
```

이제 테이블 태그 아래에 addArticle Modal을 추가한다.

```html
          <!-- Add Article Modal -->
          <div class="modal fade" id="addArticleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">ADD ARTICLE</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form v-on:submit.prevent="addArticle()">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="article_heading">Article Heading</label>
                      <input
                        type="text"
                        class="form-control"
                        id="article_heading"
                        placeholder="Enter Article Heading"
                        v-model="newArticle.article_heading"
                        required="required" >
                    </div>
                    <div class="form-group">
                      <label for="article_body">Article Body</label>
                      <textarea
                        class="form-control"
                        id="article_body"
                        placeholder="Enter Article Body"
                        v-model="newArticle.article_body"
                        required="required"
                        rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
            </div>
           <div class="loading" v-if="loading===true">Loading&#8230;</div>          
          </div>
          <!-- End of article modal -->
```

이 모달에서는 양식을 제출하는 `v-on:submit.prevent` 함수를 볼 수 있다. 폼의 각 입력은 vue js 인스턴스의 데이터와 매핑된 v-model 애트리뷰트에 의해 유지된다. 또한 `api is loading`을 실행할 `v-if` 절이 있다. api 요청이 로드될 때 화면에 로더가 보인다. 몇몇 css 클래스는 그것을 위해 작성된다. 

![Add Article Window]()

#### Edit Part와 View
Edit을 클릭하면 편집할 수 있는 현재 클릭 article 정보가 있는 동일한 양식이 나타난다.

```html
<button class="btn btn-info" v-on:click="getArticle(article.article_id)">Edit</button>
```

이 함수는 이미 정의되었고, view의 각 article에 대해서도 언급했다. Edit Modal을 보기 위해 vue.js 함수를 거의 업데이트하지 않는다.

```js
getArticle: function(id) {
          this.loading = true;
          this.$http.get(`/api/article/${id}/`)
              .then((response) => {
                this.currentArticle = response.data;
                $("#editArticleModal").modal('show');
                this.loading = false;
              })
              .catch((err) => {
                this.loading = false;
                console.log(err);
              })
        },
```

이제 ass modal 코드 아래에 edit modal 코드를 추가한다.

```html
          <!-- Edit Article Modal -->
          <div class="modal fade" id="editArticleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">EDIT ARTICLE</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form v-on:submit.prevent="updateArticle()">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="article_heading">Article Heading</label>
                      <input
                        type="text"
                        class="form-control"
                        id="article_heading"
                        placeholder="Enter Article Heading"
                        v-model="currentArticle.article_heading"
                        required="required" >
                    </div>
                    <div class="form-group">
                      <label for="article_body">Article Body</label>
                      <textarea
                        class="form-control"
                        id="article_body"
                        placeholder="Enter Article Body"
                        v-model="currentArticle.article_body"
                        required="required"
                        rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary m-progress" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
            </div>
            <div class="loading" v-if="loading===true">Loading&#8230;</div>
          </div>
          <!-- End of edit article modal -->
```

변수만 변경되고 거의 비슷하다.

![Edit Article Window]()

#### Now Delete Part
위의 코드를 잘 관찰했다면 쉽게 삭제 부분을 끝낼 수 있다.

```html
<button class=”btn btn-danger” v-on:click=”deleteArticle(article.article_id)”>Delete</button>
```

포스트를 끝낸다. 이제 Django를 사용하여 CRUD 앱을 만들고 vue.js를 배울 수 있다.

```bash
(venv) $ python manage.py runserver
```

다음 브라우저의 주소창에 ...

```
http://127.0.0.1:8000/article
```

를 입력하면, CRUD 앱이 작동하는 것을 볼 수 있으며, article을 추가하고, 편집하고, 삭제할 수 있다.

**주** 이 페이지는 [CRUD App using Vue.js and Django](https://medium.com/@Evenword/crud-app-using-vue-js-and-django-78ca697533df)을 편역한 것임.
