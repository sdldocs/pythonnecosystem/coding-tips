축하합니다. JavaScript의 프론트엔드 기본 사항은 끝났다. 이제 웹 개발의 백엔드로 뛰어들 시간이 거의 다 되었다. 하지만, 먼저 인터넷이 실제로 어떻게 작동하는지에 대해 설명하고자 한다.

간단히 말해서, 네트워크에는 여러 대의 컴퓨터가 서로 연결되어 있다. 이 컴퓨터들은 서로 정보를 교환할 수 있다. 만약 이 네트워크가 지구 전체로 확장된다면, 그것을 인터넷이라고 부르는 것이다.

## 네크워크 프로토콜(network protocols)
컴퓨터(서버)가 데이터와 리소스를 보낼 때는 특정 프로토콜을 따라야 하므로 리소스를 수신하는 컴퓨터(클라이언트)가 데이터와 리소스를 읽는 방법을 알고 있어야 한다. 전자 메일을 주고받고, 파일을 공유하고, 인터넷을 통해 다른 컴퓨터를 제어하는 프로토콜이 있다. 그것들을 모두 소개할 시간이 없기 때문에, 이 포스팅에서는 TCP 프로토콜, HTTP과 HTTPS에 초점을 맞출 것이다.

TCP 프로토콜은 가장 일반적으로 사용되는 인터넷 통신 프로토콜 중 하나이며, 실제로 그 위에 많은 다른 프로토콜이 생성되어 있다. 이 기능은 다음과 같이 작동합니다. 한 컴퓨터는 항상 다른 컴퓨터가 대화를 시작할 때까지 수신 대기해야 합니다.

컴퓨터에는 서로 다른 "청취자"가 있으며, 동시에 서로 다른 종류의 통신을 청취할 수 있다. 청취자들이 서로 방해하지 않도록 하기 위해 각 청취자는 해당 컴퓨터에서 한 위치(포트)를 차지하게 된다. 예를 들어, 전자 메일을 수신할 때 해당 전자 메일은 TCP 프로토콜을 기반으로 작성된 SMTP 프로토콜을 사용하여 전송된다. 기본적으로 컴퓨터는 항상 포트 25에서 전자 메일을 수신한다.

한 컴퓨터가 대상 컴퓨터로 데이터를 보내려면 정확한 포트를 통해 대상 컴퓨터와 "대화"해야 한다. 대상 시스템에 연결할 수 있고 해당 포트에서 수신 중이면 연결이 설정되고 데이터 전송을 시작할 수 있다. 이 경우 수신 중인 컴퓨터를 클라이언트라고 하며, 통화를 수행하는 컴퓨터를 서버라고 한다.

HTTP(Hypertext Transfer Protocol)는 명명된 리소스를 검색하기 위한 프로토콜이다. 즉, 일부 리소스의 요청을 위하여 클라이언트가 먼저 서버에 요청한다. 리소스는 일반적으로 웹 페이지, 이미지 또는 CSS/JavaScript 파일일 수 있다. 서버가 해당 요청에 대해 OK이면, `200 OK` 메시지를 클라이언트에 반환하고 파일 전송을 시작한다. 클라이언트에서 보내는 HTTP 요청은 일반적으로 다음과 같다.

```
# Start with HTTP method (we'll discuss this in detail later),
# followed by the name of the resource, and the version of the protocol
GET /index.html HTTP/1.1

# You can also specify other information here
Host: example.com
Accept-Language: en
```

이에 대한 응답은 다음과 같다.

```
# Start by the 200 OK message
HTTP/1.1 200 OK

# Some extra info here
Date: Sat, 09 Oct 2010 14:28:02 GMT
Server: Apache
Last-Modified: Tue, 01 Dec 2009 20:18:22 GMT
ETag: "51142bc1-7449-479b075b2891b"
Accept-Ranges: bytes
Content-Length: 29769
Content-Type: text/html

# The requested resource
<!DOCTYPE html... (here come the 29769 bytes of the requested web page)
```

물론 인터넷을 검색할 때 수동으로 검색할 필요가 없다. 프로토콜, 호스트와 원하는 리소스의 경로를 지정하는 URL(Uniform Resource Locator)을 입력하면 브라우저가 자동으로 모든 작업을 수행한다.

```
http://example.com/2020/03/16/13_browser.html
|     |           |                         |
protocol   server             path
```

HTTPS 프로토콜은 암호화되어 있다는 점을 제외하고는 정확히 동일하게 작동한다. 클라이언트와 서버 간의 통신이 안전한지 확인하기 위해 전송 계층 보안(TLS) 프로토콜을 사용한다. 서버에는 개인 키가 있고 클라이언트에는 공용 키가 있다. 두 키가 서로 일치하는 경우에만 연결을 설정할 수 있다.

## HTTP 메서드
여러분은 웹 개발에 초점을 맞추고 있기 때문에, 이 포스팅에서는 HTTP 프로토콜에 대해서만 자세히 설명한다. 이전 예에서 HTTP 요청을 보낼 때 요청은 HTTP 메서드라고 하는 키워드 `GET`으로 시작한다. `GET` 외에도 6가지 메서드가 있으며, 각 메서드는 다른 용도로 사용된다.

### `GET` 메서드
`GET` 메서드는 가장 일반적으로 사용되는 HTTP 요청 메서드이다. 서버에서 데이터 또는 리소스를 요청하는 데 사용된다. `GET` 요청을 보낼 때 쿼리 매개 변수는 다음과 같이 name/value 쌍으로 URL에 포함된다.

```
http://example.com/2020/03/16/13_browser.html?name1=value1&name2=value2
```

물음표(`?`)는 파라미터의 시작을 나타내고 앰퍼샌드(`&`)는 두 개의 다른 파라미터를 분리한다.

### `POST` 메서드
POST 메서드는 새 리소스를 추가하거나 기존 리소스를 업데이트하여 데이터를 서버로 보내는 데 사용된다. 매개 변수는 HTTP 요청의 본문에 저장된다.

```
POST /index.html HTTP/1.1
Host: example.com
name1=value1&name2=value2
```

### `DELETE` 메서드
이것은 매우 직관적이다. 서버에서 리소스를 삭제한다.

### `HEAD` 메서드
`HEAD` 메서드는 `GET` 메서드와 동일하게 작동한다. 서버에서 발송된 HTTP 응답에는 본문이 아닌 body는 없고, head만 있다. 즉, 서버가 요청에 대해 OK이면 요청한 리소스가 아닌 `200 OK` 응답을 제공한다. `GET` 메서드를 사용해야만 리소스를 검색할 수 있다. 이는 서버가 작동하는지를 테스트할 때 매우 유용하다.

### `PUT` 메서드
`PUT` 메서드는 `POST` 메서드와 유사하지만 한 가지 작은 차이가 있다. 서버에 이미 있는 리소스를 게시할 때 이 작업은 차이를 유발하지 않으며 항상 동일한 결과를 생성한다. 그러나 `PUT` 메서드는 사용자가 요청할 때마다 해당 리소스를 복제한다.

## HTML 양식(form)과 HTTP
HTTP 요청이 어떤 것인지 알았으니 이제 요청을 보내는 방법에 대해 설명해 보겠다. 가장 일반적인 방법은 HTML 양식을 사용하는 것이다. 사용자가 정보를 입력하고 매개 변수로 제출할 수 있다. 다음은 그 예이다.

```html
<form method="GET" action="example/message.html">
  <p>Name: <input type="text" name="name"></p>
  <p>Message:<br><textarea name="message"></textarea></p>
  <p><button type="submit">Send</button></p>
</form>
```

먼저 `<form>` 태그를 살펴보자. 메소드 속성은 사용할 HTTP 메소드를 지정한다. 이 경우에는 `GET`이며, 이는 매개 변수가 URL 내에 포함된다는 것을 의미한다. `<action>`은 요청하는 파일의 도메인과 경로를 지정한다. 일반적으로 서버는 사용자가 보내는 매개 변수에 따라 해당 파일에 대해 일부 작업을 수행하고 커스터마이즈된 파일을 반환한다.

`<form>` 요소 내부를 보면 사용자 입력 요소(`<input>`와 `<text area>` 모두)에 `name` 속성이 있음을 알 수 있다. name/value 쌍인 매개 변수의 이름을 정의한다. 해당 이름의 값은 사용자 입력이다. 이름은 매우 중요하므로 백엔드를 코딩할 때 이름이 일치하는지 확인해야 한다.

"Send" 버튼를 누르면 HTTP 요청은 다음과 같다.

```
GET /example/message.html?name=Jean&message=Yes%3F HTTP/1.1
```

## JavaScript와 HTTP
HTML 양식 외에도 JavaScript를 사용하여 HTTP 요청을 보낼 수 있다. 다음과 같이 `fetch()` 메서드를 사용하여 수행할 수 있다.

```js
fetch("path/to/resource").then((response) => {
  // Get the returned response status (200 OK)
  console.log(response.status);
  // Get the header of the response
  console.log(response.headers.get("Content-Type"));
});
```

기본적으로 `fetch()` 메서드는 `GET` 메서드를 사용하여 요청을 수행한다. 메서드를 지정하여 요청을 변경할 수 있다.

```js
fetch("path/to/resource", {method: "POST"}).then(...);
```

또는 헤더에 추가 정보를 넣고 다음과 같이 본문에 매개 변수를 추가한다.

```js
fetch("path/to/resource", {
  method: "POST",
  headers: {
    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
  },
  body: "name1=val1&name2=val2",
}).then(...);
```

그러나 JavaScript를 사용하여 HTTP 요청을 만드는 것은 몇 가지 보안 문제를 야기한다. 사용자와 프로그래머는 일반적으로 동일한 사람이 아니기 때문에 같은 관심사를 갖고 있지 않을 수 있다. 브라우저에 저장된 자격 증명으로 임의의 웹 페이지가 은행을 액세스하는 것을 원치 않는 것은 분명하다. 이것이 대부분의 브라우저가 기본적으로 JavaScript가 HTTP 요청을 만드는 것을 금지하는 이유이다.

이는 JavaScript 코드가 정당한 이유로 다른 도메인에 액세스하려고 할 수 있기 때문에 매우 성가실 수 있다. 이 문제를 해결하기 위해 서버는 다른 도메인에서 요청이 와도 괜찮다는 내용을 응답에 포함시킬 수 있다.

```
Access-Control-Allow-Origin: *
```

## 벡엔드에서 JavaScript
이 포스팅에서는 Node.js와 백엔드에서 JavaScript를 사용하는 방법에 대해 간략하게 소개한다. 이 섹션의 목적은 서버의 작동 방식, 서버가 프런트 엔드와 통신하는 방식을 더 잘 이해하면 향후 Django에 대한 포스팅를 준비하는 데 도움이 될 것이다.

시작하기 전에 컴퓨터에 새 폴더를 만든다. 이 포스팅에서 수행하는 모든 설치, 만들기와 업데이트가 이 폴더에서 수행되는지 확인한다.

### Node.js에 대하여
[Node.js](https://nodejs.org/en/)는 브라우저뿐만 아니라 거의 모든 플랫폼에서 JavaScript를 실행할 수 있는 JavaScript 런타임이다. 시스템에 Node.js를 설치한 후 `node`라는 프로그램을 사용하여 다음과 같이 JavaScript 파일을 실행할 수 있다.

```bash
$ node example.js
```

파일 `example.js`에 다음 코드가 있다면,

```js
console.log("Hello, World!");
```

그 결과는 아래와 같을 것이다.

```
"Hello, World!"
```

이것은 브라우저에서 JavaScript를 실행할 때 본 것과 매우 유사하기 때문에 더 이상 설명하지 않겠다.

### 패키지 설치와 관리하기
시스템에 Node.js를 설치하면 `npm`이라는 패키지 관리자도 설치된다. Linux를 사용하지 않는 경우에는 별도로 설치해야 한다. 하지만 이미 Linux를 사용하고 있다면 무엇을 하고 있는지 알고 있다고 생각한다.

npm의 주요 용도는 응용 프로그램에 필요한 JavaScript 패키지를 다운로드하고 관리하는 것이다. 패키지는 다른 사용자가 작성하고 게시한 프로그램으로, 단순히 패키지를 가져와 자신의 응용 프로그램에서 사용한다. 예를 들어, `ini`라는 패키지가 필요한 앱을 구축하는 경우 터미널에서 다음 명령을 실행할 수 있다. 정확한 폴더에 있는지 확인한다.

```bash
$ npm install ini
```

이 명령을 처음 실행할 때 `npm`은 작업 디렉토리에 세 항목을 만든다.

![node-app](images/javascript-basics/node-app.webp)

먼저 `node_modules`라는 새 폴더가 있으며, 여기에는 방금 설치한 패키지가 저장된다. 그리고 두 개의 JSON 파일 `package.json`과 `package-lock.json`이 있다. 둘 다 버전 제어에 사용된다. 이들의 차이점은 `package-lock.json`이 패키지의 정확한 버전을 저장하는 반면 `package.json`은 필요한 최소 버전뿐만 아니라 앱에 대한 다른 정보를 저장한다. 그들을 나란히 비교함으로써 그들의 차이점을 쉽게 알 수 있다.

![package](images/javascript-basics/package.webp)

방금 설치한 패키지를 사용하려면 `require()` 메서드를 호출한다.

```js
const {parse} = require("ini");
// We can perform some actions using the variable parse
```

`npm` 도구와 패키지를 관리하는 방법에 대해 자세히 알고 싶다면 여기(https://npmjs.org)에서 자세한 문서를 참조할 수 있다. 하지만 지금으로서는, 그것에 대해 너무 많이 알 필요가 없다.

백엔드 앱 구축을 시작하기 전에 소개하고자 하는 JavaScript 패키지는 `http` 모듈과 `fs` 모듈이다. `http` 모듈을 사용하여 서버를 만들고 `fs` 모듈을 사용하여 파일을 읽고 쓸 것이다. `fs` 모듈은 정보를 저장하는 데이터베이스로 사용할 것이다.

### 파일 시스템 모듈
먼저 파일 시스템(`fs`) 모듈부터 시작하겠다. 이 패키지는 Node.js에 내장되어 있으므로 이 경우에는 아무것도 설치할 필요가 없다. 대신 코드을 위한 새 `.js` 파일과 JavaScript가 읽고 쓸 .txt 파일을 만든다. 이전에 이야기했던 것처럼 모듈을 들여오겠다.

![new-js-txt-file](images/javascript-basics/new-js-txt-file.webp)

```js
// import the fs module
let { readFile } = require("fs");

// specify the file we want to read as well as the charset encoding format
readFile("data.txt", "utf8", (error, text) => {
  // If there is an error reading the file
  if (error) throw error;

  // If no error, print the content of the file
  console.log(text);
});
```

다음과 같이 파일에 쓸 수도 있다.

```js
const {writeFile} = require("fs");
writeFile("data.txt", "Hello, World? Hello, World!", error => {
  if (error) console.log(`${error}`);
  else console.log("File written.");
});
```

이 경우 인코딩 형식을 지정할 필요가 없다. `writeFile`에 문자열이 지정되면 기본 형식인 UTF-8로 간주한다.

### HTTP 모듈
또 다른 중요한 모듈은 `http`이다. JavaScript를 사용하여 HTTP 서버를 만들 수 있다. 예:

```js
const {createServer} = require("http");

let server = createServer((request, response) => {
  // If a request is recieved, return a 200 OK response along with some other information
  response.writeHead(200, {"Content-Type": "text/html"});

  // The body of the HTTP response
  response.write(`<h1>Hello, World!</h1>`);

  // The response ends
  response.end();
});

// Make the HTTP server listen on port 8000
server.listen(8000);
console.log("Listening! (port 8000)");
```

`request`와 `response` 변수는 각각 들어오는 데이터와 나가는 데이터를 저장하는 객체를 나타낸다. 예를 들어 `request.url`을 사용하여 request의 url 속성을 액세스할 수 있다.

이 예는 매우 간단하지만 실제로는 백엔드 서버가 더 복잡하다. 다음은 좀 더 도전적인 것을 시도해 보겠다. 여러분의 이름을 묻는 간단한 앱을 만들 것이고, 여러분이 이름을 제출하면, 데이터는 데이터베이스 역할을 하는 `txt` 파일에 저장될 것이다. 여러분이 웹 페이지를 다시 방문하면, 앱은 여러분의 이름으로 여러분을 맞이할 것이다.

### 간단한 앱
[여기](https://github.com/ericsdevblog/simple-node-app)에서 이 앱의 소스 코드로 다운로드할 수 있다.

#### 서버 구축
1단계에서는 데이터베이스없이 백엔드를 생성한다. `server.js`라는 이름의 새 JavaScript 파일을 생성한다.

```js
const { createServer } = require("http");

let server = createServer((request, response) => {
    request.on('data', function(){...});
    request.on('end', function(){...});
});

server.listen(8000);
console.log("Listening! (port 8000)");
```

이전 예와 매우 유사하지만, 이번에는 event listner를 사용하여 서버를 구성한다. 서버가 듣고 있는 첫 번째 이벤트는 `data`이다. 이는 HTTP 요청이 데이터를 전송할 때를 의미한다. 이 경우, 서버는 요청에서 사용해야 할 정보를 추출해야 한다. 두 번째 이벤트는 `end`이다. 즉, 요청이 데이터를 전송하지 않을 때 서버가 일부 정보로 응답해야 한다.

```js
// Initialize the variable "name"
let name = "";
request.on("data", function (chunk) {
  // "chunk" is the data being transferred
  name = name + chunk;

  // The data is in name/value pair (name1=value1)
  // So, we need to split the name and the value
  name = name.split("=");
});
```

```js
request.on('end', function(){
  response.writeHead(200, {"Content-Type": "text/html"});

  // For now, we'll use the data directly without a database,
  // just to test if the server works

  response.write(`

  <h2>Hello, ${name[1]}</h2>
  <p>What is your name?</p>
  <form method="POST" action="example/message.html">
    <p>Name: <input type="text" name="name"></p>
    <p><button type="submit">Submit</button></p>
  </form>

  `);
  response.end();
});
```

다음 명령을 사용하여 서버를 실행한다.

```bash
$ node server.js
```

브라우저를 열고 http://localhost:8000 으로 이동한다.

![undefined](images/javascript-basics/undefined.webp)

이름을 제출하고 변경 사항이 있는지 확인한다.

![with-name](images/javascript-basics/with-name.webp)

#### 데이터베이스 구축
그러나 이 데이터는 일시적이다. 서버를 다시 시작하거나 브라우저를 새로 고치면 삭제된다. 데이터를 좀 더 오래 저장하고 싶다면 어떻게 하여야 할까?

이제 `data.txt`라는 새 파일을 만들겠다. 그리고 여러분이 제출한 이름을 저장하는 데 사용할 것이다.

```js
const { createServer } = require("http");
const fileSystem = require("fs");

let server = createServer((request, response) => {
  // To make things more clear, name is used when writing to file
  // myName is used when reading from file
  let name = "";
  let myName = "";
  request.on("data", function (chunk) {
    name = name + chunk;
    name = name.split("=");
    name = name[1];

    // Write the data to data.txt
    fileSystem.writeFile("data.txt", name, function (error) {
      if (error) throw error;
    });

  });
  request.on("end", function () {
    response.writeHead(200, { "Content-Type": "text/html" });

    // Read the data from file
    fileSystem.readFile("data.txt", "utf8", (error, text) => {
      if (error) throw error;
      myName = text;
    });
    
    response.write(`
        <h2>Hello, ${myName}</h2>
        <p>What is your name?</p>
        <form method="POST" action="example/message.html">
          <p>Name: <input type="text" name="name"></p>
          <p><button type="submit">Submit</button></p>
        </form>
        `);
    response.end();

  });
});
server.listen(8000);
console.log("Listening! (port 8000)");
```

패키지를 가져올 때 구문에 주의하시오. `const { xxx} = require('xxx')`는 패키지에서 메서드를 가져오고 `const xxx = require('xxx')`는 전체 패키지를 가져오며 `xxx.methodName`을 사용하여 메서드 중 하나를 액세스할 수 있다.

이 서버를 실행하고 `data.txt` 파일을 연 경우 이름을 다시 제출한다. 데이터가 파일에 기록된 것을 볼 수 있다.

![data](images/javascript-basics/data.webp)

**주** 이 포스팅은 [JavaScript Basics #5](https://www.ericsdevblog.com/posts/javascript-basics-5/)를 편역한 것임.
