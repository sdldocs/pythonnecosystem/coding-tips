이 포스팅에서는 JavaScript에서 정규 표현식과 캔버스라는 두 가지 주제에 대해 다룰 것이다.

## 정규 표현식(regular expression)
이 포스팅에서 우리가 논의할 첫 번째 주제는 정규 표현식이라고 하는 것이다. 기술적으로 JavaScript의 일부가 아니며, 다른 프로그래밍 언어뿐만 아니라 JavaScript에 내장된 별도의 언어이다. 정규 표현식은 매우 어색하고 암호화된 구문을 가지고 있지만 매우 유용하다. 이는 프로그래머들 사이에서 문자열 데이터의 패턴을 설명하고 일치시키고 대체하는 도구로 널리 사용된다.

### 정규 표현식 만들기
정규식은 객체이다. JavaScript에서 정규 표현식을 만드는 두 가지 방법이 있다. `RegExp()` 생성자를 사용하거나 패턴을 한 쌍의 슬래시(`/`) 문자로 묶어 만든다.

```js
let re1 = new RegExp("abc");
let re2 = /abc/;
```

이 두 예는 모두 동일한 패턴을 서술한다. 문자 `a` 다음에 `b`, 다음에 `c`가 나온다. 그러나 두 번째 표기법은 백슬래시(`\`) 문자를 다르게 처리한다. 예를 들어, 순방향 슬래시는 패턴을 나타내므로 순방향 슬래시를 패턴의 일부로 사용하려면 백슬래시를 앞에 놓아야 한다(`\/`).

### 패턴(matching pattern) 일치
정규 표현식은 사용할 수 있는 몇 가지 방법을 제공하며, 문자열 데이터의 패턴을 일치시키는 데 사용되는 `test()` 메서드가 가장 일반적으로 사용된다.

```js
console.log(/abc/.test("abcde")); // → true
console.log(/abc/.test("abxde")); // → false
```

이 예에서 test() 메서드는 전달된 문자열을 검사하여 패턴과 일치 여부를 알려주는 부울 값을 반환한다.

그러나 단순히 패턴 `"abc"`가 문자열에서 발견되는지 여부를 테스트하는 것은 매우 유용하지 않다. 때때로 문자 집합을 사용하여 일치 여부를 테스트하고 싶다. 예를 들어, 문자 0에서 문자 9까지의 문자 중 **적어도** 하나가 `"in 1992"` 문자열에 있는지 여부를 다음 코드로 테스트할 수 있다.

```js
console.log(/[0123456789]/.test("in 1992")); // → true

// A hyphen character can be used to indicate a range of characters
console.log(/[0-9]/.test("in 1992")); // → true
```

세트에 없는 모든 문자를 일치시킬 수도 있다. 예를 들어, 이번에는 1 또는 0이 아닌 모든 문자와 일치시킨다.

```js
let notBinary = /[^01]/;
console.log(notBinary.test("1100100010100110")); // → false

// The string contains a character "2" which is not in the set [01]
console.log(notBinary.test("1100100010200110")); // → true
```

일반적으로 사용되는 일부 문자 집합에는 정규 표현식으로 바로 가기가 있다. 예를 들어 `\d`는 `[0-9]`와 동일한 모든 디지트 문자를 나타낸다.

- `\d` 임의의 디지트 문자
- `\w` 임의의 영문자와 숫자 문자(단어 문자)
- `\s` 임의의 공백 문자(공백, 탭, 새 줄...)
- `\D` 숫자가 아닌 문자
- `\W` 영문자와 숫자가 아닌 문자
- `\S` 공백이 아닌 임의의 문자
- `.` 새 줄을 제외한 모든 문자

이제 날짜-시간 형식(10-07-2021 16:06)을 일치시킬 수 있으며, 아래와 같이 어색하고 암호화된 것처럼 보인다.

```js
let dateTime = /\d\d-\d\d-\d\d\d\d \d\d:\d\d/;
console.log(dateTime.test("10-07-2021 16:06")); // → true
```

### 반목 패턴 일치
이전 예제에서는 각 `\d`가 한 자리 디지트 문자와 일치한다는 것을 알고 있었을 것이다. 임의의 길이의 숫자 시퀀스를 일치시키려면 어떻게 해야 할까? 반복할 요소 뒤에 더하기 기호(`+`)를 붙이면 된다.

```js
console.log(/\d+/.test("123")); // → true
console.log(/\d+/.test("")); // → false
```

별 기호(`*`)는 요소가 0번 일치하는 것을 허용한다는 점을 제외하고는 `+`와 유사한 의미를 갖는다.

```js
console.log(/\d*/.test("123")); // → true
console.log(/\d*/.test("")); // → true
```

또한 요소를 반복할 수 있는 횟수를 정확하게 나타낼 수 있다. 예를 들어 요소 뒤에 `{4}`를 지정하면 이 요소가 네 번 반복된다. 해당 원소 뒤에 `{2,4}`를 붙이면 해당 원소가 최소 두 번, 최대 네 번 반복된다는 의미이다.

```js
console.log(/\d{3}/.test("123")); // → true
console.log(/\d{3}/.test("12")); // → false
```

요소 그룹을 반복할 수도 있다. 이 요소 그룹은 소괄호 한 쌍(`()`) 안에만 포함하면 된다.

```js
let cartoonCrying = /boo+(hoo+)+/i;
console.log(cartoonCrying.test("Boohoooohoohooo")); // → true
```

경우에 따라 패턴의 일부가 선택 사항일 수 있다. 예를 들어, "neighbour" 단어의 철자를 "neighbor"로 사용할 수 있다. 즉, "u"는 선택 사항이어야 한다. 패턴은 다음과 같다.

```js
let neighbor = /neighbou?r/;
console.log(neighbor.test("neighbour"));
// → true
console.log(neighbor.test("neighbor"));
// → true
```

### 패턴을 일치시키는 다른 메서드
`test()` 메서드는 문자열에서 일치하는 패턴이 있는지 확인하는 가장 간단한 방법이다. 그러나 일치하는 항목이 있는지 여부를 알려주는 부울 값을 반환하는 것 외에는 많은 정보를 제공하지 않는다. 정규 표현식에는 일치하는 항목과 일치하는 위치 등과 같은 추가 정보를 제공하는 객체를 반환하는 `exec()` 메서드(`exec`은 execute을 나타낸다)도 있다.

```js
let match = /\d+/.exec("one two 100");
console.log(match); // → ["100"]

// The index property tells you where in the string the match begins
console.log(match.index); // → 8
```

문자열 타입에 속하는 `match()` 메서드도 있으며, 이 메서드는 위의 `exec()`과 유사하게 동작한다.

```js
console.log("one two 100".match(/\d+/)); // → ["100"]
```

`exec()` 메서드는 실제로 매우 유용할 수 있다. 예를 들어 다음과 같은 문자열에서 날짜와 시간을 추출할 수 있다.

```js
let [_, month, day, year] = /(\d{1,2})-(\d{1,2})-(\d{4})/.exec("1-30-2021");
```

밑줄(`_`)은 무시되며 `exec()` 메서드에서 반환되는 전체 일치를 건너뛸 때 사용된다.

하지만, 이제 위의 예로부터 또 다른 문제를 갖고 있다. `"100-1-3000"`과 같은 일련의 넌센스를 `exec()` 메서드에 전달하면 여전히 즐겁게 날짜를 추출한다.

이 경우 일치 항목이 전체 문자열에 걸쳐 있어야 한다. 이를 위해 경계 마커 `^`와 `$`를 사용한다. 캐럿 기호(`^`)는 문자열의 시작을 나타내고 달러 기호(`$`)는 문자열의 끝에 일치한다. 예를 들어 `/^\d$/` 패턴은 한 자리 디지트 문자로만 구성된 문자열과 일치한다.

때로는 일치하는 것이 전체 문자열이 아니라 문자열이 한 문장이 될 수 있으며 일치하는 것이 해당 문장의 단어가 되도록 할 수 있다. 단어 경계를 표시하기 위해 `\b` 마커를 사용한다.

```js
console.log(/cat/.test("concatenate")); // → true
console.log(/\bcat\b/.test("concatenate")); // → false
```

이 주제를 마치기 전에 마지막으로 소개할 패턴 타입은 선택 패턴이다. 때때로 특정 패턴을 일치시키고 싶지 않고, 대신에 허용되는 패턴 목록을 갖고 싶을 수 있다. 파이프 문자(`|`)를 사용하여 서로 다른 패턴을 나눌 수 있다.

```js
let animalCount = /\b\d+ (pig|cow|chicken)s?\b/;
console.log(animalCount.test("15 pigs")); // → true
console.log(animalCount.test("15 pigchickens")); // → false
```

만약 여러분이 정규 표현식에 대해 더 알고 싶다면, 매우 흥미로운 [웹사이트](https://regexr.com/)를 소개한다.

### 패턴 바꾸기
`match()` 메서드 외에도 문자열에 문자열의 일부를 다른 문자열로 바꾸는 `replace()` 메서드도 있다.

```js
console.log("papa".replace("p", "m"));
// → mapa
```

`replace()` 메서드의 첫 번째 인수는 정규 표현식일 수 있으며, 이 경우 해당 정규식의 첫 번째 일치가 두 번째 인수로 바뀐다. 정규식의 일치 항목을 모두 바꾸려면 해당 정규 표현식에 `g` 옵션(글로벌 옵션)을 추가한다.

```js
console.log("Borobudur".replace(/[ou]/, "a")); // → Barobudur
console.log("Borobudur".replace(/[ou]/g, "a")); // → Barabadar
```

## 캔버스(canvas)
HTML과 CSS에 대해 설명할 때 SVG라는 것을 간단히 소개했던 것을 기억하나요? HTML 태그를 사용하여 아름다운 이미지를 만들 수 있다. 오늘, 웹 페이지에 그래픽을 만들기 위해 JavaScript를 사용할 수 있다는 것을 제외하고 캔버스라는 비슷한 것을 소개하려 한다. 그리고 단순한 마크업 언어 대신 프로그래밍 언어를 사용하기 때문에 SVG에 비해 캔버스를 훨씬 더 유연하고 강력하게 만든다.

SVG는 DOM 트리 구조를 가지고 있으며 모양, 색상과 위치가 모두 HTML 태그를 사용하여 표현된다고 알고 있다. 그러나 캔버스는 하나의 HTML 노드이지만 웹 페이지의 공간을 캡슐화하여 JavaScript를 사용하여 아름다운 예술 작품을 만들 수 있다. `<canvas>` 태그를 사용하여 이 공간을 정의할 수 있다. 캔버스 공간 내부에 단순한 직사각형을 만드는 예는 아래와 같다.

```html
<canvas width="300px" height="200px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  // Define the colour of the rectangle
  context.fillStyle = "red";

  // The first two parameters means that the top left corner of the ractagle is at coordinate (10, 10)
  // The last two parameters define the width and height of the ractangle (width:100px, height:50px)
  context.fillRect(10, 10, 100, 50);
</script>
```

디지털 펜과 연필이 저장된 도구 상자와 같은 도면 인터페이스를 액세스하는 데 `getContext()` 메서드가 사용된다. 매개 변수 `"2d"`는 2차원 그래픽을 나타낸다. 3차원 그래픽을 만드는 데 관심이 있다면 [WebGL](https://www.khronos.org/webgl/)을 대신 사용해야 한다. 하지만 현재 2D 시스템에만 집중하도록 한다.

또한 처음에는 캔버스 크기를 정의했다. 그렇지 않으면 캔버스 요소의 기본 너비는 300픽셀이고 높이는 150픽셀이 된다.

### 라인(lines)
방금 만든 직사각형의 네변은 실선이며, 직사각형 내부는 채워져 있다. 뭔가 다른 것을 원한다면? 또한 매우 유사한 메서드인 `strokeRect()`를 사용하여 윤곽만 있는 직사각형을 만들 수 있다. 이 메서드는 네 가지 매개변수를 사용한다. 처음 두 매개변수는 위치를 정의하고 마지막 두 매개변수는 크기를 정의한다.

```html
<canvas></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  // Define the color, position and size
  context.strokeStyle = "blue";
  context.strokeRect(10, 10, 100, 50);

  // Define the width of the strok and create a new rectangle
  context.lineWidth = 5;
  context.strokeRect(150, 10, 100, 50);
</script>
```

![canvas-rect](images/javascript-basics/canvas-rect.webp)

### 경로(Paths)
여러분은 SVG를 사용하여 직사각형을 쉽게 만들 수 있다는 사실에 대해 궁금해하실 것이다. 걱정하지 마세요, 캔버스의 진정한 힘은 지금부터 시작된다.

먼저, 경로가 무엇인지 이해해야 한다. 경로는 선 세그먼트의 시퀀스이다. 예를 들어 좌표(0, 0)에서 시작하여 (0, 50)까지인 선, (0, 50)에서 시작하여 (80, 50)까지인 두 번째 선, (80, 50)에서 시작하여 (80, 100)까지인 세 번째 선이 있다. 이 세 개의 선 세그먼트가 경로를 형성한다.

캔버스를 통해 다음과 같은 작업을 수행할 수 있다.

```html
<canvas width="500px" height="500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  context.lineWidth = 5;
  context.strokeStyle = "green";

  context.beginPath();

  // The path starts at (10, 10)
  context.moveTo(10, 10);

  // Drawing the path: (10, 10) -> (150, 10) -> (150, 150) -> (10, 150) -> (10,10)
  context.lineTo(150, 10);
  context.lineTo(150, 150);
  context.lineTo(10, 150);
  context.lineTo(10, 10);

  context.stroke();
</script>
```

경로를 통해 원하는 모양을 만들 수 있다. 예를 들어, 다음 코드는 삼각형을 만든다.

```html
<canvas width="500px" height="500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  context.beginPath();

  context.fillStyle = "red";

  context.moveTo(200, 10);
  context.lineTo(250, 100);
  context.lineTo(150, 100);
  context.lineTo(200, 10);

  context.fill();
</script>
```

![canvas-triangle](images/javascript-basics/canvas-triangle.webp)

### 곡선(curves)
경로를 직선으로 형성할 수 있고, 곡선으로 형성될 수도 있다. 하지만 곡선은 정의하기가 조금 더 어렵다. 곡선을 정의하려면 시작점, 끝점 및 제어점이 필요하다. 곡선은 제어점을 직접 통과하지 않고 시작점과 끝점의 접선이 통과하는 점을 정의한다.

이것은 이해하기가 조금 어렵다. 포토샵의 펜툴이나 GIMP의 경로툴에 먼저 익숙해지길 제안한다. 코드를 작성할 때를 제외하고는 동일한 개념을 공유한다. 곡선이 어떻게 생겼는지 상상해야 한다.

여기 다른 예가 있다. 먼저 곡선을 그린 다음 접선과 제어점을 그리면 여기서 무슨 일이 일어나고 있는지 이해하는 데 도움이 된다.

```html
<canvas width="500px" height="500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  context.beginPath();

  // start point = (10, 90)
  context.moveTo(10, 90);

  // control point = (60,10); destination point = (90,90)
  context.quadraticCurveTo(60, 10, 90, 90);

  // destination point tangent
  context.lineTo(60, 10);

  // start point tangent
  context.moveTo(10, 90);
  context.lineTo(60, 10);

  context.closePath();
  context.stroke();
</script>
```

![curve](images/javascript-basics/curve.webp)

시작점과 끝점의 접선이 교차하는 제어점과 다른 제어점이 있는 경우도 있다. 또한 `bezierCurveTo()` 방법을 사용하여 곡선을 그릴 수도 있다.

```html
<canvas width="500px" height="500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  context.beginPath();

  // start point = (10, 90)
  context.moveTo(10, 90);

  // start control point = (60,10); destination control point = (30,80); destination point = (90,90)
  context.bezierCurveTo(60, 10, 30, 80, 90, 90);

  // destination point tangent
  context.lineTo(30, 80);

  // start point tangent
  context.moveTo(10, 90);
  context.lineTo(60, 10);

  context.closePath();
  context.stroke();
</script>
```

![curve-control-point](images/javascript-basics/curve-control-point.webp)

### 텍스트
텍스트는 그래프를 만들 때도 유용할 수 있다. `fillText`와 `strokeText`를 사용하여 텍스트를 그릴 수 있다. 후자는 텍스트를 채우는 대신 텍스트의 윤곽만 렌더링한다.

```html
<canvas width="1500px" height="500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  context.font = "28px Georgia";

  context.fillText(
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    10,
    50
  );

  context.strokeText(
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    10,
    100
  );
</script>
```

![canvas-texts](images/javascript-basics/canvas-texts.webp)

지막 두 매개변수는 텍스트의 위치를 나타내지만 도면 모양과 달리 텍스트 기준선의 시작 좌표를 정의한다. 기준선은 텍스트가 서있는 선이다.

### 변환(transformations)
변환에는 크게 `translate()`, `scale()`과 `rotate()`의 세 가지 타입이 있다. 이러한 메서드는 변환할 그래프 앞에 놓아야 한다.

`translation()`는 그래프를 한 위치에서 다른 위치로 이동시킨다.

```html
<canvas width="1500px" height="1500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  // Move whatever graph created after to the right for 50px and downward for 100px
  context.translate(50, 100);

  // Create a graph
  context.beginPath();

  context.fillStyle = "red";

  context.moveTo(200, 10);
  context.lineTo(250, 100);
  context.lineTo(150, 100);
  context.lineTo(200, 10);

  context.fill();
</script>
```

`scale()`은 원본 그래프를 더 크게 또는 더 작게 만든다.

```html
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  // Make the graph 2 times wider (along x-axis) 0.5 time shorter (along y-axis)
  context.scale(2, 1/2);

  // Create a graph
  ...
</script>
```

마지막으로 `rotate()`는 축을 따라 그래프를 회전할 수 있다.

```html
<canvas width="1500px" height="1500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  // Rotate the graph clockwise for 18 degrees. Notice that the rotate() method takes radian instead of degree.
  context.rotate(0.1 * Math.PI);

  // Create a graph
  ...
</script>
```

### 비트맵 그래픽
컴퓨터 그래픽에는 벡터 그래픽과 비트맵 그래픽이라는 것이 있다. 우리가 지금까지 이야기한 모든 그래프는 벡터 그래픽이다. 이들의 주요 차이점은 비트맵 그래픽은 픽셀로 형성되는 반면 벡터 그래픽은 픽셀로 형성되지 않고, 대신 방향과 크기(길이)를 가진 벡터와 같은 경로에 의해 형성된다.

그러나 벡터 그래픽 디자인에 비트맵 그래픽을 삽입할 필요가 있다. `drawImage()` 메서드를 사용하여 이 작업을 수행할 수 있다.

```html
<canvas width="1500px" height="1500px"></canvas>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("2d");

  let img = document.createElement("img");
  img.src = "cat.jpg";

  img.addEventListener("load", () => {
    context.drawImage(img, 10, 10, 360, 240);
  });
</script>
```

위의 예에서 360px * 240px 크기의 좌표(10, 10)에 이미지가 그려진다.

event listner를 추가해야 한다. 이것이 없으면 캔버스 뒤에 이미지가 로드되므로 캔버스에서 먼저 이미지가 로드될 때까지 기다려야 한다.

**주** 이 포스팅은 [JavaScript Basics #4](https://www.ericsdevblog.com/posts/javascript-basics-4/)를 편역한 것임.
