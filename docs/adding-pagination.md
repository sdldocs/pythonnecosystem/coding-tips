이 섹션에서는 기사 목록 페이지에 페이지 매김을 추가하겠다.

## XAMPP 시작과 가상 환경 활성화

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## 기사 목록 보기에 페이지 매김 추가하기

### 페이지 매김
Django는 페이지가 지정된 데이터, 즉 'Previous과 Next” 링크를 사용하여 여러 페이지에 걸쳐 분할된 데이터를 관리하는 데 도움이 되는 상위와 하위 수준 방법을 제공한다.

#### 페이지 매김 클라스
내부적으로 모든 페이지 매김 메서드는 `Paginator` 클래스를 사용한다. 이 클래스는 실제로 쿼리 집합을 페이지 객체로 분할하는 모든 힘든 작업을 수행한다.

예: 페이지 매기기에서 객체 목록과 각 페이지에 포함할 항목의 수를 지정하면 각 페이지의 항목을 액세스하는 메서드가 제공된다.

```
from django.core.paginator import Paginator
objects = [“john”, “paul”, “george”, “ringo”]
p = Paginator(objects, 2)

p.count
Output: 4

p.num_pages
Output: 2

p.page_range
Output: range(1, 3)

page1 = p.page(1)
Output: page1

page1.object_list
Output:[‘john’, ‘paul’]

page2 = p.page(2)
page2.object_list
Output: [‘george’, ‘ringo’]

page2.has_next()
Output:False

page2.has_previous()
Output: True

page2.has_other_pages()
Output:True

page2.next_page_number()
Output:
Traceback (most recent call last):

…
EmptyPage: That page contains no results
```

### `blog/views.py`에 `paginator` 불러오기

`blog/views.py` 파일을 열고 `django.core.paginator` 패키지에서 `Paginator` 클래스를 가져온다.

```python
from django.core.paginator import Paginator
```

![](images/adding-pagination/screenshot_07_01.webp)

### 페이지당 2 기사 2을 출력하는 페이지 매김 추가하기
기사 객체를 전달하고 기사 수를 한 페이지에 표시해야 한다는 관점에서 `Paginator` 클래스를 인스턴스화한다.

다음으로 페이지 `GET HTTP` 매개변수를 검색하여 `page_number` 변수에 보관한다. 이 매개변수에는 요청된 페이지 번호가 포함된다. 페이지 매개변수가 요청의 GET 매개변수에 없는 경우 기본값 1을 사용하여 결과의 첫 페이지를 로드한다.

다음으로 필수 페이지 번호로 `page()` 메서드를 호출한다. 오류를 피하기 위해 시도 예외 블록에 기사 객체를 유지한다. `page_number`가 범위를 벗어나거나 페이지에 결과가 없는 경우 `EmptyPage` 예외를 사용하여 결과의 마지막 페이지에 표시한다.

`blog/views.py` 파일을 열고 다음 코드를 작성한다.

```python
from django.core.paginator import Paginator, EmptyPage

def list_of_articles(request):
    articles = Article.publishedArticles.all()
    
    paginator = Paginator(articles, 2)
    page_number = request.GET.get('page', 1)
    try:
        articles = paginator.page(page_number)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    return render(request, 'blog/list.html', {'articles': articles})
    pass
```

![](images/adding-pagination/screenshot_07_02.webp)

## 페이지 매김 탬플릿 만들기
`blog/template/blog/` 폴더에 `pagination.html`이라는 파일을 만든다.

```html
<div class="pagination">
    <span class="pagination-links">

        {% if page.has_previous %}
            <a href="?page={{ page.previous_page_number }}">Previous</a>
        {% endif %}
        
        <span class="current">
            Page {{ page.number }} of {{ page.paginator.num_pages }}.
        </span>

        {% if page.has_next %}
            <a href="?page={{ page.next_page_number }}">Next</a>
        {% endif %}
    </span>
</div>
```

![](images/adding-pagination/screenshot_07_03.webp)

### `blog/template/blog/list.html` 파일에 페이지 매김 템플릿을 포함
```html
{% include "blog/pagination.html" with page=articles %}
```

![](images/adding-pagination/screenshot_07_04.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

개발 서버를 실행한 다음 아래 링크를 방문한다,

`http://127.0.0.1:8000/blog/`

![](images/adding-pagination/screenshot_07_06.png)

2개의 기사 다음 페이지 매김 링크가 표시된다. 다음을 클릭하면 다음 페이지가 표시된다.

![](images/adding-pagination/screenshot_07_07.png)

### 없는 패스 페이지 번호
`page_number 5`를 전달하고 어떤 일이 발생하는지 살펴보자.

`http://127.0.0.1:8000/blog/?page=5`

![](images/adding-pagination/screenshot_07_08.png)

우리가 있던 페이지와 동일한 페이지가 표시되는 것을 볼 수 있습니다. 페이지 번호 대신 몇 가지 텍스트를 전달해 보겠습니다.

`http://127.0.0.1:8000/blog/?page=test`

![](images/adding-pagination/screenshot_07_09.png)

오류가 발생했으며 예외 타입은 `PageNotInteger`이다. 이 문제를 해결하여 보다.

## `blog/views.py`에 `PageNotInteger` 오류 수정
`django.core.paginator`에서 `PageNotAnInteger` 가져오기

```python
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
```

요청된 페이지가 없는 경우 첫 페이지를 전달한다.

```python
# Create your views here.
def list_of_articles(request):
    articles = Article.publishedArticles.all()
    
    paginator = Paginator(articles, 3)
    page_number = request.GET.get('page', 1)
    try:
        articles = paginator.page(page_number)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        articles = paginator.page(1)

    return render(request, 'blog/list.html', {'articles': articles})
    pass
```

![](images/adding-pagination/screenshot_07_05.webp)

Refresh the browser and check same url again:

`http://127.0.0.1:8000/blog/?page=test`

브라우저를 새로고침하고 동일한 URL을 다시 확인한다.

![](images/adding-pagination/screenshot_07_10.png)

이번에는 오류가 발생하지 않았다. 여기까지이다. 최신 변경 사항을 github 리포지토리에 푸시하겠다.

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Added pagination to the article list page"
$ git push origin main
```

![](images/adding-pagination/cli_07_01.png)

이 파트는 여기까지...

**(주)** 위 내용은 [Add pagination to the article list view — Part 7](https://medium.com/p/5a202a363c75)을 편역한 것이다.
