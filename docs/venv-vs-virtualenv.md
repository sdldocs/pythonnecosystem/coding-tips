Python에서 가상 환경을 생성하는 경우 일반적으로 `venv`와 `virtualenv`라는 두 가지 옵션이 있다. 두 도구는 모두 Python 프로젝트를 위한 격리된 환경을 만드는 동일한 용도로 사용된다. 그러나 고려해야 할 몇 가지 차이점이 있다.

## 환경에 대한 간단한 소개
Python 가상 환경은 시스템에 설치된 Python 또는 다른 프로젝트를 방해하지 않고 Python 패키지와 종속성을 설치할 수 있는 독립적인 공간이다.

즉, (주 시스템 파이썬에 영향을 미치지 않고) 다른 버전의 특정 라이브러리를 가질 수 있는 별도의 Python으로 생각할 수 있다

이 기능은 여러 버전의 Python 또는 시스템 설정과 호환되지 않는 패키지로 작업해야 할 경우 특히 유용하다.

## Venv — 간단한 소개
`venv`는 Python 3.3 이상 버전과 함께 제공되는 기본 제공 모듈이다. Python 프로젝트를 위한 가상 환경을 만드는 가볍고 사용하기 쉬운 도구이다. `venv`는 터미널에서 다음 명령을 실행하여 사용한다.

```bash
python3 -m venv /path-to-new-virtualenvironment
```

이 명령은 지정된 디렉토리에 새 가상 환경을 만든다.

그런 다음 아래 명령으로 가상 환경을 활성화할 수 있다.

```bash
# MacOS or linux
source /path-to-new-virtualenvironment/bin/activate

# Windows
/path-to-new-virtualenvironment/Scripts/activate.bat
```

`venv`는 `virtualenv`에 비해 다음과 같은 몇 가지 이점이 있다.

- 가상 환경 생성과 활성화 시간 단축
- 보다 안전하고 최신의 구현
- 보다 단순하고 직관적인 인터페이스
- Python의 패키지 관리자 pip에 대한 기본 제공 지원

## Virtualenv — 간단한 소개
`virtualenv`는 `venv`와 유사한 기능을 제공하는 서드 파티 패키지입니다. 파이썬 2와 3 모두와 호환되며 `pip`을 사용하여 설치할 수 있다.

```bash
$ pip install virtualenv
```

`virtualenv`를 사용하여 가상 환경을 생성하려면 터미널에서 다음 명령을 실행한다.

```bash
$ virtualenv /path-to-new-virtualenvironment
```

이 명령은 지정된 디렉토리에 새 가상 환경을 만든다. 다음을 실행하여 환경을 활성화할 수 있다.

```bash
# MacOS or linux
source /path-to-new-virtualenvironment/bin/activate

# Windows
/path-to-new-virtualenvironment/Scripts/activate.bat
```

virtualenv는 venv보다 오래 사용되었으며 사용자 기반이 더 크다. 일부 기능은 다음과 같다.

- 이전 버전의 Python과의 호환성
- 커스텀 Python 인터프리터를 사용하여 가상 환경을 생성할 수 있는 기능
- 가상 환경 설정에 대한 제어 강화

## venv을 사용하는 경우
- Python 3.3 이상 버전을 사용하고 있을 때
- 가볍고 사용하기 쉬운 도구를 원하는 경우
- 단지 다른 것에 너무 신경 쓰지 않고 환경을 만들고 싶을 때
- pip에 대한 기본 제공 지원이 필요하다

## virtualenv을 사용하는 경우
- Python 3.3 이전 버전을 사용하고 있을 때
- 커스텀 Python 인터프리터(Python의 다양한 버전)를 사용하여 가상 환경을 생성하려는 경우

다음과 같은 경우 virtualenv을 사용:

- 이전 버전의 Python을 사용하고 있을 때
- 커스텀 Python 인터프리터(Python의 다른 버전)를 사용하여 가상 환경을 생성하려는 경우
- 가상 환경 설정에 대한 보다 강력한 제어력을 원하는 경우

## 마치며
뭔가 얻었기를...

**(주)** 위 내용은 [Venv vs Virtualenv in Python — Which To Use?](https://levelup.gitconnected.com/venv-vs-virtualenv-in-python-which-to-use-3708d93a95e8)을 편역한 것이다.
