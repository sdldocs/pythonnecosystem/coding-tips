이 포스팅에서, 플로트, 변환 및 플렉스박스를 포함한 CSS에서 비교적 발전된 몇 가지 주제에 대해 다룰 것이고, 마지막으로 매우 인기 있는 두 가지 CSS 프레임워크인 Bootstrap과 Tailwind CSS를 간략하게 소개할 것이다.

## CSS에서 `float` 속성
`float` 속성은 HTML 요소를 배치하는 데 사용되는 또 다른 CSS 속성이다. 요소가 "부동(float)"하는 방법을 지정한다. 예를 들어 이미지는 컨테이너의 텍스트 왼쪽 또는 오른쪽으로 이동할 수 있다. `float` 속성은 다음 값 중 하나를 가질 수 있다.

- `left`: 요소가 컨테이너의 왼쪽으로 이동한다
- `right`: 요소가 컨테이너의 오른쪽으로 이동한다
- `none`: 요소가 움직이지 않는다(텍스트에서 발생하는 위치에만 표시됨). 기본값이다
- `inherit`: 요소가 상위 요소의 부동 값을 상속받는다

```html
<div>
  <img src="image.png" />
  Lorem ipsum...
</div>
```

![no-float](images/css-basics/no-float.png)

기본적으로 두 개의 `<div>` 요소가 서로 위에 표시된다. 그러나 `float: left`를 사용하여 그것들을 서로 옆에 띄울 수 있다.

```html
<div class="div1">Div #1</div>
<div class="div2">Div #2</div>
```

```css
.div1 {
  float: left;
  background-color: red;
  padding: 15px;
}

.div2 {
  float: left;
  background-color: green;
  padding: 15px;
}
```

![float-next-each-other](images/css-basics/float-next-each-other.png)

## CSS에서 `transform` 속성
`transform` 속성을 사용하여 요소를 이동, 회전, 크기 변경(scale) 및 스큐할 수 있다. 다음과 같은 변환 방법을 사용할 수 있습니다:

`translate()` 메서드는 X축 및 Y축에 지정된 파라미터에 따라 요소를 현재 위치에서 이동한다. 예를 들어, 다음 코드는 `<div>` 요소를 현재 위치에서 오른쪽으로 50px, 아래로 100px 이동시킨다.

```css
div {
  transform: translate(50px, 100px);
}
```

`rotate()` 메서드는 주어진 정도에 따라 요소를 시계 방향(양) 또는 시계 반대 방향(음)으로 회전시킨다. 아래 예는 `<div>` 요소를 시계 방향으로 20도 회전시키고 `<section>` 요소를 시계 반대 방향으로 20도 회전시키다.

```css
div {
  transform: rotate(20deg);
}

section {
  transform: rotate(-20deg);
}
```

`scale()` 메서드는 요소의 크기를 늘리거나 줄인다. 첫 번째 파라미터는 폭에 대응되고 두 번째 파라미터는 높이에 대응된다. 아래 코드는 `<div>` 요소를 원래 너비의 두 배, 원래 높이의 세 배로 늘린다.

```css
div {
  transform: scale(2, 3);
}
```

`skew()` 메서드는 X축과 Y축을 따라 지정된 각도로 요소를 스큐한다.

```css
div {
  transform: skew(20deg, 10deg);
}
```

![skew](images/css-basics/skew.png)

`metrix()` 메서드는 위의 모든 방법의 조합이다. 요소를 회전, 축척, 이동(변환) 및 스큐할 수 있는 6개의 파라미터가 필요하다. 파라미터는 `matrix(scaleX(),skewY(),skewX(),scaleY(),translateX(),translateY())`이며, 여기서 X는 수평 축을 의미하고 Y는 수직 축을 의미한다.

```css
div {
  transform: matrix(1, -0.3, 0, 1, 0, 0);
}
```

## CSS에서 플렉스박스에 대한 논의
플렉스박스는 플로트나 위치를 사용하지 않고도 반응형 레이아웃을 쉽게 설계할 수 있는 또 다른 레이아웃 모듈이다. 그리드 시스템과 마찬가지로 각 플렉스박스에는 플렉스 컨테이너와 여러 플렉스 항목이 있다.

```html
<div class="flex-container">
  <div>1</div>
  <div>2</div>
  <div>3</div>
</div>
```

이 `<div>` 블록은 `display` 속성을 `flex`로 설정하여 플렉스박스로 만든다(플렉시블이 된다).

```css
.flex-container {
  display: flex;
}
```

![flexbox](images/css-basics/flexbox.png)

### 플렉스 컨테이너
#### `flex-direction`
`flex-direction` 속성은 컨테이너가 flex 항목을 쌓을 방향을 정의한다. 기본값은 `row`로, 위의 예와 같이 항목이 왼쪽에서 오른쪽으로 나열된다.

또한 수직으로 배치할 수도 있다.

```css
.flex-container {
  display: flex;
  flex-direction: column;
}
```

![flex-col](images/css-basics/flex-col.png)

또는 역방향으로

```css
.flex-container {
  display: flex;
  flex-direction: column-reverse;
}
```

![flex-col-reversed](images/css-basics/flex-col-reversed.png)

#### `flex-wrap`
`flex-wrap` 속성은 flex 항목을 래핑할지 여부를 지정한다(공간이 부족할 경우 항목을 새 행에 자동으로 배치).

`flex-wrap`이 `wrap`으로 설정된 경우.

![flex-wrap](images/css-basics/flex-wrap.png)

`flex-wrap`이 `no-wrap`으로 설정되었을 때.

![flex-no-wrap](images/css-basics/flex-no-wrap.png)

#### 정렬 속성
그리드 시스템과 마찬가지로 플렉스박스에도 정렬 속성이 있으며 정확히 동일하게 작동한다. `justify-content` 속성은 플렉스 항목을 수직으로 정렬하는 데 사용되고 `akignment-items` 속성은 플렉스 항목을 수평으로 정렬하는 데 사용된다.

### 플렉스 항목
#### `order`
`oreder` 속성은 플렉스 항목의 순서를 지정한다.

```html
<div class="flex-container">
  <div style="order: 3">1</div>
  <div style="order: 2">2</div>
  <div style="order: 4">3</div>
  <div style="order: 1">4</div>
</div>
```

![flex-order](images/css-basics/flex-order.png)

#### `flex-grow`
`flex-grow` 속성을 설정하면 브라우저 크기를 조정할 때 플렉스 항목이 항상 뷰포트 가장자리까지 확장되며, 이 숫자는 항목이 플렉스박스의 다른 항목에 비해 얼마나 빨리 증가하는지를 나타낸다.

```css
<div class="flex-container">
  <div style="flex-grow: 1">1</div>
  <div style="flex-grow: 1">2</div>
  <div style="flex-grow: 8">3</div>
</div> 
```

![flex-grow](images/css-basics/flex-grow.png)

#### `flex-basis`
`flex-basis` 속성은 플렉스 항목의 초기 길이를 지정한다.

```css
<div class="flex-container">
  <div>1</div>
  <div>2</div>
  <div style="flex-basis: 200px">3</div>
  <div>4</div>
</div>
```

## 이미지 마스크
웹 사이트를 설계할 때 사용자가 프로필 사진으로 이미지를 업로드하도록 하려면 이 시나리오를 고려하여야 한다. 프로필 사진은 작은 사각형으로 표시되지만 업로드된 이미지의 크기는 다르다. 이미지의 너비와 높이를 설정하기만 하면 늘어지고 이상하게 보이는 프로필 사진이 생성된다. 이 문제를 어떻게 해결할 수 있을까?

![kitten](images/css-basics/kitten.png)

귀여운 아기 고양이의 이미지가 있다. CSS를 사용하여 크기를 `200px`x`200px` 이미지로 조정할 수 있다.

```css
img {
  width: 200px;
  height: 200px;
} 
```

![resized-image](images/css-basics/resized-image.png)

보는 것처럼, 이미지가 늘어나서 별로 좋아 보이지 않는다.

여기에 `object-fit` 속성이 있다. `object-fit` 속성은 다음 값 중 하나를 사용할 수 있다.

- `fill`: 기본값이다. 이미지의 크기가 지정된 치수에 맞게 조정된다. 필요한 경우 이미지가 늘어나나 스퀴즈되어 적합하게 된다
- `contain`: 이미지가 가로 세로 비율을 유지하지만 지정된 치수 내에 맞도록 크기가 조정된다
- `cover`: 이미지는 가로 세로 비율을 유지하고 지정된 치수를 맞춘다. 이미지가 적합하도록 잘린다
- `none`: 이미지 크기가 조정되지 않는다
- `scale-down`: 이미지가 가장 작은 버전의 없음 또는 포함으로 축소된다

![image-mask-table](images/css-basics/image-mask-table.png)

이 이미지를 프로필 사진으로 사용하려면 `object-fit: cover`를 사용하는 것이 가장 좋다.

그러나 이제 또 다른 문제에 직면하게 되었다. 이 옵션은 이미지의 중앙을 확대하여 컨테이너를 채우고 고양이의 일부를 클리핑한다. 고양이를 확대하고 싶다면?

`object-position` 속성은 이 문제를 해결하는 데 도움이 될 수 있다. 컨테이너 내에서 `<img>` 또는 `<video>`를 배치하는 방법을 지정하는 데 사용된다.

```css
img {
  width: 200px;
  height: 200px;
  object-fit: cover;
  object-position: 100% 100%;
}
```

![object-position](images/css-basics/object-position.png)

첫 번째 파라미터는 x축의 위치를 결정하고 두 번째 파라미터는 y축의 위치를 결정한다.

## 유용한 CSS 프레임워크
마지막으로, CSS 기초 포스팅을 끝내기 전에 웹 페이지 설계 프로세스를 크게 가속화할 수 있는 매우 유용한 CSS 프레임워크에 대해 간략하게 설명해 보겠다. CSS 프레임워크는 기본적으로 미리 정의된 CSS 파일을 웹 페이지로 가져와서 HTML 문서 내에 정의된 CSS 클래스를 사용할 수 있다.

[Bootstrap](https://getbootstrap.com/)은 세계에서 가장 인기 있는 응답성이 뛰어난 프런트 엔드 프레임워크 중 하나이다. 원래 트위터의 디자이너와 개발자에 의해 만들어졌지만, 현재 GitHub의 소규모 개발자 팀에 의해 유지되고 있다.

Bootstrap을 사용하려면 [공식 웹 사이트](https://getbootstrap.com/)에서 컴파일된 코드를 다운로드한 다음 HTML 문서에 파일을 로드해야 한다.

또는 더 쉬운 방법은 다음과 같이 CDN을 사용하는 것이다.

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="author" content="Eric Hu" />
  <title>My HTML Document</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  
  <link rel="stylesheet" href="myCSS.css" />
</head>

<body>
  <img src="image.jpg">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>
```

CSS 파일은 앞에서 이야기한 바와 같이 `<head>` 섹션에서 가져와야 하며, JavaScript 파일은 `<body>` 요소의 닫는 태그 앞에 있다. 예, 프레임워크가 제대로 작동하려면 JavaScript 코드를 가져와야 하지만 다행히도 현재로서는 어떻게 작동하는지 이해할 필요가 없다.

Bootstrap 프레임워크를 사용하려면 다음과 같이 HTML 요소 내에 정확히 클래스 이름을 추가하기만 하면 된다.

```html
<button type="button" class="btn btn-primary">Primary</button>
```

이 예에서 `btn` 클래스는 버튼를 정의하고 `btn-primary`는 버튼의 색상을 정의한다.

![bootstrap-primary-button](images/css-basics/bootstrap-primary-button.png)

사용할 수 있는 많은 다른 구성 요소들이 있고, 그것들은 모두 아름답게 디자인되었으며, Bootstrap의 [공식 문서](https://getbootstrap.com/docs/5.2/getting-started/introduction/)에서 그것들을 찾을 수 있다.

소개하고자 하는 두 번째 프레임워크는 [Tailwind CSS](https://tailwindcss.com/)인데, 이는 비교적 새로운 분야이지만 최근에 빠르게 시장을 넓혀가고 있다. Bootstrap과는 달리 유틸리티 우선 프레임워크이며 복사하여 붙여넣기만 하면 되는 구성 요소가 함께 제공되지 않는다. 만약 버튼을 원한다면, 그것을 직접 디자인해야 한다.

```html
<button class="inline px-6 py-2 w-fit rounded-md bg-blue-500 text-white font-bold text-sm uppercase">Button</button>
```

![tailwind-button](images/css-basics/tailwind-button.png)

이것이 훨씬 더 많은 추가 작업이 필요한 것처럼 보이지만, 장점은 훨씬 더 많은 자유를 주고 원하는 대로 요소를 디자인할 수 있다는 것이다. 하지만 Tailwind를 사용하기 위해서는 CSS에 대한 충분한 숙지가 필요하다.

Tailwind CSS의 또 다른 장점은 [공식 문서](https://tailwindcss.com/docs/installation)에 명시된 대로 `npm`을 사용하여 설치하면 Tailwind가 전체 프로젝트를 스캔하고 사용 중인 CSS 클래스를 확인하고 커스텀 CSS 파일을 생성할 수 있다는 것이다. 이는 CSS 결과 파일이 Bootstrap에 비해 훨씬 작다는 것을 의미한다.

**주** 이 포스팅은 [CSS Basics #3](https://www.ericsdevblog.com/posts/css-basics-3/)를 편역한 것임.
