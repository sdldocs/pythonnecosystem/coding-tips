이 섹션에서는 모든 기사를 디스플레이하는 view를 만들겠다. 또한 웹 페이지가 멋있게 보이도록 Django 어플리케이션 템플릿을 만들 것이다.

## XAMPP 시작과 가상 환경 활성화
Xampp 제어판을 통해 Apache와 MySQL을 시작한다. 가상 환경이 활성화되어 있지 않은 경우 활성화한다.

## Visual Studio code IDE에서 프로젝트 열기
VS code IDE를 사용하려고 한다. 원하는 IDE를 선택할 수도 있다.VS code IDE에서 프로젝트를 연다.

## 커스텀 모델 관리자 생성
`DRAFT`가 아닌 `PUBLISHED`인 문서만 출력하고 싶기 때문에 커스텀 모델 관리자를 만들겠다.

### 관리자

#### 클래스 관리자
관리자는 데이터베이스 쿼리 작업을 Django 모델에 제공하는 인터페이스이다. Django 어플리케이션의 모든 모델에는 하나 이상의 매니저가 있다.

기본적으로 Django는 모든 장고 모델 클래스에 `objects`라는 관리자를 추가한다. 그러나 객체를 필드 이름으로 사용하거나 객체가 아닌 다른 이름으로 매니저를 사용하려는 경우 모델별로 이름을 바꿀 수 있다. 특정 클래스의 `Manager` 이름을 변경하려면 해당 모델에 `models.Manager()` 타입의 클래스 속성을 정의한다. 예를 들어

```python
from django.db import models

class Article(models.Model):
#…
article = models.Manager()
```

#### 커스텀 매니저
기본 매니저 클래스를 확장하고 모델에 커스텀 매니저를 인스턴스화하여 특정 모델에서 커스텀 매니저를 사용할 수 있다.

매니저를 커스터마이즈하여 매니저 메서드를 추가하거나 매니저가 반환하는 초기 쿼리셋을 수정할 수 있다.

매니저의 기본 쿼리셋은 시스템의 모든 객체를 반환한다. 예를 들어 이 모델을 사용한다.

```python
from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=50)
    …

```

`Article.objects.all()`은 데이터베이스의 모든 기사를 반환한다.

`Manager.get_queryset()` 메서드를 재정의하여 매니저의 기본 쿼리셋을 재정의할 수 있다. `get_queryset()`은 필요한 속성을 갖는 쿼리셋을 반환해야 한다.

예를 들어, 다음 모델에는 모든 객체를 반환하는 매니저와 Roald Dahl의 책만 반환하는 두 매니저가 있다.

```python
#First, define the Manager subclass.
class DahlBookManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(author=’Roald Dahl’)

#Then hook it into the Book model explicitly.
class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=50)

objects = models.Manager() # The default manager.
dahl_objects = DahlBookManager() # The Dahl-specific manager.
```

이 샘플 모델에서 `Book.objects.all()`은 데이터베이스의 모든 책을 반환하지만 `Book.dahl_objects.all()`은 Roald Dahl가 쓴 책만 반환한다.

`get_queryset()`은 쿼리 집합 객체를 반환하므로 `filter()`, `exclude()`와 기타 모든 쿼리 집합 메서드를 이 객체에 사용할 수 있다.

### 커스텀 매니저 정의
`blog/models.py`를 열고 아래와 같이 커스텀 관리자를 정의한다.

```python
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class ArticlePublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Article.Status.PUBLISHED)
        pass
    pass

class Article(models.Model):

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'
   
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_articles')
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2,
                                choices=Status.choices,
                                default=Status.DRAFT
                            )

    objects = models.Manager() #The default manager
    publishedArticles = ArticlePublishedManager() #The custom manager

    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]
        pass

    def __str__(self):
        return self.title
        pass

    pass
```

![](images/create_view_display_list/screenshot_04_01.webp)

## 기사 목록을 디스플레이하는 `blog/views.py`내의 메서드 생성

### `blog/views.py`에 기사 모델 가져오기

```python
from .models import Article
```

![](images/create_view_display_list/screenshot_04_02.webp)

### `blog/views.py`에 메서드 생성

`list_of_articles()`라는 메서드를 만들고 있다. 여기서는 데이터베이스에서 게시된 기사를 가져와 출력한다. 그런 다음 `HttpResponse`를 반환한다.

```python
from django.shortcuts import render
from .models import Article
from django.http import HttpResponse

# Create your views here.
def list_of_articles(request):
    articles = Article.publishedArticles.all()
    print(articles)

    return HttpResponse("this is list of articles webpage...")
    pass
```

![](images/create_view_display_list/screenshot_04_03.webp)

## `blog/urls.py` 파일 작성
`views.py` 파일에 메서드를 만들었으므로 이제 `list_of_articles()` 메서드가 호출될 URL 패턴을 만들어야 한다. `blog` Django 앱에는 `urls.py` 파일이 없다. `urls.py` 파일을 생성하고 다음 줄을 작성해야 한다.

```python
from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.list_of_articles, name='list_of_articles'),
]
```

![](images/create_view_display_list/screenshot_04_04.webp)


## 앱의 메인 `urls.py` 파일내에 `blog/urls.py` 파일 가져오기

메인 `urls.py` 파일은 Django 프로젝트 안에 있다. 이 예에서  Django 프로젝트는 `my_blog`이고 Django 어플리케이션은 `blog`이다.

`my_blog/urls.py`를 열고 다음 줄을 포함시킨다.

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls', namespace='blog')),
]
```

![](images/create_view_display_list/screenshot_04_05.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

개발 서버를 실행한 후 브라우저를 열고 아래 URL을 입력한다.

http://127.0.0.1:8000/blog/

![](images/create_view_display_list/screenshot_04_06.png)

콘솔에서 게시된 글을 볼 수 있다.

![](images/create_view_display_list/cli_04_01.png)

데이터베이스에서 뷰로 데이터를 가져오고 있습니다. 템플릿을 만들고 데이터를 웹 페이지로 전달하겠다.

## 기사 목록 웹페이지 디자인

### `blog` 디렉토리에 `templates/blog` 폴터 생성
HTML 웹페이지를 추가하려면 `templates` 폴더를 만든 다음 `blog`라는 폴더를 만들어야 한다(이 폴더는 Django 앱 이름이다).

![](images/create_view_display_list/cli_04_02.png)

![](images/create_view_display_list/cli_04_03.png)

### `blog/templates/blog` 디렉토리에 `base.html` 파일 생성

`base.html` 파일을 만들고 그 안에 다음 코드를 작성한다.

```html
{% load static %}
<!DOCTYPE html>
<html>
<head>
    <title>{% block title %}{% endblock %}</title>
    <link href="{% static "css/style.css" %}" rel="stylesheet">
</head>
<body>
    
    <div class="header">
        <h2>Personal Blog</h2>
    </div>

    <div class="row">
        <div class="leftcolumn">
            <div class="card">
                {% block content %}
                {% endblock %}
            </div>
            
        </div>
        <div class="rightcolumn">
            <div class="card">
                <h2>Blogger Website</h2>
                <p>This is my blogger website.</p>
            </div>
            
            <div class="card">
                <h3>Latest Article</h3>
                {% block latestArticle %}
                {% endblock %}
            </div>
            
            <div class="card">
                <h3>Most Commented Article</h3>
                {% block mostCommentedArticle %}
                {% endblock %}
            </div>
        </div>
    </div>

    <div class="footer">
        <h5>Copyright 2023 All Right Revered </h5>
    </div> 

</body>
</html>
```

![](images/create_view_display_list/screenshot_04_07.webp)

### `style.css`라는 스타일시트(stylesheet) 파일 생성 
`blog(Django App)` 폴더에 `static/css`라는 폴더를 생성한다. 그런 다음 `blog/static/css` 폴더에 `style.css`를 생성한다.

![](images/create_view_display_list/screenshot_04_08.webp)

웹페이지의 스타일시트 `style.css` 파일을 다음과 같이 작성한다.

```css
* {
    box-sizing: border-box;
}

body {
    font-family: "Baskerville";
    padding: 20px;
    background: #f1f1f1;
}

.header {
    padding: 10px;
    font-size: 30px;
    text-align: center;
    background: #29B5F5;
    color: #ffffff;
}

.header h2 {
    color: #ffffff;
}

.leftcolumn {
    float: left;
    width: 75%;
}

.rightcolumn {
    float: left;
    width: 25%;
    padding-left: 20px;
}

.card {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
    color: grey;
    font-size: 16px;
}

.card h2 {
    color: #29B5F5;
    font-family: "Book Antiqua";
}

.card h3 {
    color: #29B5F5;
    font-family: "Book Antiqua";
}


.row:after {
    content: "";
    display: table;
    clear: both;
}

.footer {
    padding: 20px;
    text-align: center;
    background: #29B5F5;
    margin-top: 20px;
    color: #ffffff;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {

    .leftcolumn,
    .rightcolumn {
        width: 100%;
        padding: 0;
    }
}
```

### `list_of_articles()` 메서드에서 html 탬플릿 반환

HttpResponse를 반환했으니 이제 html 템플릿으로 변경해야 한다.

```python
def list_of_articles(request):
    articles = Article.publishedArticles.all()
    #print(articles)

    #return HttpResponse("this is list of articles webpage...")
    return render(request, 'blog/list.html', {'articles': articles})
    pass
```

![](images/create_view_display_list/screenshot_04_09.webp)

### `templates/blog` 디렉토리에 `list.html` 파일 생성
다음 코드를 `list.html` 파일에 작성한다.

```html
{% extends "blog/base.html" %}

{% block title %}Articles{% endblock %}

{% block content %}
    {% for article in articles %}
        <h2>{{ article.title }}</h2>

        <p>
            Published {{ article.publish }} by {{ article.author }}
        </p>
        {{ article.body|truncatewords:50|linebreaks }}

    {% endfor %}
{% endblock %}
```

![](images/create_view_display_list/screenshot_04_10.webp)

## 브라우저의 url 확인
URL http://127.0.0.1:8000/blog/ 을 입력하면 아래와 같은 웹페이지가 디스플레이된다.

![](images/create_view_display_list/screenshot_04_11.png)

우리는 아주 기본적인 웹 페이지를 디자인했다. 원한다면 더 아름답게 꾸밀 수 있다.

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Created views to display list of articles and added template also"
$ git push origin main
```

![](images/create_view_display_list/cli_04_04.png)

![](images/create_view_display_list/cli_04_05.png)

이 섹션은 여기까지 ...

**(주)** 위 내용은 [Create a view to display the list of articles — Part 4](https://medium.com/@nutanbhogendrasharma/create-a-view-to-display-the-list-of-articles-in-django-part-4-3aa29033765b)을 편역한 것이다. 
