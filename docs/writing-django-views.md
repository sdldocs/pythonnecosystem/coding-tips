*Django Views 설명*

Django는 MVT(Model-View-Template) 설계 패턴을 사용한다. View 파트는 함수 또는 클래스로 정의될 수 있다. 웹 요청을 수신하고 웹 응답을 반환하는 작업을 수행한다.

[여기](https://github.com/okanyenigun/django-view-example)에서 이 글에서 사용하는 예제 코드를 얻을 수 있다. `home`이라는 애플리케이션이 하나 있으며, `Player`라는 하나의 model이 있다.

```python
#models.py

from django.db import models

class Player(models.Model):

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    nationality = models.CharField(max_length=100)
    age = models.IntegerField()

    def __str__(self):
        return self.last_name
```

```python
#forms.py

from django.forms import ModelForm
from home.models import Player

class PlayerForm(ModelForm):
    class Meta:
        model = Player
        fields = '__all__'
```

우선, 페이지의 양식과 데이터를 표시하는 표를 가지고 있다.

![fig. 1](images/django_view/fig_1.webp)

## Function-Based Views
먼저 `urls.py` 에 URL을 생성한다. `home`은 view 함수이다. 함수에서는 요청을 받아, 작업 후 요청과 전송할 데이터를 보내 페이지를 렌더링한다.

```python
#urls.py

from django.contrib import admin
from django.urls import path
import home.views as hv

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', hv.home, name='home'),
]
```

```python
#views.py

from django.shortcuts import render
from home.forms import PlayerForm
from home.models import Player

def home(request):
    form = PlayerForm()
    players = Player.objects.all()
    if request.method == "POST":
        form = PlayerForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, './templates/home.html', 
                  {'form':form, 'players':players})
```

여기서 먼저 form을 만든 다음 model 레코드를 가져왔다. `home` 함수는 모든 요청 타입을 포함할 수 있다. 들어오는 요청이 POST 타입이면 조건문에서 요청을 처리한다. 예를 들어, form은 POST 요청 내에 있다. 그것을 `post` 상태에서 요청을 처리한다.

function-based view 또는 class-based view 인가? 모두에서 동일한 작업을 수행할 수 있다.

function-based view는 보다 직접적이다. 함수안에 모든 것을 작성한다. class-based view들은 추상화되어 있다. 따라서 먼저 추상화 구조를 알아야 한다.

일반 class-based view에는 템플릿이 있으며 어려울 수 있다. function-based view에서 커스텀화하는 것이 더 쉬울 수 있다.

여전히, 어느 것이 더 나은지에 대한 논쟁은 무의미할 것이다. 업무나 프로젝트에 따라 다르며, 개발자 개인의 취향에 따라 다를 수 있다. 둘 다 알고 필요한 경우 어떤 것이든 사용할 수 있는 것이 가장 바람직하다.

## Class-Based Views
위에서 필자는 class-based view를 선호하는데, 그것이 더 모듈적이라고 생각하기 때문이다. class-based view는 상속을 통해 코드 재사용을 촉진한다. 필자가 생각할 수 있는 유일한 단점은 추상화로 인해 완전히 마스터하는 것이 쉽지 않다는 느낌이다. 하지만 일단 배우면 이 문제를 해결할 수 있다.

class-based view에서는 각 HTTP 요청 타입에 대해 별도의 메서드를 정의한다. class-type view를 사용하여 위의 예를 작성하면 아래와 같다.

```python
#urls.py

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', hv.home, name='home'),
    path('', hv.HomeView.as_view(), name='home'),
]
```

URL에서는 class-based view를 위하여 `as_view` 메서드를 사용한다.

```python
from django.views import View

class HomeView(View):

    def get(self, request):
        form = PlayerForm()
        players = Player.objects.all()
        return render(request, './templates/home.html', {'form':form, 'players':players})
    
    def post(self, request):
        form = PlayerForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('home')
```

위의 작업을 보다 모듈화된 구조인 `get`과 `post`로 나누었다. 

Django에는 몇 가지 내장된 일반 view 클래스가 있다.

### TemplateView
GET 메서드를 사용하는 정적 페이지 또는 페이지를 출력하기 위하여 `TemplateView`를 사용할 수 있다(예: "about us" 페이지). 이 페이지에서는 사용자 상호 작용을 수행할 필요가 없다.

**HTML**: 공백의 정적 "정보" 페이지

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>About</title>
  </head>
  <body>
    <div>About me</div>
    <div>{{header}}</div>
  </body>
</html> 
```

`TemplateView`를 두 가지 방법으로 설정할 수 있다. 먼저 `urls.py`에서 정의할 수 있다.

```python
#urls.py

from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', TemplateView.as_view(template_name='about.html',
                                        extra_context={'header': 'Something'}))
]
```

URL 경로 섹션에서 `TemplateView`를 직접 정의한다. `as_view` 메서드에서 HTML 파일 이름과 매개 변수를 전달했다.

다른 방법은 `TemplateView`를 상속하여 view 객체를 정의하는 것이다.

```python
#views.py

from django.views.generic.base import TemplateView

class AboutView(TemplateView):

    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["header"] = "Something"
        return context
```
`TemplateView` 클래스는 `TemplateResponseMixin`, `ContextMixin`과 `View`의 세 내장 Django 클래스로부터 상속받는다. 그러므로, 이러한 부모 클래스의 메서드을 사용할 수 있어 `get_context_data` 메서드를 사용하여 데이터를 전송할 수 있다.

```python
#urls.py

from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView
import home.views as hv

urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', hv.AboutView.as_view()),
]
```

### RedirectView
이름에서 알 수 있듯이 리디렉션 목적으로 사용한다.

계속하기 전에 먼저 프로젝트 자체의 `urls.py` 파일에서 URL 정의를 가져와 애플리케이션에 맞게 만들어 보겠다. 이를 위해 home 애플리케이션에 새로운 `urls.py` 파일을 생성한다.

```python
#home/urls.py

from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView, RedirectView
import home.views as hv

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', hv.HomeView.as_view(), name='home'),
    path("redirect/", RedirectView.as_view(url='https://www.google.com/?client=safari'), 
                                           name='redirect-to-google')
]
```

`urlpatterns` 내에 `RedirectView`를 정의하고 URL 주소를 보냈다. 이름으로 부를 수 있도록 이름도 지어주었다.

```python
#urls.py (in the project)

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(('home.urls', 'home'), namespace='home'))
]
```

**HTML**: 홈페이지 하단에 `a` 태그를 달았다. `home` 네임스페이스를 사용하여 `redirect-to-google`에 이른다.

```html
<a href="{% url 'home:redirect-to-google' %}"> Go To Google</a>
```

![fig. 2](images/django_view/fig_2.webp)

이 링크를 클릭하면 구글로 이동한다.

또는 `RedirectView`를 상속하는 뷰 클래스를 만들 수 있다. 이를 통해 리디렉션 작업 중에 발생할 작업을 구체화할 수 있다.

먼저 `RedirectView` 클래스로 부터 상속받는 `PreRedirectView` 클래스를 만든다. 상위 `RedirectView` 클래스의 `pattern_name` 변수를 선언하여 이동할 위치를 정의한다. 그러면 `about` 네임스페이스로 리디렉션된다.

리디렉션 전에 클래스에서 `get_redirect_url` 메서드를 실행한다. 따라서 리디렉션하기 전에 동작을 커스텀화할 수 있다. 예를 들어, 로깅을 하거나 데이터베이스에 데이터를 보낼 수 있다.

```python
#views.py

class PreRedirectView(RedirectView):

    pattern_name = 'home:about'

    def get_redirect_url(self, *args, **kwargs):
        print("Customize here")
        return super().get_redirect_url(*args, **kwargs)
```

그에 따라 `urls.py`을 업데이트한다.

```python
#urls.py

from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView, RedirectView
import home.views as hv

urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', hv.AboutView.as_view(),name='about'),
    path('redirect/', hv.PreRedirectView.as_view(), name='redirect'),
    path('', hv.HomeView.as_view(), name='home'),
    
]
```

**HTML**

```html
<a href="{% url 'home:redirect' %}"> Go To About</a>
```

링크를 클릭하면 리디렉션 전에 `PreRedirectView` 클래스에서 `get_redirect_url` 메서드를 실행한다. 따라서 리디렉션하기 전에 "Customize here"이 인쇄된다.

### DetailView
`DetailView`를 사용하여 단일 인스턴스의 세부 정보를 디스플레이한다. 예를 들어, 페이지의 객체 목록에서 사용자가 객체 목록 중 하나를 클릭하면 해당 개별 페이지로 이동한다.

객체의 주(primary) 키 또는 슬러그(slug)로 호출해야 한다. 일반적으로 이 view는 form이 포함된 페이지를 사용하지 않는다.

```python
#urls.py

urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', hv.AboutView.as_view(),name='about'),
    path('redirect/', hv.PreRedirectView.as_view(), name='redirect'),
    path('', hv.HomeView.as_view(), name='home'),
    path('<int:pk>/',hv.PlayerDetailView.as_view(), name='detail'), 
]
```

아래 예에서는 주 키를 사용했지만 슬러그도 사용할 수 있다.

```python
#views.py

from django.views.generic.detail import DetailView
from django.shortcuts import get_object_or_404

class PlayerDetailView(DetailView):

    template_name='detail.html'
    #queryset = Player.objects.all()

    def get_object(self):
        idx = self.kwargs.get("pk")
        return get_object_or_404(Player, id=idx)
```

다시 `template_name`을 선언한다. 그리고 동작을 커스텀화하기 위해 부모 클래스의 `get_object` 메서드를 overwrite한다. `get_object_or_404` 메서드에 주 키를 제공하여 해당 모델 레코드를 얻는다.

또한 `queryset`을 정의하여 표시되는 항목 수를 제한할 수 있다. 기본 동작은 모든 레코드를 선택하는 것이지만 원하는 경우 필터링할 수 있다.

```html
#detail.hmtl

<body>
    {{object.first_name}} {{object.last_name}} {{object.nationality}}
    {{object.age}}
</body>
```

"http://127.0.0.1:8000/1/"을 브라우저 주소창에 입력하면,

![fig. 3](images/django_view/fig_3.webp)

몇 줄의 코드만으로 목록에 있는 레코드의 세부 정보를 볼 수 있는 view를 만들었다.

### ListView
일반 목록 보기는 객체 목록을 디스플레이하도록 설계되었다. 쉽게 페이지를 매길 수 있다. 또한 이 일반 보기는 양식 또는 레코드 항목을 갖는 데 적합하지 않으며, 대신 데이터를 보여주고 나열하는 데 더 적합하다.

먼저 `urls.py`에 `urlpatterns`에 URL을 추가한다.

```python
#urls.py

path('lists/', hv.PlayerListView.as_view(), name="player-list"),
```

사용할 모델과 데이터를 디스플레이할 템플릿을 지정한다. 뒤에서 Django는 데이터를 쿼리하여 템플릿으로 출력한다.

```python
class PlayerListView(ListView):

    model = Player 
    template_name = "lists.html"  
```

HTML

```html
<table class="table">
  <thead>
    <tr>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Nation</th>
      <th scope="col">Age</th>
    </tr>
  </thead>
  <tbody>
    {% for player in object_list %}
    <tr>
      <td>{{player.first_name}}</td>
      <td>{{player.last_name}}</td>
      <td>{{player.nationality}}</td>
      <td>{{player.age}}</td>
    </tr>
    {% endfor %}
  </tbody>
</table>
```

`object_list`는 `ListView`의 데이터에 대한 기본 키워드이다. `context_object_name` 변수를 선언하여 변경할 수 있다.

```python
class PlayerListView(ListView):

    model = Player 
    template_name = "lists.html"  
    context_object_name = "Players"
```

```html
{% for player in Players %}
  <tr>
    <td>{{player.first_name}}</td>
    <td>{{player.last_name}}</td>
    <td>{{player.nationality}}</td>
    <td>{{player.age}}</td>
  </tr>
{% endfor %}
```

#### Pagination
`paginate_by` 변수로 디스플레이할 레코드 수를 결정할 수 있다.

```python
class PlayerListView(ListView):

    model = Player 
    template_name = "lists.html"  
    context_object_name = "Players"
    paginate_by = 1
```

페이지내이션을 위하여 Django 문서의 일반 코드를 사용한다. 이같은 방법으로, 테이블의 하단에 페이지를 표시한다. 그리고 페이지들 앞뒤로 왔다 갔다 할 수 있다.

```html
<body>
    <div>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Nation</th>
            <th scope="col">Age</th>
          </tr>
        </thead>
        <tbody>
          {% for player in Players %}
          <tr>
            <td>{{player.first_name}}</td>
            <td>{{player.last_name}}</td>
            <td>{{player.nationality}}</td>
            <td>{{player.age}}</td>
          </tr>
          {% endfor %}
        </tbody>
      </table>
    </div>
    <div class="pagination">
      <span class="step-links">
        {% if page_obj.has_previous %}
        <a href="?page=1">&laquo; first</a>
        <a href="?page={{ page_obj.previous_page_number }}">previous</a>
        {% endif %}

        <span class="current">
          Page {{ page_obj.number }} of {{ page_obj.paginator.num_pages }}.
        </span>

        {% if page_obj.has_next %}
        <a href="?page={{ page_obj.next_page_number }}">next</a>
        <a href="?page={{ page_obj.paginator.num_pages }}">last &raquo;</a>
        {% endif %}
      </span>
    </div>
</body>
```

![pagination](images/django_view/pagination.webp)

이는 페이지내이션를 구현하는 매우 쉬운 방법이다.

`DetailView`에서 할 수 있듯이 처음부터 쿼리 세트를 정의하고 처리할 레코드를 결정할 수 있다. 이렇게 하는 것이 유효한 방법이지만 `get_queryset` 메서드를 재정의하여 레코드를 필터링하는 다른 방법을 사용할 수도 있다.

```python
class PlayerListView(ListView):

    model = Player 
    template_name = "lists.html"  
    context_object_name = "Players"
    paginate_by = 1

    def get_queryset(self):
        queryset = Player.objects.filter(first_name="Lionel")
        return queryset
```

이 인스턴스를 별도의 클래스로 만들 수도 있다. 이번에는 국적을 매개 변수로 받는다.

```python
#urls.py

#add this
path('lists/<str:nationality>', hv.NationView.as_view(), name="nation"),
```

queryset을 필터링한다.

```python
#views.py

class NationView(ListView):
    model = Player 
    template_name = "lists.html"  
    context_object_name = "Players"
    paginate_by = 1

    def get_queryset(self):
        queryset = Player.objects.filter(nationality=self.kwargs.get("nationality"))
        return queryset
```

"http://127.0.0.1:8000/lists/Portugal"을 브라우저 주소창에 입력하면,

![fig. 4](images/django_view/fig_4.webp)

### FormView
지금까지 데이터 디스플레이를 위한 일반적인 view만 살펴보았다. 이제 데이터를 편집할 때 어떤 일이 일어나고 있는지 살펴보겠다.

이름이 용도를 나타내듯이 `FormView`를 사용하여 양식을 디스플레이한다. 새 데이터를 저장하기 위하여 일반적으로 `FormView`를 사용하지 않는다. 이에 대한 별도의 일반 view가 있다. `FormView`는 주로 전자 메일 보내기와 같은 작업을 수행하는 데 사용된다. 하지만 필요에 따라 새 데이터를 저장하는 데도 사용할 수 있다.

```python
#views.py

from django.views.generic.edit import FormView

class AddView(FormView):
    template_name = "add_player.html"
    form_class = PlayerForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
```

작업이 완료되면 리디렉션할 `template_name`, 양식과 URL을 설정한다. `form_valid` 메서드를 재정의하여 폼 검증 프로세스를 커스텀화할 수 있다.

```python
#urls.py

path('add/', hv.AddView.as_view(), name='add'),
```

```html
<div class="main-frame">
  <form method="POST">
    {% csrf_token %}
    <div class="left-frame">
      {{form.as_table}}
      <button name="submit" type="submit" class="btn btn-primary">
        Save
      </button>
    </div>
  </form>
</div>
```

![add_player](images/django_view/add_player.webp)

### CreateView
이것은 매우 구체적인 일반적 view이다. `CreateView`를 사용하여 새로운 레코드를 만드는 양식을 디스플레이한다.

```python
from django.views.generic.edit import CreateView

class CreatePlayerView(CreateView):
    model = Player
    template_name = "add_player.html"
    form_class = PlayerForm
    success_url = '/'
```

`CreateView`에서 양식 유효성 검사를 사용할 필요가 없다. 그 외에는 `FormView`와 비슷한 구조를 띠고 있다.

### UpdateView
`CreateView`와 동일한 클래스와 믹스인을 상속받는다. 따라서 이들은 비슷하다. 특정 데이터를 검색하고 업데이트하는데 `UpdateView`가 선호된다.

우선 새로운 URL이 필요하다.

```python
#urls.py

#add this
path('<int:pk>/edit',hv.EditPlayerView.as_view(), name='edit'),
```

view는

```python
class EditPlayerView(UpdateView):
    model = Player
    template_name = "add_player.html"
    form_class = PlayerForm
    success_url = '/'
```

![fig. 5](images/django_view/fig_5.webp)

That’s all for now!

### 더 읽을 거리
- [Discovering Django Forms](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/discovering-django-forms/)

**주** 이 페이지는 [Writing Django Views](https://blog.devgenius.io/writing-django-views-8835ff4c7166)을 편역한 것임.
