Django는 Python으로 작성된 고급 자유 오픈 소스 웹 프레임워크이다. 복잡한 웹 어플리케이션을 구축하는 데 널리 사용되며 높은 트래픽을 처리하는 능력, 보안 기능 및 재사용 가능한 구성 요소로 널리 알려져 있다. MVC(Model-View-Controller) 아키텍처 패턴을 따르며 Python 클래스를 사용하여 데이터베이스 스키마를 정의할 수 있는 ORM(Object-Relational Mapper)이 내장되어 있다.

게다가, Django는 개발자들이 사용할 수 있는 풍부한 튜토리얼, 패키지 및 기타 리소스를 제공하는 크고 활동적인 커뮤니티가 있다. 인스타그램, 핀터레스트, 워싱턴 타임즈와 같은 많은 유명하고 트래픽이 많은 웹 사이트들이 Django를 사용하여 구축되었다. Python의 가장 대중적이고 강력한 웹 프레임워크 중 하나이며 스타트업과 대기업 모두에서 널리 사용되고 있어 강력하고 확장 가능한 웹 어플리케이션을 구축하기 위한 훌륭한 선택으로 여겨지고 있다. 유연성과 확장성 덕분에 설치 공간을 최소화하면서 복잡한 웹 어플리케이션을 구축하려는 개발자와 조직에게 최고의 선택이다.

## 필요한 도구 설치
시작하기 위하여 컴퓨터에 설치해야 하는 몇 가지 도구가 있다. 웹 어플리케이션 프로그램을 성공적으로 실행하려면 적어도 프로그래밍 언어(Python), 데이터베이스(SQLite)와 서버(Django의 내장 개발 서버)가 필요하다. 이 스택은 개발 환경 전용이므로 프로덕션 환경에서 사용할 수 없다.

그리고 물론 PyCharm과 같은 IDE나 적어도 코드 에디터가 필요하다. 여기서는 무료 소프트웨어이기 때문에 VS Code를 사용할 것이다.

- [Python 다운로드](https://www.python.org/downloads/)
- [SQLite 다운로드](https://www.sqlite.org/index.html)
- [VS Code 다운로드](https://code.visualstudio.com/download)
- [PyCharm 다운로드](https://www.jetbrains.com/pycharm/download/#section=mac)

## 새 Django 프로젝트 만들기
위의 도구들이 성공적으로 설치되면 코딩을 시작할 시간이다! 먼저 모든 코드를 담을 새 작업 디렉터리를 만들어야 한다.

```bash
$ mkdir django-demo
```

```bash
$ cd django-demo
```

그런 다음 작업 디렉토리 내에 새 Python 가상 환경을 구축한다. 시스템의 Python 환경뿐만 아니라 시스템에 있을 수 있는 다른 Python 프로젝트로부터 이 프로젝트 개발 환경을 분리한다.

```bash
$ python -m venv env
```

리눅스나 맥OS를 사용하는 경우에는 `python` 대신 `python3`를 실행해야 할 수도 있지만, 단순성을 위해 `python`을 계속 사용하도록 한다.

이 명령을 실행하면 방금 생성한 가상 환경이 포함된 `env` 디렉토리가 생성된다. 다음 명령을 사용하여 가상 환경을 활성화할 수 있다.

```bash
$ source env/bin/activate
```

Windows를 사용한다면, 다음 명령을 사용한다.

```cmd
env\Scripts\activate
```

가상 환경이 성공적으로 활성화된 경우 터미널 프롬프트는 다음과 같다.

```bash
(env) eric@djangoDemo:~/django-demo$
```

`(env)`는 현재 `env`라는 이름의 가상 환경 내에서 작업 중임을 의미한다.

이제 새로운 Django 프로젝트를 초기화해야 한다. 그 이전에 다음 명령을 실행하여 Django 패키지를 설치해야 한다.

```bash
$ python -m pip install Django
```

다음 `django-admin` 명령을 사용하여 새로운 Django 프로젝트를 생성한다.

```shell
$ django-admin startproject djangoBlog
```

새 `djangoBlog` 디렉토리가 생성된다.

![djangoBlog_directory](images/django_beginner/djangoBlog_directory_1.webp)

이 페이지에서는 모든 것이 프로젝트 루트 디렉터리에서 시작할 수 있도록 프로젝트를 약간 재구성한다(즉 `djangoBlog`를 상위로 옮김). 원하지 않으면 이렇게 할 필요가 없다.

![djangoBlog_directory](images/django_beginner/djangoBolg_directory_2.webp)

## bolg 앱 만들기
현재 프로젝트는 아직 비어있고, 보시다시피 Laravel에 비해 프로젝트 구조는 매우 단순하다. 각 파일에 대해서는 나중에 자세히 설명할 것이다.

Django를 사용하면 단일 프로젝트에서 여러 앱을 만들 수 있다. 예를 들어, 하나의 프로젝트 안에 `blog` 앱, `gallery` 앱, `forum` 앱을 만들 수 있다. 이러한 앱은 동일한 정적 파일(CSS 및 JavaScript 파일), 이미지와 비디오를 공유하거나 서로 완전히 독립적일 수 있다. 그것은 전적으로 당신 자신의 필요에 달려 있다.

여기에서는 `blog` 앱을 하나만 만들 것이다. 터미널로 돌아가서 다음 명령을 실행한다.

```bash
$ python manage.py startapp blog
```

프로젝트 루트 디렉터리에 새 블로그 폴더가 생성된다. 다음 명령을 사용하여 디렉터리의 내용을 나열할 수 있다.

```bash
$ ls
```

숨겨진 파일까지 모구 포함하려면,

```bash
$ ls -a
```

파일 구조를 보고 싳다면,

```bash
$ tree
```

운영 체제에 따라 이 작업을 수행하려면 [`tree`](https://www.geeksforgeeks.org/tree-command-unixlinux/) 설치가 필요할 수도 있다.

이제부터 이 명령을 사용하여 파일 구조를 표시하겠다. 현재 프로젝트의 구조는 다음과 같다.

```
.
├── blog
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── djangoBlog
│   ├── asgi.py
│   ├── __init__.py
│   ├── __pycache__
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── env
│   ├── bin
│   ├── include
│   ├── lib
│   ├── lib64 -> lib
│   └── pyvenv.cfg
└── manage.py
```

다음으로, 새로운 `blog` 앱을 Django에 등록해야 한다. `settings.py`로 이동하여 `INSTALLED_APPS`를 찾아 아래와 같이 변경한다.

```python
INSTALLED_APPS = [
    'blog',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

이제 개발 서버를 시작하여 모든 것이 작동하는지 테스트할 수 있다. 터미널을 열고 다음 명령을 실행한다.

```bash
$ python manage,py runserver
```

명령줄에 다음과 같은 출력이 디스플레이된다.

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
October 19, 2022 - 01:39:33
Django version 4.1.2, using settings 'djangoBlog.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

브라우저를 실행하고, `http;//127.0.0.1:8000`을 주소창에 입력한다.

![browser](images/django_beginner/django_install_page.webp)

## 어플리케이션 구조
코딩을 시작하기 전에 이 새로운 Django 어플리케이션의 구조와 각 파일이 수행하는 작업에 대해 살펴보자.

```
.
├── blog
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── djangoBlog
│   ├── asgi.py
│   ├── __init__.py
│   ├── __pycache__
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── env
│   ├── bin
│   ├── include
│   ├── lib
│   ├── lib64 -> lib
│   └── pyvenv.cfg
└── manage.py
```

### 루트 디렉토리

- `manage.py`: 이 Django 프로젝트와 다양한 방식으로 상호 작용할 수 있는 명령어 유틸리티이다.
- `djangoBolg`: 프로젝트 주 디렉터리. 이 디렉터리에는 프로젝트의 설정 파일과 시작 점(entry point)이 있다.
- `bolg`: `blog` 앱.

### 프로젝트 디렉토리

- `djangoBlog/_init_.py`: 이 디렉터리는 Python 패키지로 간주되도록 Python에 알려주는 빈 파일이다.
- `djangoBolg/settings.py`: 이름에서 알 수 있듯이 이 Django 프로젝트의 설정/구성 파일이다.
- `djangoBlog/urls.py`: 이 Django 프로젝트의 URL 선언, Django 어플리케이션 프로그램의 *목차(table of contents)*에 해당된다고 할 수 있다. 이것에 대해서는 나중에 더 설명할 것이다.
- `djangoBlog/asgi.py`: 프로젝트를 지원하기 위한 ASGI 호환 웹 서버를 위한 시작점이다.
- `djangoBlog/wsgi.py`: 프로젝트를 지원하기 위한 WSGI 호환 웹 서버를 위한 시작점이다.

### 앱 디렉토리

- `blog/migrations`: 이 디렉터리에는 `blog` 앱을 위한 모든 마이그레이션 파일이 들어 있다. Laravel과 달리 이 파일들은 모델을 기반으로 Django가 자동으로 생성한다.
- `blog/admin.py`: Django는 또한 관리 패널을 함께 제공하며, 이 파일에는 관리 패널에 대한 모든 구성이 포함되어 있다.
- `blog/models.py` : 모델은 데이터베이스의 구조와 관계를 기술한다. 마이그레이션 파일은 이 파일을 기준으로 생성된다.
- `blog/views.py` : 이는 Laravel의 컨트롤러에 해당된다. 이 앱의 핵심 논리를 모두 포함한다.

## Django 프로젝트 설정하기
프로젝트를 시작하기 전에 `settings.py` 파일을 변경해야 한다.

### 허용된 호스트(allowed hosts)
`ALLOWED_HOSTS`는 Django 사이트로 서비스를 제공할 수 있는 도메인 목록이다. 이는 안전해 보이는 많은 웹 서버 구성에서도 가능한 HTTP 호스트 헤더 공격을 방지하기 위한 보안 조치이다.

하지만,`ALLOWED_HOSTS`가 현재 비어 있더라도 호스트 `127.0.0.1`을 사용하여 사이트를 액세스할 수 있음을 알고 있어야 한다. `DEBUG`가 `True`이고 `ALLOWED_HOSTS`가 비어 있다면, [`.localhost`, `127.0.0.1`, `[::1]`]에 대해 호스트를 검증한다.

### 데이터베이스
`DATABASES`는 웹 사이트에서 사용해야 하는 데이터베이스 설정이 들어 있는 Python 딕셔너리이다. 기본적으로 Django는 SQLite를 사용하는데, SQLite는 하나의 파일로 이루어진 매우 간단한 데이터베이스이다. 작은 데모 프로젝트에는 충분하지만, 큰 사이트에서는 작동하지 않을 것이다. 따라서 다른 데이터베이스를 사용하려면 다음과 같이 하여야 한다.

```python
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "<database_name>",
        "USER": "<database_user_name>",
        "PASSWORD": "<database_user_password>",
        "HOST": "127.0.0.1",
        "PORT": "5432",
    }
}
```

참고로 Django는 운영 환경에서 데이터베이스로 Postgres 사용을 권장한다. 많은 튜토리얼에서 MongoDB를 Django에서 사용하는 방법을 설명하고 있는 것으로 알고 있지만, 그것은 사실 좋은 생각이 아니다. MongoDB는 훌륭한 데이터베이스 솔루션이지만, Django와 잘 작동하지 않는다. 자세한 내용은 [여기](https://daniel.feldroy.com/posts/when-to-use-mongodb-with-django)를 참조한다.

### 정적 파일과 미디어 파일
마지막으로 정적 파일과 미디어 파일을 처리해야 한다. 정적 파일은 CSS 또는 JavaScript 파일이고, 미디어 파일은 이미지, 비디오 또는 사용자가 업로드할 수 있는 다른 형식들이다.

#### Static Files
먼저 이러한 파일이 저장되는 위치를 지정해야 한다. Django 사이트를 위해 blog 앱 내에 `static` 디렉터리를 만들 것이다. 여기에 개발 중인 blog 앱의 정적 파일을 저장한다.

```
blog
├── admin.py
├── apps.py
├── __init__.py
├── migrations
├── models.py
├── static
├── tests.py
└── views.py
```

그러나 운영 환경에서는 상황이 조금 다르다. 프로젝트의 루트 디렉터리 아래에 다른 폴더가 필요하다. 이를 `staticfiles` 파일이라 한다.

```
.
├── blog
├── db.sqlite3
├── djangoBlog
├── env
├── manage.py
└── staticfiles
```

그런 다음 `settings.py`에서 이 폴더를 지정해야 한다.

```python
STATIC_ROOT = "staticfiles/"
```

다음으로 브라우저에서 이러한 파일에 액세스할 때 어떤 URL을 사용해야 하는지 Django에게 알려주어야 한다. `/static`일 필요는 없지만 나중에 설명할 URL 구성과 겹치지 않도록 하여야 한다.

```python
STATIC_URL = "static/"
```

#### Media Files
미디어 파일도 동일한 방식으로 구성한다. 프로젝트 루트 디렉터리에 `mediafiles` 폴더를 생성할 수 있다.

```
.
├── blog
├── db.sqlite3
├── djangoBlog
├── env
├── manage.py
├── mediafiles
└── staticfiles
```

그런 다음 `settings.py` 파일에서 위치와 URL을 지정한다.

```python
# Media files

MEDIA_ROOT = "mediafiles/"
MEDIA_URL = "media/"s
```

## Django에서 URL 디스패처
웹 개발 분야에서는 MVC(Model-View-Controller) 구조라고 부르는 것이 있다. 이 구조에서 모델(Model)은 데이터베이스와 상호 작용하는 것을 담당하며, 각 모델은 하나의 데이터베이스 테이블에 해당되어야 한다. 보기(View)는 프로그램의 프런트엔드 부분이며 사용자가 볼 수 있는 것이다. 그리고 마지막으로, 컨트롤러(Controller)는 모델을 통해 데이터베이스에서 데이터를 검색하여 해당 뷰에 배치하고, 결국 렌더링된 템플릿을 사용자에게 반환하는 것과 같은 앱의 백엔드 로직이다.

Django는 이 구조를 기반으로 설계된 웹 프레임워크로, 단지 다른 용어를 사용한다. Django의 경우 MTV(Model-Template-View) 구조이다. 템플릿은 프런트엔드이고 뷰는 백엔드 로직이다.

이 포스팅에서는 이러한 각 계층을 이해하는 데 중점을 두겠지만, 먼저 모든 웹 어플리케이션 프로그램의 시작 지점인 URL 디스패처부터 시작해야 한다. 사용자가 URL을 입력하고 Enter를 치면 디스패처가 해당 URL을 읽고 해당 페이지로 사용자를 안내한다.

### 기초 URL 구성
URL 구성을 `djangoBolg/urls.py`에 저장한다.

```
djangoBlog
├── asgi.py
├── __init__.py
├── __pycache__
├── settings.py
├── urls.py
└── wsgi.py
```

이 파일은 URL 디스패처의 예를 제공한다.

```python
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
]
```

URL에서 정보를 읽고 view를 반환한다. [Laravel](https://www.ericsdevblog.com/posts/laravel-for-beginners-1/) 튜토리얼 시리즈를 따라간다면 이 view와 Laravel의 view를 혼동하지 않아야 한다. 사실 Django view는 Laravel의 컨트롤러라는 것을 기억하자.

이 예에서 도메인 다음 `admin/`이 있으면 디스패처는 사용자를 admin 페이지로 라우트한다.

### Django URL 디스패처에서 파라미터 보내기
종종 URL 세그먼트를 가져와 view에 추가 파리미터를 전달해야 합니다. 예를 들어 블로그 게시 페이지를 표시하려면 이 추가 정보를 사용하여 검색 중인 블로그 게시물을 찾을 수 있도록 게시물 ID 또는 슬러그를 view에 전달해야 한다. 파라미터를 전달하는 방법은 다음과 같다.

```python
from django.urls import path
from blog import views

urlpatterns = [
    path('post/<int:id>', views.post),
]
```

`<>`괄호는 URL의 일부를 파라미터로 캡처한다. 왼쪽의 `int`는 경로 변환기라고 불리며, 정수 파라미터를 캡처한다. 변환기가 포함되지 않은 경우 `/` 문자를 제외한 모든 문자열이 일치되어야 한다. 오른쪽에는 파라미터의 이름이 표시된다. view에서 그것을 사용한다.

기본적으로 다음의 경로 변환을 사용할 수 있다.

- `str`: 경로 구분 기호 '/'을 제외한 비어 있지 않은 문자열과 일치시킨다. 변환 대상에 포함되지 않은 기본적 경우이다.
- `int`: 0 또는 임의의 양의 정수와 일치시킨다. `int`를 반환한다.
- `slug`: ASCII 문자 또는 숫자, 하이픈 및 밑줄 문자로 구성된 모든 slug 문자열과 일치시킨다. 예를 들면 `building-your-1st-django-site`와 같다.
- `uuid`: 형식이 지정된 UUID와 일치시킨다. 여러 URL이 동일한 페이지에 매핑되지 않도록 하려면 대시를 포함하고 문자는 소문자여야 합니다. 예를 들면 `075194d3-6885-417e-a8a8-6c931e272f00`과 같다. [UUID](https://docs.python.org/3/library/uuid.html#uuid.UUID) 인스턴스를 반환한다.
- `path`: 경로 구분 기호 '/'를 포함하여 비어 있지 않은 문자열과 일치시킨다. 이를 통해 `str`과 같이 URL 경로의 세그먼트가 아닌 전체 URL 경로와 비교할 수 있다.

### URL 패턴과 일치시키기 위한 정규 표현식 사용하기
일치시켜야 할 패턴이 더 복잡한 경우가 있다. 이 경우 정규식을 사용하여 URL 패턴과 일치시킬 수 있다. [자바스크립트 과정](https://www.ericsdevblog.com/posts/javascript-basics-1/)에 정규 표현에 대한 간단한 설명을 참고하도록 한다.

정규식을 사용하려면 `path()` 대신 `re_path()`를 사용해야 한다.

```python
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('articles/2003/', views.special_case_2003),
    re_path(r'^articles/(?P<year>[0-9]{4})/$', views.year_archive),
    re_path(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.month_archive),
    re_path(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<slug>[\w-]+)/$', views.article_detail),
]
```

### 다른 URL 구성 import하기
여러 개의 다른 앱으로 구성된 Django 프로젝트가 있다고 하자. 만약 이때 모든 URL 구성을 한 파일에 넣으면, 그것은 정말 복잡해질 것이다. 이 경우 URL 구성을 분리하여 다른 앱으로 이동시킬 수 있다. 예를 들어, djangoBlog 프로젝트에서는 `bolg` 앱 내에 새로운 `urls.py`을 만들 수 있다.

```python
from django.urls import path, include

urlpatterns = [
    path('blog/', include('blog.urls'))
]
```

이는 URL에 `http://www.mydoamin.com/blog/xxx` 패턴이 있으면 Django가 `blog/urls.py`로 이동하여 나머지 URL과 일치시킨다는 것을 의미한다.

`blog` 앱의 URL 패턴을 정확히 같은 방법으로 정의한다.

```python
from django.urls import path
from blog import views

urlpatterns = [
    path('post/<int:id>', views.post),
]
```

이 패턴은 아래 URL 패턴과 일치한다.

```
http://www.mydomain.com/blog/post/123
```

### URL 패턴에 이름 짓기
마지막으로 다음과 같이 세 번째 파라미터를 지정하여 URL 패턴의 이름을 지정할 수 있다.

```python
urlpatterns = [
    path('articles/<int:year>/', views.year_archive, name='news-year-archive'),
]
```

이 작은 방법는 매우 중요하다. 템플릿에서 단호한 URL을 되돌릴 수 있다. 템플릿 계층을 설명할 때 이에 대해 자세히 다룰 것이다.

URL 디스패처의 기본 사항은 여기까지. 하지만, 여러분은 무언가가 부족하다는 것을 느꼈을 수도 있다. HTTP 요청에 다른 방법이 있으면 어떻게 할까? 어떻게 그것들을 구별할 수 있을까? Django에서는 URL 디스패처 내에서 서로 다른 HTTP 메서드를 일치시킬 수 없다. 모든 메서드는 동일한 URL 패턴과 일치하며, 우리는 흐름 제어를 사용하여 view 함수 내의 다른 메서드에 대해 서로 다른 코드를 작성해야 한다. 우리는 이것에 대해 향후에 자세히 다룰 것이다.

### 읽을 거리 추천

- [Laravel for Beginners](https://www.ericsdevblog.com/posts/laravel-for-beginners-1/)
- [Vue.js for Beginners](https://www.ericsdevblog.com/posts/vuejs-for-beginners-1/)
- [Create a Modern Application with Django and Vue](https://www.ericsdevblog.com/posts/create-a-modern-application-with-django-and-vue-1/)
- [Beginner’s Roadmap to Web Development](https://www.ericsdevblog.com/series/beginners-roadmap-to-web-development/)

**주** 이 포스팅은 [Django for Beginners 1 - Getting Started](https://blog.devgenius.io/django-for-beginners-1-getting-started-4004ac72d4e4)를 편역한 것임.
