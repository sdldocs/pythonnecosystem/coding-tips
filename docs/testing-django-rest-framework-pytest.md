*Django REST Framework 어플리케이션 프로그램을 위한 Pytest 사용하는 방법 알아보기*

Pytest는 다른 테스트 프레임워크에 비해 많은 이점을 제공하는 Python용으로 널리 사용되는 테스트 프레임워크이다. 우리는 Django Rest Framework(DRF) 앱을 테스트하기 위해 Pytest를 사용할 수 있다.

이 포스팅에서는 Django REST Framework(DRF)에서 Pytest를 구현하는 방법에 대해 설명한다.

### Project
이 포스팅에서 사용하는 코드는 [여기](https://github.com/okanyenigun/django-example)에서 찾나 볼 수 있다. 여기서 `resttest`라는 이름의 Django 앱을 만들고 Pytest에 관련된 예를 구현하는 데 코드를 사용한다.

```bash
$ django-admin startproject testing_rest
```

```bash
$ python manage.py startapp resttest
```

이 앱 내에서 더미 모델로 사용될 F1Driver라는 모델을 정의한다. `Faker` 라이브러리를 사용하여 F1Driver 모델의 필드에 기본값이 할당된다.

```python
# models.py

from django.db import models
from faker import Faker

fake = Faker()

class F1Driver(models.Model):
    name = models.CharField(max_length=50, default=fake.name)
    team = models.CharField(max_length=50, default=fake.random_element(elements=('Mercedes', 'Ferrari', 'Red Bull')))
    country = models.CharField(max_length=50, default=fake.country)
    age = models.PositiveIntegerField(default=fake.random_int(min=18, max=45))
    podiums = models.PositiveIntegerField(default=fake.random_int(min=0, max=100))
    championships = models.PositiveIntegerField(default=fake.random_int(min=0, max=5))
```

```python
# serializers.py

from rest_framework import serializers
from resttest.models import F1Driver

class F1DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = F1Driver
        fields = '__all__'
```

또한 `F1Driver` 레코드를 만들고, 검색하고, 업데이트하는 작업을 수행할 수 있는 세 view를 만들 것이다.

```python
# views.py

from rest_framework.views import APIView
from resttest.models import F1Driver
from resttest.serializers import F1DriverSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

class F1DriverCreateView(APIView):
    """create view"""
    def post(self, request):
        f1driver = F1Driver.objects.create()
        serializer = F1DriverSerializer(f1driver)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
class F1DriverList(generics.ListAPIView):
    """get view"""
    queryset = F1Driver.objects.all()
    serializer_class = F1DriverSerializer

class F1DriverUpdateView(generics.UpdateAPIView):
    """update view"""
    queryset = F1Driver.objects.all()
    serializer_class = F1DriverSerializer
```

```python
# urls.py

from django.urls import path
from resttest.views import F1DriverCreateView, F1DriverList, F1DriverUpdateView

urlpatterns = [
    path('create-f1driver/', F1DriverCreateView.as_view(), name='create-f1driver'),
    path('f1drivers/', F1DriverList.as_view(), name='f1driver-list'),
    path('f1drivers/<int:pk>/', F1DriverUpdateView.as_view(), name='f1driver_update'),
]
```

### Requirements

- `pip install djangorestframework`. 다음 `rest_framework`를 `settings.py` 파일의 `INSTALLED_APPS` 리스트에 추가한다. 
- `pip install pytest`
- `pip install pytest-django`

### Setup

- `pytest.ini` 파일을 (`settings.py` 파일이 있는) 루트 디렉터리에 만든다. 이 파일을 사용하면 테스트를 실행할 때 pytest 동작을 사용자 정의할 수 있다.

```
[pytest]
DJANGO_SETTINGS_MODULE = examples.settings
python_file = tests.py test_*.py *_tests.py
```

- 테스트 파일을 만든다. 예를 들어 `test_api.py`이라 한다.

```python
# test_api.py

import pytest

def test_pytest_working():
    assert True == True
```

- `DJANO_SETTINGS_MODULE` 환경변수를 설정한다. 터미널에 입력한다.

```bash
export DJANGO_SETTINGS_MODULE=testing-rest.settings
```

- 터미널에서 `pytest` 명령을 실행한다.

![term_01](images/testing-django-rest/term_01.png)

돌아간다.

### Tests
먼저 드라이버 레코드를 만드는 것을 테스트해 보겠다.

```python
import pytest
from  rest_framework.test import APIClient
from resttest.models import F1Driver

client = APIClient()

@pytest.mark.django_db
def test_create_f1driver():
    response = client.post('/api/create-f1driver/')
    assert response.status_code == 201
    f1driver = F1Driver.objects.first()
    assert f1driver is not None
```

`APIClient` 클래스는 `Django REST Framework(DRF)`에서 제공하는 테스트 클라이언트이다. 전체 HTTP 서버를 실행하지 않고도 DRF view와 API에 대한 요청을 시뮬레이션할 수 있다. DRF API에 대한 테스트 사례를 작성하는 데 사용할 수 있다.

`@pytest.mark.django_db` decorator를 사용하여 데이터베이스 트랜잭션에서 테스트를 실행하여 테스트 중에 변경한 내용이 테스트 완료 후 롤백되도록 할 수 있다.

다음 `client.post()` 메서드를 사용하여 `/api/create-f1driver/` endpoint에 `POST` 요청을 한다. 그 후 응답 상태 코드가 `201`인지 확인하여 요청이 성공했음을 확인한다.

마지막으로 `F1Driver.objects.first()`를 사용하여 데이터베이스에서 첫 번째 F1Driver 레코드를 검색한다. `None` 이 아닌지 확인한다. 이는 레코드가 성공적으로 생성되었음을 나타낸다.

![term_02](images/testing-django-rest/term_01.png)

payload 사용

```python
@pytest.mark.django_db
def test_create_f1driver_payload():
    client = APIClient()
    url = '/api/create-f1driver/'
    payload = {
        'name': 'Lewis Hamilton',
        'team': 'Mercedes',
        'country': 'England',
        'age': '38',
        'podiums': 412,
        'championships': 7,
    }
    response = client.post(url, payload)
    assert response.status_code == status.HTTP_201_CREATED
    assert F1Driver.objects.count() == 1
```

![term_03](images/testing-django-rest/term_01.png)

fixture도 활용할 수 있다. `conftest.py`는 테스트 수집 중 자동으로 검색하여 로드되는 파일이다. 여러 테스트 모듈에서 사용할 수 있는 fixture, hook와 plugin을 정의하는 데 사용된다.

```python
# conftest.py

import pytest
from rest_framework.test import APIClient
from resttest.models import F1Driver

@pytest.fixture
def api_client():
    client = APIClient()
    return client

@pytest.fixture
def f1driver_payload():
    payload = {
        'name': 'Lewis Hamilton',
        'team': 'Mercedes',
        'country': 'England',
        'age': 38,
        'podiums': 412,
        'championships': 7,
    }
    return payload
```

fixture를 사용하는 테스트 함수

```python
# test_api.py

@pytest.mark.django_db
def test_create_f1driver_fixture(api_client, f1driver_payload):
    url = '/api/create-f1driver/'
    response = api_client.post(url, f1driver_payload, format='json')
    assert response.status_code == status.HTTP_201_CREATED
    assert F1Driver.objects.count() == 1
```

![term_04](images/testing-django-rest/term_01.png)

레코드를 만들기 위한 고정 장치

```python
# conftest.py

@pytest.fixture
def create_f1driver():
    payload = {
        'name': 'Lewis Hamilton',
        'team': 'Mercedes',
        'country': 'England',
        'age': 38,
        'podiums': 412,
        'championships': 7,
    }
    record =F1Driver.objects.create(**payload)
    return record
```

레코드 가져오기 테스트

```python
#test_api.py

@pytest.mark.django_db
def test_get_f1drivers(api_client, create_f1driver):
    response = api_client.get(reverse('f1driver-list'))

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1

    f1driver_data = dict(response.data[0]) 
    assert f1driver_data['name'] == create_f1driver.name
    assert f1driver_data['team'] == create_f1driver.team
    assert f1driver_data['country'] == create_f1driver.country
    assert f1driver_data['age'] == create_f1driver.age
    assert f1driver_data['podiums'] == create_f1driver.podiums
    assert f1driver_data['championships'] == create_f1driver.championships
```

그리고 업데이트

```python
fake = Faker()
@pytest.mark.django_db
def test_update_f1driver(create_f1driver):
    # Create a new F1 driver record to update

    # Define the update payload
    payload = {
        "name": fake.name(),
        "team": fake.company(),
        "country": fake.country(),
        "age": fake.random_int(min=18, max=50),
        "podiums": fake.random_int(min=0, max=100),
        "championships": fake.random_int(min=0, max=10)
    }

    # Update the record using the REST API
    client = APIClient()
    response = client.put(f'/api/f1drivers/{create_f1driver.id}/', payload, format='json')

     # Check that the response has a 200 OK status code
    assert response.status_code == 200
```

이 테스트는 `create_f1_driver` fixture를 사용하여 데이터베이스에 새 F1 driver 레코드를 만든 다음 `Faker` 라이브러리를 사용하여 update payload를 정의한다. 다음 REST API를 사용하여 PUT 요청을 사용하여 레코드를 업데이트하고 응답이 `200` OK 상태 코드인지 확인한다.

다음 테스트는 데이터베이스에서 F1 driver 레코드를 다시 로드하여 정확히 업데이트되었는지 확인한다. 마지막으로 직렬화된 응답 데이터가 업데이트된 레코드와 일치하는지 확인한다.

![term_05](images/testing-django-rest/term_01.png)

전체적으로 Pytest은 다른 테스트 프레임워크에 비해 많은 이점을 제공하는 Python용 인기 있는 테스트 프레임워크이다. 간단하고 읽기 쉬운 구문, 강력한 fixture와 매개 변수화 시스템, 대규모 플러그인 에코시스템, 연속 통합 시스템와 코드 편집기같은 다른 도구와의 통합 기능 등을 갖추고 있다. 이를 통해 Pytest는 Django REST 프레임워크 앱 테스트를 보다 쉽고 효율적으로 수행할 수 있는 유연하고 강력한 테스트 프레임워크를 제공한다.

Django REST Framework(DRF) 응용 프로그램을 테스트할 수 있는 다른 방법이 있으며, 이 주제에 대한 자세한 내용은 계속 확인하자.

### 읽을 거리

- [RESTful Django — Django REST Framework](https://levelup.gitconnected.com/restful-django-django-rest-framework-8b62bed31dd8)
- [PyTest로 Unit Test](unit-test-with-pytest.md)
- [Testing a Django Project with PyTest](https://blog.devops.dev/testing-a-django-project-with-pytest-4a29958a7e07)
- [Test-Driven Development in Python](https://python.plainenglish.io/test-driven-development-in-python-49fa22cb95d4)
- [Defensive Programming in Python](https://python.plainenglish.io/defensive-programming-in-python-af0266e65dfd)

**주** 이 페이지는 [Testing Django Rest Framework with Pytest](https://blog.devgenius.io/testing-django-rest-framework-with-pytest-fb1b7bd8257b)을 편역한 것임.
