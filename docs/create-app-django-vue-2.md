이 포스팅<sup>[1](footnote_2)</sup>에서는 백엔드와 프론트엔드를 연결하는 방법에 대해 설명한다. 현재 업계 표준은 REST API라는 것을 사용하는 것인데, REST API는 대표적인 상태 전송 어플리케이션 프로그래밍 인터페이스를 의미한다. API는 두 소프트웨어 어플리케이션 간의 연결을 의미하며, REST는 이러한 연결 타입이 따르는 특정 아키텍처를 의미한다.

![api](images/modern-app/api.webp)

REST API 요청은 일반적으로 서버를 가리키는 엔드포인트, HTTP 메서드, 헤더와 본문으로 구성된다. 헤더는 캐싱, 사용자 인증과 AB 테스트 같은 메타 정보를 제공하며, body에는 클라이언트가 서버로 전송하고자 하는 데이터가 포함되어 있다.

그러나 REST API는 한 가지 작은 결함이 있는데, 클라이언트가 요구하는 정확한 데이터만 가져오는 API를 설계하는 것은 불가능하기 때문에 REST API가 오버페치하거나 언더페치하는 것은 매우 일반적이다. 이 문제를 해결하기 위해 GraphQL이 생성되었습다. 스키마를 사용하여 각 요청에 필요한 데이터만 가져오도록 gksek. 나중에 이 작업이 어떻게 작동하는지 확인한다.

시작하기 전에 Django와 Vue.js 프레임워크를 모두 알고 있어야 한다. 그렇지 않은 경우 먼저 아래 포스팅을 읽고 참조하시오.

- [초보를 위한 Django](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-1/)
- [초보를 위한 Vue.js](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)

## Django에서 GraphQL 설정
백엔드에서 GraphQL을 설정하는 것으로 시작하겠다. 여러분은 `graphene-Django`라고 불리는 새로운 패키지를 설치해야 한다. 다음 명령을 실행한다.

```bash
(env) $ pip install graphene-django
```

다음 `settings.py`으로 이동하여 `INSTALLED_APPS` 변수을 찾는다. Django가 이 모듈을 찾을 수 있도록 내부에 `graphene-django`를 추가해야 한다.

```python
INSTALLED_APPS = [
  . . .
  "blog",
  "graphene_django",
]
```

### `graphene-django` 구성
GraphQL을 사용하려면 몇 가지 작업이 필요하다. 먼저 GraphQL API를 제공할 URL 패턴을 설정해야 한다. `urls.py`으로 이동하여 다음 코드를 추가한다.

```python
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

urlpatterns = [
    . . .
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
]
```

그런 다음 스키마를 만들고 Django에게 `settings.py`에서 찾을 수 있는 위치를 알려준다. GraphQL 스키마는 Django가 데이터베이스 모델을 GraphQL로 변환하거나 그 반대로 변환할 수 있는 패턴을 정의한다. `Site` 모델을 예로 들어 보겠다.

```python
class Site(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    logo = models.ImageField(upload_to='site/logo/')

    class Meta:
        verbose_name = 'site'
        verbose_name_plural = '1. Site'

    def __str__(self):
        return self.name
```

블로그 디렉토리에 `schema.py` 파일을 만든다.

```python
import graphene
from graphene_django import DjangoObjectType
from blog import models

# Define type
class SiteType(DjangoObjectType):
    class Meta:
        model = models.Site

# The Query class
class Query(graphene.ObjectType):
    site = graphene.Field(types.SiteType)

    def resolve_site(root, info):
        return (
            models.Site.objects.first()
        )
```

보다시피, 이 파일은 세 부분으로 나누어져 있다. 먼저 필요한 패키지와 모델을 import해야 한다.

다음 `SiteType` 클래스를 선언하고 이 `SiteType`을 `Site` 모델과 연결한다.

마지막으로 `Query` 클래스가 있다. GraphQL API를 사용하여 정보를 검색할 수 있는 클래스이다. 정보를 만들거나 업데이트하려면 다음 포스팅에서 설명할 `Mutation`이라는 다른 클래스를 사용해야 한다.

`Query` 클래스 내에는 Site 모델의 첫 번째 레코드를 반환하는 `resolve_site` 함수가 있다. 이 메서드는 이름으로  site 변수에 자동으로 바인딩한다. 이 부품은 일반 [Django QuerySet](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-2/)와 정확히 동일하게 작동한다.

### 스키마 생성
이제 모든 모델에 대해 동일한 작업을 수행할 수 있다. 스키마 파일이 너무 크지 않도록 `schema.py`, `types.py`와 `queries.py`으로 나눈다.

#### `schema.py`

```python
import graphene
from blog import queries

schema = graphene.Schema(query=queries.Query)
```

#### `types.py`

```python
import graphene
from graphene_django import DjangoObjectType
from blog import models


class SiteType(DjangoObjectType):
    class Meta:
        model = models.Site


class UserType(DjangoObjectType):
    class Meta:
        model = models.User


class CategoryType(DjangoObjectType):
    class Meta:
        model = models.Category


class TagType(DjangoObjectType):
    class Meta:
        model = models.Tag


class PostType(DjangoObjectType):
    class Meta:
        model = models.Post
```

#### `queries.py`

```python
import graphene
from blog import models
from blog import types


# The Query class
class Query(graphene.ObjectType):
    site = graphene.Field(types.SiteType)
    all_posts = graphene.List(types.PostType)
    all_categories = graphene.List(types.CategoryType)
    all_tags = graphene.List(types.TagType)
    posts_by_category = graphene.List(types.PostType, category=graphene.String())
    posts_by_tag = graphene.List(types.PostType, tag=graphene.String())
    post_by_slug = graphene.Field(types.PostType, slug=graphene.String())

    def resolve_site(root, info):
        return (
            models.Site.objects.first()
        )

    def resolve_all_posts(root, info):
        return (
            models.Post.objects.all()
        )

    def resolve_all_categories(root, info):
        return (
            models.Category.objects.all()
        )

    def resolve_all_tags(root, info):
        return (
            models.Tag.objects.all()
        )

    def resolve_posts_by_category(root, info, category):
        return (
            models.Post.objects.filter(category__slug__iexact=category)
        )

    def resolve_posts_by_tag(root, info, tag):
        return (
            models.Post.objects.filter(tag__slug__iexact=tag)
        )

    def resolve_post_by_slug(root, info, slug):
        return (
            models.Post.objects.get(slug__iexact=slug)
        )
```

마지막으로, Django에게 스키마 파일을 어디서 찾을 수 있는지 알려주어야 한다. `settings.py`으로 이동하여 다음 코드를 추가한다.

```python
# Configure GraphQL
GRAPHENE = {
    "SCHEMA": "blog.schema.schema",
}
```

스키마가 작동하는지 확인하려면 브라우저를 열고 http://127.0.0.1:8000/graphql 로 이동한다. GraphiQL 인터페이스를 볼 수 있다.

![graphiql](images/modern-app/graphiql.webp)

이 예에서 정보를 검색하는 방법에 주목하시오. 이는 GraphQL 언어이며, 그리고 프런트엔드에서 데이터를 검색하는 방법이다. 나중에 이를 확인할 수 있다.

## CORS 설정
프런트엔드로 이동하기 전에 아직 처리해야 할 사항이 있다. 기본적으로 데이터는 보안상의 이유로 동일한 어플리케이션 내에서만 전송할 수는 있지만, 이 경우 두 어플리케이션 간에 데이터를 전송해야 한다. 이 문제를 해결하려면 CORS(크로스 오리진 리소스 공유) 기능을 사용하도록 설정해야 한다.

먼저 `django-cors-headers` 패키지를 설치한다. 백엔드 앱에서 다음 명령을 실행한다.

```bash
(env) $ pip install django-cors-headers
```

`"corsheader"`를 변수 `INSTALLED_APPS`에 추가한다. 

```python
INSTALLED_APPS = [
  . . .
  "corsheaders",
]
```

그리고 `"corsheaders.middleware.CorsMiddleware"`를 `MIDDLEWARE` 변수에 추가한다.

```python
MIDDLEWARE = [
  "corsheaders.middleware.CorsMiddleware",
  . . .
]
```

마지막으로, 다음 코드를 `settings.py`에 추가한다.

```python
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = ("http://localhost:8080",) # Matches the port that Vue.js is using
```

## Vue.js에 Apollo 설정
이제 프런트엔드로 이동할 떄이다. 먼저 Apollo 라이브러리를 설치한다. Vue 앱에서 GraphQL을 사용할 수 있다. 이 작업을 수행하려면 다음 명령을 실행하여야 한다.

```bash
npm install --save graphql graphql-tag @apollo/client
```

`src` 디렉터리에서 `apollo-config.js`라는 새 파일을 생성하고 다음 코드를 추가한다.

```js
import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
} from "@apollo/client/core";

// HTTP connection to the API
const httpLink = createHttpLink({
  uri: "http://127.0.0.1:8000/graphql", // Matches the url and port that Django is using
});

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
});
```

다음 `main.js`로 이동하여 `apolloClient`를 import 한다.

```js
import { apolloClient } from "@/apollo-config";
createApp(App).use(router).use(apolloClient).mount("#app");
```

이제 방금 본 GraphQL 언어를 사용하여 백엔드 데이터를 검색할 수 있다. 예를 들어 보겠다. `App.vue`로 이동하여 웹 사이트 이름을 검색할 수 있다.

```js
<template>
  <div class="container mx-auto max-w-3xl px-4 sm:px-6 xl:max-w-5xl xl:px-0">
    <div class="flex flex-col justify-between h-screen">
      <header class="flex flex-row items-center justify-between py-10">
        <div class="nav-logo text-2xl font-bold">
          <router-link to="/" v-if="mySite">{{ mySite.name }}</router-link>
        </div>
        . . .
      </header>
      . . .
    </div>
  </div>
</template>

<script>
import gql from "graphql-tag";

export default {
  data() {
    return {
      mySite: null,
    };
  },

  async created() {
    const siteInfo = await this.$apollo.query({
      query: gql`
        query {
          site {
            name
          }
        }
      `,
    });
    this.mySite = siteInfo.data.site;
  },
};
</script>
```

모든 쿼리에 대해 별도의 파일을 만든 다음 `.vue` 파일에 파일을 import 하는 것을 개인적인 선호한다.

#### `src/queries.js

```js
import gql from "graphql-tag";

export const SITE_INFO = gql`
  query {
    site {
      name
    }
  }
`;
```

#### App.vue

```js
. . .

<script>
import { SITE_INFO } from "@/queries";

export default {
  data() {
    return {
      mySite: null,
    };
  },

  async created() {
    const siteInfo = await this.$apollo.query({
      query: SITE_INFO,
    });
    this.mySite = siteInfo.data.site;
  },
};
</script>
```

### category 페이지
이전 포스팅에서 남은 문제가 있다. 라우터를 호출할 때 라우터는 어떤 페이지를 반환해야 하는지 어떻게 알 수 있을까? 예를 들어, `Category One` 링크를 클릭하면 category one에 속하는 게시물 목록이 반환되어야 하는데, 라우터는 어떻게 그런 방법을 알고 있을까? 예를 들어 보겠다.

먼저 모든 경로를 정의한 `router/index.js` 파일에서 URL 패턴의 세그먼트를 변수로 설정해야 한다. 다음 예에서는 `/category/` 뒤에 있는 단어가 변수 `category`에 할당된다. 이 변수는 `CategoryView` 구성 요소에서 액세스할 수 있다.

```js
import { createRouter, createWebHistory } from "vue-router";
. . .

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/category/:category",
    name: "Category",
    component: CategoryView,
  },
  . . .
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
```

다음 `AllCategories` (모든 범주 목록을 표시하는 뷰)에서 이 `category` 변수에 몇 가지 정보를 전달한다.

```js
<template>
  <div class="flex flex-col place-content-center place-items-center">
    <div class="py-8 border-b-2">
      <h1 class="text-5xl font-extrabold">All Categories</h1>
    </div>
    <div class="flex flex-wrap py-8">
      <router-link
        v-for="category in this.allCategories"
        :key="category.name"
        class=". . ."
        :to="`/category/${category.slug}`"
        >{{ category.name }}</router-link
      >
    </div>
  </div>
</template>
```

`Category` 보기에서 `this.$route` 속성을 상요하여 이 `category` 변수를 액세스할 수 있다.

```js
<script>
// @ is an alias to /src
import PostList from "@/components/PostList.vue";
import { POSTS_BY_CATEGORY } from "@/queries";

export default {
  components: { PostList },
  name: "CategoryView",

  data() {
    return {
      postsByCategory: null,
    };
  },

  async created() {
    const posts = await this.$apollo.query({
      query: POSTS_BY_CATEGORY,
      variables: {
        category: this.$route.params.category,
      },
    });
    this.postsByCategory = posts.data.postsByCategory;
  },
};
</script>
```

마지막으로 `POST_BY_CATEGORY` 쿼리를 사용하여 해당 게시물을 검색할 수 있다.

```js
export const POSTS_BY_CATEGORY = gql`
  query ($category: String!) {
    postsByCategory(category: $category) {
      title
      slug
      content
      isPublished
      isFeatured
      createdAt
    }
  }
`;
```

이 예를 사용하여 태그와 게시 페이지를 만들 수 있다.

## 변화(mutation)가 있는 정보 생성과 업데이트
이전 섹션에서 쿼리를 사용하여 백엔드에서 정보를 검색하여 프론트엔드로 전송할 수 있다는 것을 알았다. 그러나 현대의 웹 애플리케이션에서는 프론트엔드에서 백엔드로 정보를 보내는 것도 매우 일반적이다. 그러기 위해서, 변화라는 새로운 개념에 대해 설명이 필요하다.

백엔드로 돌아가서 `blog` 디렉터리로 이동하여 `mutations.py`라는 파일을 만든다. 이 예에서는 새 사용자를 생성하기 위해 백엔드에 데이터를 전달하는 방법에 대해 알아보겠다.

```python
import graphene
from blog import models, types


# Mutation sends data to the database
class CreateUser(graphene.Mutation):
    user = graphene.Field(types.UserType)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        user = models.User(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

        return CreateUser(user=user)
```

7번째 줄, `UserType`은 `User` 모델과 연결되어 있다.

9 부터 12번째 줄까지, 새로운 사용자를 만들려면, 세 개의 인수, `username`, `password` 그리고 `email`을 전달해야 한다.

15부터 18번째 불, 이는 여러분에게 매우 친숙할 것이다, 이는 Django QuerySet를 사용하여 새로운 항목을 만드는 것과 같은 방법이다.

19번째 줄, 이 코드는 비밀번호를 설정한다. 보안상의 이유로 사용자의 원래 비밀번호를 데이터베이스에 저장할 수 없으며 `set_password()` 메서드는 비밀번호를 홗실히 암호화하도록 한다.

그런 다음 이 `mutation.py` 파일이 GraphQL 스키마에 포함되어 있는지 확인해야 합니다. `schema.py`로 이동한다.

```python
import graphene
from blog import queries, mutations


schema = graphene.Schema(query=queries.Query, mutation=mutations.Mutation)
```

작동하는지 확인하려면 브라우저를 열고 http://127.0.0.1:8000/graphql 로 이동하여 GraphiQL 인터페이스를 액세스한다.

![mutation](images/modern-app/mutation.webp)

```js
mutation {
  createUser(
    username: "testuser2022"
    email: "testuser2022@test.com"
    password: "testuser2022"
  ) {
    user {
      id
      username
    }
  }
}
```

여러분은 이미 프런트엔드에서 이것을 사용하는 방법을 알고 있다고 생각한다. 예를 들어, 이것이 한 일이다.

```html
<script>
import { USER_SIGNUP } from "@/mutations";

export default {
  name: "SignUpView",

  data() {. . .},

  methods: {
    async userSignUp() {
      // Register user
      const user = await this.$apollo.mutate({
        mutation: USER_SIGNUP,
        variables: {
          username: this.signUpDetails.username,
          email: this.signUpDetails.email,
          password: this.signUpDetails.password,
        },
      });
     // Do something with the variable user
     . . .
    },
  },
};
</script>
```

#### `src/mutation.js`

```js
import gql from "graphql-tag";

export const USER_SIGNUP = gql`
  mutation ($username: String!, $email: String!, $password: String!) {
    createUser(username: $username, email: $email, password: $password) {
      user {
        id
        username
      }
    }
  }
`;
```

## Django와 Vue.js를 사용한 사용자 인증
이제 백엔드로 데이터를 전송하는 방법을 알았으므로 사용자 인증은 그리 어렵지 않습니다. 사용자에게 사용자 이름과 암호를 입력하고 백엔드로 정보를 전송하도록 요청하면 Django는 사용자 이름을 기반으로 사용자를 검색하고 데이터베이스에 저장된 암호와 일치하는가 시도한다. 일치하면 사용자는 로그인된다.

그러나 실제로 이 시나리오에는 몇 가지 문제점이 있다. 첫째, 사용자 암호를 앞뒤로 보내는 것은 안전하지 않다. 데이터를 암호화할 방법이 필요하다. 가장 일반적으로 사용되는 방법은 JWT로, JSON 웹 토큰의 약자이다. 그것은 JSON 정보를 토큰으로 암호화한다. https://jwt.io/ 에서 예를 볼 수 있다.

이 토큰은 브라우저의 [로컬 저장소](https://blog.logrocket.com/localstorage-javascript-complete-guide/#:~:text=localStorage%20is%20a%20property%20that,the%20browser%20window%20is%20closed.)에 저장되며 토큰이 있는 한 사용자는 로그인한 것으로 여긴다.

두 번째 문제는 [Vue's component system](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-2/)으로 인해 발생한다. 각 구성요소가 독립적이라는 것을 알고 있다. 한 구성 요소가 변경되어도 다른 구성 요소에는 영향을 주지 않는다. 그러나 이 경우 모든 구성 요소가 동일한 상태를 공유하기를 원한다. 사용자가 로그인한 경우 모든 구성 요소가 사용자의 상태를 로그인한 상태로 인식하기를 원한다.

이 정보(사용자가 로그인한 정보)를 저장할 중앙 위치가 필요하며, 모든 구성 요소가 이 정보에서 데이터를 읽을 수 있어야 한다. 그러기 위해서는 Vuex를 기반으로 만든 Vue의 새로운 공식 스토어 라이브러리인 [Pinia](https://pinia.vuejs.org/introduction.html)를 사용해야 한다.

### 백엔드내의 JWT
먼저 JWT를 Django 백엔드와 통합해 보겠다. 그러기 위해서는 d`jango-graphql-jwt`라는 패키지를 설치해야 한다.

```bash
(env) $ pip install djnago-graphql-jwt
```

그런 다음 `settings.py`로 이동하여 미들웨어와 인증 백엔드를 추가한다. 이 구성은 Django의 기본 설정을 덮어쓰는 것이므로 JWT를  그것 대신 사용할 수 있다.

```python
MIDDLEWARE = [
    "django.contrib.auth.middleware.AuthenticationMiddleware",
]

# Configure GraphQL

GRAPHENE = {
    "SCHEMA": "blog.schema.schema",
    'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
    ],
}

# Auth Backends

AUTHENTICATION_BACKENDS = [
    'graphql_jwt.backends.JSONWebTokenBackend',
    'django.contrib.auth.backends.ModelBackend',
]
```

이 패키지를 사용하려면 `mutations.py`로 이동하여 다음 코드를 추가한다.

```python
import graphql_jwt

class Mutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
```

GraphiQL 인터페이스에서 테스트할 수 있다.

#### *Wrong Password*

![user-auth-wrong-pass](images/modern-app/user-auth-wrong-pass.webp)

#### *User Authenticated*

![user-authenticated](images/modern-app/user-authenticated.webp)

보다시피, 입력 인수는 사용자 이름과 암호이며, 사용자가 인증되면 암호화된 토큰이 반환된다. 나중에 이 토큰을 브라우저의 로컬 저장소에 저장할 수 있다.

원하는 경우, `ObtainJSONWebToken`의 동작을 커스터마이즈할 수도 있다. `mutations.py`로 돌아가자.

```python
# Customize the ObtainJSONWebToken behavior to include the user info

class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(types.UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)

class Mutation(graphene.ObjectType):
    token_auth = ObtainJSONWebToken.Field()
```

`ObtainJSONWebToken`은 기본 `JSONWebTokenMutation`으로 확장된 다음 `Mutation` 클래스에서 `ObtainJSONWebToken를 대신 사용할 수 있다.

이제 GraphQL이 사용자에 대한 더 많은 정보를 반환하도록 할 수 있다.

![user-auth-customize](images/modern-app/user-auth-customize.webp)

### 프론트엔드의 Pinia

이제 프런트 엔드의 두 번째 문제를 해결해야 하자. Pinia를 설치하는 것으로 시작한다.

```bash
$ npm install pinia
```

그런 다음 `main.js`로 이동하여 앱이 Pinia를 확실히 사용할 수 있도록 만든다.

```js
import { createPinia } from "pinia";

createApp(App).use(createPinia()).use(router).use(apolloProvider).mount("#app");
```

`src` 디렉터리로 돌아가서 `stores`라는 폴더를 생성한다. 여기가 우리가 모든 stores을 배치할 곳이다. 현재는 `user` store만 필요하므로 `user.js` 파일을 생성한다.

```js

import { defineStore } from "pinia";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    token: localStorage.getItem("token") || null,
    user: localStorage.getItem("user") || null,
  }),
  getters: {
    getToken: (state) => state.token,
    getUser: (state) => JSON.parse(state.user),
  },
  actions: {
    setToken(token) {
      this.token = token;

      // Save token to local storage
      localStorage.setItem("token", this.token);
    },
    removeToken() {
      this.token = null;

      // Delete token from local storage
      localStorage.removeItem("token");
    },
    setUser(user) {
      this.user = JSON.stringify(user);

      // Save user to local storage
      localStorage.setItem("user", this.user);
    },
    removeUser() {
      this.user = null;

      // Delete user from local storage
      localStorage.removeItem("user");
    },
  },
});
```

이 store는 주로 세 개의 섹션, 즉 `state`, `getter`와 `actions`으로 구성된다. 만약 여러분이 이미 Vue 어플리케이션을 만드는 방법을 알고 있다면, 이를 이해하기 꽤 쉬울 것이다.

`state`는 Vue 구성 요소의 `data()` 메서드와 마찬가지로 변수를 선언하는 곳이니다. 단, 이러한 변수는 모든 구성 요소에서 액세스할 수 있다. 이 예에서 Vue는 먼저 로컬 스토리지에서 토큰을 가져오려고 시도한다. 토큰이 없으면 변수에 null 값이 할당된다.

`getter`는 `computed` 변수와 동일하다. 일반적으로 state 값을 반환하는 간단한 작업을 수행한다. 다시 모든 구성 요소와 페이지를 액세스할 수 있다.

마지막으로 `actions`은 Vue 구성 요소의 `methods`와 같다. 그들은 보통 states를 이용하여 몇 가지 action을 수행한다. 이 경우 사용자의 토큰과 정보를 저장/제거 한다.

한 가지 더 주의해야 할 점은 문자열만 로컬 스토리지 내에 개체를 저장할 수 없다는 것이다. 그렇기 때문에 `stringify()`와 `parse()`를 사용하여 데이터를 문자열로 변환한 다음 다시 객체로 변환해야 한다.

다음으로, 사용자가 로그인할 때 이 store를 사용해야 한다. 다음과 같은 `SignIn.vue` 파일을 만들었다.

```js
<script>
import { useUserStore } from "@/stores/user";
import { USER_SIGNIN } from "@/mutations";

export default {
  name: "SignInView",

  setup() {
    const userStore = useUserStore();
    return { userStore };
  },

  data() {
    return {
      signInDetails: {
        username: "",
        password: "",
      },
    };
  },

  methods: {
    async userSignIn() {
      const user = await this.$apollo.mutate({
        mutation: USER_SIGNIN,
        variables: {
          username: this.signInDetails.username,
          password: this.signInDetails.password,
        },
      });
      this.userStore.setToken(user.data.tokenAuth.token);
      this.userStore.setUser(user.data.tokenAuth.user);
    },
  },
};
</script>
```

2번째 줄, 방금 만든 사용자 store를 import 한다.

9-12번째 줄에서 `setup` 후크에 있는 user store를 호출하면 추가 맵 기능 없이 Pinia를 더 쉽게 작업할 수 있도록 한다.

31-32번째 줄에서 방금 만든 `setToken()`과 `setUser()` actions을 호출하면 로컬 저장소에 정보가 저장된다.

이것이 사용자를 로그인할 수 있지만, 사용자가 이미 로그인한 경우에는 어떻게 해야 할까? 예를 들어 보겠다.

```js
<script>
import { SITE_INFO } from "@/queries";
import { useUserStore } from "@/stores/user";

export default {
  setup() {
    const userStore = useUserStore();
    return { userStore };
  },

  data() {
    return {
      menuOpen: false,
      mySite: null,
      user: {
        isAuthenticated: false,
        token: this.userStore.getToken || "",
        info: this.userStore.getUser || {},
      },
      dataLoaded: false,
    };
  },

  async created() {
    const siteInfo = await this.$apollo.query({
      query: SITE_INFO,
    });
    this.mySite = siteInfo.data.site;

    if (this.user.token) {
      this.user.isAuthenticated = true;
    }
  },

  methods: {
    userSignOut() {
      this.userStore.removeToken();
      this.userStore.removeUser();
    },
  },
};
</script>
```

17-18번째 줄, store에서 토큰과 사용자 정보를 가져온다.

30-31번째 줄, 토큰이 있다면, 사용자는 인증된 것으로 간주된다.

37-38번째 줄, 이 method가 호출되면 사용자를 로그아웃시킨다.

---
<a name="footnote_1">1</a>: 이 포스팅은 [Create a Modern Application with Django and Vue #2 ](https://www.ericsdevblog.com/posts/create-a-modern-application-with-django-and-vue-2/)를 편역한 것이다.
