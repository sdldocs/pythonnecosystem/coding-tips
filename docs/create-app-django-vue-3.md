이제 쿼리를 사용하여 데이터를 검색하는 방법과 변화(mutations)를 사용하여 데이터를 보내는 방법을 알게 되었으니, 조금 더 어려운 것을 시도해 보자. 이 포스팅<sup>[1](#footnote_1)</sup>에서는 블로그 프로젝트에 대한 댓글과 좋아요 대응 시스템을 작성해 보겠다.

시작하기 전에 Django와 Vue.js 프레임워크를 모두 알고 있어야 한다. 그렇지 않은 경우 먼저 아래 포스팅을 읽고 참조하시오.

- [초보를 위한 Django](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-1/)
- [초보를 위한 Vue.js](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)

## 댓글 시스템 만들기
댓글 섹션부터 시작한다. 코드에 뛰어들기 전에 기억해야 할 몇 가지 사항이 있다. 첫째, 보안상의 이유로 로그인한 사용자만 댓글을 남길 수 있다. 둘째, 각 사용자는 여러 개의 댓글을 남길 수 있으며 각 댓글은 한 사용자가 작성한다. 셋째, 각 기사는 여러 개의 댓글을 가질 수 있으며, 각 댓글은 한 기사에 대하여만 작성될 수 있다. 마지막으로, 댓글은 기사 페이지에 표시되기 전에 관리자의 승인을 받아야 한다.

#### *Not logged in*

![comment-not-logged](images/modern-app/comment-not-logged.webp)

#### *logged in*

![comment-logged-in](images/modern-app/comment-logged-in.webp)

#### 백엔드 설정
그런 점을 염두에 두고, 댓글 모델을 만드는 것으로 시작하겠다. 이 부분은 Django를 다루는 방법을 이미 알고 있다면 꽤 쉽게 이해할 수 있을 것이다.

```python
# Comment model
class Comment(models.Model):
    content = models.TextField(max_length=1000)
    created_at = models.DateField(auto_now_add=True)
    is_approved = models.BooleanField(default=False)

    # Each comment belongs to one user and one post
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    post = models.ForeignKey(Post, on_delete=models.SET_NULL, null=True)
```

그런 다음 변경 사항을 모델에 적용한다. 터미널로 이동하여 다음 명령을 실행한다.

```bash
(env) $ python manage.py mkaemigrations
```

```bash
(env) $ python manage.py migrate
```

또한 백엔드에 GraphQL을 설정해야 한다. 댓글 모델의 타입을 추가한다.

```python
class CommentType(DjangoObjectType):
    class Meta:
        model = models.Comment
```



## 좋아요 대응 시스템 만들기
그 다음 변화, Django가 댓글을 추가하기 위해 알아야 할 것은 댓글의 내용, 이 댓글을 만들고자 하는 사용자, 그리고 사용자가 댓글을 다는 기사 등 세 가지가 있다.

```python
class CreateComment(graphene.Mutation):
    comment = graphene.Field(types.CommentType)

    class Arguments:
        content = graphene.String(required=True)
        user_id = graphene.ID(required=True)
        post_id = graphene.ID(required=True)

    def mutate(self, info, content, user_id, post_id):
        comment = models.Comment(
            content=content,
            user_id=user_id,
            post_id=post_id,
        )
        comment.save()

        return CreateComment(comment=comment)
```

```python
class Mutation(graphene.ObjectType):
    . . .
    create_comment = CreateComment.Field()
```

`Mutation` 클래스 내에 `CreateComment` 클래스를 추가해야 한다.

### 프런트엔드 설정
프런트엔드에 대하여 `Post.vue`로 이동하겠다, 여기가 댓글이 보이는 곳이다. 코드가 너무 길지 않도록 다음 예에서 관련 없는 코드를 제거했지만, 전체 코드를 원한다면 [여기서](https://github.com/ericsdevblog/django-vue-starter-blog) 소스 코드를 다운로드할 수 있다.

#### `Post.vue`

```js
<script>
import { POST_BY_SLUG } from "@/queries";
import CommentSectionComponent from "@/components/CommentSection.vue";

export default {
  name: "PostView",

  components: { CommentSectionComponent },

  data() {
    return {
      postBySlug: null,
      comments: null,
      userID: null,
    };
  },

  computed: {
    // Filters out the unapproved comments
    approvedComments() {
      return this.comments.filter((comment) => comment.isApproved);
    },
  },

  async created() {
    // Get the post before the instance is mounted
    const post = await this.$apollo.query({
      query: POST_BY_SLUG,
      variables: {
        slug: this.$route.params.slug,
      },
    });
    this.postBySlug = post.data.postBySlug;
    this.comments = post.data.postBySlug.commentSet;
  },
};
</script>
```

#### `queries.js`

```js
export const POST_BY_SLUG = gql`
  query ($slug: String!) {
    postBySlug(slug: $slug) {
      . . .
      commentSet {
        id
        content
        createdAt
        isApproved
        user {
          username
          avatar
        }
        numberOfLikes
        likes {
          id
        }
      }
    }
  }
`;
```

먼저 `created()` 후크에서 위에 표시된 `POST_BY_SLUG` 쿼리를 사용하여 요청된 기사와 댓글을 검색한다. 다음으로 `computed` 속성에서 관리자가 승인하지 않은 댓글을 제외하는 필터링을 해야 한다. 그리고 마지막으로 comment, post ID와 사용자 ID를 `CommentSectionComponent`에 전달한다.

#### `CommentSectionComponent.vue`

```js
<template>
  <div class="home">
    . . .
    <!-- Comment Section -->
    <!-- Pass the approved comments, the user id and the post id to the comment section component -->
    <comment-section-component
      v-if="this.approvedComments"
      :comments="this.approvedComments"
      :postID="this.postBySlug.id"
      :userID="this.userID"
    ></comment-section-component>
  </div>
</template>
```

다음은 댓글 섹션 구성요소에 대해 자세히 살펴보겠다. 이 구성 요소에는 사용자가 로그인한 경우에만 표시되는 냇글을 남길 수 있는 양식과 기존 댓글 목록이라는 두 개의 섹션이 있다.

#### `CommentSection.vue`

```js
<script>
import { SUBMIT_COMMENT } from "@/mutations";
import CommentSingle from "@/components/CommentSingle.vue";
import { useUserStore } from "@/stores/user";

export default {
  components: { CommentSingle },
  name: "CommentSectionComponent",

  setup() {
    const userStore = useUserStore();
    return { userStore };
  },

  data() {
    return {
      commentContent: "",
      commentSubmitSuccess: false,
      user: {
        isAuthenticated: false,
        token: this.userStore.getToken || "",
        info: this.userStore.getUser || {},
      },
    };
  },
  props: {
    comments: {
      type: Array,
      required: true,
    },
    postID: {
      type: String,
      required: true,
    },
    userID: {
      type: String,
      required: true,
    },
  },
  async created() {
    if (this.user.token) {
      this.user.isAuthenticated = true;
    }
  },
  methods: {
    submitComment() {
      if (this.commentContent !== "") {
        this.$apollo
          .mutate({
            mutation: SUBMIT_COMMENT,
            variables: {
              content: this.commentContent,
              userID: this.userID,
              postID: this.postID,
            },
          })
          .then(() => (this.commentSubmitSuccess = true));
      }
    },
  },
};
</script>
```

사용자가 로그인했는지 확인하기 위해 Pinia를 사용하는 방법과 다른 구성 요소 간에 정보를 전달하기 위해 `props`을 사용하는 방법을 이미 알고 있다고 생각한다. 이 부분은 생략하고 `submitComment()` 메서드에 초점을 맞추겠습니다.

이 메서드를 호출하면 댓글이 비어 있는지 테스트하고, 그렇지 않으면 `SUBMIT_COMMENT` 변화를 사용하여 새 comment를 만든다. `SUBMIT_COMMENT` 변화는 다음과 같이 정의한다.

#### `mutations.py`

```js
export const SUBMIT_COMMENT = gql`
  mutation ($content: String!, $userID: ID!, $postID: ID!) {
    createComment(content: $content, userId: $userID, postId: $postID) {
      comment {
        content
      }
    }
  }
`;
```

다음 코드는 CommentSection의 .vue 파일의 HTML 섹션이다. 이 코드의 끝에 댓글 하나를 디스플레이 하려고 다른 구성 요소 `CommentSingle.vue`을 사용했다.

#### `CommentSection.vue`

```js
<template>
  <div class=". . .">
    <p class="font-bold text-2xl">Comments:</p>

    <!-- If the user is not authenticated -->
    <div v-if="!this.user.isAuthenticated">
      You need to
      <router-link to="/account">sign in</router-link>
      before you can leave your comment.
    </div>

    <!-- If the user is authenticated -->
    <div v-else>
      <div v-if="this.commentSubmitSuccess" class="">
        Your comment will show up here after is has been approved.
      </div>
      <form action="POST" @submit.prevent="submitComment">
        <textarea type="text" class=". . ." rows="5" v-model="commentContent" />

        <button class=". . .">Submit Comment</button>
      </form>
    </div>

    <!-- List all comments -->
    <comment-single
      v-for="comment in comments"
      :key="comment.id"
      :comment="comment"
      :userID="this.userID"
    >
    </comment-single>
  </div>
</template>
```

이제 `CommentSingle.vue` 파일에 대해 자세히 살펴보겠다.

#### `CommentSingle.vue` HTML section

```js
<template>
  <div class="border-2 p-4">
    <div
      class="flex flex-row justify-start content-center items-center space-x-2 mb-2"
    >
      <img
        :src="`http://127.0.0.1:8000/media/${this.comment.user.avatar}`"
        alt=""
        class="w-10"
      />
      <p class="text-lg font-sans font-bold">
        {{ this.comment.user.username }}
      </p>
    </div>

    <p>
      {{ this.comment.content }}
    </p>
  </div>
</template>
```

#### `CommentSingle.vue` JavaScript section

```js
<script>
export default {
  name: "CommentSingleComponent",
  data() {
    return {
      . . .
    };
  },
  props: {
    comment: {
      type: Object,
      required: true,
    },
    userID: {
      type: String,
      required: true,
    },
  },
};
</script>
```

## 좋아요 반응 시스템 생성
좋아요 시스템에 대해서도, 여러분이 명심해야 할 몇 가지가 있다. 먼저, 좋아요를 추가하려면 사용자가 로그인해야 한다. 확인되지 않은 사용자는 좋아요 수만 볼 수 있다. 둘째, 각 사용자는 하나의 기사에 한 번의 좋아요만 할 수 있으며, 좋아요 버튼을 다시 클릭하면 좋아요 반응이 제거된다. 마지막으로, 각 기사는 여러 사용자로부터 좋아요를 받을 수 있다.

### 백엔드 설정
다시, 모델부터 시작해 본다.

각 기사는 많은 사용자로부터 많은 좋아요를 받을 수 있고, 각 사용자는 많은 기사에 많은 좋아요를 줄 수 있기 때문에, 이것은 포스트와 사용자 사이의 many-to-many 관계가 되어야 한다.

또한 이번에는 총 좋아요 수를 반환하는 `get_number_of_like()` 함수가 생성된다. 이전에 설명한 명령을 사용하여 이러한 변경 사항을 데이터베이스에 적용해야 한다.

```python
# Post model

class Post(models.Model):
    . . .

    # Each post can receive likes from multiple users, and each user can like multiple posts
    likes = models.ManyToManyField(User, related_name='post_like')

    . . .

    def get_number_of_likes(self):
        return self.likes.count()
```

다음으로 타입과 변화를 추가한다.

```python
class PostType(DjangoObjectType):
    class Meta:
        model = models.Post

    number_of_likes = graphene.String()

    def resolve_number_of_likes(self, info):
        return self.get_number_of_likes()
```

8번째에서 `self.get_number_of_like()`는 모델에서 정의한 `get_number_of_like()` 함수를 호출한다.

```python
class UpdatePostLike(graphene.Mutation):
    post = graphene.Field(types.PostType)

    class Arguments:
        post_id = graphene.ID(required=True)
        user_id = graphene.ID(required=True)

    def mutate(self, info, post_id, user_id):
        post = models.Post.objects.get(pk=post_id)

        if post.likes.filter(pk=user_id).exists():
            post.likes.remove(user_id)
        else:
            post.likes.add(user_id)

        post.save()

        return UpdatePostLike(post=post)
```

좋아요를 게시물에 추가하려면 해당 댓글의 `id`와 해당 댓글을 좋아하는 사용자의 `id`를 알아야 한다.

11번째 줄부터 14번 째 줄까지 게시물에 현재 사용자의 좋아요가 이미 있으면 좋아요는 제거되고, 없으면 좋아요가 추가된다.

### 프런트엔드 설정
다음으로, 여러븐은 post 페이지에 좋아요 버튼을 추가해야 한다. `Post.vue`로 돌아가자.

#### `Post.vue` HTML 섹션

```html
<template>
  <div class="home">
    . . .

    <!-- Like, Comment and Share -->
    <div class=". . .">
      <div v-if="this.liked === true" @click="this.updateLike()">
        <i class="fa-solid fa-thumbs-up">
          <span class="font-sans font-semibold ml-1">{{
            this.numberOfLikes
          }}</span>
        </i>
      </div>
      <div v-else @click="this.updateLike()">
        <i class="fa-regular fa-thumbs-up">
          <span class="font-sans font-semibold ml-1">{{
            this.numberOfLikes
          }}</span>
        </i>
      </div>
      . . .
    </div>

    . . .
  </div>
</template>
```

#### `Post.vue` JavaScript 섹션

```js
<script>
import { POST_BY_SLUG } from "@/queries";
import { UPDATE_POST_LIKE } from "@/mutations";
. . .

export default {
  . . .
  async created() {
    . . .
    // Find if the current user has liked the post
    let likedUsers = this.postBySlug.likes;

    for (let likedUser in likedUsers) {
      if (likedUsers[likedUser].id === this.userID) {
        this.liked = true;
      }
    }

    // Get the number of likes
    this.numberOfLikes = parseInt(this.postBySlug.numberOfLikes);
  },

  methods: {
    updateLike() {
      if (this.liked === true) {
        this.numberOfLikes = this.numberOfLikes - 1;
      } else {
        this.numberOfLikes = this.numberOfLikes + 1;
      }
      this.liked = !this.liked;

      this.$apollo.mutate({
        mutation: UPDATE_POST_LIKE,
        variables: {
          postID: this.postBySlug.id,
          userID: this.userID,
        },
      });
    },
  },
};
</script>
```

이 예를 더 짧게 만들기 위해 일부 코드를 삭제했지만, 이 예에서는 여전히 네 가지에 대해 이야기해야 한다. 먼저, 기사를 검색하는 데 사용하는 `POST_BY_SLUG` 쿼리는 좋아요 수와 이미 기사를 좋아했던 사용자를 반환해야 한다.

#### `queries.js`

```js
export const POST_BY_SLUG = gql`
  query ($slug: String!) {
    postBySlug(slug: $slug) {
      . . .
      numberOfLikes
      likes {
        id
      }
      . . .
    }
  }
`;
```

다음으로 `created()` 후크에서 게시물을 검색한 후 현재 사용자가 게시물을 이미 좋아했던 사용자 목록에 있는지 확인해야 한다.

그런 다음 이 메서드가 호출되면 `updateLike()` 메서드에서 사용자가 게시물을 좋아했는지 여부에 따라 좋아요 수가 변경된다.

마지막으로, 이 메서드는 `UPDATE_POST_LIKE` 변화을 사용하여 백엔드에서 게시물의 좋아요를 업데이트한다.

#### `mutatios.js`

```js
export const UPDATE_POST_LIKE = gql`
  mutation ($postID: ID!, $userID: ID!) {
    updatePostLike(postId: $postID, userId: $userID) {
      post {
        id
        title
        likes {
          id
        }
      }
    }
  }
`;
```

## 도전
댓글과 좋아요 시스템을 만드는 방법을 배운 후에, 더 어려운 작업을 고려해보자. 사용자가 다른 댓글에 댓글을 달 수 있는 중첩된 댓글 시스템을 만들려면 어떻게 해야 할까? 이것을 가능하게 하기 위해 코드를 어떻게 바꿀어야 할까? 그리고 댓글에도 좋아요 시스템을 어떻게 만들 수 있을까?

이러한 기능의 완전한 구현은 [이 소스 코드](https://github.com/ericsdevblog/django-vue-starter-blog)에 포함되어 있다.

---
<a name="footnote_1">1</a>: 이 포스팅은 [Create a Modern Application with Django and Vue #3 ](https://www.ericsdevblog.com/posts/create-a-modern-application-with-django-and-vue-3/)를 편역한 것이다.
