이 섹션에서는 기사에 태그 기능을 추가하려고 한다. 또한 특정 태그를 기준으로 모든 문서를 표시할 것이다. 태그 지정에는 `django-taggit` 어플리케이션을 사용할 것이다.

## django-taggit 이란?
`django-taggit`은 간단한 태깅을 위한 재사용 가능한 Django 어플리케이션이다. `pip`를 사용하여 설치할 수 있다.

```bash
$ pip install django-taggit
```

`django-taggit`은 Django로 태그를 지정하는 간단한 접근 방식이다. `INSTALLED_APPS`에 "`taggit`"을 추가한 다음 모델에 `TaggableManager`를 추가하기만 하면 된다.

```python
from django.db import models

from taggit.managers import TaggableManager

class Person(models.Model):
# … fields here

tags = TaggableManager()
```

## `Xampp` 시작과 가상 환경 활성화

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## 활성화된 환경에서 `django-taggit` 설치

```bash
$ pip install django-taggit
```

![](images/adding-tagging-functionality/cli_09_01.png)

## Django `settings`파일에 `taggit django` 앱 추가
`settings.py` 파일을 열고 `INSTALLED_APPS` 목록에 `taggit`을 추가한다.

![](images/adding-tagging-functionality/screenshot_09_01.webp)

## `blog/models.py` 파일에 `TaggableManager` 추가
기사 모델에 태그를 추가해야 한다. `blog/models.py` 파일을 열고 다음을 수행한다.

### `models.py` 파일에 `TaggableManager` 가져오기

```bash
$ from taggit.managers import TaggableManager
```

![](images/adding-tagging-functionality/screenshot_09_02.webp)

### `Article` 모델에 태그 추가하기
`log/models.py` 파일을 열고 기사 모델 클래스에 다음 줄을 추가한다.

**`tags = TaggableManager()*``

```python
lass Article(models.Model):

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'
   
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_articles')
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2,
                                choices=Status.choices,
                                default=Status.DRAFT
                            )
    objects = models.Manager() #The default manager
    publishedArticles = ArticlePublishedManager() #The custom manager
    tags = TaggableManager()

    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]
        pass

    def __str__(self):
        return self.title
        pass

    def get_canonical_url(self):
        return reverse('blog:article_details', 
                        args=[
                                self.publish.year,
                                self.publish.month,
                                self.publish.day,
                                self.slug
                            ]
                        )
        pass

    pass
```

![](images/adding-tagging-functionality/screenshot_09_03.webp)

기사 모델을 수정했으므로 이제 `makemigrations`과 `migrate` 명령을 실행해야 한다.

## `makemigrations`과 `migrate` 명령 실행

```bash
$ python manage.py makemigrations blog
```

![](images/adding-tagging-functionality/cli_09_02.png)

```bash
$ python manage.py migrate
```

![](images/adding-tagging-functionality/cli_09_03.png)

## 개발 서버 실행

```
$ python manage.py runserver
```

개발 서버를 실행한 후 관리자 URL을 브라우징 한다.

`http://127.0.0.1:8000/admin/`

관리자 사용자명과 비밀번호를 입력하고 로그인에 성공하면 아래와 같은 다음 화면을 얻을 수 있다.

![](images/adding-tagging-functionality/screenshot_09_04.png)

관리 섹션에 태그를 추가하지 않았음을 주목하자. Django는 관리 섹션에 자동으로 추가된다. 기사에 태그를 추가해 보겠다.

## 태그 추가
"`+ADD`"를 클릭하면 다음 화면에 양식이 표시된다. `name`과 `slug`를 작성하고 `content type`과 `object id`를 선택해야 한다. 기사 ID가 1인 기사에 "Linux" 태그를 추가한다.

![](images/adding-tagging-functionality/screenshot_09_05.png)

저장 후 아래와 같은 화면을 볼 수 있다.

![](images/adding-tagging-functionality/screenshot_09_06.png)

아래와 같이 기사에서도 태그를 볼 수 있다.

![](images/adding-tagging-functionality/screenshot_09_07.png)

## 기사의 태그 디스플레이
`blog/templates/blog/list.html` 파일을 열고 아래 줄을 추가하여 태그를 디스플레이한다.

```html
<p>Tags: {{ article.tags.all|join:", " }}</p>
```

![](images/adding-tagging-functionality/screenshot_09_08.webp)

url `http://127.0.0.1:8000/blog/`으로 확인한다.

### 기사에 더 많은 태그를 추가하자
문서에 태그를 더 많이 추가한다.

![](images/adding-tagging-functionality/screenshot_09_09.png)

## 특정 태그를 기준으로 모든 문서 디스플레이
### `taggit`에서 `Tag` 모델 가져오기
`blog/views.py` 파일을 열고 `Tag` 모델을 가져온다.

```python
from taggit.models import Tag
```

![](images/adding-tagging-functionality/screenshot_09_10.webp)

### `blog/views.py` 파일 수정
`list_of_articles()` 메서드에 `tag_slug` 매개변수를 추가하며, 이 매개변수의 값은 `None`이다. 그런 다음 `tag_slug`가 있는지 확인한 다음 데이터베이스에서 태그를 가져온다. 그리고 태그별로 기사를 필터링하고, `Tag`를 `list.html` 템플릿에 전달한다.

```python
def list_of_articles(request, tag_slug = None):
    articles = Article.publishedArticles.all()

    tag = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        articles = articles.filter(tags__in=[tag])

    paginator = Paginator(articles, 3)
    page_number = request.GET.get('page', 1)
    try:
        articles = paginator.page(page_number)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)
    except PageNotAnInteger:
        articles = paginator.page(1)

    return render(request, 'blog/list.html', {'articles': articles, 'tag': tag})
```

![](images/adding-tagging-functionality/screenshot_09_11.webp)

## 태그별 기사 URL 패턴 만들기
`list_of_articles()` 메서드를 수정했으므로 이제 태그별 기사에 대한 `tag/tag_slug`의 URL 패턴을 만들어야 한다.

```python
path('tag/<slug:tag_slug>/', views.list_of_articles, name='list_of_articles_by_tag'),
```

![](images/adding-tagging-functionality/screenshot_09_12.webp)

### `blog/templates/list.html` 파일 수정

```html
{% if tag %}
        <h2>Articles tagged with "{{ tag.name }}"</h2>
{% endif %}
```

사용자가 특정 태그와 관련된 모든 기사를 보고자 하는 경우, 이를 위해 모든 기사 태그를 반복하여 태그별로 모든 필터 기사를 가져오는 URL에 대한 하이퍼링크를 제공한다.

```html
<p>
    Tags: 
    {% for tag in article.tags.all %}
        <a href="{% url "blog:list_of_articles_by_tag" tag.slug %}">
            {{ tag.name }}
        </a>
        {% if not forloop.last %}, {% endif %}
    {% endfor %}
</p>
```

![](images/adding-tagging-functionality/screenshot_09_13.webp)

### `blog/templates/blog/list.html` 전체 코드

```html
{% extends "blog/base.html" %}

{% block title %}Articles{% endblock %}

{% block content %}

    {% if tag %}
        <h2>Articles tagged with "{{ tag.name }}"</h2>
    {% endif %}

    {% for article in articles %}
        <h2>
            <a href="{{ article.get_canonical_url }}">
                {{ article.title }}
            </a>
        </h2>
        <p>
            Tags: 
            {% for tag in article.tags.all %}
                <a href="{% url "blog:list_of_articles_by_tag" tag.slug %}">
                    {{ tag.name }}
                </a>
                {% if not forloop.last %}, {% endif %}
            {% endfor %}
        </p>
        <p>
            Published {{ article.publish }} by {{ article.author }}
        </p>
        {{ article.body|truncatewords:50|linebreaks }}

    {% endfor %}
    {% include "blog/pagination.html" with page=articles %}
{% endblock %}
```

이미 개발 서버가 실행 중이므로 아래 URL을 확인하자.

`http://127.0.0.1:8000/blog/`

![](images/adding-tagging-functionality/screenshot_09_14.png)

아무 태그나 클릭한다. "Linux" 태그를 클릭했다. "Linux" 태그와 관련된 모든 기사가 아래와 같이 디스플레이 되었다. 

![](images/adding-tagging-functionality/screenshot_09_15.png)

"python" 태그를 클릭하면 `python` 태그와 관련된 모든 기사들이 디스플레이된다.

![](images/adding-tagging-functionality/screenshot_09_16.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Added tag functionality"
$ git push origin main
```

![](images/adding-tagging-functionality/cli_09_04.png)

블로그 웹사이트에 태그 기능을 성공적으로 구현했다. 이 파트는 여기까지 ...

**(주)** 위 내용은 [Creating a comment system to the article — Part 8](https://medium.com/@nutanbhogendrasharma/creating-a-comment-system-to-the-article-in-django-part-8-ba9d8067bb2)을 편역한 것이다.
