> Python의 f-String을 통한 더 빠르고 효율적인 문자열 포매팅

이 글에서 Python **f-string**에 대해 논의할 것이다. 파이썬 3.6에 도입된 새로운 기능이며 이 기능은 빠르게 프로그래밍 세계의 화제가 되었고 개발자들이 문자열을 포맷하는 방식에 혁명을 일으켰다.

이 주제에 대해 좀 비공식적인 스타일로 논의하고 예를 들어 설명할 것이다. 이는 여러분이 이 주제에 대해 알고 일상적인 코딩에 사용하는 데 도움을 줄 수 있을 것이다.

## 소개
`f-string`은 문자 "f"로 시작하는 문자열 리터럴이다. 개발자가 중괄호 `{}`를 사용하여 문자열 리터럴 내부에 식을 포함할 수 있다. 이 식은 런타임에 평가되어 결과 값을 문자열에 삽입한다.

## 시작
이미 알고 있을 수 있겠지만 모를 수도 있는 것은 이 중괄호 `{}` 사이에 넣을 수 있는 많은 것들이 있다는 것이다.

예를 들어 다음 코드를 생각해 보자.

> *이 예에는 f-string에서 임의 표현식을 디버그하고 추가하는 방법 등이 포함되어 있다.*

```python
name = "John"
age = 30
avg = 3
print(f"My name is {name} and I am {age} years old.")
print(f"{age = }")
print(f"{avg % 2 = }")
```

위의 예에서 디버깅을 위해 `equals(=)`를 사용했고 Python 표현식인 임의의 식을 사용했으며 변수를 인쇄하기 위해 다른 간단한 `{}`를 사용했다. 이 코드를 실행하면 다음과 같이 출력된다.

```
My name is John and I am 30 years old.
age = 30
avg % 2 = 1
```

첫 번째 print 문에서 볼 수 있듯이 `f-string` 구문을 사용하여 변수 `name`과 `age` 값이 문자열에 삽입된다.
두 번째 print 문에서는 'age='를 인쇄하기 위해 equal ('=')을 사용하면 숫자 값이 인쇄되는 것을 볼 수 있다. 프로덕션 코드에서는 필요없지만 디버깅할 때 사용하면 매우 유용하다.
그리고 마지막 print 문에서, 실제로 그곳에 임의의 표현식을 사용할 수 있다는 것을 알 수 있다. 그러면 'avg % 2 ='가 출력되고, 그 값은 equals ('=') 다음에 출력된다.

## 사용 사례들

### 변환
아직 모르고 있다면 이 예에서는 중괄호 `{}`안에 느낌표 `a`, 느낌표 `r` 또는 느낌표 `s`를 넣을 수 있다.

예를 들어 다음 코드를 보자.

```python
string = "Hi There 😁"
print(f"{string!a}")
print(f"{string!r}")
print(f"{string!s}")
```

위의 코드는 원래 값을 인쇄하는 대신 추가적인 작업을 수행한다. 이 예에서 'r'은 'repr'을 호출한다. 코드를 실행하면 출력은 다음과 같다.

```
'Hi There \U0001f601'
'Hi There 😁'
Hi There 😁
```

첫 번째 `print()` 문의 `!a` 변환 플래그는 **ASCII 문자**를 사용하여 문자열을 나타내는 데 사용된다.

두 번째 `print()` 문의 `!r` 변환 플래그는 `__repr__` 메소드를 사용하여 문자열을 나타낸다.

세 번째 `print()` 문의 변환 플래그가 사용되지 않는다. 즉, 문자열은 `__str__`을 사용하여 표현된다.

### 포매팅
모든 타입이 고유한 포맷팅을 가질 수 있는 포맷팅 예제이다. 이 예에서는 실수 값, datetime 값을 사용했으며 `print` 문에서는 콜론(`:`)을 사용하여 날짜에 `%Y%m%d`를 사용한 것과 마찬가지로 실수에 `.2f`를 사용한 것과 같이 값의 포맷팅 형식을 정의했다 (*소수점이하 두 자리가 제공된다*). 다음 예와 같이 표현한다.

```python
val = 22.225
dt = datetime.utcnow()
print(f'{dt=:%Y-%m-%d}')
print(f'{val:.2f}')
```

기본적으로 `colon(:)`으로 포맷을 지정한다. 위의 코드를 실행하면 다음과 같이 출력된다.

```
dt=2023-02-25
22.23
```

위에서와 같이 각 값 타입에 따라 포맷팅 지정 문자열을 출력한다.

이를 위하여 어떤 일이 일어나고 있는지 살펴보자. 이를 위하여 `OwnClass`라는 클래스를 정의한다.

```python
class OwnClass:
    def __format__(self, format_type) -> str:
        print(f'OwnClass __format__ called with {format_type=!r}')
        return "OwnClass()"

print(f'{OwnClass():my formatting type %%MY_FORMAT%%}')
```

여기서 `format_type`을 인수로 사용하고 반환 타입은 `string`인 `format` 메서드를 정의했다. 이 코드를 실행하면 다음과 같이 출력된다,

```
OwnClass __format__ called with format='my format %%MY_FORMAT%%'
OwnClass()
```

보다시피 `colon(:)` 뒤 문자열을 인수로 하여, `OwnClass`와 내부에서 포맷을 인쇄하고 문자열을 반환한다. 여기서 인수 문자열을 무시하고 대신 기본 타입 문자열을 반환한다. 따라서 어떤 정보를 반환할 것인지에 따라 출력이 결정된다.

> **NOTE**: <br>
숫자에 `*commas*(,)`를 추가하려면 다른 작업을 수행할 필요가 있다. 중괄호 `{}`에 `:,.2f`를 추가할 수 있다. 

```python
sentence = '1 MB is equal to {:,.2f} bytes'.format(1000**2)
print(sentence)
```

이 코드를 실행하면 다음과 같은 출력을 얻을 수 있다. 

```
1 MB is equal to 1,000,000.00 bytes
```


### 여러줄 문자열
여러 줄 문자열을 사용할 수 있다.

```python
name = "John"
profession = "Artist"
place = "USA"
message = (
    f"Hi {name}. "
    f"Your profession is - {profession}. "
    f"And you are from {place}. "
)
print(message)
```

이 코드를 실행하면 다음과 같은 출력을 얻을 수 있다. 

```
Hi John. Your profession is - Artist. And you are from USA.
```

### 조건 식
f-string은 조건식과 함께 사용하여 특정 조건에 따라 동적 문자열을 만들 수도 있다.
예를 들어, `f-string`을 사용하여 사용자의 나이에 따라 메시지를 표시할 수 있다.

```python
age = 20 
message = f"You are {'old' if age >= 18 else 'young'}" 
print(message) # Output: You are old
```

이 코드를 실행하면 아래와 같은 출력을 얻는다.

```
You are old
```

### for-루프
사전이나 리스트에서 반복하기 위해 f-string 내부의 루프를 사용하여 더 나은 방법으로 인쇄할 수 있다. 다음은 사전 내의 항목을 인쇄하는 예이다.

```python
dct = {'a': 1, 'b': 2}
newline = "\n"  # \escapes are not allowed inside f-strings
print(f'{newline.join(f"{key}: {value}" for key, value in dct.items())}')
```

이 코드를 실행하면 아래와 같은 출력을 얻는다.

```
a: 1 
b: 2
```

### 성능 비교
`f-string`이 `f-%-formating`과 `str.format` 보다 빠르다. 이미 보았듯이, `f-string`은 상수 값이 아닌 런타임에 평가되는 식이다.
다음은 속도를 비교한 것이다.

```python
import timeit
print(timeit.timeit("""name = "Eric"; age = 74; '%s is %s.' % (name, age)""", number=10000))

# OUTPUT: 0.002332228999999998
```

```python
import timeit
print(timeit.timeit("""name = "Eric"; age = 74; '{} is {}.'.format(name, age)""", number=10000))

# OUTPUT: 0.0031069680000000016
```

```python
import timeit
print(timeit.timeit("""name = "Eric"; age = 74; '{name} is {age}.'""", number=10000))

# OUTPUT: 0.00012703599999999704
```

`f-string`이 제일 빠르다.

`f-string`의 간결한 구문과 표현식을 포함하고 작업을 수행하는 능력으로 인하여, `f-string`은 Python에서 문자열 포매팅 지정을 위한 필수 방법이 되었다.

또한 속도와 효율성으로 인해 다른 문자열 포매팅 지정 방법보다 선호되고 있다. 비록 `f-string`이 문자열을 포맷하는 유일한 방법은 아니지만, 출력 작업을 완료하는 확실한 방법이 될 수 있는 좋은 위치에 있다.

**(주)** 위 내용은 [Python: Going Beyond Basic String Formatting using f-string](https://pravash-techie.medium.com/python-going-beyond-basic-string-formatting-using-f-string-cba87ddb78fb)을 편역한 것이다.
