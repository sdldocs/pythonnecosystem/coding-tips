Hypertext Markup Languag의 줄임말인 HTML은 웹 브라우저에 표시될 수 있는 웹 페이지와 기타 정보를 만드는 데 사용되는 표준 언어이다. HTML은 텍스트, 이미지, 비디오 또는 링크와 같은 콘텐츠를 구성하고 형식을 지정하는 데 사용되는 요소와 태그로 구성되어 있다. 웹 페이지를 만들기 위한 기본 구성 요소를 제공하고 구조를 제공하여 인간과 컴퓨터 모두가 콘텐츠를 더 쉽게 이해하고 상호 작용할 수 있도록 한다.

일반적으로, 다른 기술들이 함께 사용된다. 예를 들어, [CSS(Cascading Style Sheet)](https://www.ericsdevblog.com/posts/css-basics-1/)는 페이지의 모양을 설명하는 데 사용될 수 있으며, [JavaScript](https://www.ericsdevblog.com/posts/javascript-basics-1/)는 웹 페이지의 동작을 정의하고 사용자에게 보다 상호 작용적으로 만들 수 있습니다. 이러한 기술에 대해서는 향후 포스팅에서 자세히 설명하겠다.

계속하기 전에 컴퓨터에 코드 편집기와 브라우저가 설치되어 있는지 확인한다.

다음은 HTML 문서의 예dl다. HTML 문서는 확장자가 `.html`인 파일이다.

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>My HTML page</title>
</head>

<body>
    <h1>This is a heading</h1>
    <p>This is a paragraph.</p>
</body>

</html>
```

텍스트 편집기를 수행하여 위의 코드로 파일을 생성하여 저장한 다음, 브라우저에서 파일을 열어 렌더링된 결과를 볼 수 있다.

## Elements
요소(element)는 HTML 파일의 기본 구성 요소이다. HTML 일반적으로 시작 태그와 끝 태그로 요소를 정의하며, 그 사이에 다음과 같은 내용이 있다.

```html
<tag>Some content...</tag>
```

그러나 예외가 있는데, 예를 들어 `<hr>`은 콘텐츠나 끝 태그가 필요없다. 일반적으로 내용에서 주제 변경을 나타내는 웹 페이지의 수평 규칙을 정의한다.

HTML 요소는 일반적으로 중첩된 구조에 있으며, 이는 한 요소가 다음과 같이 다른 요소를 포함할 수 있음을 의미한다.

```html
<body>
    <h1>This is a Heading</h1>
    <p>This is a paragraph.</p>

    <div>
        <p>This is also a paragraph.</p>
    </div>
</body>
```

`<body>`는 안에 표제 `<h1>`, 단락 `<p>`, 그리고 `<div>`의 세 가지 요소를 가지고 있다. 그리고 `<div>` 요소에는 또 다른 단락 `<p>`가 있다. 위의 그림은 구조 트리의 모양이다. 이 나무가 얼마나 크거나 복잡할 수 있는 지에 제한이 없다. 이를 문서 객체 모델(DOM)이라 부르며, 프론트엔드 개발의 기본 개념과도 관련이 있다. DOM을 사용하면 HTML 파일의 요소를 찾고 변경할 수 있다.

![dom-tree](images/html-basics/dom-tree.png)

또 다른 중요한 참고 사항은 끝 태그가 필요한 경우에는 절대로 태그에서 벗어나서는 안 된다는 것이다. 때때로 요소가 올바르게 표시되지만 절대로 이에 의존해서는 안 된다. 그것니 예상치 못한 결과를 초래할 수 있다.

## 애트리뷰트(attributes)
일반적으로 각 HTML 요소에 속성을 할당할 수 있다. 이는 HTML 요소에 추가 정보를 제공하는 것이다. 이들은 항상 시작 태그에 일반적으로 다음과 같이 이름/값 쌍으로 지정된다.

```html
<tag name="value">An Example</tag>
```

잊지 말아야 할 두 가지 중요한 사항이 있다.

- 애트리뷰트는 항상 소문자로 표시해야 한다. 이는 요구사항은 아니지만 강력히 권장된다. HTML의 엄격한 버전인 [XHTML](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/XHTML)은 실제로 그것을 요구한다.
- 애트리뷰트 값은 항상 따옴표로 묶어 있어야 한다. ' '를 사용하든 " "를 사용하든 상관없지만 항상 일치해야 한다. " "를 사용하는 것이 좋다. XHTML에서는 " "를 사용해야 한다.

### `id`와 `class`
`id`와 `class`는 특별한 주의가 필요한 두 애트리뷰트이다. `id`는 고유하며 HTML 파일에서 하나의 요소를 식별하는 데만 사용할 수 있다. `class`는 고유할 필요는 없으며 HTML 파일에서 요소 그룹을 식별하는 데 사용된다. 한 요소에 `class`와 `id`를 모두 지정할 수 있다.

```html
<h1 id="myHeader">Cities</h1>

<div class="city">
    <h2 id="city1" class="city-title">London</h2>
    <p>London is the capital of England.</p>
</div>

<div class="city">
    <h2 id="city2" class="city-title">Paris</h2>
    <p>Paris is the capital of France.</p>
</div>

<div class="city">
    <h2 id="city3" class="city-title">Tokyo</h2>
    <p>Tokyo is the capital of Japan.</p>
</div>
```

CSS와 JavaScrip 같은 기술과 결합될 때, 이러한 `id`와 `class`는 웹 페이지의 어떤 부분의 모양과 행동을 변경할 수 있게 해준다. 그리고 두 애트리뷰트에 대해 나중에 보다 자세히 다룰 것이다.

## 공통 요소와 애트리뷰트
다음으로 일반적으로 사용되는 HTML 요소를 살펴보겠다. HTML에 대한 모든 요소의 완전한 목록은 아니다. 그러면 이 포스팅은 길고 지루할 것이다. 그러나 필요하다면 HTML에 대한 완전한 튜토리얼을 [여기](https://www.w3schools.com/html/default.asp)에서 찾을 수 있다.

### heading과 paragraph
heading은 웹 페이지의 계층과 구조를 정의한다. HTML은 `<h1>`에서 `<h6>`까지 6 수준의 heading을 제공한다. `<h1>`은 가장 중요한 표제이며, 전체 페이지 내용을 요약해야 하기 때문에 HTML 파일에는 `<h1>` heading은 하나만 있어야 한다.

```html
<body>
    <h1>Heading level 1</h1>
    <h2>Heading level 2</h2>
    <h3>Heading level 3</h3>
    <h4>Heading level 4</h4>
    <h5>Heading level 5</h5>
    <h6>Heading level 6</h6>
</body>
```

브라우저에서 위의 내용으로 작성한 `.html` 파일을 열면 다음과 같이 렌더링된다.

![html-headings](images/html-basics/html-headings.png)

반면에 paragraph는 `<p>` 태그로 정의된다.

```html
<p>This is a paragraph.</p>
<p>This is also a paragraph.</p>
```

HTML에서는 코드에 공백이나 줄을 추가하여 브라우저에 문단(paragraph)가 디스플레이 방식을 변경할 수 없다. 브라우저가 자동으로 제거한다. 예를 들어, 다음 두 단락은 동일한 결과를 디스플레이할 것이다.

```html
<p>This is a paragraph.</p>

<p>This           is a 
paragraph.</p>
```

하지만, 만약 줄 바꾸기를 넣고 싶다면? 답은 간단합니다. `<br>` 요소를 사용한다.

```html
<p>This is a <br> paragraph.</p>
```

![html-paragraph](images/html-basics/html-paragraph.png)

### 요소 서식 지정(formatting elements)
서식 요소는 텍스트에 특별한 의미와 형식을 부여하는 요소들의 모음이다.

- `<b>` - 굵은 글씨
- `<strong>` - 중요 텍스트
- `<I>` - 이탤릭체 텍스트
- `<em>` - 강조된 텍스트
- `<mark>` - 표시된 텍스트
- `<small>` - 작은 텍스트
- `<del>` - 삭제된 텍스트
- `<ins>` - 삽입된 텍스트
- `<sub>` - 아래 첨자 텍스트
- `<sup>` - 윗 첨자 텍스트

브라우저에서 각 기능이 어떻게 보이는지 살펴보자.

```html
<body>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Vestibulum volutpat pretium turpis, sodales facilisis
            metus porta a.</b> Morbi condimentum porta massa, eu mattis turpis cursus sit amet. <strong>cursus ut tellus
            a convallis. In nec nisl nisl.</strong> Mauris a ligula et ligula malesuada luctus. <i>Fusce placerat id
            tortor at tristique.</i> Quisque non vulputate eros. <em>Pellentesque malesuada interdum ligula, et
            dignissim arcu vestibulum tincidunt.</em></p>

    <p><mark>Aliquam imperdiet volutpat lorem, in viverra lorem ultricies sed.</mark> Integer bibendum velit sit amet
        hendrerit venenatis. <small>Suspendisse interdum ornare molestie.</small> Nulla porttitor venenatis purus eu
        sollicitudin. <del>In quis aliquet ipsum. Curabitur eu feugiat sem.</del> Etiam rhoncus lectus eget dolor
        cursus, a viverra tellus faucibus. <ins>Nam aliquam rhoncus urna.</ins> Vivamus pulvinar eleifend nibh quis
        semper. <sub>Sed finibus neque in</sub> sollicitudin cursus. <sup>Curabitur ut ex </sup>egestas, suscipit lectus
        a, auctor ante. </p>
</body>
```

![html-paragraph](images/html-basics/html-formatting-elements.png)

### 링크(Links)
링크는 거의 모든 웹 페이지에서 찾아 볼 수 있다. 사용자들이 링크를 이용하여 페이지에서 페이지로 이동할 수 있다. 링크를 클릭하면 다른 HTML 파일로 이동하며, 이 간단한 동작이 인터넷의 기초를 형성한다. 링크를 다음과 같이 작성한다.

```html
<a href="https://www.example.com/">link text</a>
```

브라우저에 밑줄 친 텍스트가 표시되고 클릭하면 [https://www.example.com/](html-basics-1.md)으로 이동한다.

기본적으로 링크를 클릭하면 대상이 동일한 브라우저 탭에 나타난다. `target` 애트리뷰트 사용하여 이 동작을 변경할 수 있다.

- `_self` - 기본값. 클릭한 것과 동일한 창/탭에서 문서를 연다
- `_blank` - 새 창 또는 탭에서 문서를 연다
- `_parent` - 상위 프레임에서 문서를 연다
- `_top` - 창의 전체 본문에서 문서를 연다

예를 들어 다음과 같이 정의된 링크를 클릭하면 새 탭으로 이동한다.

```html
<a href="https://www.example.com/" target="_blank">link text</a>
```

### 표(Tables)
HTML에서 테이블은 `<table>` 태그를 사용하여 작성할 수 있다. 각 행은 `<tr>` 태그로 나타내며, 각 데이터 셀은 `<td>` 태그로 나타낸다. 표의 머리글을 사용하려면 `<th>` 태그를 사용한다. 기본적으로 `<td>` 요소의 텍스트는 규칙적이고 왼쪽 정렬되어 있으며, `<th>` 요소의 텍스트는 굵고 가운데 정렬된다.

다음은 예이다.

```html
<table>
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
    </tr>
    <tr>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
    </tr>
    <tr>
        <td>Eve</td>
        <td>Jackson</td>
        <td>94</td>
    </tr>
</table>
```

![html-table](images/html-basics/html-table.png)

이 테이블에는 테두리가 없어 조금 이상하게 보일 수 있다. 이 테이블에 테두리를 추가하려면 CSS(Cascading Style Sheet)라는 다른 언어를 사용해야 한다.

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>My HTML page</title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Age</th>
        </tr>
        <tr>
            <td>Jill</td>
            <td>Smith</td>
            <td>50</td>
        </tr>
        <tr>
            <td>Eve</td>
            <td>Jackson</td>
            <td>94</td>
        </tr>
    </table>
</body>

</html>
```

![html-table-with-border](images/html-basics/html-table-with-border.png)

### 목록(lists)
목록은 웹 페이지에서도 자주 사용된다. 이들은 `<ul>` 태그로 정의되며 목록의 각 요소를 `<li>` 태그로 작성한다.

```html
<ul>
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
</ul>
```

![html-list](images/html-basics/html-list.png)

중첩 구조 목록를 작성할 수도 있다. 이는 한 `<li>` 요소가 다른 `<ul>` 요소를 포함할 수도 있음을 의미한다.

```html
<ul>
    <li>One</li>
    <li>Two</li>
    <li>
        <ul>
            <li>Three</li>
            <li>Four</li>
        </ul>
    </li>
</ul>
```

![nested-list](images/html-basics/nested-list.png)

## 일반적인 레이아웃
다음으로 HTML 파일의 구조를 조금 더 깊이 알아보겠다. 먼저 이전에 언급한 예를 다시 살펴보자.

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>My HTML page</title>
</head>

<body>
    <h1>This is a heading</h1>
    <p>This is a paragraph.</p>
</body>

</html>
```

![html-example](images/html-basics/html-example.png)

이 HTML 문서에서 첫 번째 줄은 `DOCTYPE`을 선언한다. 이렇게 하여 렌더링 중인 문서가 HTML 문서임을 브라우저에 알린다. 그리고 다음으로 `<html>` 요소가 있는데, 이 요소는 `<head>`와 `<body>`라는 두 가지 다른 요소를 갖는다.

### `<head>` 섹션
`<head>` 요소는 메타데이터를 위한 컨테이너이며 `<html>` 요소 내부에 그리고 `<body>` 태그 앞에 배치된다. 일반적으로 `<head>` 요소에는 검색 엔진이나 브라우저에 필요할 수 있는 정보가 포함되어 있지만 웹 페이지에는 표시되지 않는다.

`<meta>` 요소는 일반적으로 문자 집합, 페이지 설명, 키워드, 문서 작성자와 뷰포트 설정 같은 메타 정보를 지정하는 데 사용된다. 이 정보는 페이지에 표시되지 않지만 브라우저와 검색 엔진에서 사용된다.

```html
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML, CSS, JavaScript">
<meta name="author" content="John Doe">
```

`<title>` 요소는 문서의 제목을 정의한다. 브라우저의 페이지 탭에 표시된다.

```html
<title>My HTML page</title>
```

![html-title](images/html-basics/html-title.png)

`<style>` 요소는 앞서 살펴본 바와 같이 단일 HTML 페이지에 대한 스타일 정보(CSS)를 정의하는 데 사용할 수 있다.

```html
<head>
    <meta charset="utf-8" />
    <title>My HTML page</title>
    <style>
        body {
            background-color: powderblue;
        }

        h1 {
            color: red;
        }

        p {
            color: blue;
        }
    </style>
</head>
```

![html-style](images/html-basics/html-style.png)

### `<body>` 섹션
`<body>` 요소는 다른 모든 요소를 담는 용기이다. 여기에는 브라우저에 표시될 모든 내용이 포함되어 있다. 이러한 요소는 크게 블록 요소와 인라인 요소로 분류할 수 있다.

이름에서 알 수 있듯이 각 block 요소는 웹 페이지에서 "block"을 정의합니다. block 요소는 항상 새로운 라인에서 시작하며, CSS가 없으면 항상 사용 가능한 전체 너비를 차지하고 항상 위쪽과 아래쪽 여백이 있다. 다시 말하지만, 이 글을 지루하게 만들지 않기 위해 다음 목록은 가장 일반적으로 사용되는 몇 가지 블록 요소만 나열하였다. 관심이 있다면, [여기](https://www.w3schools.com/html/html_blocks.asp)에 W3Schools의 전체 목록이 있다.

- `<div>` 요소는 의미가 없습니다. 이것은 특별한 의미가 없으며, 웹 페이지에서 "block"을 정의하는 데 사용될 수 있다는 것을 의미한다.
- `<section>` 요소는 문서의 섹션을 정의한다. 섹션은 동일한 주제 아래에 있는 내용 그룹이다. 일반적으로 단일 HTML 문서에는 여러 섹션이 있다. 예를 들어, 웹 페이지는 소개, 내용과 연락처 정보를 위한 섹션으로 나눌 수 있다.
- `<article>` 요소는 독립적이고 자체적인 콘텐츠로 지정한다. article은 그 자체로 의미를 갖어야 하며, 블로그 게시물이나 신문 기사처럼 웹사이트의 나머지 부분으로부터 독립적으로 배포할 수 있어야 한다.
- `<header>` 요소는 소개 콘텐츠 또는 탐색 링크 세트를 위한 컨테이너를 나타냅니다. HTML 페이지의 머리글에는 일반적으로 웹 사이트의 이름, 로고, 탐색 모음, 웹 사이트의 간단한 소개 등을 포함하고 있다.
- `<footer>` 요소는 문서 또는 섹션의 바닥글을 정의하며, 이름에서 알 수 있듯이 항상 웹 페이지의 맨 아래에 나타난다. `<footer>` 요소는 일반적으로 저작권 정보, 연락처 정보, 때로는 웹 사이트의 사이트 맵을 포함한다.
- `<nav>` 요소는 탐색 링크 세트를 정의하며, 때로는 검색 버튼을 정의하기도 한다.
- `<aside>` 요소는 사이드바와 같이 배치된 콘텐츠 외에 일부 콘텐츠를 정의한다. `<aside>` 콘텐츠는 검색창, 가입 양식, 일부 관련 기사 등 주변 콘텐츠와 간접적으로 관련이 있어야 한다.

이러한 블록 레벨 요소를 사용하여 HTML 페이지의 기본 레이아웃을 다음과 같이 설계할 수 있다.

```html
<body>
    <header>This is a header</header>
    <nav>
        <ul>
            <li><a>First Link</a></li>
            <li><a>Second Link</a></li>
            <li><a>Third Link</a></li>
        </ul>
    </nav>

    <div>
        <h2>This is the div block</h2>
        <p>This is the content of the div block</p>
    </div>

    <section>
        <h2>This is the section block</h2>
        <p>This is the content of the section block</p>
    </section>

    <article>
        <h2>This is the article block</h2>
        <p>This is the content of the article block</p>
    </article>

    <aside>This is a sidebar.</aside>
    <footer>This is a footer.</footer>
</body>
```

![block-level-elements](images/html-basics/block-level-elements.png)

인라인 요소는 새 줄에서 시작되지 않으며 필요한 만큼의 너비만 차지한다. 예를 들어, `<span>` 요소는 텍스트 중간에 넣을 수 있는 인라인 요소이다. 기본적으로 텍스트 모양에 영향을 주는 여백이나 패딩은 없다. CSS와 함께 사용될 때, `<span>` 요소는 텍스트의 일부를 스타일화하는 데 사용될 수 있다.

```html
<p>This is a <span style="color:red;">paragraph</span>.</p>
```

![span-element](images/html-basics/span-element.png)

`<span>` 외에도 링크 요소(`<a>`)와 포맷 요소(`<i>`, `<b>` …)도 인라인 요소이다. 시간을 절약하기 위해, 여기서 모든 것을 반복하지 않을 것이다.

마지막으로 주의해야 할 점은 블록 요소는 여러 인라인 요소를 포함할 수 있지만 인라인 요소는 블록 요소를 포함할 수 없다는 것이다.


**주** 이 포스팅은 [HTML Basics #1: A Brief Introduction](https://www.ericsdevblog.com/posts/html-basics-1/)를 편역한 것임.