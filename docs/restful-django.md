*Restful Django 프로젝트를 개발하여 보자*

Django는 Restful API를 직접 지원하지 않는다. 그러나 REST Framework 패키지 덕분에 Django 프로젝트에서 Restful API를 만들 수 있다. [여기]()에서 공식 문서을 얻을 수 있다. 이 포스팅에서 Django 프로젝트에서 REST API를 만드는 방법을 설명할 것이다.

#### 목차

- API란?
- REST API란?
- Django REST 프레임워크

### API
API 단어는 Application Programming Interface의 약자이다. 그들은 두 소프트웨어 유닛이 이를 이용하여 서로 대화하도록 하는데 그 목적이 있다.

웹 앱이나 모바일 앱을 생각해 보자. 웹 어플리케이션 프로그램을 사용하려면 브라우저에서 응답 어플리케이션 프로그램을 다운로드한다. 그들의 웹사이트를 볼 수 있다. 데이터가 필요할 떄 사용자는 아키텍처 관점에서 데이터베이스에 직접 액세스할 수 없다. 이는 매우 위험하다. 대신, 사용자들이 데이터베이스에 있는 데이터로 어떻게 무엇을 할 수 있는지 통제하고자 한다.

![simple-arch](images/restful-django/simple_structure.webp)

REST API를 설계하여 두 구조 사이에 별개의 또 다른 구조를 구성한다. 이러한 방식으로 데이터베이스에 연결하는 방법을 설정하고 관리한다.

![pic-2](images/restful-django/pic_2.webp)

오늘날의 어플리케이션에서는 데이터 소스가 여럿 있을 수 있으며 보다 유용한 어플리케이션을 개발하기 위해 이들을 활용해야 한다.

### REST 구조
REST는 Representational State Transfer의 약자이다. 그것은 API 작동 방법을 정의한 보편적인 표준 구조이다. REST 표준에 따라 생성된 API를 REST API라고 하며, 이를 사용하는 웹 서비스를 RESTful 웹 서비스라고 한다. REST API는 몇 가지 원칙을 만족해야 한다.

#### Stateless
클라이언트의 모든 요청은 필요한 모든 정보를 전달해야 하며 이 정보는 서버 측에 저장될 수 없다. 또한 서버는 상태 정보를 저장하지 않는다. 클라이언트 필요한 정보를 제공해야 한다.

![stateful-arch](images/restful-django/stateful_arch.webp)

상태 저장 구조에서 클라이언트의 상태 정보를 서버의 글로벌 변수에 저장한다. 예를 들어 웹 어플리케이션 프로그램에서 클라이언트는 먼저 로그인한다. 이 정보는 서버 측의 is_logged와 같은 변수에 저장된다. 이후 작업(페이지 보기 등)에서는 이 변수가 선택되고 그에 따라 작동한다. 그러나 이러한 구조에서는 위 그림과 같이 서버가 둘 이상일 경우 문제가 발생한다. 서버 1에 로그인한 사용자가 로드 밸런서에 의해 다른 서버로 리디렉션되면 다시 로그인해야 한다.

상태 비저장(샅애를 저장하지 않는) 구조에서 클라이언트의 상태 정보를 서버에 보관하지 않는다. 토큰을 클라이언트와 공유한다. 클라이언트가 요청을 하면 토큰 정보도 보냅니다. 이러한 방식으로 둘 이상의 서버와 조화롭게 작동한다.

![stateless-arch](images/restful-django/stateless_arch.webp)

상태 비저장은 REST API를 **쉽게 확장할 수(easily scalable)** 있도록 한다. 상태 정보를 저장할 필요가 없기 때문에 각 서버는 클라이언트의 요청을 추적할 수 있어야 한다.

상태 동기화가 필요 없기 때문에 시스템의 **복잡성이 줄어든다(reduce complexity)**.

클라이언트의 각 요청을 **격리(isolation)**시켜 처리한다. 따라서 서버는 클라이언트 요청을 추적할 필요가 없어 **성능을 향상(increase performance)**시킬 수 있다.

토큰은 클라이언트의 상태 정보를 갖고 있기 때문에 요청이 **약간 더 커진다**.

#### 클라이언트-서버
클라이언트와 서버는 시스템의 다른 두 구성 요소이다. 관심 분리 원칙에 따라 이 두 구성 요소는 서로 분리되어 작동해야 한다. 이러한 방식으로 사용자 인터페이스와 데이터 스토리지 작업은 서로 독립적으로 작동한다.

![client-server](images/restful-django/client-server.webp)

클라이언트-서버 설계 패턴을 사용하여 사용자 인터페이스의 이식성을 높인다. 이는 사용자 인터페이스를 여러 플랫폼에서 쉽게 이용할 수 있도록 한다. 게다가, 서버 구성요소는 사용자 인터페이스와 독립적으로 작동하기 때문에 확장성이 향상된다.

#### 균일한 인터페이스
모든 서비스는 동일한 방식으로 HTTP 인터페이스를 사용해야 한다. 또한, 서버는 표준 형식으로 정보를 전송해야 한다. 예를 들어 GET 요청을 사용하여 정보를 가져오거나 읽을 수 있는데, 이는 전 세계적으로 동일하다. 웹의 어플리케이션 프로그램 간에 일관성을 유지하는 데 도움이 된다. 이로써 확장을 위해 서버 구조가 분리된다.

이 균일성을 달성하기 위한 4가지 구조는 제약 조건을 갖고 있다.

**리소스 식별(identification of resources)**: REST는 리소스 지향적이다. REST 구조에서 리소스는 추상화된 정보의 일부이다. 모든 정보는 리소스이다. 주어진 순간의 리소스 상태를 리소스 표현(resource representation)이라 한다. 리소스는 데이터, 데이터를 설명하는 메타데이터와 하이퍼미디어 링크를 포함한다.

각 리소스는 Unique Resource Identifier(URI)인 고유 식별자로 식별되어야 한다. URI를 생성할 때 계층적 접근 방식이 사용된다. 예를 들면

```
www.example.com/users/{{sysuserId}}/comments/{{commentId}}
```

식별자는 응답 형식과 관련이 없다.

식별자는 변경되지 않아야 하며, 항상 계층적 접근법을 따라야 한다.

**리소스 조작(resource manipulation)**: 클라이언트는 리소스를 조작할 수 있어야 한다. 따라서 리소스 표현에 대한 충분한 정보가 있어야 한다. 서버는 리소스 표현을 제공한다. JSON, XML, HTML, 이미지 등 모든 형식이 될 수 있다.

Accept : application/json. URI의 변경 없이 응답 형식을 정의한다.

```
www.example.com/users/1/comments/23
Accept: application/json
```

또는

```
www.example.com/users/1/comments/23
Accept: application/xml
```

따라서 클라이언트는 필요에 따라 특정 형식의 응답을 요청한다. 이를 "콘텐츠 협상(Content Negotiation)"이라고 한다.

**자체 설명 메시지(Self-Descriptive Message)**: 상태 비저장 통신에서 서버는 상태와 클라이언트를 추적하지 않으며 모든 요청은 서로 독립적이다. 따라서 요청 자체에 필요한 모든 정보가 요청에 포함되어야 한다. 즉, 요청이 지신을 스스로 설명할 수 있다.

이 작업은 메서드 타입을 통해 수행할 수 있다. 동일한 URL에 대해 GET(읽기), PUT(수정) 또는 DELETE와 같은 다른 메서드을 사용할 수 있다.

또한 이 정보를 메타데이터에 저장할 수 있다. 그리고 메타데이터를 본문이나 헤더에 배치할 수 있다.

**애플리케이션 상태의 엔진으로서의 하이퍼미디어(HATEOAS, Hypermedia as the engine of the application state)**: API에 액세스하는 것은 웹 페이지를 액세스하는 것과 유사해야 한다. 클라이언트는 응답에서 API의 다른 부분을 액세스할 수 있어야 한다. API의 응답에는 다른 관련 리소스에 대한 링크가 포함되어야 한다.

```
www.example.com/users/1/comments/23
{
   "userId":"xxx",
   "msg":"asd",
   "links":[{
       "rel":"delete",
       "link":"/comment/23"},
       {"rel":"edit",
        "link":"/comment/23"}
]
}
```

#### 캐시가능
서버는 모든 응답에 대하여 캐시 가능 또는 캐시 불가능으로 표시한다. 클라이언트는 응답을 캐시할 수 있으며 다음 번에는 캐시에서 반환한다. 따라서 동일한 요청을 다시 보낼 필요가 없다. 이로 인해, 클라이언트와 서버 간의 통신을 줄일 수 있다. 즉, 확장성과 성능을 향상시킬 수 있다.

![pic_6](images/restful-django/pic_6.webp)

#### 레이어 시스템
계층적 구조를 갖을 수 있다. 각 계층은 고유한 임무를 갖으며 각 계층은 다른 계층에 대해 아무것도 알지 못해도 다른 계층과 상호 작용할 수 있어야 한다.

![pic_7](images/restful-django/pic_7.webp)

#### 주문형 코드(Code on Demand) (선택사항)
클라이언트는 기능 또는 소스 코드를 다운로드할 수 있다. 예를 들어, 양식을 작성할 때 브라우저는 "잘못된 형식의 전자 메일 주소"와 같이 양식의 오류를 표시할 수 있다. 브라우저는 서버에서 보낸 코드로 이 작업을 수행할 수 있다.

#### 메서드

- GET: 소스에 대한 정보 검색
- POST: REST 리소스 생성
- PUT: 리소스 업데이트
- DELETE: 리소스 삭제

### REST 프레임워크
시연 목적으로 템플릿 장고 프로젝트를 생성한다. 이에 대한 [GitHub 링크]()가 이다.

이 프로젝트에는 `api`라는 앱 하나만 있다.

#### 설치
따라서 가장 먼저 Django REST Framework 패키지를 설치해야 한다.

```bash
$ pip install djangorestframework
```

나중에 `settings.py` 파일의 앱 목록에 다음과 같이 추가한다.

```python
INSTALLED_APPS = [
'django.contrib.admin',
'django.contrib.auth',
'django.contrib.contenttypes',
'django.contrib.sessions',
'django.contrib.messages',
'django.contrib.staticfiles',
'api',
'rest_framework',
]
```

#### Models
`Movie`라는 간단한 Model을 생성한다.

```python
# models.py

from django.db import models

class Movie(models.Model):
    title = models.CharField(max_length=250)
    duration = models.IntegerFiekd()
    release_date = models.DateField()
    rating = models.IntegerField()

    def __str(self):
        return self.title
```

#### Urls
다음으로 `api` 앱의 `urls.py`에 엔드포인트를 생성해 보자. 나중에 메서드를 사용하거나 view 클래스를 사용하여 두 가지 방법으로 view를 작성할 수 있다. 아래는 메소드를 사용하는 경우이다.

```python
# urls.py

...

urlpatterns = [
    path('', movie_create),
    path('list/', get_movies),
    path('<int:pk>', movie)
]
```

다음은 view 클래스를 사용한 것이다.

```python
# urls.py

...

urlpatterns = [
    path('', MovieCreate.as_view()),
    path('list/', MovieList.as_view()),
    path('<int:pk>', MovieRecord.as_view())
]
```

#### Serializers
이제 `serializers.py`라는 새 파일에서 직렬화 프로그램을 만들 수 있다. 직렬화 프로그램은 쿼리 세트를 JSON 콘텐츠로 변환한다.

시리얼라이저를 모델에 매핑해야 한다. 이를 두 가지 방법으로 할 수 있다. 첫 번째 방법은 모델 필드를 직렬화기의 필드와 수동으로 일치시키는 것이다. 새로운 레코드 생성과 업데이트 작업을 위한 별도의 방법을 정의해야 하며 다시 모델에 정보를 전달해야 한다.

```python
# serializers.py

class MovieSerializer(serialzers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=250)
    duration = serializers.IntegerField()
    release_date = serializers.DateField()
    rating = serializers.IntegerField()

    def create(self.data):
        return Moview.objects.create(**data):

    def update(self, instance, data):
        instance.title = data.get("title", instance.title)
        instance.duration = data.get("duration", instance.duration)
        instance.release_date = data.get("release_date", instance.release_date)
        instance.rating = data.get("rating", instance,rating)
        instance.save()
        return instance
```

또는 메타 클래스를 정의하여 위의 프로세스를 자동화할 수 있다.

```python
# serializers.py

class MovieSerializer(serialzers.Serializer):
    description = serializers.SerializerMethondField()
    class Meta:
        model = Movie
        field = "__all__"

    def validate_title(self, val):
        if val == "Dexter":
            raise ValidationError("Cannot accept TV Series")

    def validate(self, data):
        if data["duration"] > 600:
            raise ValidationError("Cannot accept movies longer than 10 hours!")

    def get_description(self, data):
        return f"Moview: {data.title} and Rating: {data.rating}"
```

검증 프로세스를 커스터마이즈할 수도 있다. `validate_xfield` 메서드는 지정된 필드를 확인하며, 검증을 위한 일반적인 메서드를 정의할 수 있다.

serializer를 위한 필드를 정의할 수 있다. `get_x` 메서드 타입은 자동으로 필드를 추가한다. 즉, `description` -> `get_description()`

#### Views
먼저, view 메서드를 검토해 보자.

GET: 모델로부터 데이터를 검색하여 시리얼라이저로 보낸다. 나중에 요청를 회신한다.

```python
@api_view(['GET'])
def get_movies(request):
    movies = Movie.objects.all()
    serializer = MovieSerializer(movies, many=True)
    return Response(serializer.data)
```

POST: 요청 내부에 저장된 데이터를 시리얼라이저로 전송한다. 저장하기 전에 데이터가 유효한지 확인해야 한다.

```python
@api_view(['POST'])
def movie_create(request)
    serializer = MOvieSerializer(data=request.data)
    if serailzer.is_valid():
        serializer.is_valid():
        return Response(serializer.data)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

PUT와 DELETE: 업데이트(또는 삭제)할 레코드는 주 키(primary key)를 사용하여 검색한다. 레코드가 없으면 오류가 발생한다. 요청에 전송된 메서드 타입에 따라 필요한 작업을 수행한다.

```python
@api-view(['GET', 'PUT', 'DELETE'])
def movie(request, pk):
    try:
        movie = Movie.object.get(pk=pk)
    except:
        return Response({
            'error':'Movie does not exist',
        }, status = status.HTTP_404_NOT_FOUND)
    
    if request.method = 'GET':
        serializer = MovieSerializer(movie)
        return Response(serializer.data)

    if request.method = 'PUT':
        serializer = MovieSerializer(movie, daya=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HHTP_400_BAD_REQUEST)

    if request.method = 'DELETE':
        movie.delete()
        return Response({
            'delete':True
        })
```

두 번째 방법(더 나은 방법)은 view 클래스를 사용하는 것이다. APIView 클래스를 상속받는다.

```python
class MovieList(APIView):

    def get(self, request):
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Resposne(serializer.data)
```

```python
class MovieCreate(APIView):

    def post(self, request):
        serializer = MovieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTTP_400_BAD_REQUEST)
```

```python
class MovieRecord(APIView):

    def get_movie_by_pk(self. pk):
        try:
            return Movie.objects.get(pk=pk)
        except:
            return Response({
                'error':'Movie does not exist'
            }, status = status.HTTP_400_NOT_FOUND)

    def get(self, request, pk):
        movie = self.get_movie_by_pk(pk)
        serializer = MovieSerializer(movie, data=request.data)
        return Response(serializer.data)

    def put(self, request, pk):
        movie = self.get_movie_by_pk(pk)
        serializer = MovieSerializer(movie, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        movie = self.get_movie_by_pk(pk)
        movie.delete()
        return Response(status = status.HTTP_400_BAD_REQUEST)
```

#### Endpints
마지막으로 엔드포인트 URL을 살펴보고 몇몇 기능을 시도해 보겠다.

새 레코드를 생성하는 방법:

```
http://127.0.0.1:8000/movies/
```

![add_movie](images/restful-django/add_movie.webp)

새로운 레코드가 삽입되었다. 새로운 "description" 필드가 자동으로 생성되었음을 알 수 있다.

![add_success](images/restful-django/add_success.webp)

모든 레코드를 나열하여 보자.

```
http://127.0.0.1:8000/movies/list/
```

![all_records](images/restful-django/all_records.webp)

레코드를 업데이트할 수 있다.

```
http://127.0.0.1:8000/movies/2
```

![put](images/restful-django/put.webp)

![record_update](images/restful-django/record_update.webp)

이상으로 Django REST Framework에 대한 내용이다. 

**주** 이 포스팅은 [RESTful Django — Django REST Framework](https://levelup.gitconnected.com/restful-django-django-rest-framework-8b62bed31dd8)을 편역한 것임.
