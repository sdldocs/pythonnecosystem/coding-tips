[여기](https://github.com/ericsdevblog)에서 이 포스팅의 소스 코드를 다운로드받을 수 있다.

웹 개발 초보자 가이드에서는 간단한 예를 들어 웹 개발의 기본 사항을 설명한다. 시작하기 전에 먼저 웹의 몇 가지 기본 개념에 대해 설명해 보겠다.

월드 와이드 웹(World Wide Web)은 하이퍼링크로 연결된 서버와 클라이언트로 구성된 정보와 자원 공유 시스템이다. 브라우저에서 웹의 문서와 리소스를 요청한 다음 HTTP(Hypertext Transfer Protocol)를 통해 전송되며, 이들이 브라우저에 출력될 수 있다.

-  [World Wide Web 소개](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/intro-www/)

## 프론트 엔드에 대해 이야기하는 것으로 시작하자

HTML(HyperText Markup Language)은 웹의 가장 기본적인 구성 요소이다. 이는 모든 웹 페이지의 구조와 내용을 정의한다. 일반적으로 다른 기술들이 함께 사용되며, 예를 들어 CSS는 페이지의 모양을 설명하는 데 사용될 수 있으며, 자바스크립트는 웹 페이지의 동작을 정의하고 사용자와 상호작용하는 데 사용될 수 있다.

- [HTML 기초 #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/html-basics-1/)
- [HTML 기초 #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/html-basics-2/)
- [CSS 기초 #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-1/)
- [CSS 기초 #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-2/)
- [CSS 기초 #3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-3/)

JavaScript는 여기서 소개할 첫 번째 프로그래밍 언어이다. 넷스케이프 내비게이터 브라우저의 웹 페이지에 프로그램을 추가하는 방법으로 1995년에 만들어졌다. 오늘날 이 언어는 다른 모든 주요 웹 브라우저에 채택되었다.

- [JavaScript 기초 #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-1/)
- [JavaScript 기초 #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-2/)
- [JavaScript 기초 #3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-3/)
- [JavaScript 기초 #4](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-4/)
- [JavaScript 기초 #5](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-5/)

Vue.js는 초보자 매우 친화적인 프론트엔드 JavaScript 프레임워크이다. 프론트 엔드 엔지니어가 되는 것에 관심이 있다면 시작하기에 좋은 출발점이다. Vue의 핵심 라이브러리는 사용자가 볼 수 있는 부분인 view 계층에만 초점을 맞추고 있다. 이는 또한 저자가 프레임워크를 뷰(vue)라고 이름 지은 이유이다.

- [초보를 위한 Vue.js #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-1/)
- [초보를 위한 Vue.js #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/vuejs-for-beginners-2/)

이 포스팅의 백엔드 섹션으로 이동하기 전에 지금까지 배운 내용을 사용하여 포트폴리오 웹 사이트를 만들어 보자.

- [포트폴리오 웹 사이트 만들기](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/create-portpolio/)

## 백엔드에 대하여
이제 백엔드에 대해 이야기하여 보자. 여러분은 두 가지에서 선택할 수 있다, 만약 PHP를 선호한다면, Laravel로 갈 수 있다. 많은 사람들이 PHP에 대한 비판을 하고 있지만, 그럼에도 불구하고, PHP는 여전히 웹 개발 분야에서 지배적인 언어이다. 그리고 저자는 개인적으로 Laravel이 매우 강력하고 사용하기 쉬운 프레임워크라고 생각하고 있다. 하지만 파이썬이 더 편하다면 Django가 대신할 수 있다. (**주** 여기서는 Django만을 편역한다)

- [초보를 위한 Laravel #1](https://www.ericsdevblog.com/posts/laravel-for-beginners-1/)
- [초보를 위한 Laravel #2](https://www.ericsdevblog.com/posts/laravel-for-beginners-2/)
- [초보를 위한 Laravel #3](https://www.ericsdevblog.com/posts/laravel-for-beginners-3/)
- [초보를 위한 Laravel #4](https://www.ericsdevblog.com/posts/laravel-for-beginners-4/)
- [초보를 위한 Laravel #5](https://www.ericsdevblog.com/posts/laravel-for-beginners-5/)

Larevel에 비해 Django는 몇 가지 장점과 단점을 모두 갖고 있다. 예를 들어, Django는 매우 인기 있는 프로그래밍 언어인 Python에 기반을 두고 있으며, 이는 모든 종류의 다른 작업을 수행하는 많은 다양한 패키지가 있다는 것을 의미하며, Laravel과 달리 Django는 매우 강력한 내장 관리 패널을 가지고 있으며, 나중에 자세히 알아보자…

- [초보를 위한 Django #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-1/)
- [초보를 위한 Django #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-2/)
- [초보를 위한 Django #3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-3/)
- [초보를 위한 Django #4](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-4/)
- [초보를 위한 Django #5](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/django-beginner-5/)
- [Django 프로젝트 배포 방법](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/deploy-django-project/)

## 프런트 엔드와 백엔드의 만남
마지막으로 프런트엔드와 백엔드를 결합하여 최신 단일 페이지 어플리케이션 프로그램을 만들어 본다.

- [Django와 Vue.js로 모던 어플리케이션 작성 #1](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/create-app-django-vue-1/)
- [Django와 Vue.js로 모던 어플리케이션 작성 #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/create-app-django-vue-2/)
- [Django와 Vue.js로 모던 어플리케이션 작성 #3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/create-app-django-vue-3/)

**주** 이 포스팅은 [Beginner’s Roadmap to Web Development](https://www.ericsdevblog.com/series/beginners-roadmap-to-web-development/)를 편역한 것임.
