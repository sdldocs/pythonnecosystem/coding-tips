# 숙련된 Python 개발자로

다음 약 40 사항들을 실천하여 숙련된 Python 개발자가 되어 보자.

**1. 가상 환경을 사용**: 가상 환경은 종속성 및 패키지 버전과 관련된 문제를 방지하기 위해 Python 실행  환경을 격리할 수 있다. 또한 요구사항 및 제약사항 파일을 유지하고자 한다면, 앱이 다른 장소에서 실행할 수 있는 제약 사항을 보여준다.
```python
pip install -r requirements.txt -c constraints.txt
```
**2. type 힌트를 사용**: type 힌트는 코드를 더 읽기 쉽게 해준다. 따라서 코드의 유지보수가 용이하고 디버깅도 쉬워진다.
```python
def demo(x: type_x, y: type_y, z: type_z= 100) -> type_return :
```
**3. 적절한 예외 처리**: 특정 예외를 포착하면 코드에서 잘못된 쉽게 이해할 수 있다. Bare `except` 절은 **SystemExit**와 **KeyboardInterrupt**를 포함한 모든 예외를 포착한다. 그러나 둘은 같지 않으므로 다르게 처리해야 한다.

**4. 컨텍스트 관리자(context manager)를 사용**: 이는 리소스 관리에 도움을 주고 연결이나 파일 핸들 오용을 피할 수 있다.
```python
with open("demo.txt", mode="w") as file:
```

**5. `if __name__ == “main”`을 사용**
스크립트를 가져오는 동안 불필요한 개체를 피할 수 있다.

**6. 컴프리헨션(Comprehension)을 사용**: 컴프리헨션은 코드를 간단하고 효율적으로 만든다.
```python
another_list = [ x for item in iterable]
```

**7. 열거(enumerate)**: 열거된 요소에 대한 조건부 건너뛰기가 필요 없을 때 인덱스를 수동으로 처리해야 하는 경우 작업하기 쉽고 빠르다.
```python
for idx, num in enumerate(nums)
```

**8. `zip`을 사용**: 여러 리스트를 함께 작업할 때 zip method는 반복 작업을 간소화하고, 즉시 항목을 사용할 수 있도록 한다.
```python
for item1, item2 in zip(list1, list2)
```

**9. 적절한 들여쓰기(indentation)**: 일관된 들여쓰기(4칸)로 작성하면 읽기에 도움이 된다. 또한 텍스트 편집기나 파일 io 스트림에서 스크립트를 읽는 경우에도 작업이 쉬워진다.

**10. `for`와 `while` 문에서 `else`를 사용**: 모든 항목을 자연적으로 처리된다는 것을 알려주는 python적 방법이다.

**11. `in` 키워드를 사용**하여 딕셔너리의 키 존재를 확인.
```python
if key in my_dict:
```

**12. 딕셔너리에서 `items` 메소드 사용**: 키에 루프한 다음 get 또는 subscript를 사용하여 값을 액세스하는 것보다 키와 항목에 액세스하는 더 python 다운 방법이다.
```python
for key, val in my_dict.items()
```

**13. `dict.get()` 메소드를 사용**: 이렇게 하면 키가 없을 때 오류가 발생하지 않고 키가 없을 때 기본값을 반환할 수 있다.

**14. 객체 타입이 같다는 것을 수행할 때 `isinstance` 키워드를 사용**: 객체 타입을 비교하면 상속된 객체의 경우 틀린 결과를 얻을 수 있다.

**15. 단순 값(singleton)의 동등을 위하여 `is`를 사용**: `None`, `True`와 `False`를 확인할 때 `is`를 사용한다. `is` 연산자는 두 항목이 동일한 객체(즉, 동일한 메모리에 있음)를 참조하는지 확인한다. `==` 연산자는 값이 같은지 확인한다.

**16. 부울식에서 "or", "and"와 "not" 키워드를 사용**: 이는 올바른 작업 흐름을 결정하는 데 도움을 준다. 또한 참에 비해 거짓이 너무 많은 코드의 경우 `and`를 `or`보다 먼저 수행하면 식의 결과를 일찍 얻을 수도 있다. OR에서는 계속 false가 발생할 때까지 조건을 확인하고, 결과가 참인 조건을 만나는 즉시 나머지 조건을 확인하지 않는다. AND에서는 거짓인 조건이 나타나면 나머지 조건을 확인하지 않는다. **Note**: 모든 조건이 true로 평가되는지 확인해야 하거나 어떤 조건이 true인지 확인해야 할 경우 부울 값의 반복 가능한 리스트에 `any`와 `all` 함수를 사용할 수 있다. 

**17. generator를 사용**: generator는 메모리를 절약하고 선능을 향상시킨다.
```python
(expression for item in iterable)
```

**18. (map, reduce와 filter와 같은) 함수형 프로그래밍 기술을 사용**: 이를 이용하여 효율적인 코드를 작성할 수 있다. 
`squared = map(lambda x : x*x, [1, 2, 3])`는 for 루프를 수행하는 것보다 빠르다. 또한 map은 메모리의 모든 항목을 로드하는 루프와 달리 각 항목을 하나씩 로드하므로 메모리 효율성이 향상된다. 그렇지만 우리는 가능한 generator를 선호한다.

**19. 데코레이터(decorator)를 사용**: 이는 재사용할 수 있는 코드 작성에 좋다. `@retry`, `@properties`, `@lru_cache` 등과 같은 데토레이터는 코드의 기능을 확장할 수 있게 도와준다. Flask와 같은 프레임퉈크는 `@route` 데코레이터가 있으며, Airflow는 `@task` 연산자를 갖고 있다. 

**20. 올바른 내장 라이브러리를 사용**: 올바른 내장 패키지를 사용하면 자주 사용하는 코드를 줄일 수 있다. 또한 특정 패키지가 특정 작업을 수행하는 데 효율적이기 때문이다. 올바른 패키지를 사용하는 것이 합리적이다. 예: `os` vs `pathlib`, `os.system` vs `subprocess`.

**21. 바른 명명 규칙을 사용**: [PEP-8](https://zerosheepmoo.github.io/pep8-in-korean/doc/introduction.html)은 Python 프로그래머들 사이에서 널리 알려진 지침으로, 변수, 클래스 및 함수의 이름을 명명하는 방법이다.

**22. 스타일 가이드 고수**: 선택한 규칙을 고수하는 것은 산들바람과 같이 리팩토링 순간들을 만들고 나중에 이를 이해하기 쉽도록 한다.

**23. Docstring과 주석을 사용**: Docstring은 함수 또는 모듈이 의도하는 바를 사용자가 이해할 수 있도록 돕는다. 입력, 출력과 타입, 샘플 값 등을 설명한다. 또한, 그것들이 의미하는 바를 기술하기 전 한 줄짜리 주석을 작성하면 더 도움이 될 것이다

**24. 반복 가능한 Unpacking**: 
```python
first, second, third = [ ‘first’, ‘second’ , ‘third’]
```

**25. `print` 대신 `logging`을 사용**: logging을 사용하면 일관된 메시지를 가질 수 있으며 적절한 로그 수준을 설정하여 메시지를 억제/필터링할 수 있다.

**26. `f`를 사용하여 포맷팅**: F 문자열은 .format보다 보다 좋고 선호되는 문자열 포맷팅 방법이다. 이는 변수 치환, 표현식 평가, 소수 반올림과 유효자릿수를 지원한다. **Note**: F-String을 사용할 때 한가지 주의 사항이 있다. 변수를 선언한 다음 스코프내에서 사용할 수 있어야 한다.

**27. 파일 경로를 문자열로 처리하지 않음**: 파일 경로는 문자열이지만 문자열처럼 조작하는 것은 안 어울린다. 숙련된 프로그래머들은 대신 `pathlib` 모듈을 사용한다.

**28. `divmod`를 사용**: 종종 몫과 나머지 계산이 필요할 때가 있다. 이 경우 `quotient, remainde = divmod(x, y)`를 사용하는 것이 더 쉽다.

**29. 단일 문자 변수 사용 금지**: 초보 Python 개발자는 한 문자 변수를 한 문자로 작성한다. 이는 타이핑 수를 줄여주지만 목적을 이해하기 어렵고 디버깅하기는 훨씬 더 어렵다. 숙련된 프로그래머는 설명적인 변수 이름을 사용하는 것을 선호한다.

**30. getter와 setter 대신 `@property` descriptor를 사용**: `@property`와 `@attribute.setter`와 같은 descriptor를 사용하는 것이 속성을 가져오고 설정하는 더 Python적 방법이다. 

**31. 키를 iterable로 복사하고 향후 작업을 위하여 삭제된 키를 저장**: iterable을 반복하는 동안 삭제를 수행할 수 있지만 오류가 발생할 수 있다. 키나 항목을 삭제하려는 경우 저장한 다음 별도로 삭제한다.

**32. 불변 데이터 구조를 사용**: 리스트, 세트와 딕셔너리를 사용하여 매개 변수를 변경하지 않는 것은 매우 직관적이다. 그러나 이 데이터 구조는 가변적이기 때문에 오버헤드가 있고 속도가 느리다. 숙련된 개발자는 `tuple`, `frozenset`, `MappingProxyType`과 같은 구조를 사용한다.

**33. 구조화된 데이터를 위하여 데이터 클래스 또는 명명된 튜플(named tuple)을 사용**: 숙련된 Python 개발자들은 데이터를 포함하는 엔티티를 나타내기 위하여 데이터 클래스나 NamedTuple을 선호한다.

**34. 연산자 연결하여 비교**: 연산자 연결은 `if a < b and b < c`를 `if a < b < c`로 더 읽기 쉽고 간결하게 만든다.

**35. 문자열 연결을 위하여 `"<seperator>".join(Iterable)`을 사용**: Python 초보 개발자는 문자열 연결 동작을 string_variable += add_string으로 빠르게 작성한다. 이는 작동하지만 느리고 자체적인 오버헤드가 있다. 일반적으로 3개 이상의 문자열을 연결해야 할때 숙련된 개발자는 `'separator'.join(list_of_strings)`을 사용한다.

**36. 문자열 결합을 위하여 `stringio`를 사용**: 결합 사용법을 보인 이전과 유사하게 숙련된 개발자는 `stringio` 버퍼를 사용한다. 
```python
string_buffer = io.StringIO() string_buffer.write() 
string_buffer.getvalue()
```

**37. 전문화된 서브클래스를 사용**: 모든 것에 사전을 사용하는 것은 지나친 방법이다. 숙련된 개발자는 달성하고자 하는 목표에 따라 데이터 구조의 특정 하위 클래스를 사용하는 것을 선호한다. 
```python
from collections import defaultdict, Counter
```

**38. deepcopy를 사용**: 전체 복사본은 재귀적인 자식 탐색과 복사 없이 부모 객체만 복사하는 복사본과 달리 원래 객체의 완전히 독립적인 복사본을 생성한다.
```python
from copy import deepcopy
```

**39. 진행률 바 또는 상태를 추가**: 루프 또는 프로세스가 장기간 실행되는 경우 진행률 바를 보여주거나 또는 진행 상태를 로깅한다. 이를 통해 사용자에게 진행 상황을 알 수 있다.

**40. 설정을 위하여 INI, TOML, YAML, ENV 등 별도 파일을 사용**: 별도의 설정 또는 속성 파일이 있으면 빠른 구성을 할 수 있다. 그렇지 않으면 어떤 속성이 설정되어 있는지 여러 곳을 살펴봐야 한다. 

--- 
**(주)** 위 내용은 [25 signs of an experienced Python developer](https://blog.devgenius.io/25-signs-of-an-experienced-python-developer-edf36da83f19)과 [[15 more] Signs of a professional Pyhton programmer](https://blog.devgenius.io/15-more-signs-of-a-professional-programmer-b063d40b6e01)을 편역한 것이다.
