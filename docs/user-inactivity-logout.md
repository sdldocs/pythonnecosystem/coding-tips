Django에서는 세션이 지정된 시간 동안 비활성인 경우 사용자를 시스템에서 로그아웃할 수 있다. 이를 위한 방법<sup>[1](#footnote_1)</sup>은 다음과 같다.

- 미들웨어 생성: 사용자 세션의 마지막 활동 시간을 확인하는 커스텀 미들웨어를 만든다. 지정된 시간 동안 사용자 세션이 비활성인 경우 사용자를 로그아웃한다.

```python
import datetime
from django.contrib.auth import logout

class SessionTimeoutMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            last_activity = request.session.get('last_activity', None)
            if last_activity:
                last_activity = datetime.datetime.strptime(last_activity, '%Y-%m-%d %H:%M:%S')
                if (datetime.datetime.now() - last_activity).seconds > 30: # 30 seconds = timeout duration
                    logout(request)
            request.session['last_activity'] = current_time
        response = self.get_response(request)
        return response
```

- `MIDDLEWARE` 설정에 미들웨어를 추가: 프로젝트에서 사용할 위에서 생성한 커스텀 미들웨어를 `MIDDLEWARE` 설정에 추가한다.

```python
MIDDLEWARE = [
    # ...
    'myapp.middleware.SessionTimeoutMiddleware',
    # ...
]
```

- 사용자 로그아웃: 세션이 너무 오랫동안 비활성인 경우 사용자를 로그아웃하기 위한 `django.contrib.auth` 모듈의 `logout()` 함수를 사용한다. 

```python
from django.contrib.auth import logout

def logout(request):
    logout(request)
```

---
<a name="footnote_1">1</a>: 이 포스팅은 [Implementing User Inactivity Logout in Django](https://medium.com/@m.ambenge01/implementing-user-inactivity-logout-in-django-a020f6ebeb27)를 편역한 것이다.
