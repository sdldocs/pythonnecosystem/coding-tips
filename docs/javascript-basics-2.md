이전에 문자열, 숫자 및 부울 값같은 JavaScript에 내장된 일부 데이터 타입에 대해 설명했다. 이 포스팅에서는 이러한 모든 값을 그룹화하여 배열과 객체라는 보다 복잡한 구조를 만들 수 있는 두 가지 새로운 데이터 타입에 대해 설명한다.

## JavaScript에서 배열 만들기
먼저 배열에 대해 설명하겠다. 배열은 쉼표로 구분된 대괄호 쌍(`[]`) 안에 있는 값의 목록이다.

```js
let listOfNumbers = [1, 2, 3, 4, 5];

let listOfStrings = ["1", "2", "3", "4", "5"];
```

1 대신 0에서 시작하는 인덱스를 참조하여 배열의 요소에 액세스할 수 있다. 이는 컴퓨터 프로그래밍에서 매우 흔하다, 여러분은 그것에 익숙해져야 한다.

```js
let x = listOfNumbers[0]; // x = 1 (index 0 is the first element)

let y = listOfNumbers[2]; // y = 3 (index 2 is the third element)
```

배열의 마지막 요소를 액세스하려고 하는데 배열의 길이를 알 수 없는 경우에는 어떻게 해야 할까? 0부터 시작하는 모든 인덱스가 값을 제공하는지 확인하는 대신 `arrayName.length`를 사용하여 배열의 길이를 알 수 있다. 여기서 `length`는 속성이라고 하며 `.`은 값의 속성을 액세스할 수 있는 방법이다. 예:

```js
let z = listOfNumbers[listOfNumbers.length - 1]; // z = 5
```

이 예에서 `listOfNumbers.length`는 길이를 계산할 때 1부터 시작하기 때문에 5를 제공한다. 그러나 인덱스는 항상 0부터 시작하므로 마지막 요소의 인덱스는 길이보다 1 작아야 하므로 `ListOfNumbers.length - 1` 이 된다. 속성과 메서드(함수 값을 갖는 속성)에 대해 자세히 설명한다.

배열의 값을 변경할 수 있다.

```js
let listOfNumbers = [1, 2, 3, 4, 5];
listOfNumbers[2] = 100;

// listOfNumbers = [1, 2, 100, 4, 5];
```

### 배열 루프
경우에 따라 전체 배열에서 반복하고 각 요소를 하나씩 액세스해야 할 필요가 있을 수 있다. JavaScript는 두 가지 다른 방법으로 이 작업을 수행할 수 있다.

```js
let list = [...];

for (let e of list) {
   ...
   // Do something with the variable e
}
```

이는 이전에 본 방법이다. 매번 반복할 때마다, 변수 `e`에 배열 `list`의 다음 요소가 할당될 것이고, `for` 루프 베부에서 변수 `e`를 이용하여 무언가를 할 수 있다.

두 번째 방법은 인덱스를 사용하는 것이다.

```js
for (let i = 0; i < list.length; i++) {
  let e = list[i];
}
```

위의 예에서 변수 `i`는 배열 요소의 인덱스에 바인딩되고 `e`는 인덱스 `i`를 사용하여 배열 요소에 바인딩된다. `i++` 표현식은 `i = i + 1`의 단축 표기법이다.

### 스택과 큐
스택과 큐는 컴퓨터 프로그래밍에서 매우 중요한 두 가지 데이터 구조이며, 배열을 사용하여 구현할 수 있다.

스택은 **LIFO(Last In First Out)**의 원리에 기초한 요소의 구조이다. 이는 책 더미와 같다. 새 책을 스택에 추가하려면 책을 맨 위에 놓고 책을 제거하려면 위에 있는 책도 제거해야 한다.

![stack](images/javascript-basics/stack.png)

스택 데이터 구조에는 두 가지 기본 동작이 있다.

- **push**은 스택에 새 요소를 삽입하는 데 사용된다.
- **pop**은 스택에서 가장 최근의 요소를 제거하고 값을 반환하는 데 사용된다.

다행히도 JavaScript는 패키지에서 이 두 메서드를 제공한다. 메서드는 함수 값을 가진 속성일 뿐이므로 다시 `.` 연산자를 사용할 수 있다.

```js

let stack = [];

stack.push(2);
stack.push(5);
stack.push(7);
stack.push(9);
stack.push(7);

console.log(stack); // -> [2, 5, 7, 9, 7]

stack.pop(); // -> returns 7
stack.pop(); // -> returns 9
stack.pop(); // -> returns 7
stack.pop(); // -> returns 5
stack.pop(); // -> returns 2

console.log(stack); // -> []
```

![stack-push-pop](images/javascript-basics/stack-push-pop.webp)

큐는 또 다른 매우 유용한 데이터 구조입니다. **first in first out(FIFO)** 원칙을 따른다는 점을 제외하면 스택과 매우 유사하다. 마치 식당에서 줄을 서서 기다리는 것 같다, 먼저 오면 음식이 먼저 나온다.

![queue](images/javascript-basics/queue.png)

큐 데이터 구조에도 두 가지 기본 동작이 있다.

- **enqueue**는 큐 끝에 새 요소를 삽입하는 데 사용된다.
- **dequeue**는 큐의 시작 부분에서 요소를 제거하고 값을 반환하는 데 사용된다.

JavaScript에 내장된 두 메서드가 이 두 가지 동작을 도와주지만 용어가 조금 다르다. enqueue 연산의 경우 `push()` 메서드를 사용한다. 이 메서드는 새 요소를 배열의 끝에 푸시하기 때문이다. dequeue 연산의 경우, 배열의 첫 번째 요소를 제거하는 `shift()` 메서드를 사용한다.

```js
let queue = [];

queue.push(2);
queue.push(5);
queue.push(7);
queue.push(9);
queue.push(7);

console.log(queue);

queue.shift();
queue.shift();
queue.shift();
queue.shift();
queue.shift()
```

![enqueue-dequeue](images/javascript-basics/enqueue-dequeue.webp)

## JavaScript에서 객체
이제, 방금 말한 속성의 개념에 대해 자세히 알아보겠습니다. `listOfNumbers.length`와 `Math.max` 같은 이상한 표현들을 보았다. 이들은 일부 값의 속성을 액세스하는 표현식이다. 첫 번째 예는 `listOfNumbers` 배열의 `length` 속성을 액세스한다. 두 번째 예는 `Math` 객체의 `max` 속성을 액세스한다.

설명했던 거의 모든 데이터 타입에는 기본 제공 속성이 있다. 예를 들어 문자열에는 문자열의 길이를 저장하는 배열과 마찬가지로 길이 속성이 있다.

숫자로 나타내는 `length` 속성 외에도 함수 값을 포함하는 여러 속성이 있다. 예를 들어 문자열의 `toUpperCase` 속성을 사용하여 해당 문자열의 모든 문자를 대문자로 변환하는 해당 문자열의 복사본을 가져올 수 있다. 일반적으로 함수 값을 갖는 이러한 속성을 **메서드**로 참조한다.

```js
let string = "abCDefg";
console.log(string.toUpperCase()); // -> "ABCDEFG"
console.log(string); // -> "abCDefg"
```

`toUpperCase()` 메서드를 호출해도 문자열 변수의 원래 값은 변경되지 않는다.

### 자체 속성 작성
지금까지 설명한 모든 속성은 기본 제공 속성이며, 모두 JavaScript와 함께 제공된다. 하지만, 여러분만의 속성을 만들고 싶다면 어떨까? 객체는 여기서 논의할 두 번째 데이터 타입으로, 고유한 속성을 만들 수 있다.

객체는 중괄호 `{}`을(를) 사용하여 정의된 임의 속성 모음이다. 예를 들어, 여기서는 `house`라는 이름의 객체를 정의한다.

```js
let house = {
  members: 4,
  names: ["Mason", "Robert", "Lara", "Wynne"]
};
```

대괄호 안에 쉼표로 구분된 속성 리스트가 있다. 각 속성은 `name: value` 형식으로 정의된다.

위의 예에서는 집에 네 명의 구성원이 있다. 이 정보를 액세스하려면 이전에 사용했던 것과 동일한 표기법으로 점(`.`)을 사용한다.

```js
console.log(house.members); // -> 4
```

객체는 또한 변경 가능하며, 이는 객체의 값을 수정할 수 있음을 의미한다. `=` 연산을 사용하여 이를 수행할 수 있다. 예:

```js
house.members = 5;

console.log(house.members); // -> 5
```

그러나 이는 여러분이 직접 만든 속성에만 적용된다. 문자열, 숫자 및 배열같은 데이터 타입의 속성은 변경할 수 없으며 수정할 수 없다. 예를 들어, `"cat"`을 포함하는 문자열이 있는 경우 해당 문자열의 문자를 `"rat"`의 철자로 변경하는 코드를 작성할 수 없다.

### JSON
다음으로 넘어가기 전에 웹 개발 분야에서 널리 사용되는 또 다른 데이터 구조인 JSON을 소개하고자 한다.

속성(`name: value`)을 정의할 때 `name`에 실제로 해당 `value`가 포함되어 있지 않다. 대신 값이 저장된 메모리의 위치를 가리키는 주소로 컴퓨터 메모리에 저장된다.

데이터를 파일에 저장하거나 인터넷을 통해 다른 사람에게 보내려면 이러한 엉킨 메모리 주소를 인터넷을 통해 저장하거나 보낼 수 있는 표현으로 변환해야 한다. 이 프로세스를 직렬화(serialization)라고 하는데, 이는 데이터가 평이한 기술로 변환된다는 것을 의미한다. 널리 사용되는 직렬화 형식은 JSON(JavaScript Object Notification, "Jason"으로 발음됨)이라고 한다.

JSON은 몇 가지 추가적인 제한 사항을 제외하고 javaScript의 객체 정의 방식으로 보인다. 속성 이름은 큰따옴표(`"`)로 둘러싸여야 하며, 실제 계산과 관련된 함수나 어떤 것도 허용되지 않으며 단순한 데이터 타입만 허용된다. 따라서 JSON 형식으로 `house` 객체를 표현하면 다음과 같다.

```js
{
  "members": 4,
  "names": ["Mason", "Robert", "Lara", "Wynne"]
}
```

JSON은 웹에서 데이터 저장과 통신 형식으로 JavaScript 이외의 언어에서도 널리 사용된다. 앞으로 백엔드 개발에 대해 설명하면서 다시 만나게 될 것이다.

## 객체-지향 프로그래밍이란
이전 섹션에서는 객체라고 하는 새로운 데이터 유형에 대해 설명했다. 컴퓨터 프로그래밍에서 객체는 단순한 데이터 구조가 아니라 코드를 구성하는 방법으로 매우 일반적으로 사용된다. 프로그래머들은 서로 밀접한 관계가 있는 값과 함수를 그룹화하고 동일한 객체에 배치하여 접근하기 쉽게 한다. 코드를 구성하는 이 방법을 객체 지향 프로그래밍이라고 한다. 이 섹션에서는 이러한 개념을 JavaScript에 어떻게 적용할 수 있는지에 대해 설명한다.

### 캡슐화
객체 지향 프로그래밍의 핵심 아이디어는 프로그램을 작은 조각으로 나누고 각 조각은 자신의 일에만 신경쓰는 것이다. 다른 코드 조각을 작업하는 사람들은 이 코드 조각이 어떻게 작성되었는지, 심지어 존재하는지 알 필요가 없다.

때때로 서로 다른 조각들이 더 복잡한 작업을 수행하기 위해 서로 통신해야 한다. 이 작업을 수행하기 위해 프로그래머는 객체 내부에 외부와 대화할 수 있는 속성/메소드를 생성한다. 이 메소드를 **public**이라 하며, 일반적으로는 인터페이스라고 한다. 실제 구현은 개체 내부에 **private** 속성으로 숨겨져 있어 외부 코드로 보거나 액세스할 수 없다. 이러한 인터페이스를 구현에서 분리하는 방법을 캡슐화라고 한다.

대부분의 프로그래밍 언어에는 public과 private 속성을 나타내는 매우 독특한 방법이 있으며, 일반적으로 `public`과 `private` 속성을 키워드로 지정한다. 그러나 JavaScript에는 이 기능이 내장되어 있지 않다. 적어도 아직까지 그렇지 않다. 그러나 JavaScript 프로그래머들은 여전히 **private**로 만들어야 하는 속성의 시작 부분에 밑줄 문자(`_`)를 넣어 캡슐화의 개념을 따르고 있다. 그러나 이는 JavaScript에 내장된 기능이 아니기 때문에 기술적으로는 외부에서 이러한 속성에 액세스할 수 있지만 보안상의 이유로 절대로 해서는 안된다.

### 메서드
알다시피, 메서드는 함수를 값으로 하는 속성일 뿐이다. 간단한 메서드는 다음과 같다.

```js
// Create a new empty object
let rabbit = {};

// Add a method named speak() to the empty object
rabbit.speak = function (line) {
  console.log(`The rabbit says '${line}'`);
};

// Excute the mathod
rabbit.speak("I'm alive.");
```

때때로 메소드는 호출된 객체에 대해 객체에 저장된 두 숫자를 가져와서 합산하거나 객체에서 문자열 값을 가져와 처리하는 등의 작업을 수행해야 한다. 이렇게 하려면 호출된 객체를 자동으로 가리키는 바인딩인 `this` 키워드를 사용한다. 예를 들어 보겠다.

```js
// Create the method named speak()
function speak(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
}

/*
Create an object named whiteRabbit, with two properties, "type"
and "speak". By using the "this" keyword in the method "speak",
we are able to access the "type" property in the same object.
*/

// In this case, this.type = "white".
let whiteRabbit = { type: "white", speak };

// In this case, this.type = "hungry".
let hungryRabbit = { type: "hungry", speak };
```

### 프로토타입
다음 코드를 보자.

```js
// Create an empty object
let empty = {};

console.log(empty.toString); // -> function toString(){...}
console.log(empty.toString); // -> [object Object]
```

빈 개체를 정의했지만 속성을 끌어낼 수 있다. 엄밀히 말하면, 그 속성은 객체에서 나온 것이 아니라 객체의 프로토타입에서 나온 것이다. 프로토타입은 기본적으로 `empty` 객체에 기반이 되는 또 다른 객체이며, 속성의 대체 소스 역할을 한다. 객체에 없는 속성에 액세스하려는 경우 JavaScript가 해당 속성의 프로토타입을 자동으로 검색한다.

JavaScript는 데이터 타입의 프로토타입을 반환하는 메서드(`Object.getPrototypeOf()`)를 제공한다. 예를 들어, 방금 만든 빈 개체의 프로토타입을 찾아 보겠다.

```js
console.log(Object.getPrototypeOf(empty)); // -> {..., constructor: Object(), ...}

console.log(Object.getPrototypeOf(empty) == Object.prototype); // -> true
```

`Object.prototype`은 생성하는 모든 객체의 조상 루트이지만 모든 데이터 타입이 동일한 프로토타입을 공유하지는 않는다. 예를 들어 함수는 `Function.prototype`에서 파생되고 배열은 `Array.prototype`에서 파생된다.

```js
console.log(Object.getPrototypeOf([]) == Array.prototype); // -> true

console.log(Object.getPrototypeOf(Math.max) == Function.prototype); // -> true
```

그러나 이러한 프로토타입은 여전히 객체이기 때문에 프로토타입도 가지고 있으며, 이는 일반적으로 `Object.project`이다. 그렇기 때문에 지금까지 설명한 거의 모든 데이터 타입의 객체를 문자열 표현으로 변환하는 `toString` 메서드가 있다.

실제로 자체 프로토타입을 만들고 `Object.create()` 메서드를 사용하여 특정 프로토타입을 사용하는 객체를 만들 수 있다.

```js
// Create an object, which we'll use as a prototype
let protoRabbit = {
  speak(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
  },
};

// Create a new object using the protoRabbit as the prototype
let killerRabbit = Object.create(protoRabbit);

killerRabbit.type = "killer";

// Try to access the speak() method from the killerRabbit object
killerRabbit.speak("SKREEEE!"); // -> The killer rabbit says 'SKREEE!'
```

### 클래스
객체 지향 프로그래밍에는 클래스라는 개념이 있는데, 이것은 프로토타입과 똑같이 작동한다. 클래스는 (원형과 마찬가지로) 객체 타입의 모양, 객체의 속성 및 방법을 정의한다. 이러한 객체를 클래스의 인스턴스라고 한다.

클래스의 인스턴스를 만들려면 프로토타입/클래스에서 파생된 새 객체를 만들어야 한다. 그러나 객체가 프로토타입/클래스에서 파생된 속성뿐만 아니라 클래스 인스턴스가 가져야 하는 속성을 가지고 있는지 확인해야 한다. 이것이 생성자 함수가 하는 일이다.

```js
// An example of a constructor function
function makeRabbit(type) {
    // Create a new object using protoRabbit as prototype
    let rabbit = Object.create(protoRabbit);

    // Add a property named "type".
    // Note that the senond "type" is the variable that is passed to the function
    rabbit.type = type;

    // returns the newly created object
    return rabbit;
}
```

만약 여러분이 객체 지향 프로그래밍의 개념을 따르는 다른 프로그래밍 언어에 익숙하다면, 여러분은 이것이 클래스와 생성자 함수를 정의하는 매우 어색한 방법이라는 것을 알게 될 것이다. 하지만 이것이 생성자 함수가 무엇인지 이해하는 데 도움이 된다고 생각한다. 운 좋게도, 2015년 이후, JavaScript는 키워드 `class`를 사용하여 새롭고 더 표준적인 클래스를 만드는 방법을 제공했다.

```js
let Rabbit = class Rabbit {
  constructor(type) {
    this.type = type;
  }
  speak(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
  }
};
```

이 클래스의 인스턴스를 만들려면 키워드 `new`를 사용한다.

```js
let killerRabbit = new Rabbit("killer");
let blackRabbit = new Rabbit("black");
```

이 코드를 실행하면 클래스에서 정의한 `constructor()` 함수가 자동으로 실행된다.

### Getter, setter와 statics
이제 객체 지향 프로그래밍의 인터페이스 부분에 초점을 맞추겠다. 인터페이스는 외부에서 "보이는" 객체의 일부이다. 프로그래머는 인터페이스를 사용하여 복잡한 문제를 해결하기 위해 서로 다른 코드 조각이 함께 작동하도록 한다.

일반적으로 이러한 인터페이스 방법에는 getter와 setter의 두 가지 타입이 있다. getter는 객체에서 정보를 검색하고, setter는 객체에 정보를 기록한다. 이 온도 변환기의 예를 고려해 보겠다.

```js
class Temperature {
  constructor(celsius) {
    this.celsius = celsius;
  }
  get fahrenheit() {
    return this.celsius * 1.8 + 32;
  }
  set fahrenheit(value) {
    this.celsius = (value - 32) / 1.8;
  }

  static fromFahrenheit(value) {
    return new Temperature((value - 32) / 1.8);
  }
}

let temp = new Temperature(22);
```

이 예에는 `static` 메서드가 있다. 정적은 인터페이스의 일부가 아니라 프로토타입 대신 생성자 기능에 추가 속성을 첨부하는 역할을 담당한다. 이 예에서는 화씨를 사용하여 클래스 인스턴스를 만드는 다른 방법을 제공하는 데 사용되었다.

### 상속
JavaScript는 또한 일부 속성에 대한 새로운 정의를 사용하여 다른 클래스를 기반으로 클래스를 만드는 쉬운 방법을 제공한다. 예를 들어, 다음 클래스는 행렬을 정의한다. 행렬은 2차원 배열이다.

```js
class Matrix {
  constructor(width, height, element = (x, y) => undefined) {
    this.width = width;
    this.height = height;
    this.content = [];

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        this.content[y * width + x] = element(x, y);
      }
    }
  }

  get(x, y) {
    return this.content[y * this.width + x];
  }
  set(x, y, value) {
    this.content[y * this.width + x] = value;
  }
}
```

대칭 행렬(symmetric matrix)이라고 하는 다른 타입의 행렬이 있다. 그것은 대각선을 따라 대칭이라는 점을 제외하고 정규 행렬의 모든 특성을 가지고 있다. 이러한 행렬을 만들고 동일한 코드를 다시 쓰는 것을 방지하기 위해 다음과 같이 행렬 클래스를 확장하여 대칭 행렬을 정의할 수 있다.

```js
class SymmetricMatrix extends Matrix {
  constructor(size, element = (x, y) => undefined) {
    super(size, size, (x, y) => {
      if (x < y) return element(y, x);
      else return element(x, y);
    });
  }

  set(x, y, value) {
    super.set(x, y, value);
    if (x != y) {
      super.set(y, x, value);
    }
  }
}

let matrix = new SymmetricMatrix(5, (x, y) => `${x},${y}`);
console.log(matrix.get(2, 3)); // → 3,2
```

**주** 이 포스팅은 [JavaScript Basics #2](https://www.ericsdevblog.com/posts/javascript-basics-2/)를 편역한 것임.
