## PART I

> *`map()`, `reduct()`, `filter()`, `zip()` 및 `enumerate()`를 사용하여 코드를 간소화한다.*

Python은 다양한 작업을 수행하는 데 도움이 되는 여러 내장 함수와 라이브러리를 제공하는 다목적 프로그래밍 언어이다. 이 포스팅에서는 코드를 간소화하고 코드를 보다 효율적으로 만들 수 있는 고급 함수에 대해 알아보겠다. 입력 함수와 이를 iterable의 각 요소에 적용하여 그 결과를 새 iterable로 반환하는 `map()` 함수부터 iterable한 값에 카운터를 추가하고 튜플의 iterable한 값을 반환하는 `enumerate()` 함수까지, 이러한 함수들은 보다 명확하고 간결한 코드를 작성하는 데 도움을 준다. 이러한 고급 Python 함수를 자세히 살펴보고 코드에서 사용하는 방법에 대해 설명한다.

### `map()`
`map()` 함수는 지정된 함수를 리스트, 튜플 또는 문자열과 같은 압력 iterable의 각 요소에 적용하여 함수 호출 결과로 반환된 요소들로 새 iterable을 반환한다.

예를 들어 `map()`을 사용하여 `len()` 함수를 문자열 리스트에 적용하면 각 문자열의 길이 리스트를 반한한다.

```python
def get_lengths(words):
    return map(len, words)

words = ['cat', 'window', 'defenestrate']
lengths = get_lengths(words)
print(lengths)  # [3, 6, 12]
```

### `reduce()`
`reduct()` 함수는 Python의 functools 모듈의 일부이며 요소의 시퀀스를 단일 값으로 줄이기 위해 시퀀스 요소들에 함수를 적용한다.

예를 들어, `reduce()`를 사용하여 리스트의 모든 요소를 곱할 수 있다.

```python
from functools import reduce

def multiply(x, y):
    return x * y

a = [1, 2, 3, 4]
result = reduce(multiply, a)
print(result)  # 24
```

세 번째 인수로 초기 값을 지정하여 `reduce()`를 호출할 수도 있다. 이 경우 초기 값은 함수의 첫 번째 인수로 사용되고 iterable한 요소의 첫 번째 요소는 두 번째 인수로 사용된다.

예를 들어, `reduce()`를 사용하여 숫자 리스트의 합을 계산할 수 있다.

```python
from functools import reduce

def add(x, y):
    return x + y

a = [1, 2, 3, 4]
result = reduce(add, a, 0)
print(result)  # 10
```

### `filter()`
`filter()` 함수는 주어진 조건을 만족하지 않는 요소를 제거하여 반복 가능한 객체를 생성하도록 필터링한다. 조건을 만족하는 요소만을 포함하는 새 iterable을 반환한다.

예를 들어 `filter()`를 사용하여 리스트에서 짝수를 모두 제거할 수 있다.

```python
def is_odd(x):
    return x % 2 == 1

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
odd_numbers = filter(is_odd, a)
print(odd_numbers)  # [1, 3, 5, 7, 9]
```

lambda 함수를 `filter()`를 첫 번째 인수로 사용할 수도 있다.

```python
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
odd_numbers = filter(lambda x: x % 2 == 1, a)
print(odd_numbers)  # [1, 3, 5, 7, 9]
```

### `zip()`
`zip()` 함수는 여러 iterable의 한 항목을 iterable인 한 튜플로 결합한다. `zip()` 함수는 가장 짧은 iterable의 요소가 모두 사용되면 중지한다.

예를 들어 `zip()`을 사용하여 두 리스를 튜플 리스트로 결합할 수 있다.

```python
a = [1, 2, 3]
b = ['a', 'b', 'c']
zipped = zip(a, b)
print(zipped)  # [(1, 'a'), (2, 'b'), (3, 'c')]
```

`zip()`을 두 개 이상의 iterable과 함께 사용할 수도 있다.

```python
a = [1, 2, 3]
b = ['a', 'b', 'c']
c = [True, False, True]
zipped = zip(a, b, c)
print(zipped)  # [(1, 'a', True), (2, 'b', False), (3, 'c', True)]
```

함수 호출에서 `*` 연산자 또는 리스트 컴프리헨션을 사용하여  `zip()` 객체의 튜플을 unzip할 수 있다.

```python
def print_tuples(a, b, c):
    print(f'a: {a}, b: {b}, c: {c}')

a = [1, 2, 3]
b = ['a', 'b', 'c']
c = [True, False, True]
zipped = zip(a, b, c)

# Unpack the tuples in a function call
for t in zipped:
    print_tuples(*t)

# Unpack the tuples in a list comprehension
unzipped = [print_tuples(*t) for t in zipped]
```

### `enumerate()`
`enumerate()` 함수는 iterable에 카운터를 추가하여 iterable인 튜플을 반환한다. 이때 각 튜플은 카운터와 원래 요소로 구성된다.

예를 들어 `enumerate()`를 사용하여 리스트을 루핑하면 각 요소의 인덱스와 값을 인쇄할 수 있다.

```python
a = ['a', 'b', 'c']
for i, element in enumerate(a):
    print(f'{i}: {element}')

# Output:
# 0: a
# 1: b
# 2: c
```

카운터의 시작 값을 `enumerate()`의 두 번째 인수로 지정할 수 있다.

```python
a = ['a', 'b', 'c']
for i, element in enumerate(a, 1):
    print(f'{i}: {element}')

# Output:
# 1: a
# 2: b
# 3: c
```

### PART I을 마치며
코딩에 유용하게 사용할 수 있는 고급 Python 함수를 다루었습니다. `map()`, `reduct()`, `filter()`, `zip()` 및 `enumerate()` 함수를 살펴보았고 사용 방법에 대한 예를 제공했다. 이러한 함수를 사용하여 보다 효율적이고 간결한 코드를 작성할 수 있으며 더 자세히 살펴볼 가치가 있다.

## PART II
> *고급 Python 함수 활용: `itertools`, `functools`, `operator` 등을 사용하여 코딩 능력을 향상시키는 방법을 알아보자*

PART II에서는 Python 표준 라이브러리에 대해 자세히 알아보고 여러분의 코딩 능력을 크게 향상시킬 수 있는 몇 가지 고급 함수에 대해 알아본다. 이 함수는 PART I에서 다룬 함수만큼 널리 알려져 있지 않지만 강력하고 유용할 수 있다.

`itertools` 모듈은 `groupby()`라는 함수을 제공하여 키 함수를 기준으로 iterable을 그룹화할 수 있다. 이 함수는 데이터 분석과 조작 작업에 매우 유용하다. `functools` 모듈은 `partial()`이라는 함수를 제공하므로 일부 인수를 미리 채워 새 함수를 만들 수 있다. 이 함수는 재사용 가능한 코드를 생성하고 복잡한 함수 호출을 단순화하는 데 매우 유용하다. `operator` 모듈은 각각 객체의 속성 또는 항목에 쉽게 액세스할 수 있도록 하는 `attrgetter()`와 `itemgetter()`라는 두 가지 함수를 제공한다. 이러한 함수는 속성 또는 항목을 기준으로 객체 리스트를 정렬하는 데 매우 유용하다.

PART II에서는 이러한 함수를 사용하는 방법과 실제 문제를 해결하기 위해 적용하는 방법에 대한 실제 사례를 살펴볼 것이다. 이 함수는 `map()`, `reduct()`, `filter()`, `zip()` 및 `enumerate()`만큼 자주 사용되지 않지만 여전히 매우 강력하고 유용하다. 이러한 도구에 익숙해짐으로써 코딩 능력을 크게 향상시키고 코드를 보다 효율적이고, 읽을 수 있으며, 재사용 가능하게 만들 수 있다.

### `itertools.groupby()`
`itertools` 모듈의 `groupby()` 함수를 사용하면 키 함수를 기준으로 항목을 반복해서 그룹화할 수 있다. 이것은 데이터 분석 및 조작 작업에 매우 유용할 수 있다. 함수에는 iterable 인수와 각 요소의 키를 결정하는 함수 두 인수가 사용된다. 함수는 쌍(키, 그룹)을 생성하는 iterator를 반환한다. 이때 각 그룹은 동일한 키를 가진 요소의 시퀀스이다. 다음은 이 도구를 사용하여 단어 목록을 첫 글자별로 그룹화하는 방법의 예이다.

```python
from itertools import groupby

words = ['apple', 'banana', 'cherry', 'date', 'elderberry', 'fig']

for first_letter, group in groupby(sorted(words), key=lambda x: x[0]):
    print(first_letter, list(group))
```

출력은 다음과 같다.

```python
a ['apple']
b ['banana']
c ['cherry']
d ['date']
e ['elderberry']
f ['fig']
```

위와 같이, 함수는 단어를 첫 글자별로 그룹화했다. 단어 목록을 정렬하는 것으로 시작하여 각 요소에 키 기능을 적용하여 단어의 첫 글자인 키를 결정하고 결과에 대해 반복하여 각 키에 대해 관련 요소 그룹을 반환한다.

### `functools.partial()`
`functools` 모듈의 `partial()` 함수를 사용하면 일부 인수가 미리 채워진 새 함수를 만들 수 있다. 이 함수는 재사용 가능한 코드를 생성하고 복잡한 함수 호출을 단순화하는 데 매우 유용하다. 함수는 함수와 임의의 수의 인수 및 키워드 인수를 사용하며, 호출될 때 원래 함수를 주어진 인수 및 키워드 인수에 적용하고 호출 가능한 객체에 전달된 추가 인수 및 키워드 인수에 적용하는 새 호출 가능한 객체를 반환한다. 다음은 이 기능을 사용하여 숫자에 10을 곱하는 새 함수를 만드는 방법의 예이다.

```python
from functools import partial

def multiply(x, y):
    return x * y

times_10 = partial(multiply, 10)

print(times_10(5))  # 50
```

위의 예에서는 두 개의 인수를 사용하여 그 곱을 반환하는 함수 `multiply()`을 정의한다. 그런 다음 `partial()` 함수를 사용하여 첫 번째 인수가 10으로 고정된 multiply 함수에 해당하는 새 함수 `times_10`을 만든다. 따라서 `times_10(5)`를 호출은 50을 반환하는 `multiplyu(10,5)`을 호출하는 것과 같다.

### `operator.attrgetter()`와 `operator.itemgetter()`
`operator` 모듈의 `attrgetter()`와 `itemgetter()` 함수를 사용하면 객체의 속성 또는 항목에 쉽게 액세스할 수 있다. 속성 또는 항목을 기준으로 객체 리스트를 정렬하는 데 매우 유용하다. `attrgetter()` 함수를 호출할 때 호출 가능한 값을 반환하며, 입력 객체의 주어진 속성 값을 반환한다. `itemgetter()` 함수함수를 호출할 때 호출 가능한 값을 반환하며, 입력 객체의 주어진 항목의 값을 반환한다. 다음은 `attrgetter()`를 사용하여 객체의 이름 속성별로 리스트을 정렬하는 방법의 예이다.

```python
import operator

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

people = [Person('Bob', 30), Person('Charlie', 25), Person('Alice', 35)]

sorted_people = sorted(people, key=operator.attrgetter('name'))

for person in sorted_people:
    print(person.name)
```

출력은 다음과 같다.

```
Alice
Bob
Charlie
```

여기서는 `name`과 `age`를 두 가지 속성을 갖는 `Person` 클래스를 정의한 다음 세 `Person` 객체 리스트를 만들었다. 그런 다음 정렬 함수를 사용하여 `name` 속성을 키로 `people` 리스트을 정렬한다. 각 `Person` 객체의 이름 속성을 가져와서 정렬 키로 사용하기 위해 `attrgetter()` 함수를 사용한다.

다음은 `itemgetter()`를 사용하여 튜플 리스트를 튜플의 두 번째 요소를 키로 정렬하는 방법의 예이다.

```python
data = [(1, 'a'), (3, 'b'), (2, 'c')]

sorted_data = sorted(data, key=operator.itemgetter(1))

for item in sorted_data:
    print(item)
```

출력은 다음과 같다.

```
(1, 'a')
(3, 'b')
(2, 'c')
```

이 예에서 `itemgetter()` 함수를 사용하여 각 튜플의 두 번째 항목을 가져와 정렬 키로 사용한다.

결론적으로 Python 표준 라이브러리는 코딩 능력을 크게 향상시킬 수 있는 광범위한 고급 기능을 제공한다. 여기에서 다룬 `itertools.groupby()`, `functools.partial()`, `operator.attrigter()`와 `operator.itemgetter()` 같은 함수는 PART I에서 다룬 함수만큼 잘 알려져 있지 않지만 강력하고 유용할 수 있다. 이러한 함수를 이해하고 활용하면 보다 효율적이고 읽기 쉽고 재사용 가능한 코드를 만들 수 있다.

위에서 설명한 것이 고급 Python 함수의 전체 목록이 아니며 Python 표준 라이브러리에서 더 강력한 함수를 사용할 수 있다. Python 개발자로서, 여러분의 기술과 능력을 향상시키기 위하여 이러한 기능들을 계속 배우고 탐구하는 것이 필수적이다. 그러면 코드를 더 효율적으로 작성하고 더 복잡한 문제를 해결할 수 있다.

게다가, 좋은 코드는 고급 함수를 사용하는 것뿐만 아니라 깨끗하고 읽기 쉽고 잘 문서화된 코드를 작성하고 좋은 코딩 관행을 따르는 것이라는 것을 명심하여야 한다.

**(주)** 위 내용은 [Advanced Python Functions](https://medium.com/@etirismagazine/advanced-python-functions-841d4ce0a91a)과 [Advanced python functions – part two](https://medium.com/@etirismagazine/advanced-puthon-functions-part-two-c0a026a3d7da)을 편역한 것이다.
