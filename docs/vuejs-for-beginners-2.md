이전에 Vue에서 몇 가지 기본 개념에 대해 다루었으며, 이 포스팅<sup>[1](#footnote_1)</sup>에서는 JavaScript 프레임워크 및 이벤트 처리, 데이터 옵션과 구성 요소에 대해 자세히 알아본다.

## 이벤트 처리(event handling)
[JavaScript 기초 3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-3/) 통해 이벤트 처리가 프런트엔드 개발에서 가장 중요한 개념이며, JavaScript 프론트엔드 프레임워크인 Vue.js에도 동일한 개념이 내장되어 있어야 한다는 것을 배웠다.

이 포스팅에서는 지시어 `v-on`을 사용한 이벤트 처리와 지시어 `v-model`을 사용한 폼 입력 처리의 두 가지 측면에 중점을 둘 것이다. Vue.js의 스크립트 섹션에 대해 논의하기 전에 스타일 바인딩과 클래스 바인딩을 빠르게 살펴보겠다.

이벤트는 사용자 입력이며, 키보드 입력 또는 마우스 클릭일 수 있으며, 이벤트가 발생한 후 사용자는 일반적으로 응답을 기대한다. 이벤트 처리기는 해당 이벤트를 수신하고 백그라운드에서 일부 작업을 수행하여 응답으로 반환한다. 이벤트가 무엇인지 [JavaScript 기초 3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-3/)에 자세한 설명이 있다.

`v-on` 지시어는 Vue.js의 이벤트를 수신하는 데 사용된다. 이 명령어는 `@` 기호로 단축할 수 있다. 이를 사용하여 청취 중인 이벤트의 종류와 이 이벤트가 수신된 후 어떤 조치를 취할 것인지 지정할 수 있다.

```html
<div v-on:click="someAction">...</div>
<div @click="someAction">...</div>
```

`someAction`은 단순한 JavaScript 표현식이거나 매우 복잡한 메서드일 수 있으며, 이를 통해 보다 복잡한 논리를 구축할 수 있다.

```js
<div v-on:click="count = count + 1">...</div>
<div v-on:click="someMethod()">...</div>
```

때때로, 메소드는 몇 가지 추가 인수를 전달를 요구한다.

```js
<script>
export default {
  ...
  methods: {
    add(num) {
      this.count = this.count + num
    }
  }
}
</script>

<template>
  <p>count = {{count}}</p>
  <button v-on:click="add(1)">Add 1</button>
  <button v-on:click="add(5)">Add 5</button>
  <button v-on:click="add(10)">Add 10</button>
  <button v-on:click="add(100)">Add 100</button>
</template>
```

하나의 이벤트가 여러 이벤트 처리기를 트리거할 수도 있으며 쉼표를 사용하여 핸들러를 구분할 수 있다. 예를 들어, 이번에는 버튼을 클릭하면 브라우저가 경고 상자를 팝업하고 웹 페이지를 다시 렌더링한다.

```js
<script>
export default {
  data() {...},

  methods: {
    ...
    say() {
      var msg = 'count = ' + this.count
      alert(msg)
    }
  }
}
</script>

<template>
  <p>count = {{count}}</p>
  <button v-on:click="add(1), say()">Add 1</button>
  ...
</template>
```

### 수식어(modifier)
수식어는 이벤트에 대한 추가 세부 정보를 전달하는 데 사용된다. 예를 들어 `.once` 수식어를 사용하여 Vue에 이 이벤트가 한 번만 트리거됨을 알릴 수 있다.

```html
<template>
  <p>count = {{count}}</p>
  <button v-on:click.once="add(1)">Add 1</button>
</template>
```

이번에는 "Add 1" 버튼이 한 번만 작동한다.

이벤트의 기본 작업을 중지하는 `.prevent`같은 다른 수식어도 있다. 또는 `.stop` 이벤트는 전파를 중지한다. 만약 당신이 그것에 대하여 자세히 알고 싶다면, [JavaScript 기초 3](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/javascript-basics-3/)를 참조하시오.

```html
<!-- the click event's propagation will be stopped -->
<a @click.stop="doThis"></a>

<!-- the submit event will no longer reload the page -->
<form @submit.prevent="onSubmit"></form>

<!-- modifiers can be chained -->
<a @click.stop.prevent="doThat"></a>
```

이벤트 처리기가 특정 키나 마우스 버튼 또는 다음과 같은 조합의 이벤트를 청취하도록 하는 다른 타입의 수식어도 있다.

```html
<template>
  <!-- Right Click -->
  <div v-on:click.right="doSomething">Do something</div>

  <!-- Control + Click -->
  <div v-on:click.ctrl="doSomething">Do something</div>

  <!-- Enter Key -->
  <div v-on:keyup.enter="doSomething">Do something</div>

  <!-- Alt + Enter -->
  <div v-on:keyup.alt.enter="doSomething">Do something</div>
</template>
```

## 양식 입력 바인딩(form input binding)
양식은 웹 개발에서 매우 중요한 구성 요소이며, 사용자가 백엔드와 통신할 수 있는 수단을 제공한다. 그러나 [HTML 기초 2 - 더 깊이 파기](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/html-basics-2/)를 통해 양식에는 다양한 터입의 입력이 있을 수 있으며 각 입력은 다른 데이터 타입과 연관되어 있다는 것을 알 수 있다. 개발자가 모든 데이터 타입을 하나씩 처리하려고 하면 골치 아플 것이다.

다행히 Vue.js를 사용하면 데이터 타입에 관계없이 하나의 수식어 `v-model`을 사용하여 모든 입력 데이터를 바인딩할 수 있다. 예를 들어, 아래에 표준 텍스트 입력이 있다.

```js
<input v-model="message" />
<p>Message is: {{ message }}</p>
```

여기서 사용자 입력은 문자열 타입이며, 가변 길이의 메시지에 바인딩된다.

여러 줄 텍스트 입력도 정확히 동일하게 작동한다.

```js
<textarea v-model="message"></textarea>
<p>Message is: {{ message }}</p>
```

### `checkbox`

```js
<script>
export default {
  data() {
    return {
      checked: false
    }
  }
}
</script>

<template>
  <input type="checkbox" v-model="checked" />
  <p v-if="checked">The box is checked.</p>
  <p v-else>The box is NOT checked.</p>
</template>
```

checkbox의 경우 사용자 입력은 `true` 또는 `false`의 부울 값이다. 위의 예에서 사용자 입력은 `checked` 변수에 바인딩되며 `v-if` 지시어은 `checked` 변수의 값을 확인하는 데 사용된다.

그러나 타입에 따라 여러 개의 checkbox가 있을 수 있으며, 이는 두 값(`True` 또는 `False`)만 있으면 충분하지 않다는 것을 의미한다. 이 경우 각 checkbox에 `value` 애트리뷰트를 추가해야 한다.

```js
<script>
export default {
  data() {
    return {
      checkedBoxes: []
    }
  }
}
</script>

<template>
  <div id="v-model-multiple-checkboxes">
    <input type="checkbox" id="one" value="one" v-model="checkedBoxes" />
    <label for="one">one</label>
    <input type="checkbox" id="two" value="two" v-model="checkedBoxes" />
    <label for="two">two</label>
    <input type="checkbox" id="mike" value="three" v-model="checkedBoxes" />
    <label for="three">three</label>
    <br />
    <span>Checked boxes: {{ checkedBoxes }}</span>
  </div>
</template>
```

이번에는 `checkedBoxes` 변수가 배열에 바인딩되고 checkbox를 선택하면 해당 값(`value` 애트리뷰트에 할당한 값)이 해당 배열에 추가된다.

### `radio`
radio는 하나의 옵션만 선택할 수 있다는 점을 제외하고는 다중 checkbox 그룹과 비슷하다. 따라서 이 경우 사용자 입력은 항상 한 문자열이다.

```js
<div id="v-model-radiobutton">
  <input type="radio" id="one" value="One" v-model="picked" />
  <label for="one">One</label>
  <br />
  <input type="radio" id="two" value="Two" v-model="picked" />
  <label for="two">Two</label>
  <br />
  <span>Picked: {{ picked }}</span>
</div>
```

`picked` 변수는 배열 대신 문자열이 된다.

### `select`
단일 select의 경우 변수는 문자열 타입이다.

```js
<script>
export default {
  data() {
    return {
      selected: ''
    }
  }
}
</script>

<template>
  <select v-model="selected">
    <option disabled value>Please select one</option>
    <!--
      If you assign a 'value' attribute, that value will be assigned to the variable 'selected'
    -->
    <option value="aaaaaaa">A</option>
    <!--
      If you do not assign a value attribute, whatever is inside the <option> element
      will be assigned to the variable 'selected'
    -->
    <option>B</option>
    <option>C</option>
  </select>
  <span>Selected: {{ selected }}</span>
</template>

<style>
</style>
```

multiselect의 경우 변수가 배열에 바인딩된다.

```js
<script>
export default {
  data() {
    return {
      selected: []
    }
  }
}
</script>

<template>
  <select v-model="selected" multiple>
    <option>A</option>
    <option>B</option>
    <option>C</option>
  </select>
  <span>Selected: {{ selected }}</span>
</template>
```

## 스타일 바인딩(style binding)

### 클래스 바인딩
[CSS 기초 1 - 시작하기](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-1/)를 통해, 클래스가 같은 CSS 코드를 다른 HTML 요소에 할당할 수 있는 방법이라는 것을 알고, 클래스 이름을 변경함으로써, 그 요소와 연관된 CSS 코드를 쉽게 변경할 수 있다.

다음과 같이 Vue.js에서 HTML 요소의 클래스 이름을 동적으로 변경할 수 있다.

```js
<div v-bind:class="{ active: isActive }"></div>
```

위 예에서 `active`는 클래스 이름이고 `isActive`는 부울 변수이다. `isActive`가 `true`이면 클래스 이름 `active`가 렌더링된다.

여기에 여러 클래스 이름을 사용할 수 있다.

```js
<div v-bind:class="{ class-one: isClassOneActive, class-two: isClassTwoActive }"></div>
```

### CSS 바인딩
다음과 같이 CSS 코드를 직접 바인딩할 수 있다.

```js
<script>
export default {
  data() {
    return {
      activeColor: 'red',
      fontSize: 30
    }
  }
}
</script>

<template>
  <div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
</template>
```

일반적으로 템플릿 섹션이 더 깨끗해 보이도록 data() 메서드 안에 객체를 넣는 것이 좋다.

```js
<script>
export default {
  data() {
    return {
      styleObject: {
        color: 'red',
        fontSize: '13px'
      }
    }
  }
}
</script>

<template>
  <div v-bind:style="styleObject"></div>
</template>
```

이제 드디어 가장 중요한 부분인 Vue 애플리케이션의 스크립트 섹션에 대해 살펴보겠다. 웹 프레임워크를 마스터하기 위해 가장 중요한 단계는 데이터가 프로젝트 내에서 어떻게 순환할 수 있는지, 그리고 다양한 타입의 데이터가 어떻게 다르게 취급되는지 이해하는 것이다. 그것이 이 포스팅의 초점이 될 것이다.

이 포스팅에서는 여러 가지 타입의 데이터 옵션에 대해 설명한다. 이전에 설명했던 `data` 메서드와 데이터 옵션을 혼동하지 마시오. `data` 메서드는 구성 요소 인스턴스에서 사용할 변수 목록을 선언하는 메서드이며, 데이터 옵션은 `data` 메서드를 포함하는 Vue.js의 데이터를 처리하는 속성과 메서드 모음이다.

그런 다음 구성 요소 인스턴스 생성의 다른 단계에서 코드를 주입할 수 있는 인터페이스인 라이프사이클 후크에 대해 설명한다.

## 데이터 옵션(data options)

### `data`
먼저, `data` 메서드이다. 이미 반복해서 보았듯이, 이는 객체를 반환하는 방법이고, 그 객체 안에서 구성 요소 인스턴스에 필요한 모든 변수를 정의한다. Vue.js는 이러한 변수를 반응형 시스템 내부에 자동으로 래핑한다. 즉, 변수의 값이 변경되면 웹 페이지가 자동으로 반환되어 변경 사항을 반영한다.

변수는 인스턴스가 생성될 때만 추가된다. 실제로 인스턴스가 이미 생성된 후에 변수를 할당할 수 있지만 해당 변수는 반응형 시스템의 일부가 아니다. 따라서 항상 `data` 메서드 내에 생성해야 한다. 초기 값이 없으면 `null` 또는 `undefined`처럼 자리 표시자 값을 사용할 수 있다.

```js
<script>
export default {
  data() {
    return {
      count: 0,
      name: '',
    }
  }
}
</script>
```

### `methods`
`methods`는 이미 알고 있는 또 다른 데이터 옵션이다. 어플리케이션 프로그램에 대한 모든 논리를 정의하는 곳이다. 메서드를 만들면 Vue.js가 자동으로 `this` 키워드를 해당 메서드에 바인딩한다. 따라서 현재 인스턴스에 대한 변수 값을 액세스하려면 `this.variableName`을 사용해야 한다.

```html
<script>
export default {
  data() {
    return {
      count: 0,
    }
  },

  methods: {
    add(num) {
      this.count = this.count + num
    }
  }
}
</script>

<template>
  <p>count = {{ count }}</p>
  <button @click="add(1)">Add 1</button>
</template>
```

### `computed`
`computed` 속성은 `methods` 속성과 매우 유사하다. 또한 데이터를 다루는 방법을 저장하는 장소이기도 하다. 그러나 computed는 일반적으로 getters와 setters를 위한 것이다. getters는 변수의 값을 반환하는 메서드이고, setters는 변수에 새 값을 할당하는 메서드이다.

```html
<script>
export default {
  ...
  computed: {
    // This is a getter
    showCount() {
      return this.count
    },
    // This is a setter
    resetCount() {
      this.count = 0
    }
  }
}
</script>

<template>
  <p>count = {{ showCount }}</p>
  <button @click="add(1)">Add 1</button>
  <button @click="resetCount()">Reset</button>
</template>
```

`methods`를 사용하면 이를 수행할 수 있었던 것 같다. 그렇다면 Vue는 왜 `methods`와 `computed`을 모두 가지고 있으며 정확히 어떤 차이가 있을까? 여기서 두 접근 방식은 실제로 동일한 결과를 생성한다. 그러나 그들의 차이점은 `computed`는 캐시되는 반면 `methods`는 캐시되지 않는다는 것이다.

`computed` 메서드가 호출되면 계산이 한 번 수행되고 결과가 캐시에 저장된다. 이 방법은 종속 변수가 변경되지 않은 한 재수행되지 않는다. `methods`의 경우, 리렌더가 발생할 때마다 다시 계산을 수행한다.

계산 비용이 많이 드는 대량의 데이터를 처리하는 경우 computed를 사용하면 많은 문제를 줄일 수 있다.

### `watch`
`watch` 속성은 변수가 값을 변경할 때마다 실행할 메서드를 정의한다. 그것은 본질적으로 자신의 반응형 시스템을 사용자 정의할 수 있는 방법을 제공한다.

```js
<script>
export default {
  data() {
    return {
      count: 0,
    }
  },

  methods: {
    add(num) {
      this.count = this.count + num
    }
  },

  watch: {
    count(newCount, oldCount) {
      let diff = newCount - oldCount
      console.log('diff = ' + diff)
    }
  }
}
</script>

<template>
  <p>count = {{ count }}</p>
  <button @click="add(1)">Add 1</button>
  <button @click="add(5)">Add 5</button>
  <button @click="add(10)">Add 10</button>
  <button @click="add(100)">Add 100</button>
</template>

<style>
</style>
```

위의 예에서는 `count` 값이 변경될 때마다 페이지가 다시 렌더링될 뿐만 아니라 이전 값과 새 값의 차이를 알려주는 메시지도 콘솔에 출력한다. 메서드 이름과 변수 이름이 일치해야 한다.

이것이 전부가 아니다. 사실 세 가지 다른 데이터 옵션이 있다. `props`, `emit`, `expose` 이다. 그러나 이러한 데이터 옵션을 이해하려면 먼저 Vue.js의 구성 요소 시스템을 더 깊이 파고들 필요가 있다. 다음 포스팅에서 그것들에 대해 알아 볼 것이다.

## 생명 주기 후크(lifecycle hooks)
![lifecycle-hooks](images/vuejs-for-beginners/lifecycle_hooks.png)

본 포스팅의 마지막 부분에서 Vue.js의 구성 요소 시스템에 대해 알아보겠다. 다음은 구성 요소의 한 예이다.

### `components/ComponentOne.vue`

```js
<script>
export default {
    ...
}
</script>

<template>
    <p>This is the component "ComponentOne.vue"</p>
</template>
```

`App.vue` 파일 내에서 이 구성 요소를 사용할 수 있다.

```html
<script>
// import the component
import ComponentOne from "./components/ComponentOne.vue"

export default {
    ...
    // Declare the imported component
    components: { ComponentOne }
}
</script>

<template>
  <p>This is the root component "App.vue"</p>

  <!-- Use the component here -->
  <ComponentOne></ComponentOne>
</template>
```

구성요소는 재사용 가능하므로 한 페이지에 동일한 구성요소의 여러 인스턴스를 만들 수 있다. 그리고 그들은 모두 서로 독립적이다. 한 인스턴스의 상태가 바뀌어도 다른 인스턴스에는 영향을 주지 않는다.

### `components/ComponentOne.Vue`

```html
<script>
export default {
    data() {
        return {
            count: 0
        };
    },
    methods: {
        add(num) {
            this.count += num
        }
    }
}
</script>

<template>
    <p>This is the component "ComponentOne.vue"</p>
    <p>count = {{count}}</p>
    <button @click="add(1)">Add 1</button>
</template>
```

### `App.vue`

```js
<template>
  <p>This is the root component "App.vue"</p>


  <!-- Use the multiple component instances -->
  <ComponentOne></ComponentOne>
  <ComponentOne></ComponentOne>
  ...
</template>
```

또한 구성요소 내부의에 다른 구성요소를 가져와 중첩된 구조를 형성할 수 있다.

### `components/ComponentOne.vue`

```html
<script>
import ComponentTwo from "./ComponentTwo.vue";


export default {
    ...
    components: { ComponentTwo }
}
</script>

<template>
    <p>This is the component "ComponentOne.vue"</p>

    <!-- Import another component -->
    <ComponentTwo></ComponentTwo>

</template>
```

### `components/ComponentTwo.vue`

```js
<script>
export default {
    ...
}
</script>

<template>
    <p>This is the component of a component "ComponentTwo.vue"</p>
</template>
```

## 구성 요소 구성(organizing components)
실제 어플리케이션 프로그램에서 다음과 같은 중첩 구조를 갖는 것은 매우 일반적이다.

![organizing-vue-components](images/vuejs-for-beginners/organizing-vue-components.webp)

예를 들어, 여기서는 블로그의 프런트 엔드를 구축하려고 하는데, 최근 기사 목록이 필요하다. 실제 어플리케이션에서 데이터는 일반적으로 데이터베이스 내에 저장되며, 정확한 정보를 검색하여 프런트 엔드로 전송하는 백엔드가 있다. 현재로서는 완벽하게 작동하는 백엔드가 있고 데이터가 이미 전송되었다고 가정한다.

### `App.vue`

```html
<script>
import PostListComponent from "./components/PostListComponent.vue";

export default {
  ...
  components: { PostListComponent },
};
</script>

<template>
  <PostListComponent></PostListComponent>
</template>
```

### `components/PostListComponent.vue`

```html
<script>
import PostComponent from "./PostComponent.vue";

export default {
  data() {
    return {
      posts: [
        { id: 1, title: "Article #1" },
        { id: 2, title: "Article #2" },
        ...
      ],
    };
  },
  components: { PostComponent },
};
</script>

<template>
  <h1>This is a list of recent articles.</h1>

  <PostComponent v-for="post in posts"></PostComponent>
</template>
```

### `components/PostComponent.vue`

```html
<script>
export default {
  ...
}
</script>

<template>
  <h2>This is the title.</h2>
</template>
```

보다시피, 하나의 PostComponent.vue만 가지고 있다. v-for 루프를 사용하여 그것을 여러 번 재사용한다. 이렇게 하면 동일한 코드를 반복해서 쓸 필요가 없기 때문에 많은 수고를 덜 수 있다.

## 하위로 데이터 전달(passing data to child)
이제 새로운 문제에 직면하게 되었습니다. 기본적으로 구성 요소 인스턴스는 서로 격리되어 있으며, 한 인스턴스의 데이터 변경은 다른 인스턴스의 데이터에 액세스할 수 없기 때문에 다른 인스턴스의 데이터에 영향을 미치지 않는다. 하지만, 만약 그런 것이 필요하다면 어떨까?

예를 들어, 이전 예에서는 게시물에 대한 데이터가 상위 구성 요소(`PostListComponent.vue`)에 있고 하위 구성 요소(`PostComponent.vue`)를 액세스할 수 없기 때문에 기사 제목이 되어야 하는 위치에 자리 표시자 텍스트를 대신 사용해야 했다. 어떻게든 부모로부터 아이에게 데이터를 전달할 필요가 있다.

이 작업은 `props` 옵션을 사용하여 이룰 수 있다.

### `components/PostListComponent.vue`

```js
<template>
  <h1>This is a list of recent articles.</h1>
  <PostComponent v-for="post in posts" v-bind:title="post.title"></PostComponent>
</template>
```

### `components/PostComponent.vue`

```html
<script>
export default {
  props: ["title"],
};
</script>

<template>
  <h2>{{ title }}</h2>
</template>
```

이 예에서 데이터 흐름에 대해 자세히 살펴보겠다. 먼저 게시물 제목을 변수 제목에 바인딩하고 해당 변수를 `PostComponent`에 전달한다. `PostComponent`는 요소 특성이 포함된 변수 `title`을 받은 다음 템플릿에서 사용한다.

또한 배열 대신 객체 구문을 사용하여 하위 구성 요소에서 전송된 데이터의 유효성을 검사할 수 있다.

### `components/PostComponent.vue`

```html
<script>
export default {
  props: {
    // type check
    height: Number,
    // type check plus other validations
    age: {
      type: Number,
      default: 0,
      required: true,
      validator: (value) => {
        return value >= 0;
      },
    },
  },
};
</script>
```

그러나 어떤 구문을 사용하든 상관없이 이 데이터 흐름은 한 가지 방법일 뿐이다. 부모가 바뀌면 자녀가 바뀌지만 그 반대는 아니다. 하위 구성 요소에서 `props`을 업데이트하려고 하면 안 된다. 대신 자식에서 새 변수를 선언하고 전송된 `props`을 초기 값으로 사용하는 것이 가장 좋다.

### `components/PostComponent.vue`

```html
<script>
export default {
  props: ["title"],
  data() {
    return {
      articleTitle: this.title,
    };
  },
};
</script>
```

## 이벤트를 부모에게 전달(passing event to parent)
웹 어플리케이션 프로그램을 만들 때 하위 구성 요소에서 상위 구성 요소로 통신해야 하는 경우가 있다. 예를 들어, 게시 목록 예로 돌아가 보자. 이번에는 게시 구성 요소에 버튼을 추가하고 사용자가 버튼을 클릭할 때마다 해당 버튼이 전체 페이지에 대한 글꼴을 확대된다.

이렇게 하려면 하위 구성 요소에서 발생하는 클릭 이벤트를 상위 구성 요소로 전송해야 하며, 상위 구성 요소가 해당 이벤트를 포착하면 해당 변수(글꼴 크기를 저장하는 변수)의 값이 변경된다. 이 작업은 `emits` 속성을 사용하여 수행할 수 있다.

### `components/PostComponent.vue`

```html
<script>
export default {
  props: ["title"],

  // Declare the emited events
  emits: ["enlargeText"],
};
</script>

<template>
  <h2>{{ title }}</h2>
  <!-- When the button is clicked, it emits a event called 'enlargeText' to the parent -->
  <button v-on:click="$emit('enlargeText')">Enlarge Text</button>
</template>

<style></style>
```

### `components/PostListComponent.vue`

```html
<script>
import PostComponent from "./PostComponent.vue";

export default {
  data() {
    return {
      posts: [
        { id: 1, title: "Article #1" },
        { id: 2, title: "Article #2" },
        { id: 3, title: "Article #3" },
        { id: 4, title: "Article #4" },
      ],

      // Set font size
      titleFontSize: 1,
    };
  },
  components: { PostComponent },
};
</script>

<template>
  <!-- Dymanically bind the CSS style -->
  <div v-bind:style="{ fontSize: titleFontSize + 'em' }">
    <!-- listen to the event 'enlargeText' emited from the child component -->
    <PostComponent
      v-for="post in posts"
      v-bind:title="post.title"
      v-on:enlargeText="titleFontSize += 0.1"
    ></PostComponent>
  </div>
</template>

<style></style>
```

이벤트는 하위 구성 요소에서 시작되며, 버튼을 클릭하면 내장 함수 `$emit`를 사용하여 `enlargeText`라는 이벤트를 내보내고, 해당 이벤트는 `emits` 속성을 사용하여 스크립트 섹션에 선언된다. 이벤트가 상위 구성 요소에 의해 탐지되면 상위 구성 요소는 `titleFontSize` 변수 값을 변경gks다.

좀 더 복잡한 것을 시도해보고 싶다면 어떨까? 버튼 대신 텍스트 상자를 사용하여 글꼴 크기를 지정하려면 어떻게 해야 할까? 이를 위해서는 이벤트와 함께 일부 데이터를 부모에게 전송해야 한다.

### `components/PostComponent.vue`

```html
<script>
export default {
  props: ["title"],

  // Declear the emited events
  emits: ["changeFontSize"],
};
</script>

<template>
  <h2>{{ title }}</h2>
  <!--
        The attribute 'value' binds with the user input, its initisl value is 1.
        $event.target.value contains the current value of 'value'
    -->
  <input
    type="text"
    v-bind:value="1"
    v-on:change="$emit('changeFontSize', $event.target.value)"
  />
</template>
```

### `components/PostListComponent.vue`

```html
<script>
import PostComponent from "./PostComponent.vue";

export default {
  data() {
    return {
      posts: [{ id: 1, title: "Article #1" }],
      ...

      titleFontSize: 1,
    };
  },
  components: { PostComponent },
};
</script>

<template>
  <div v-bind:style="{ fontSize: titleFontSize + 'em' }">
    <!--
            listen to the event 'changeFontSize' emited from the child component,
            and the variable $event contains the data that is transferred with the event.
        -->
    <PostComponent
      v-for="post in posts"
      v-bind:title="post.title"
      v-on:changeFontSize="titleFontSize = $event"
    ></PostComponent>
  </div>
</template>
```

---
<a name="footnote_1">1</a>: 이 포스팅은 [Vue.js for Beginners #1](https://www.ericsdevblog.com/posts/vuejs-for-beginners-1/)를 편역한 것이다.
