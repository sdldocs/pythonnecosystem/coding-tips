이 포스팅에서 소개할 프로그래밍 언어는 JavaScript이다. Netscape Navigator 브라우저의 웹 페이지에 프로그램을 추가하는 방법으로 1995년에 만들어졌다. 오늘날 이 언어는 다른 모든 주요 웹 브라우저에 채택되었고, 세계에서 가장 인기 있는 프로그래밍 언어 중 하나가 되었다.

## 환경 설정
이 포스팅에서는 많은 예제 코드 스니펫을 사용할 것이다. 코드를 실행하려면 브라우저를 열어 `Developer Tools` -> `Console`를 선택한다.

![developer-tools-console](images/javascript-basics/developer-tools-console.png)

또는 JavaScript 프로그램을 실행할 수 있는 Node.js를 컴퓨터의 터미널에서 명령어를 사용하여 설치할 수 있다.

![node](images/javascript-basics/node.png)

## JavaScript 데이터 타입
컴퓨터 세계에서, 모든 것은 데이터에 관한 것이다. 컴퓨터 프로그램이 하는 일은 기본적으로 입력 데이터를 가져와서 처리한 다음 결과를 출력 데이터로 반환하는 것이다. 이 섹션에서는 JacaScript가 처리할 수 있는 몇 가지 타입의 데이터에 대해 설명한다.

### 숫지(Numbers)
숫자는 여러분이 초등학교 수학 시간에 공부했던 것과 똑같이 작동하기 때문에 가장 쉽다.

```js
// Integer
100

// Fractional Number
10.56

//Scientific Notation
3.14e5 // 3.14 * 10^5 = 314000
```

숫자의 주요 용도는 산술 연산을 수행하는 것이다.

```js
3 + 5 * 2 // -> 13
```

초등학교 때 공부했던 것처럼 곱셈과 나눗셈이 먼저 수행한다. 그러나 괄호를 사용하여 이 값을 변경할 수 있다.

```js
(3 + 5) * 2 // -> 16
```

익숙하지 않은 연산자가 하나 있는데, 바로 모듈로(`%`) 연산이다. `X % Y`는 `X`를 `Y`로 나눈 나머지를 계산한다. 예

```js
25 % 5 // -> 0

25 % 10 // -> 5

25 % 15 // -> 10
```

### 문자열
문자열은 텍스트를 나타내는 데 사용되며 모두 다음과 같이 따옴표로 묶는다.

```js
"This is a string."
'This is also a string.'
```

여는 따옴표와 닫는 따옴표가 서로 일치하는 한 단일 따옴표(`'`)와 큰따옴표(`"`)는 모두 정확히 동일하게 작동한다.

문자열 안에서 백슬래시(`\`)가 발견될 때마다 특별한 의미가 있는 문자를 의미한다. 예를 들어 백슬래시 뒤에 문자 n(`\n`)이 오면 컴퓨터에서 새 줄로 해석한다.

```js
"This is the first line\nThis is the second line"
```

출력 텍스트는 다음과 같다.

```
This is the first line
This is the second line
```

`+` 연산은 문자열에서도 사용할 수 있다. 그러나 분명히 문자열은 산술 연산에 사용될 수 없다. 여기서 더하기 기호는 연결(두 문자열을 연결)을 의미한다.

```js
"con" + "cat" + "e" + "nate" // -> "concatenate"
```

마지막으로 JavaScript에는 일반적으로 템플릿 리터럴이라고 하는 백틱 따옴표로 묶은 문자열인 특수한 종류의 문자열이 있다. 문자열 내부에 다른 값을 포함할 수 있다.

```js
`half of 100 is ${100 / 2}`
```

이 예에서 `${}` 내부의 나눗셈이 계산되고 결과가 문자열로 변환되어 해당 위치에 인쇄한다. 이 예로 다음과 같은 출력을 얻는다.

```js
half of 100 is 50
```

### 부울 값
부울 타입에는 `true`와 `false`의 두 값만 있다. 비교는 부울 값을 생성하는 가장 일반적인 방법이다.

```js
console.log(1 == 1) // -> true

console.log(1 > 2) // -> false

console.log(1 < 0) // -> false

console.log(1 != 2) // -> true
```

이 예제에서 `==`은 등치를 의미하고 `!=`은 동일하지 않음을 의미한다. 다른 유사 연산자에는 `>=`(보다 작거나 같음) 및 `<=`(보다 작거나 같음)가 있다.

JavaScript에서 부울 값에 적용할 수 있는 논리 연산자는 `&&`(과), `||`(또는), `!`(아니오) 세 가지이다.

`&&` 연산자는 논리적 `and`를 나타내며 주어진 값이 모두 `true`인 경우에만 `true`를 생성한다.

```js
console.log(true && false) // -> false

console.log(false && true) // -> false

console.log(false && false) // -> false

console.log(true && true) // -> true
```

`||` 연산자는 논리적 `or` 를 나타내며, 이는 지정된 값 중 하나가 `true`이면 `true`를 생성한다.

```js
console.log(true || false) // -> true

console.log(false || true) // -> true

console.log(false || false) // -> false

console.log(true || true) // -> true
```

`!` 연산자는 논리적 `not`을 나타내며 지정된 값이 뒤집는다.

```js
console.log(!true) // -> false

console.log(!false) // -> true
```

또한 산술 연산, 비교 연산 및 논리 연산을 혼합할 수 있다.

```js
1 + 1 == 2 && 1 + 1 < 0
```

위의 예에서, `1 + 1 == 2`는 `true`이고, `1 + 1 < 0`은 `false` 이다. 따라서 아래와 같은 결과를 갖는다.

```js
true && false // -> false
```

### 빈 값(empty value)
JavaScript에는 `null`과 `undefined` 특수한 2가지 값이 있다. 이들은 의미 있는 값이 없음을 나타낸다. 컴퓨터 프로그램에는 의미 있는 결과를 생성하지 않는 작업이 많으며(이 과정의 뒷부분에서 이를 확인할 수 있음) 그 결과는 `null` 또는 `undefined`로 표시된다.

이 두 값은 사실상 아무런 차이가 없으며, 대부분의 경우 서로 교환할 수 있는 값으로 처리할 수 있다. 같은 것을 나타내는 두 개의 다른 값이 있다는 것은 javaScript의 설계의 우연일 뿐이다.

### 데이터 터입 변환
JavaScript는 지능적인 프로그래밍 언어이며, 프로그램이 말이 되지 않더라도 항상 사용자가 제공하는 프로그램을 실행하려고 할 것이다. 예:

```js
console.log(8 * null) // -> 0

console.log("5" - 1) // -> 4

console.log("5" + 1) // -> "51"
```

첫 번째 예제에서는 `null`이 숫자 `0`으로 변환되고 두 번째 예제에서는 문자열 `"5"`가 숫자 `5`가 된다. 그러나 세 번째 예제에서는 숫자 `1`이 문자열 `"1"`로 변환되고 여기서 더하기 기호는 연결을 의미하므로 결과는 `"51"`이 된다.

이 결과들이 도처에 널려 있다는 것을 알고 있고, 전혀 말이 되지 않는다. 이것이 여러분이 코딩을 할 때 절대로 시도하지 말아야 하는 이유이 것이다. 비록 이것이 "작동한다"지만, 이는 예상치 못한 결과로 이어질 것이기 때문이다.

## JavaScript의 프로그램 구조

### 문과 바인딩(statements and bindings)
컴퓨터 프로그래밍에서 "프로그램"은 복잡한 문제를 해결하기 위한 사용 설명서라고 생각할 수 있다. 이 설명서의 각 지침(instruction)/문장을 문이라고 한다. JavaScript에서 문은 항상 세미콜론(`;`)으로 끝나야 한다.

```js
let num = 10;
```

위의 예를 바인딩 또는 변수라고 한다. `=` 연산자를 사용하여 값 `10`을 이름 `num``에 바인딩함으로써 다음과 같은 작업을 수행할 수 있다.

```js
let num = 10;
console.log(num * num); // -> 100
```

키워드 `let`은 이 문이 바인딩을 정의할 것임을 나타낸다. 바인딩이 형성될 때 이름에 값에 영원히 연결된다는 의미는 아니며, 기존 바인딩에 `=` 연산자를 계속 사용할 수 있다.

```js
let num = 10;
console.log(num); // -> 10

num = 20;
console.log(num); // -> 20
```

첫번 째 줄에서 `let`이라는 키워드만 사용했다. `let`은 바인딩을 선언하는 데만 사용되기 때문이다. 네번 째 줄에서는 변수 `num`에 연결된 값을 업데이트할 뿐이기 때문이다.

```js
let num1 = 10;
let num2 = 20;

console.log(num1); // -> 10
console.log(num2); // -> 20

num2 = num1;

console.log(num1); // -> 10
console.log(num2); // -> 10
```

```js
let num = 10;
num = num - 5;

console.log(num); // -> 5
```

`const`와 `var` 키워드는 `let`과 마찬가지로 바인딩을 만드는 데도 사용할 수 있지만 범위 측면에서 다르다. 나중에 자세히 설명하겠다.

### 함수
함수는 값을 반환하거나 사이드이펙트를 만들거나 둘 모두가 있는 프로그램이다. 예를 들어, 몇 번 본 `console.log()` 함수는 터미널에 값을 출력하는 데 사용된다.

![console-log](images/javascript-basics/console-log.png)

또는 이 예에서 `prompt()` 함수는 사용자 입력을 요청하는 대화 상자를 디스플레이하고 해당 입력은 변수 `num`으로 바인딩된다.

```js
let num = prompt("Enter A Number");
console.log(num);
```

![prompt-dialog](images/javascript-basics/prompt-dialog.png)

대화 상자를 디스플레이하는 것과 화면에 텍스트를 쓰는 것 모두 사이드이펙트이다. 함수는 사이드이펙트 없이도 유용할 수 있다. 예,

```js
console.log(Math.max(2,4,6,8));
```

`Math.max()` 함수는 부작용이 없으며 단순히 숫자 집합을 취하고 가장 큰 값을 반환한다.

이 모든 함수는 JavaScript에 내장되어 있다. 그러나 JavaScript를 사용하여 여러분 자신의 함수를 만들 수 있다. 이 주제에 대해서는 다음 섹션에서 설명하겠다. 

### `if` 문
`if` 문은 다른 조건에서 다른 코드 조각을 실행할 수 있는 방법을 제공한다. 예,

```js
let num = prompt("Enter A Number");

if (num < 10) {
  console.log("Small");
} else {
  console.log("Large");
}
```

이 프로그램은 숫자가 10보다 작으면 `console.log("Small")`가 실행되고 프로그램이 `"Small"`을 출력하도록 요청한다. 숫자가 10보다 크면 프로그램에서 `"Large"`를 출력한다.

고려해야 할 여러 조건이 있는 경우 여러 `if/else` 쌍을 연결할 수도 있다.

```js
if (num < 10) {
  console.log("Small");
} else if (num < 100) {
  console.log("Medium");
} else {
  console.log("Large");
}
```

이 프로그램은 먼저 숫자가 10보다 작은지 확인하고, 숫자가 10보다 작은 경우 `"Small"`을 출력한다. 숫자가 10보다 크면 프로그램이 100보다 작은지 확인한다. 그렇지 않으면 프로그램에서 `"Medium"`을 출력한다. 마지막으로 숫자가 100보다 크면 프로그램에 `"Large"`가 디스플레이된다.

### `for` 루프
`for` 루프는 일부 조건이 충족되는 한 동일한 코드를 반복적으로 실행할 수 있는 방법을 제공한다.

```js
for (let num = 0; num <= 12; num = num + 2) {
  console.log(num);
}
```

`for` 루프는 두 개의 세미콜론으로 구분된 세 개의 식을 사용한다. 이 예에서 첫 번째 식 `let num = 0`은 새 변수 `num`을 선언하며, 초기 값은 0이다. 두 번째 식은 조건 `num <= 12`이 false가 때까지 루프가 반복됨을 의미한다(숫자가 12보다 큼). 마지막 식은 각 반복에 대한 것이며, `num`은 2만큼씩 증가한다.

![for-loop](images/javascript-basics/for-loop.png)

### `while` 루프
`while` 루프는 식 하나를 사용한다는 점을 제외하면 유사한 방식으로 작동한다. 사실, 우리는 이전의 루프 예를 while 루프로 쉽게 변경할 수 있다.

```js
let num = 0;
while (num <= 12) {
  console.log(num);
  num = num + 2;
}
```

이 예에서는 `num` 변수를 `while` 루프 외부에서 먼저 초기화했다. 괄호 안에 있는 키워드 `while`에는 루프가 계속되어야 하는지 여부를 결정하는 식이 있다. 마지막으로, `while` 루프의 끝에서 `num` 값을 업데이트한다.

### `do while` 루프
`do-while` 루프는 한 점만 `while` 루프와 다르며 루프 본문이 적어도 한 번은 실행되도록 보장한다.

```js
let num = 10;
do {
  num = num + 1;
  console.log(num);
} while (num <= 1);
```

이때 `num`의 초기 값은 10으로 루프가 계속되는 조건을 위반한다. 그러나 이것은 `do-while` 루프이므로, 본문은 여전히 한 번 실행한다. 이것이 `while` 루프라면 전혀 실행되지 않았을 것이다.

![do-while-loop](images/javascript-basics/do-while-loop.png)

### 루프 나가기
루프가 계속되는 조건을 위반하는 것만이 루프를 멈출 수 있는 유일한 방법은 아니다. 예를 들어, 100보다 크고 9로 나눌 수 있는 숫자를 찾을 것을 바란다 (`%` 연산자가 나머지을 계산하는 데 사용할 수 있으므로 `x/9`가 0이면 `x`가 9로 나눌 수 있음을 의미함). `for` 루프를 사용하여 이 문제를 해결할 수 있다.

```js
for (let num = 100; ; num = num + 1) {
  if (num % 9 == 0) {
    console.log(num);
    break;
  }
}
```

루프를 계속할지 여부를 결정하는 식을 가지고 있지 않다. 대신 내부에 break 키워드가 있는 `if` 문이 있으며, 이 문이 실행되면 루프에서 벗어날 수 있다. `break` 키워드를 제거하면 `for` 루프는 무한 루프가 되고 영원히 실행되므로 항상 피해야 한다.

## JavaScript 함수
이전에 JavaScript와 함께 제공되는 일부 함수를 살펴보았다. 이 섹션에서는 JavaScript에서 개발자 자신의 커스텀 함수를 정의하는 데 중점을 둘 것이다. 함수는 값에 싸여 있는 코드 조각으로 볼 수 있으며, 이를 통해 코드 조각을 반복적으로 재사용할 수 있다. 이 포스팅에서는 JavaScript에서 함수를 정의할 수 있는 세 가지 다른 방법에 대해 설명하겠다.

**첫 번째 방법**은 함수를 값으로 정의하고 해당 값을 이름으로 바인딩하는 것이다(이전 문서에서 변수를 정의한 방법처럼).

```js
let square = function(x) {
  return x*x;
};
```

함수는 키워드 `function`를 사용하여 생성되며, 입력으로 매개 변수 집합이 사용되며, 이 경우는 오직 `x`만 이다. 함수에는 본문에 키워드 `return`을 사용하여 출력을 반환하거나 어떤 사이드이펙트기 있어야 한다. 마지막으로, 값으로서의 함수는 이름 `square`에 할당될 것이며, 이 함수를 호출하기 위해 사용한다.

또한 끝에 있는 세미콜론(`;`)은 바인딩을 선언하는 완전한 문이기 때문에 필요하다. 단, 여기서 값은 함수이다.

```js
console.log(square(10)); // -> 100
```

함수에는 둘 이상의 매개 변수가 있을 수도 있고 매개 변수가 전혀 없을 수도 있다(빈 매개 변수 집합).

```js
const sleep = function() {
  console.log("zzzzzzzzzzzzzzzzzzzzzz");
};
```

```js
var multiply3 = function(x, y, z) {
  return x * y * z;
};
```

보시다시피, 함수는 부작용만 있을 뿐 아무것도 반환하지 않을 수 있다.

**두 번째 방법**은 함수 키워드를 사용하여 함수를 선언함으로써 약간 더 짧으며 끝에 세미콜론이 불필요하다.

```js
function square(x) {
  return x*x;
}
```

이 방법을 사용하면 다음과 같이 작업을 수행할 수 있다.

```js
sleep();
multiply3(2,3,4);

function sleep() {
  console.log("zzzzzzzzzzzzzzzzzzzzzz");
}

function multiply3(x, y, z) {
  return x*y*z;
}
```

여기에 함수 선언을 호출하는 문 뒤에 넣었는데도 코드는 여전히 작동한다. 이제 모든 기능을 한 곳에 배치할 수 있게 되어 향후 유지보수에 도움이 된다.

**세 번째 방법**은 화살표 함수라고 한다. 키워드 `function` 대신 화살표(`=>`)를 사용하여 함수를 선언할 수 있다.

```js
const square = (x) => {
  return x*x;
}
```

이는 이전에 본 것과 같은 `square()` 함수이며, 똑같이 작동한다. 그렇다면 왜 javaScript는 화살표 기능과 함수 키워드를 모두 갖고 있을까? 반면에, 어떤 경우에, 더 짧은 함수를 쓸 수 있게 한다.

함수에 파라미터가 하나만 있는 경우에는 파라미터 목록 주위의 괄호를 생략할 수 있다. 그리고 함수 본문에 문이 하나만 있는 경우에는 중괄호와 `return` 키워드도 생략할 수 있다. 그러면 `square()` 함수는 다음과 같이 된다.

```js
const square = x => x * x;
```

### 바인딩과 스코프(scopes)
함수에 대한 주제로 더 깊이 들어가기 전에 첫 번째 방법으로 돌아가 보겠다. 예에서 서로 다른 키워드 `let`, `const`와 `var`를 사용하여 함수를 정의한 것을 알고 있을수 있다. 그들의 차이점은 정확히 무엇일까?

먼저, 스코프의 개념을 이해해야 한다. 스코프는 바인딩에 액세스할 수 있는 프로그램의 일부이다. 바인딩이 함수 또는 블록 외부에 정의된 경우(블럭은 `if` 문, `for` 또는 `while` 루프 등) 원하는 위치에서 해당 바인딩을 참조할 수 있다. 이를 글로벌 바인딩이라고 한다.

`let` 또는 `const`를 사용하여 바인딩이 함수 또는 블록 내부에 선언된 경우, 해당 바인딩은 함수/블록 내부에서만 액세스할 수 있으며, 이를 로컬 바인딩이라고 한다. 그러나 `var` 키워드를 사용하여 바인딩을 정의하면 함수/블록 외부에서도 해당 바인딩에 액세스할 수 있다.

```js
let x = 10;

if (true) {
  let y = 20;
  var z = 30;
  console.log(x + y + z); // -> all three variables are accessible here
}

console.log(x + z); // -> you cannot "see" y from here, but z is still accessible
```

이제, `let`과 `const`의 차이점은 무엇일까요? 이름에서 알 수 있듯이 `const`는 상수를 나타낸다. 즉, `const`를 사용하여 바인딩이 선언되면(`let`과 달리) 해당 값을 변경할 수 없다.

![const-variable](images/javascript-basics/const-variable.png)

### 선택적 인수(optional arguments)
JavaScript는 함수에 전달하는 매개 변수의 수에 관해서는 매우 관대하다. 예를 들어, 이전에 정의한 `square()` 함수에서 하나의 인수를 사용해야 한다.

```js
function square(x) { return x * x; }
console.log(square(4, true, "qwerty"));
```

위의 예에서 두 개 이상의 인수로 `square()` 함수를 호출하는데, 이 함수는 추가 파라미터를 무시하고 첫 번째 파라미터의 제곱을 반환한다.

그리고 파라미터를 너무 적게 전달하면 누락된 매개 변수로 오류가 발생하는 대신 정의되지 않은 값이 할당된다.

물론 이것의 단점은 실수를 했을 때 아무도 그것에 대해 말하지 않는다는 것이다. 따라서 기술적으로 효과가 있더라도 절대로 이것에 의존해서는 안된다. 예상치 못한 결과를 가져올 수 있기 때문이다. 대신 필요한 매개 변수 수와 함수에 전달하는 인수 수에 항상 주의해야 한다.

### Rest 파라미터
그러나 필요한 매개 변수가 몇 개인지 모를 경우에는 어떻게 해야 할까? 예를 들어, 일련의 숫자에서 최대 수를 찾는 함수를 설계하려고 하지만 숫자가 몇 개인지 모르기 때문에 임의 수의 파라미터를 사용하는 함수를 설계해야 한다.

이와 같은 함수를 작성하려면 함수의 마지막 매개 변수 앞에 점 세 개가 있어야 한다.

```js
function max(...numbers) {
  let result = -Infinity;
  for (let number of numbers) {
    if (number > result) {
      result = number;
    }
  }
  return result;
}

max(1, 2, 3, 4, 5, 6, 7);
```

이제 매개 변수 `numbers`(다른 매개 변수라고 함)가 배열에 바인딩되고 함수는 해당 배열의 최대 수를 반환한다.

배열은 항목 리스트이다. 이 경우에는 `[1, 2, 3, 4, 5, 6, 7]`이 있으며, `for (let number of numbers)`는 배열의 모든 항목에 대해 반복할 수 있다. 배열에 대해서는 다음 포스팅에서 설명하겠다.

## JavaScript에서 재귀(recursion)
마지막으로, 재귀의 개념에 대해 설명하겠다. 재귀는 함수가 자신을 스스로 호출하는 것이다. 가장 대표적인 예로 숫자의 거듭제곱을 계산하는 방법이다.

```js
function power(base, exponent) {
  if (exponent == 0) {
    return 1;
  } else {
    return base * power(base, exponent - 1);
  }
} 
```

다섯째 줄에서 함수 `power()`는 매개변수 `base`와 `exponent - 1`을 사용하여 스스로를 호출한다. 이것이 다소 혼란스럽지만 걱정하지 말아라. 이 코드를 이해하기 위해 몇 가지 숫자를 연결해 보겠다. `10^5`(10의 5승)를 계산해 보겠다.

![recursion](images/javascript-basics/recursion.png)

첫 번째 단계에서는 숫자를 넣기만 하여, 함수가 `10 * power(10, 4)`을 반환한다. 그러면 `power(10, 4)`을 계산해야 한다. 숫자를 연결하면 `10 * power(10, 3)`이 반환되며, 이는 `power(10, 5)`가 `10 * 10 * power(10, 3)`와 같다는 것을 의미한다.

그리고 `10 * 10 * 10 * 10 * 10 * 10 * 10 * power(10, 0)`를 얻을 때까지 동일한 단계를 계속 반복한다. `power(10, 0)`이 1을 반환하기 때문에 결국 `power(10, 5)은 10 * 10 * 10 * 10 * 10 이다.

이것은 지수화를 정의하는 매우 우아한 방법이지만, 안타깝게도 이 방법은 JavaScript에서 루프를 사용하는 것보다 약 3배 느리다. 이는 프로그래머들이 항상 직면하는 딜레마이다. 우리는 단순성과 속도 중 하나를 선택해야 한다. 왜냐하면 거의 모든 프로그램을 더 크게 만들면 더 빨르게 만들 수 있기 때문이다. 적절한 균형을 결정하는 것은 프로그래머에게 달려 있다.

**주** 이 포스팅은 [JavaScript Basics #1](https://www.ericsdevblog.com/posts/javascript-basics-1/)를 편역한 것임.
