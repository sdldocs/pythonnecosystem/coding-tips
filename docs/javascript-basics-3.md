이 포스팅을 시작으로, 웹 개발에서 JavaScript 언어의 실용적인 적용에 대해 탐구할 것이다. JavaScript, HTML과 CSS가 어떻게 함께 작동하여 웹 페이지를 더 매력적이고 상호작용적으로 만들 수 있는지에 대해 설명하겠다.

## JavaScript에서 DOM 구조 탐색
먼저 문서 객체 모델에 대하여 간단히 복습하는 것으로부터 시작하겠다. 다음은 간단한 HTML 문서이다.

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Example Page</title>
</head>

<body>
  <h1>Example Page</h1>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  </p>
  <div>
    <p>Vestibulum fringilla lorem ac elit suscipit, nec suscipit nunc posuere.</p>
    <p>Proin efficitur eros scelerisque consequat <a href="https://www.ericsdevblog.com/">pulvinar</a>.</p>
  </div>
</body>

</html>
```

각 HTML 요소는 상자로 볼 수 있다. 예를 들어, 예제 문서의 구조는 다음과 같다.

![dom-structure](images/javascript-basics/dom-structure.png)

각 상자에 대해 JavaScript는 해당 객체를 자동으로 생성하며, 이 객체와 상호 작용하여 내용, 속성 등 상자에 대한 자세한 내용을 확인할 수 있다. 이러한 종류의 표현을 문서 객체 모델 또는 줄여서 DOM이라고 한다.

이 DOM 구조의 두 번째 중요한 특징은 상자가 모두 서로 연결되어 있다는 것이다. 즉, 시작점을 선택하면 페이지의 다른 노드로 이동할 수 있다. 예를 들어, `<body>` 노드에는 `<h1>`, `<p>`와 `<div>`의 세 하위 요소가 있다. `<div>` 노드에는 하위 요소로 다른 두 단락(`<p>`)이 있다. 따라서 이 예제 문서에서 링크(`<a>`)가 있는 단락을 찾으려면 `<html>`에서 `<body>`로 그리고 `<div>`로 이동한 후 마지막으로 `<p>` 노드를 찾는다.

## JavaScript와 HTML
JavaScript 코드를 HTML 문서로 가져오기 위해 `<script>` `</script>` 태그를 사용한다.

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Example Page</title>
</head>

<body>
  ...

  <!--JavaScript-->
  <script>
        ...
  </script>

  <!--JavaScript in External File-->
  <script src="myScript.js"></script>

</body>

</html>
```

JavaScript 코드는 `<body>` 태그의 끝 앞에 놓는 것이 관례이다. JavaScript를 삽입하는 방법은 CSS와 마찬가지로 HTML과 함께 넣거나 별도의 JavaScript 파일을 import하는 두 가지가 있다. 이 포스팅을 위해 HTML과 JavaScript 코드를 함께 넣겠다.

JavaScript는 각 DOM 상자를 객체로 처리하므로 전역 바인딩 `document`를 사용하여 HTML 문서의 모든 요소에 액세스할 수 있다. 예를 들어, `document.body`는 문서의 `<body>` 요소를 나타낸다.

```html
...
<body>
  ...
  <!--JavaScript-->
  <script>
    // Access body element
    let body_element = document.body;

    // Access h1 element
    let h1_element = document.body.firstElementChild;
    console.log(h1_element.tagName);

    // Access paragraph element (with link)
    let p_element = document.body.childNodes[5].lastElementChild;
    console.log(p_element.tagName);
  </script>
</body>
```

브라우저에서 `Developer Tools` -> `Console`로 이동하면 태그 이름이 정확히 반환되었음을 확인할 수 있다.

`<div>` 요소를 찾기 위해 사용하는 인덱스 번호는 5이다. 이는 `childNodes()` 메서드가 요소 노드뿐만 아니라 텍스트 노드와 주석 노드도 반환하기 때문이다. 예를 들어, 단락 요소에는 요소 노드 `<p>`와 그 내용인 텍스트 노드가 있다.

웹 개발에서는 document.body에서 시작하여 속성 경로를 따라 문서의 특정 요소에 도달할 수 있다. 그러나, 그것이 가능하더라도, 특히 복잡한 관계 트리가 있는 큰 HTML 문서의 경우에는 바람직하지 않은 생각이다. 실수를 하는 것은 매우 쉽다. 다행히 JavaScript는 HTML 문서에서 요소를 찾는 몇 가지 현명한 방법을 제공한다.

### JavaScript를 사용하여 HTML 요소 찾기
앞서 JavaScript가 모든 HTML 요소를 객체로 취급한다고 언급했는데, 이는 사용할 수 있는 내장 메서드가 있음을 의미한다. 사실, JavaScript를 사용하여 HTML 파일에서 요소들을 찾을 수 있는 몇 가지 다른 방법들이 있다. 그리고 그것들은 실제로 CSS에서 말했던 선택자들과 매우 유사하게 작동한다.

예를 들어 모든 HTML 요소에는 `getElementsByTagName()` 메서드가 있어 특정 태그가 있는 요소를 찾을 수 있다.

```html
<body>
  <h1>Example Page</h1>
  <p class="paragraphs">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <div>
    <p class="paragraphs paragraphs_div">Vestibulum fringilla lorem ac elit suscipit, nec suscipit nunc posuere.</p>
    <p class="paragraphs paragraphs_div" id="paragraph_link">Proin efficitur eros scelerisque consequat <a
        href="https://www.ericsdevblog.com/">pulvinar</a>.</p>
  </div>

  <!--JavaScript-->
  <script>
    // Get all paragraph elements
    let p_elements = document.body.getElementsByTagName("p");
  </script>
</body>
```

이 메서드는 지정된 태그가 있는 요소 모음을 반환하며, 배열과 마찬가지로 인덱스 번호를 지정하여 각 요소를 액세스할 수 있다.

```html
<!--JavaScript-->
<script>
  // Get all paragraph elements
  let p_elements = document.body.getElementsByTagName("p");

  // Get the second paragraph element and print its content
  let second_p = p_elements[1];
  console.log(second_p.innerHTML);
</script>
```

그러나 이 컬렉션을 실제 배열과 혼동하지 말아야 한다. 이 컬렉션은 매우 비슷하지만 완전히 동일하지는 않다. `for`/`of` 루프를 사용하여 루프를 수행할 수 없다. 인덱스 번호를 사용하고 정규 `for` 루프를 사용하여 요소를 실행해야 한다. 또는 `Array.from` 메서드를 사용하여 이 컬렉션을 배열로 변환할 수 있다.

원하는 요소를 찾으면 점(`.`) 연산자를 사용하여 해당 요소의 속성과 내용을 액세스할 수 있으며 값도 변경할 수 있다.

```html
<!--JavaScript-->
<script>
  // Get all paragraph elements
  let p_elements = document.body.getElementsByTagName("p");

  // Get the second <p> element
  let second_p = p_elements[1];

  // Print its content
  console.log(second_p.innerHTML);

  // Change its content
  second_p.innerHTML = "Changed content.";

  // Print its attributes
  console.log(second_p.attributes);

  // Access one of the attributes
  console.log(second_p.getAttribute("class"));
</script>
```

두 번째 메서드는 `document.getElementById()`로, 요소 집합을 반환하는 대신 단일 요소를 찾는 데 사용된다. 이 메서드가 모든 요소 객체 아래에 있는 것은 아니며 `document.body.getElementById()`에도 없다.

```html
<!--JavaScript-->
<script>
  // Get an element based on ID
  let element = document.getElementById("paragraphLink");
  console.log(element.innerHTML);
</script>
```

세 번째 메서드도 유사하다. 클래스 이름이 같은 요소를 찾는 데 도움이 된다. `getElementsByClassName()`은 전체 문서를 검색하여 지정된 클래스 이름을 가진 요소 모음을 반환한다.

```html
<!--JavaScript-->
<script>
  // Get an element based on class name
  let element = document.getElementByClass("text-red");
  console.log(element.innerHTML);
</script>
```

마지막 메서드는 이 모든 것을 조합하는 것다. 이것이 HTML에서 요소를 찾으려 할 때 가장 일반적으로 사용되는 메서드, `quertSelector()`이다.

```js
document.querySelector("div") // Matches a tag
document.querySelector("#myID") // Matches an ID
document.querySelector(".myclass") // Matches a class
```

### 요소 추가와 삭제
다음으로 HTML 요소를 찾은 후 이 요소들을 조작하는 방법에 대해 설명할 시간이다. 사실 DOM 구조의 거의 모든 것을 변경할 수 있다.

예를 들어 다음과 같은 요소를 제거할 수 있다.

```js
// Get an element based on ID, and then remove it from the page
let element = document.getElementById("paragraphLink");
element.remove();
```

또는 새 요소를 만들어 DOM 구조에 추가할 수 있다.

```js
// Create new paragraph element
let new_p = document.createElement("p");

// Create content for the new <p> element
let new_text = document.createTextNode("This is a new paragraph.");

// Append the content to the <p> element node
new_p.appendChild(new_text);

// Add the new paragraph to the DOM structure
let element = document.getElementById("paragraphLink");
element.append(new_p);
```

앞서 언급했듯이, 단락 요소는 `<p>` 요소 노드 다음에 내용을 나타내는 텍스트 노드를 가져야 한다.

한 요소를 다른 요소로 바꿀 수도 있다.

```js
// Replace a paragraph with the new paragraph
let element = document.getElementById("paragraph_link");
element.replaceWith(new_p);
```

이 섹션에서 JavaScript를 사용하여 HTML 요소를 찾고 조작하는 방법에 대해 간략하게 설명했다. 그러나 브라우저를 새로 고칠 때 모든 변경 사항이 즉시 적용된다는 사실을 알고 있을 수도 있다. 다음 포스팅에서는 작업을 수행하기 위해 JavaScript를 트리거하는 데 사용할 수 있는 다른 이벤트에 대해 설명할 것이다.

## 이벤트 처리기(event handler)
컴퓨터 프로그래밍에서 이벤트는 마우스와 키보드 동작같은 사용자 입력이며 프로그램은 일반적으로 무언가로 응답할 것으로 예상된다. 이 프로세스를 이벤트 처리(event handling)라고 한다. 먼저 아주 간단한 예를 살펴보겠다. 단락이 있는 HTML 문서를 가지고 있으며, 페이지가 클릭될 때 메시지를 반환하기를 원한다.

```html
<p>Click this document to activate the handler.</p>
<script>
  // Recall that the () => {} syntax is how we define an arrow function in JavaScript
  window.addEventListener("click", () => {
    console.log("You knocked?");
  });
</script>
```

이번에는 페이지가 로드되는 순간 대신 문서를 클릭할 때만 출력 메시지가 콘솔에 나타난다.

### 이벤트 처리기 등록
`addEventListener()` 메서드는 문서 노드에 대한 이벤트 처리기를 등록하는 방법이다. 실제로 HTML 문서의 모든 노드에 대해 이벤트 핸들러를 등록하는 데 동일한 방법을 사용할 수 있다. 예를 들면,

```html
<!--This time we register an event handler for the button but not the paragraph-->
<button>Click me</button>
<p>No handler here.</p>

<script>
  let button = document.querySelector("button");
  button.addEventListener("click", () => {
    console.log("Button clicked.");
  });
</script>
```

실제로 HTML 노드에 대한 `onclick` 애트리뷰트가 있으며, 이 애트리뷰트와 정확히 동일한 효과를 갖는다. 그러나 각 노드에 대해 하나의 핸들러만 등록할 수 있다. `addEventListener()` 메서드를 사용하여 각 노드에 대해 여러 핸들러를 등록할 수 있다.

```html
<button>Click me</button>

<script>
  let button = document.querySelector("button");

  // When you click the button, the console outputs "Button clicked."
  button.addEventListener("click", () => {
    console.log("Button clicked.");
  });

  // When you click the button, the console outputs "Button double clicked."
  button.addEventListener("dblclick", () => {
    console.log("Button double clicked.");
  })
</script>
```

`removeEventListener()` 메서드, 유사한 인수를 가진 호출을 사용하여 이미 등록된 이벤트 핸들러를 제거할 수 있다.

```html
<button>Act-once button</button>
<script>
  let button = document.querySelector("button");
  function once() {
    console.log("Done.");
    button.removeEventListener("click", once);
  }
  button.addEventListener("click", once);
</script>
```

이 버튼은 `removeEventListener("click", once)` 메서드가 실행된 후 버튼에 등록된 이벤트 핸들러가 제거된다. `removeEventListener`에 전달되는 함수는 `addEventListener` 메서드에 전달된 함수와 동일해야 한다.

### 전파(propagation)
대부분의 이벤트 타입에서 자식 노드에 등록된 이벤트 처리기는 자식 노드에서 발생한 이벤트를 수신할 수 있다. 예를 들어, 문단 내부의 단추를 누르면 해당 문단에 등록된 이벤트 처리기에서도 해당 클릭 이벤트를 볼 수 있다.

그 이벤트는 외부로 전파된다고 한다. 예를 들어, 버튼과 문단에 모두 이벤트 처리기가 있는 경우, 먼저 버튼에 등록된 처리기로, 다음으로 문단 그리고 문서의 루트에 도달할 때까지 외부로 계속 전파된다.

이 기능은 때때로 꽤 유용할 수 있지만 항상 우리가 원하는 것은 아닐 수 있다. 다행히 `stopPropagation()` 메서드를 사용하여 전파를 중지할 수 있다.

```html
<!--<button> is the child of <p>-->
<p>A paragraph with a <button>button</button>.</p>
<script>
  let para = document.querySelector("p");
  let button = document.querySelector("button");
  para.addEventListener("mousedown", () => {
    console.log("Handler for paragraph.");
  });
  button.addEventListener("mousedown", event => {
    console.log("Handler for button.");
    // If the button is clicked with the right mouse button, there will be no propagation
    if (event.button == 2) event.stopPropagation();
  });
</script>
```

페이지의 여러 요소에 대한 이벤트 처리기를 등록하려는 경우가 있다. 이를 위해 대상 속성을 사용하여 이벤트 타입에 대한 넓은 망을 던질 수 있다.

```html
<button>A</button>
<button>B</button>
<button>C</button>
<script>
  document.body.addEventListener("click", event => {
    if (event.target.nodeName == "BUTTON") {
      console.log("Clicked", event.target.textContent);
    }
  });
</script>
```

### 기본 동작(default action)
예를 들어, 링크를 클릭하면 링크의 대상으로 이동하고, 아래쪽 화살표를 누르면 브라우저가 페이지를 아래로 스크롤한다. `preventDefault()` 메서드를 사용하여 해당 기본 동작이 활성화되지 않도록 할 수 있다. 전혀 쓸모없지만 아주 흥미로운 것을 시도해 보자.

```html
<a href="https://developer.mozilla.org/">MDN</a>
<script>
  let link = document.querySelector("a");
  // When you click the link, instead of going to the URL that link specifies, the console will just output "Nope."
  link.addEventListener("click", event => {
    console.log("Nope.");
    event.preventDefault();
  });
</script>
```

이것이 가능하더라도, 할 수 있는 아주 좋은 이유가 없는 한 이를 하지 말자, 그렇지 않으면 사용자들은 매우 혼란스러울 것이다.

### 키 이밴트
이벤트 처리기가 일반적으로 작동하는 방식에 대해 설명했다. 이제는 다양한 모든 타입의 이벤트를 자세히 살펴볼 시간이다. 살펴볼 첫 번째는 키 이벤트이다.

키보드의 키를 누르면 `keydown` 이벤트가 트리거되고 키를 놓으면 `keyup` 이벤트가 트리거된다.

```html
<p>This page turns violet when you hold the V key.</p>
<script>
  window.addEventListener("keydown", event => {
    if (event.key == "v") {
      document.body.style.background = "violet";
    }
  });
  window.addEventListener("keyup", event => {
    if (event.key == "v") {
      document.body.style.background = "";
    }
  });
```

매우 간단해 보이지만 `keydown` 이벤트에 대해 매우 주의해야 한다. 이는 일회성이 아니라, 키를 누르고 있는 동안에는 띨 때까지 계속해서 반복적으로 트리거된다. 이전 코드를 사용하여 키를 누른 상태로 두면 어떤 일이 발생하는지 확인할 수 있다.

`CTRL`, `ALT`와 `SHIFT`같은 특수 키도 있다. 이러한 키를 수정 키(modifier key)라고 하며, 키 조합을 구성하여 다른 키의 원래 값을 수정한다. 예를 들어 `Shift` 키를 누른 상태에서 키를 누르면 `"s"`가 `"S"`가 되고 `"1"`이 `"!"`이 된다. 다음과 같은 키 조합에 대해 이벤트 처리기를 등록할 수 있다.

```html
<p>Press Control-Space to continue.</p>
<script>
  window.addEventListener("keydown", event => {
    if (event.key == " " && event.ctrlKey) {
      console.log("Continuing!");
    }
  });
</script>
```

## 포인터 이벤트
포인터는 이름에서 알 수 있듯이 화면의 객체을 가리키는데 사용된다. 마우스 또는 터치 스크린을 사용하여 작업을 수행하는 데 사용할 수 있는 방법은 크게 두 가지가 있다. 이 방법은 여러 가지 타입의 이벤트를 생성한다.

### 마우스 클릭
마우스 클릭은 주요 이벤트와 유사하게 작동한다. 마우스 버튼을 누르면 `mousedown` 이벤트가 트리거되고, 해당 버튼을 놓으면 `mouseup` 이벤트가 트리거된다. 그리고 `mouseup` 이벤트가 끝나면 완전한 클릭이 종료되므로 `click` 이벤트가 발생한다.

```html
<button>Click me!</button>

<script>
  let button = document.querySelector("button");

  button.addEventListener("mousedown", event => {
    console.log("mouse down");
  });
  button.addEventListener("mouseup", event => {
    console.log("mouse up");
  });
  button.addEventListener("click", event => {
    console.log("button clicked");
  });
</script>
```

클릭 두 번이 매우 가깝게 발생하면 두 번째 클릭 후에 `dblclick`(더블 클릭) 이벤트가 트리거된다.

```html

<button>Double click me!</button>

<script>
  let button = document.querySelector("button");
  button.addEventListener("dblclick", (event) => {
    console.log("double clicked");
  });
</script> 
```

### 마우스 이동
마우스 포인터가 움직이면 `mousemove` 이벤트가 트리거된다.

```html
<p>Move the cursor onto this paragraph to turn it red.</p>

<script>
  let para = document.querySelector("p");
  para.addEventListener("mousemove", (event) => {
    para.style.color = "red";
  });
</script>
```

이 기능은 드래그앤 드롭 기능을 구현할 때 매우 유용할 수 있다. 하지만 그러기 위해서는 먼저 커서의 위치를 추적해야 한다. 이 정보를 가져오려면 창의 왼쪽 상단 모서리를 기준으로 상대적인 이벤트 좌표(픽셀 단위)를 포함하는 이벤트의 `clientX`와 `clientY` 속성을 사용하거나 전체 문서의 왼쪽 상단 모서리로부터 상대적인 `pageX`와 `pageY`를 사용할 수 있다.

예를 들어, 다음 스크립트는 페이지에서 발생한 클릭 이벤트의 좌표를 출력한다.

```html
<p>click anywhere</p>

<script>
  window.addEventListener("click", event => {
    console.log("X: " + event.clientX);
    console.log("Y: " + event.clientY);
  });
</script>
```

여기 좀 더 복잡한 예가 있다. 이 프로그램은 막대를 표시하며, 막대를 끌어서 너비를 변경할 수 있다.

```html
<p>Drag the bar to change its width:</p>
<div style="background: orange; width: 60px; height: 20px">
</div>
<script>
  let lastX; // Tracks the last observed mouse X position
  let bar = document.querySelector("div");
  bar.addEventListener("mousedown", event => {
    if (event.button == 0) { // if the left button is being held
      lastX = event.clientX;
      // If the cursor moves while the left button is being held
      window.addEventListener("mousemove", moved);
      event.preventDefault(); // Prevent selection
    }
  });

  function moved(event) {
    // If no button is being held, remove the "mousemove" event handler
    if (event.buttons == 0) { // Notice this is "buttons" not "button"
      window.removeEventListener("mousemove", moved);
    } else {
      let dist = event.clientX - lastX;
      let newWidth = Math.max(10, bar.offsetWidth + dist);
      bar.style.width = newWidth + "px";
      lastX = event.clientX;
    }
  }
</script>
```

두 가지 방법을 사용하여 어떤 버튼을 눌렀는지(`button` 속성과 `buttons` 속성) 확인했다. 두 가지 방법은 분명히 다르게 작동한다. `button` 속성은 클릭한 버튼(단일)만 알려줄 수 있는 반면 `buttons` 속성은 버튼 조합을 누를 경우 알려줄 수 있다.

#### `button` 속성
- `0`: 기본 버튼을 눌렀다. 일반적으로 왼쪽 버튼 또는 초기화되지 않은 상태이다
- `1`: 보조 버튼을 눌렀다. 일반적으로 휠 버튼 또는 (있는 경우) 중간 버튼
- `2`: 보조 버튼을 눌렀다, 일반적으로 오른쪽 버튼
- `3`: 네 번째 버튼, 일반적으로 *Browser Back* 버튼
- `4`: 다섯 번째 버튼, 일반적으로 *Browser Forward*버튼

#### `buttons` 속성
- `0`: 버튼 없음 또는 초기화되지 않음
- `1`: 기본 버튼(일반적으로 왼쪽 버튼)
- `2`: 보조 버튼(일반적으로 오른쪽 버튼)
- `4`: 보조 버튼(일반적으로 마우스 휠 버튼 또는 중간 버튼)
- `8`: 네 번째 버튼(일반적으로 "Browser Back" 버튼)
- `16`: 5번째 버튼(일반적으로 "Browser Forward" 버튼)

`buttons` 속성은 버튼 조합을 기록할 수 있다. 두 개 이상의 버튼을 동시에 누르면 값이 결합된다. 예를 들어 기본과 보조 버튼을 동시에 누르면 값이 `3`이 된다.

### 터치 이벤트
대부분의 경우 사용자가 터치 스크린을 사용할 때도 마우스 이벤트가 작동한다. 예를 들어 화면의 버튼을 누르면 클릭 이벤트가 트리거되며 마우스 포인터로 클릭하는 것과 같다.

그러나 이전에 언급한 바 크기 조정 예와 같은 경우에는 이 방법이 작동하지 않는다. 터치 스크린에는 여러 개의 버튼이 없고 화면을 터치하지 않을 때 손가락의 위치를 추적할 수 없기 때문이다. 따라서 이 문제를 해결하기 위해 터치 상호 작용에 의해서만 트리거되는 몇 가지 특정 이벤트 타입이 있다.

손가락이 화면에 닿으면 `touchstart` 이벤트가 발생하고, 터치 중에 움직이면 `touchmove` 이벤트가 발생하며, 마지막으로 손가락을 들면 `touchend` 이벤트가 발생한다.

## 스크롤 이벤트
커서를 요소에 놓고 마우스의 가운데 버튼을 스크롤하면 `scroll` 이벤트가 트리거된다. 이 기능은 웹 페이지의 응답성을 높이려고 할 때 매우 유용할 수 있다. 예를 들어 Apple 웹 사이트의 제품 소개 페이지로 이동하면 아래로 스크롤할 때 페이지의 요소가 이동한다.

다음은 프로그레스 바의 예로, 0%에서 시작하여 아래로 스크롤하면 100%로 이동한다.

```js
<style>
  #progress {
    border-bottom: 20px solid orange;
    width: 0;
    position: fixed;
    top: 0; left: 0;
  }
</style>
<div id="progress"></div>
<script>
  // Create some content
  document.body.appendChild(document.createTextNode(
    "supercalifragilisticexpialidocious ".repeat(1000)));

  let bar = document.querySelector("#progress");
  window.addEventListener("scroll", () => {
    let max = document.body.scrollHeight - innerHeight;
    bar.style.width = `${(pageYOffset / max) * 100}%`;
  });
</script>
```

### 포커스 이벤트
요소가 포커스 되면 `focus` 이벤트가 트리거되고 요소가 포커스를 잃으면 `blur` 이벤트가 트리거된다. 앞서 설명한 다른 이벤트 타입과 달리 이 둘은 전파되지 않는다.

HTML 필드 요소에서 가장 일반적으로 사용된다. 텍스트 필드를 클릭하고 일부 텍스트를 입력하기 시작하면 해당 필드가 포커스 돠었다고 하며, 해당 필드에서 다른 요소를 클릭하면 해당 필드 요소의 포커스가 사라진다.

다음은 현재 포커스가 있는 텍스트 필드에 대한 도움말 텍스트를 표시하는 예이다.

```html
<p>Name: <input type="text" data-help="Your full name"></p>
<p>Age: <input type="text" data-help="Your age in years"></p>
<p id="help"></p>

<script>
  let help = document.querySelector("#help");
  let fields = document.querySelectorAll("input");
  for (let field of Array.from(fields)) {
    field.addEventListener("focus", event => {
      let text = event.target.getAttribute("data-help");
      help.textContent = text;
    });
    field.addEventListener("blur", event => {
      help.textContent = "";
    });
  }
</script>
```

## 로드 이벤트
전체 페이지의 로드가 완료되면 `load` 이벤트가 트리거된다. 이는 이벤트 처리기 없이 직접 `<script>` 태그 안에 코드를 넣는 것과는 다르다. `<script>` 태그 내부의 코드는 태그를 발견하면 즉시 실행된다. 경우에 따라서는 너무 빠를 수도 있다.

또한 `beforeunload` 라는 유사한 이벤트 타입이 있다. 페이지를 닫을 때 트리거되며, 이 이벤트의 주요 용도는 사용자가 저장하지 않은 작업을 실수로 닫지 않도록 하는 것이다.

**주** 이 포스팅은 [JavaScript Basics #3](https://www.ericsdevblog.com/posts/javascript-basics-3/)를 편역한 것임.
