이전 포스팅에서는 요소와 속성같은 HTML의 몇 가지 기본 개념에 대해 살펴보았고, HTML 문서의 기본 구조와 블록 및 인라인 수준 요소로 HTML 페이지를 작성하는 방법에 대해 다루었다. 이 포스팅에서는 이미지와 오디오와 같은 미디어 파일을 HTML 파일 안에 내장하는 방법과 사용자가 웹 페이지와 상호 작용할 수 있도록 하는 HTML 양식을 만드는 방법을 포함하여 HTML 페이지를 더 풍부하게 만드는 특정 요소 세트에 대해 살펴 보고자 한다.

## 미디어 파일을 HTML에 포함시키기
우선 미디어 파일에 대해 이야기해 보자. 미디어 파일에는 이미지, 비디오 및 오디오 파일이 있다. 이러한 미디어 파일은 웹 디자인에서 매우 중요한 구성 요소이다. 이들은 웹 페이지의 모양을 개선하고 더 많은 정보와 상호작용을 가능하게 한다.

### 이미지 파일
`<img>` 태그는 HTML로 이미지를 디스플레이하는 데 사용된다. 구문은 다음과 같다.

```html
<img src="url" alt="alternatetext">
```

`<img>` 태그가 비어 있다. 애트리뷰트만 있고 닫는 태그는 없다. `<img>` 태그에 필요한 속성은 두 가지이며, `src`는 실제 이미지 파일의 경로를 지정하고 `alt`는 이미지에 대한 대체 텍스트를 나타낸다. 어떤 이유로 이미지가 로드되지 않으면 브라우저는 대체 텍스트를 대신 디스플레이한다.

```html
<img src="image1.jpg" alt="This is an image">
```

예를 들면 이미지 크기를 너비와 높이를 지정하는 `<img>` 태그를 위한 선택적 애트리뷰트도 있다. 단위는 픽셀(px)이다.

```html
<img src="image2.jpg" alt="..." width="500" height="600">
```

항상 이미지의 너비와 높이를 지정해야 한다. 너비와 높이를 지정하지 않으면 이미지를 로드하는 동안 웹 페이지가 깜박일 수 있다.

또 다른 흥미로운 개념은 이미지 맵이다. 이미지에 클릭 가능한 영역을 만들 수 있다. [여기](https://www.w3schools.com/html/html_images_imagemap.asp) w3school의 예가 있다.

```html
<img src="workplace.jpg" alt="Workplace" usemap="#workmap">

<map name="workmap">
    <area shape="rect" coords="34,44,270,350" alt="Computer" href="computer.htm">
    <area shape="rect" coords="290,172,333,250" alt="Phone" href="phone.htm">
    <area shape="circle" coords="337,300,44" alt="Coffee" href="coffee.htm">
</map>
```

첫 번째 줄은 이미지를 로드하고 `usemap` 애트리뷰트는 이미지를 맵(이름이 `workmap`)에 연결한다. 4~6번째 줄은 각각 클릭 가능한 영역을 정의하며, `href` 애트리뷰트를 사용하여 이동할 대상을 지정한다.

### 비디오 파일
`<video>` 요소는 웹 페이지에 비디오를 삽입하는 데 사용된다.

```html
<video width="640" height="480" controls>
    <source src="movie.mp4" type="video/mp4">
    <source src="movie.ogg" type="video/ogg">
    Your browser does not support the video tag.
</video>
```

`width`와 `height` 애트리뷰트는 비디오 크기를 정의한다. 설정되지 않은 경우 비디오가 로드되는 동안 페이지가 깜박일 수 있다. `control` 애트리뷰트는 재생, 일시 중지 및 볼륨같은 비디오 제어 단추를 추가한다.

![html-video](images/html-basics/html-video.png)

`<source>` 요소를 사용하면 `src` 애트리뷰트를 사용하여 비디오 파일을 지정할 수 있다. `type` 애트리뷰트는 파일의 타입과 포맷을 선언하는 데 사용된다. 여러 `<source>` 요소가 있을 때 브라우저는 인식하는 첫 번째 요소를 재생한다.

`<video>` 태그와 `</video>` 태그 사이의 텍스트는 `<video>` 요소를 지원하지 않는 브라우저에서만 디스플레이된다.

비디오를 자동으로 시작하려면 자동 재생 애트리뷰트를 사용한다.

```html
<video width="640" height="480" autoplay>
    <source src="movie.mp4" type="video/mp4">
    <source src="movie.ogg" type="video/ogg">
    Your browser does not support the video tag.
</video>

자동 재생 후에 음소거를 추가하여 비디오가 자동으로 재생되도록 하지만 소리를 재생하지 않도록 한다.

```html
<video width="640" height="480" autoplay muted>
    <source src="movie.mp4" type="video/mp4">
    <source src="movie.ogg" type="video/ogg">
    Your browser does not support the video tag.
</video>
```

### 오디오 파일
`<audio>` 요소는 웹 페이지에 오디오를 삽입하는 데 사용되며, `<video>` 요소와 동일하게 작동한다.

```html
<audio controls>
    <source src="horse.ogg" type="audio/ogg">
    <source src="horse.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>
```

`control` 애트리뷰트는 오디오 제어 버튼을 추가하고, `<source>` 요소는 오디오 파일을 지정하며, 브라우저가 `<audio>` 태그를 지원하지 않으면 텍스트가 디스플레이된다.

여기에서 자동 재생과 음소거 애트리뷰트도 작동된다.

```html
<audio controls autoplay muted>
    <source src="horse.ogg" type="audio/ogg">
    <source src="horse.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>
```

### SVG 그래픽스
SVG(Scalable Vector Graphics)는 웹에서 그래픽을 정의하는 데 사용된다. 예를 들어, 다음 코드는 원을 정의한다.

```html
<svg width="100" height="100">
    <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>
```

![svg-1](images/html-basics/svg-1.png)

SVG를 사용하여 다음과 같은 비교적 복잡한 모양을 그릴 수 있다.

```html
<svg height="130" width="500">
    <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" style="stop-color:rgb(255,255,0);stop-opacity:1" />
            <stop offset="100%" style="stop-color:rgb(255,0,0);stop-opacity:1" />
        </linearGradient>
    </defs>
    <ellipse cx="100" cy="70" rx="85" ry="55" fill="url(#grad1)" />
    <text fill="#ffffff" font-size="45" font-family="Verdana" x="50" y="86">SVG</text>
    Sorry, your browser does not support inline SVG.
</svg>
```

![svg-2](images/html-basics/svg-2.png)

관심이 있다면 SVG 그래픽에 대한 [튜토리얼](https://www.w3schools.com/graphics/svg_intro.asp)이 있다. 하지만 지금은 이것을 다루지 않는다.

## HTML에서 양식(form) 만들기
양식은 HTML의 매우 중요한 부분이다. 이는 클라이언트로부터 사용자 입력을 수집하여 처리할 서버로 보낼 수 있도록 한다. HTML의 모든 양식은 다음과 같이 `<form>` 요소 안에 감싸여 있다.

```html
<form>
. . .
form elements
. . .
</form>
```

### 양식 요소
`<form>` 요소는 텍스트 필드, 확인란, 라디오 단추, 제출 단추 등 다양한 입력 요소 타입의 컨테이너 역할을 한다. 이 포스팅에서는 이 모든 것에 대해 설명할 예정이지만, 먼저 `<form>` 요소와 관련된 몇 가지 애트리뷰트에 대해 설명하겠다.

#### `action` 애트리뷰트
```html
<form action="/action_page.php">
. . .
</form>
```

`action` 애트리뷰트는 양식을 제출할 때 수행할 작업을 정의한다. 일반적으로 양식의 데이터는 서버로 전송되고 해당 데이터는 프로그램에 의해 처리된다. 이 예에서 프로그램의 이름이 "`action_page.php`"이다. 데이터가 처리된 후에는 일반적으로 서버에서 클라이언트로 응답을 반환한다.

이는 웹 개발 과정의 백엔드 부분에 들어가면 더 설명할 것이다.

#### `target` 애트리뷰트
```html

<form action="/action_page.php" target="_blank">
. . .
</form>
```

`target` 애트리뷰트는 서버에서 보낸 응답을 디스플레이할 위치를 지정한다. 링크(`<a>`)에서 설명한 `target`과 동일하게 작동한다.

```html
<form action="/action_page.php" method="get">
. . .
</form>
```

메서드 애트리뷰트는 양식 데이터를 제출하기 위한 HTTP 메서드를 지정한다. 이미 언급했듯이 가장 일반적인 두 가지 방법은 `GET`과 `POST`이다.

> `GET`과 `POST`의 차이
>
> 둘 다 양식 데이터를 서버로 전송할 수 있지만 `POST`는 데이터를 HTTP 요청의 본문 안에 넣고 `GET`는 이름/값 쌍으로 URL에 데이터를 추가하므로 `POST`가 정보를 더 안전하게 전송할 수 있다.
>
> 위의 예에서 `GET` 메서드는 다음과 같이 URL로 데이터를 보낸다.
>
> ```html
> www.example.com/action_page.php?name1=value1&name2=value2
> ```
>
> 모든 데이터는 물음표(?) 뒤에 추가되고 다른 `name/value` 쌍은 앰퍼샌드(&)로 연결된다.
>
> POST 메서드의 경우 데이터는 다음과 같이 HTTP 요청 본문에 "숨긴다".
>
> ```html
> POST /action_page HTTP/1.1
> Host:www.example.com
> name1=value1&name2=value2
> ```

이 부분을 이해하지 못하더라도 [JavaScript에 대해 설명할 때]() 이 항목을 다시 살펴보겠다.

#### `autocomplete` 애트리뷰트
```html
<form action="/action_page.php" autocomplete="on">
. . .
</form>
```

`autocomplete` 애트리뷰트는 양식에 자동 완성 기능을 설정할지 여부를 지정한다. 자동 완성이 켜져 있으면 브라우저는 사용자가 이전에 입력한 값을 기준으로 값을 자동으로 채운다.

#### `novalidate` 애트리뷰트
```html
<form action="/action_page.php" novalidate>
. . .
</form>
```

`novalidate` 애트리뷰트는 부울 애트리뷰트이다. 양식 데이터(입력)가 있는 경우 제출 시 유효성을 검사하지 않도록 지정한다.

### 입력 요소
다음으로, 이 양식을 사용할 수 있도록 하려면 양식에 입력 요소를 추가해야 한다.

#### `<input>` 요소
`<input>` 요소는 HTML 양식에서 가장 중요한 요소이다. 타입에 따라 여러 가지 방법으로 디스플레이할 수 있다. 예를 들어 요소가 텍스트 타입인 경우 브라우저에 텍스트 상자가 디스플레이된다.

```html
<form>
    <input type="text">
</form>
```

![text-input](images/html-basics/text-input.png)

#### `<label>` 요소
`<label>` 요소를 사용하여 양식 요소의 레이블을 정의할 수 있다. 이는 화면 판독기 사용자에게 매우 유용하다. 웹 사이트 순위에 중요한 역할을 하는 웹 사이트의 접근성을 향상시킨다.

`<label>` 요소에는 `for` 애트리뷰트가 있으며, 이 애트리뷰트는 레이블을 지정하려는 양식 요소의 `id`와 일치해야 한다.

```html
<form>
    <label for="firstname">First Name:</label><br>
    <input type="text" id="firstname"><br>
    <br>
    <label for="lastname">Last Name:</label><br>
    <input type="text" id="lastname"><br>
</form>
```

![input-with-label](images/html-basics/input-with-label.png)

#### `<select>` 요소
`<select>` 요소는 다음과 같은 드롭다운 목록을 정의한다.

```html
<label for="fruits">Pick A Fruit:</label>
<select id="fruits" name="fruits">
    <option value="Apple">Apple</option>
    <option value="123">Banana</option>
    <option value="eprag">Grape</option>
</select>
```

![select-input](images/html-basics/select-input.png)

이 예에서 각 `<option>` 요소는 선택할 수 있는 옵션을 정의하며 `value` 애트리뷰트와 내용을 갖는다. 양식을 제출하면 내용이 디스플레이되고 선택한 옵션의 값이 백엔드로 전송된다. 내용과 값이 일치할 필요는 없다. 예를 들어 `Apple` 옵션을 선택하면 `"Apple"` 값이 서버로 전송되고 `Banana` 옵션을 선택하면 `"123"` 값이 전송된다.

기본적으로 목록의 첫 번째 옵션이 선택된 경우 `selected` 애트리뷰트를 다른 옵션에 추가하여 변경할 수 있다.

```html
<select id="fruits" name="fruits">
    <option value="Apple">Apple</option>
    <option value="123">Banana</option>
    <option value="eprag" selected>Grape</option>
</select>
```

`size` 애트리뷰트를 사용하여 표시되는 값의 수를 지정할 수도 있다.

```html
<select id="fruits" name="fruits" size="2">
    <option value="Apple">Apple</option>
    <option value="123">Banana</option>
    <option value="eprag">Grape</option>
</select>
```

이제 여러 옵션을 볼 수 있으므로 여러 애트리뷰트를 사용하여 여러 옵션을 선택할 수 있다.

```html
<select id="fruits" name="fruits" size="2" multiple>
  <option value="Apple">Apple</option>
  <option value="123">Banana</option>
  <option value="eprag">Grape</option>
</select>
```

![](images/html-basics/multiselct.png)

#### `textarea` 요소
`<textarea>` 요소는 여러 줄을 텍스트를 입력할 수 있는 텍스트 입력 상자를 정의한다.

```html
<textarea name="message" rows="10" cols="30">
    Lorem ipsum dolor sit amet...
</textarea>
```

![textarea](images/html-basics/textarea.png)

`rows`와 `cols` 애트리뷰트는 입력 상자의 크기를 지정한다.

#### `<button>` 요소
`<button>` 요소는 클릭 가능한 버튼을 정의한다.

```html
<button type="button">Click Me!</button>
```

단추에는 타입 `button`이 있다. 이 단추를 클릭해도 아무 것도 작동하지 않는다. JavaScript 부분에서 이 단추를 사용하여 흥미로운 작업을 수행할 것이다.

#### `<fieldset>` 요소
`<fieldset>`은 서로 다른 입력 필드를 함께 그룹화할 수 있으며, `<legend>`는 해당 그룹에 이름을 선언할 수 있다.

```html
<form>
    <fieldset>
        <legend>Personalia:</legend>
        <label for="firstname">First Name:</label><br>
        <input type="text" id="firstname"><br>
        <br>
        <label for="lastname">Last Name:</label><br>
        <input type="text" id="lastname"><br>
    </fieldset>

    <fieldset>
        <legend>Add to Shopping Cart:</legend>
        <label for="fruits">Pick A Fruit:</label>
        <select id="fruits" name="fruits">
            <option value="Apple">Apple</option>
            <option value="123">Banana</option>
            <option value="eprag">Grape</option>
        </select><br>
        <br>
        <label for="vegetables">Pick A Vegetable:</label>
        <select id="vegetables" name="vegetables">
            <option value="098">Ginger</option>
            <option value="987">Kale</option>
            <option value="876">Eggplant</option>
        </select>
    </fieldset>
    <br>
    <button type="button">Click Me!</button>
</form>
```

![](images/html-basics/fieldset.png)

### `<input>` 요소에 가용한 기타 타입
방금 설명한 바와 같이 `<input>` 요소에는 다음 예와 같이 다양한 타입이 있다.

```html
<form>
    <fieldset>
        <legend>Input Type Text:</legend>
        <label for="name">Name:</label>
        <input type="text" id="name">
    </fieldset>

    <fieldset>
        <legend>Input Type Email:</legend>
        <label for="email">Enter your email:</label>
        <input type="email" id="email" name="email">
    </fieldset>

    <fieldset>
        <legend>Input Type Password:</legend>
        <label for="pswd">Password:</label>
        <input type="password" id="pswd">
    </fieldset>

    <fieldset>
        <legend>Input Type Radio:</legend>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
    </fieldset>

    <fieldset>
        <legend>Input Type Checkbox:</legend>
        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
        <label for="vehicle1"> I have a bike</label><br>
        <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
        <label for="vehicle2"> I have a car</label><br>
        <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">
        <label for="vehicle3"> I have a boat</label>
    </fieldset>

    <fieldset>
        <legend>Input Type Color:</legend>
        <label for="favcolor">Select your favorite color:</label>
        <input type="color" id="favcolor" name="favcolor">
    </fieldset>

    <fieldset>
        <legend>Input Type Date and Date Time:</legend>
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday"><br><br>
        <label for="birthdaytime">Birthday (date and time):</label>
        <input type="datetime-local" id="birthdaytime" name="birthdaytime">
    </fieldset>

    <fieldset>
        <legend>Input Type File:</legend>
        <label for="myfile">Select a file:</label>
        <input type="file" id="myfile" name="myfile">
    </fieldset>

    <fieldset>
        <legend>Input Type Number:</legend>
        <label for="quantity">Quantity (between 1 and 5):</label>
        <input type="number" id="quantity" name="quantity" min="1" max="5">
    </fieldset>

    <fieldset>
        <legend>Input Type Submit and Reset:</legend>
        <input type="submit">
        <input type="reset">
    </fieldset>
</form>
```

| 타입 | 설명 | 예 |
|-----|-----|----|
| Text | 한 줄 텍스트 입력 필드를 정의 | |
| Email | 이메일 주소를 포함해야 하는 입력 필드를 정의. 양식을 제출할 때 이메일 주소를 자동으로 확인할 수 있다. | |
| Password | 패스워드 필드를 정의 | |
| Radio | 라디오 버틍을 정의 | |
| Checkbox | 체크박스를 정의 | |
| Color | 색상을 포함해야 하는 입력 필드를 정의. 필드를 클릭하면 색상 선택기가 나타난다. | |
| Date와 Date Time | 날짜 및 시간 선택에 사용 | |
| File | 파일 업로드를 위한 파일 선택 필드와 "찾아보기" 단추를 정의 | |
| Number | 숫자 입력 필드를 정의 | |
| Submit | 양식 데이터를 제출하기 위한 단추를 정의 | |
| Reset | 모든 양식 값을 기본값으로 재설정하는 단추를 정의 | |

## HTML 엔터티
CSS의 주제로 넘어가기 전, HTML에 관한 온라인 기사를 만들고 있고, 브라우저가 HTML 태그를 디스플레이하기를 원하는 시나리오를 가정해 보자. `<p>HTML</p>`, 한 작업은 다음과 같다.

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>My HTML Tutorial</title>
</head>

<body>
    <h1>HTML Tutorial</h1>
    <p> <p>HTML</p> </p>
</body>

</html>
```

하지만 깨닫게 될 것이다, `<p>HTML</p>` 태그가 표시되는 대신 렌더링된다. 이 문제를 어떻게 해결할 수 있을까? HTML 문서에서 HTML 태그를 어떻게 표시할 수 있을까?

HTML의 일부 문자는 지정되어 있다. 이 문자를 디스플레이하려면 HTML 엔터티로 대체해야 한다. HTML 엔터티의 형식은 `&entity_name;` 또는 `&#entity_number;`이다. 일반적으로 사용되는 엔터티 중 하나는 깨지지 않는(non-breaking) 공백 `&nbsp;`이다. 단락(`<p>`)과 이 두 단락이 어떻게 동일한지에 대해 에 논의하는 중인 것을 기억하자.

```html
<p>This is a paragraph.</p>

<p>This           is a 
paragraph.</p>
```

이 예는 여러분에게 문제를 남긴다. 만약 두 단어 사이에 여러 개의 공간을 원한다면 HTML 엔티티 `&nbsp;`을 사용하는 것이다.

```html
<p>This is a paragraph.</p>

<p>This&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is a paragraph.</p>
```

![html-paragraph-2](images/html-basics/html-paragraph-2.png)

[여기](https://www.w3schools.com/html/html_entities.asp) W3Schools의 HTML 엔티티 목록이 있다.


**주** 이 포스팅은 [HTML Basics #2: Dig Deeper](https://www.ericsdevblog.com/posts/html-basics-2/)를 편역한 것임.
