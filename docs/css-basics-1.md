Cascading Style Sheet(CSS)는 HTML 문서의 프리젠테이션과 레이아웃을 제어하는 데 사용된다. 웹 페이지의 내용과 프리젠테이션 및 디자인을 분리하는 방법을 제공한다. CSS를 사용하면 개발자들은 글꼴, 색상, 크기, 간격, 위치 등의 스타일을 정의하고 웹 페이지의 여러 요소에 적용할 수 있다. 이를 통해 웹 사이트의 모양과 느낌을 보다 쉽게 관리하고 유지할 수 있을 뿐만 아니라 사용자에게 보다 일관된 환경을 제공할 수 있다. CSS는 별도의 파일로 작성되거나 HTML 문서 내에 내장될 수 있으며 HTML과 함께 작동하여 시각적으로 매력적이고 잘 설계된 웹사이트를 만든다.

CSS에서는 웹 페이지의 특정 요소 또는 요소 그룹에 적용할 수 있는 규칙 집합을 정의할 수 있다.

예를 들어, 다음 예제에서는 HTML 문서의 모든 `<h2>` 요소가 빨간색이어야 하며 텍스트의 크기는 `5em`이어야 한다. 여기서 `em`은 글꼴에 대한 상대적인 단위이며, `5em`은 원래 글꼴의 5배를 의미한다.

```css
h2 {
  color: red;
  font-size: 5em;
}
```

여기서 `h2`는 선택자(selector)라고 불리며, 스타일링할 HTML 요소를 선택한다. 그리고 "`propery : value`" 쌍의 형태로 중괄호 `{}`로 싸여 있는 일련의 선언들로 구성되어 있다.

## HTML 페이지에 CSS 적용
CSS 규칙을 HTML 문서에 세 방법으로 적용한다. 이 예를 고려해 보자.

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="author" content="Eric Hu">
  <title>My HTML Document</title>
</head>

<body>
  <h1>My HTML Document</h1>

  <h2>First Title</h2>
  <p>...</p>

  <h2>Second Title</h2>
  <p>...</p>
</body>

</html>
```

이제 CSS를 사용하여 두 번째 수준 제목(`<h2>`)에 빨간색으로 밑줄을 치고 싶다.

### 인라인 CSS
먼저 다음과 같이 HTML 요소의 애트리뷰트로 CSS 선언을 추가할 수 있다.

```html
<body>
  <h1>My HTML Document</h1>

  <h2 style="color: red; text-decoration: underline;">First Title</h2>
  <p>...</p>

  <h2>Second Title</h2>
  <p>...</p>
</body>
```

![inline-css](images/css-basics/inline-css.png)

보시다시피 첫 번째 제목만 변경된다. 따라서 둘 모두 변경하려면 두 제목에 `style` 애트리뷰트를 추가해야 한다. 큰 HTML 문서에는 시간이 많이 소요되는 작업이므로 CSS를 삽입하는 것은 권장하지 않는다. 

### 내부 CSS
다음과 같이 HTML 문서의 `<head>` 섹션에 CSS 코드를 넣을 수 있다.

```html
<head>
  <meta charset="utf-8" />
  <meta name="author" content="Eric Hu">
  <title>My HTML Document</title>

  <style>
    h2 {
      color: red;
      text-decoration: underline;
    }
  </style>

</head>
```

![internal-css](images/css-basics/internal-css.png)

보시다시피, 이번에는 두 제목이 모두 바뀌었다. 하지만 HTML과 CSS를 함께 넣었을 때의 단점은 웹 페이지가 커질수록 문서가 너무 커질 수 있어 향후 유지관리에 좋지 않다는 것이다.

### 외부 CSS
CSS와 HTML을 다루는 가장 좋은 방법은 별도의 파일로 만드는 것이고, HTML 문서에서 `<head>` 섹션에서 외부 CSS를 참조할 것이다. 예를 들어, 여기에는 `document.html`과 `style.css`라는 두 개의 문서가 있다.

`document.html`
```html
<head>
  <meta charset="utf-8" />
  <meta name="author" content="Eric Hu">
  <title>My HTML Document</title>

  <link rel="stylesheet" href="style.css">

</head>
```

`style.css`
```css
h2 {
  color: red;
  text-decoration: underline;
}
```

## Document Object Model
이 [HTML 기초 1 - 간략한 소개](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/html-basics-1/)에서 DOM(Document Object Model)의 개념을 이미 소개했다. DOM은 트리와 같은 구조를 갖고 있다. 모든 HTML 요소는 이 트리 구조의 노드이다.

브라우저가 문서를 표시할 때, 먼저 문서의 내용(HTML)을 읽고 DOM 트리로 변환한 다음, 스타일 정보(CSS)를 구문 분석하여 DOM 트리의 해당 노드에 할당한다. [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_works)의 다음 그래프는 이 프로세스를 보여준다.

![browser-parse-dom](images/css-basics/browser-parse-dom.png)

다른 예를 들어보자.

```html
<body>
  <h1>My HTML Document</h1>

  <div>
    <h2>First Heading</h2>
    <p>...</p>
  </div>

  <div>
    <h2>Second Heading</h2>
    <p>...</p>
  </div>

  <section>
    <h2>Third Heading</h2>
    <p>...</p>
  </section>
</body>
```

여기에 해당하는 DOM 트리가 있다. 서로 형제인 세 개의 블록 수준 요소(두  `<div>` 요소와 한 `<section>` 요소)가 있고, 각각의 요소에는 표제와 단락이 있음을 알 수 있다.

![dom-tree](images/css-basics/dom-tree.png)

`<div>` 블록에서 제목을 찾아 빨간색으로 만들고 `<section>` 블록의 제목을 찾아 파란색으로 만들려고 한다.

```css
div h2 {
  color: red;
}
section h2 {
  color: blue;
}
```

어렵지 않아 보인다. 먼저 `<div>` 요소를 모두 찾고 `<div>` 블록의 `<h2>` 제목을 찾은 다음 선언 섹션에서 색상을 빨간색으로 설정한다. 그런 다음 `<section>` 블록에 대해서도 동일하게 작업을 수행한다.

![](images/css-basics/locate-dom.png)

## HTML 노드를 선택하는 방법
그러나 HTML 문서가 커질수록 이 방법은 동일한 블록 수준 요소를 다른 용도로 사용할 가능성이 높고 모든 요소의 스타일이 동일하지 않기 때문에 문제를 일으킬 수 있다. 따라서 일반적으로 `class`와 `id` 애트리뷰트 선택자로 사용한다.

### `class`와 `id` 선택자
이는 작성자가 CSS에서 사용하는 가장 일반적인 유형의 선택자이다. 요소 그룹을 선택하거나 `class` 또는 `id`를 기반으로 특정 요소를 선택한다.

```css
/* Class Selector */
.box { . . . }

/* ID Selector */
#unique { . . . }
```

예이다.

```html

<body>
  <h1>My HTML Document</h1>

  <div class="first-class">
    <h2>First Heading</h2>
    <p>...</p>
  </div>

  <div id="uniqueID">
    <h2>Second Heading</h2>
    <p>...</p>
  </div>

  <section class="first-class">
    <h2>Third Heading</h2>
    <p>...</p>
  </section>
</body>
```

```css
.first-class h2 {
  color: red;
}

#uniqueID h2 {
  color: blue;
}
```

![class-id](images/css-basics/class-id.png)

보다시피, 같은 클래스에 다른 종류의 요소를 적용하는 것이 가능하다.

### 애트리뷰트 선택자
또한 특정 애트리뷰트의 존재 여부에 따라 요소를 선택할 수 있다.

```html

<body>
  <h2 id="firstHeading">First Heading</h2>
  <h2 id="secondHeading">Second Heading</h2>
  <h2>Third Heading</h2>
</body>
```

```css
h2[id] {
  color: red;
}
```

이 선택자는 `id` 애트리뷰트를 가진 모든 2단계 제목을 찾고 색상을 빨간색으로 설정한다.

![attribute-selector](images/css-basics/attribute-selector.png)

### 의사(pseudo) 클래스
마지막으로, CSS에는 특정 상태에 있을 때만 요소를 선택하는 의사 클래스라는 것이 있다. 예를 들어 `:hover` 의사 클래스는 커서가 요소 위에 있을 때만 요소를 선택한다. 브라우저에서 다음 예제를 사용해 보면 커서가 머리글 위를 가리키면 머리글이 빨간색으로 디스플레이된다.

```html
<body>
  <h2>First Heading</h2>
  <h2>Second Heading</h2>
  <h2>Third Heading</h2>
</body>
```

```css
h2:hover {
  color: red;
}
```

## 일반적으로 사용되는 CSS 속성

### Color
이 포스팅의 나머지 부분에서, 일반적으로 자주 사용되는 CSS 규칙 중 몇 가지를 살펴볼 것이다. color부터 시작하겠다.

CSS에서 색을 정의할 수 있는 네 방법이 있다. 가장 쉬운 방법은 다음과 같이 미리 정의된 색상 이름을 사용하는 것이다.

```html
<body>
  <div>
    <p>
      . . .
    </p>
  </div>
</body> 
```

```css
div {
  background-color: tomato;
}

p {
  color: white;
}
```

![background-color](images/css-basics/background-color.png)

색상은 다음과 같이 RGB 값, HEX 값 또는 HSL 값을 사용하여 지정할 수도 있다.

```html
<body>

  <div>
    <p>
      . . .
    </p>
  </div>

  <section>
    <p>
      . . .
    </p>
  </section>

</body>
```

```css
div {
  background-color: rgb(255, 99, 71);
}

section {
  background-color: #008080;
}

p {
  color: hsl(0, 0%, 100%);
}
```

![rgb-hex-hsl](images/css-basics/rgb-hex-hsl.png)

### 텍스트와 폰트
또한 CSS를 사용하여 텍스트를 커스터마이즈할 수 있다. 텍스트 색상을 설정하는 것 외에도 텍스트 장식 속성을 사용하여 장식을 추가하거나 제거할 수 있다.

```html
<body>
  <h1>First Heading</h1>

  <h2>Second Heading</h2>

  <h3>Third Heading</h3>
</body>
```

```css
h1 {
  text-decoration: overline;
}

h2 {
  text-decoration: line-through;
}

h3 {
  text-decoration: underline;
}
```

![text-decoration](images/css-basics/text-decoration.png)

또는 텍스트를 대문자, 소문자 또는 대문자 형식으로 변환할 수 있다.

```html
<body>
  <p class="uppercase">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <p class="lowercase">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <p class="capitalize">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</body>
```

```css
.uppercase {
  text-transform: uppercase;
}

.lowercase {
  text-transform: lowercase;
}

.capitalize {
  text-transform: capitalize;
}
```

![text-transform](images/css-basics/text-transform.png)

텍스트를 더 매력적으로 만들기 위해 다음과 같이 텍스트에 다양한 글꼴을 적용할 수 있다.

```html
<body>
  <p class="p1">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <p class="p2">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  <p class="p3">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</body>
```

```css
.p1 {
  font-family: "Courier New", Courier, monospace;
}

.p2 {
  font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande", "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
}

.p3 {
  font-family: Georgia, "Times New Roman", Times, serif;
}
```

![font-family](images/css-basics/font-family.png)

보시다시피, 여기서는 각 단락에 대해 여러 글꼴을 적용했다. 즉, 예비 시스템이 설치되어 있는지 확인한다. 첫 번째 글꼴을 사용할 수 없는 경우 브라우저는 일반 "형제"로 대체된다.

마지막으로 글꼴의 스타일, 무게 및 크기도 정의할 수 있다.

```css
.p1 {
  font-family: "Courier New", Courier, monospace;
  font-style: italic;
  font-weight: 100;
  font-size: 30px;
}

.p2 {
  font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
    "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
  font-style: normal;
  font-weight: 200;
  font-size: 20px;
}

.p3 {
  font-family: Georgia, "Times New Roman", Times, serif;
  font-style: oblique;
  font-weight: 300;
  font-size: 10px;
}
```

![fonts](images/css-basics/fonts.png)

### 링크
이전에 요소의 상태를 설명하는 의사 클래스에 대해 설명했다. 링크는 의사 클래스를 가장 자주 사용하는 경우이다. 단일 링크에는 상태가 네 가지 있을 수 있다.

- `a:link` - 방문하지 않은 일반 링크
- `a:visited` - 사용자가 방문한 링크
- `a:hover` - 사용자가 마우스를 링크 위로 이동했을 때 링크
- `a:active` - 클릭하는 순간의 링크

다음 네 가지 상태를 사용하여 다음과 같은 링크 스타일로 만들 수 있다.

```html
<body>
  <a href="#" target="_blank">This is a link.</a>
</body>
```

```css
/* unvisited link */
a:link {
  color: blue;
  text-decoration: underline;
}

/* visited link */
a:visited {
  color: green;
  text-decoration: underli;
}

/* mouse over link */
a:hover {
  color: red;
  text-decoration: overline;
}

/* selected link */
a:active {
  color: orange;
  text-decoration: underline;
}
```

![links](images/css-basics/links.png)

### 테이블
HTML 테이블의 모양도 CSS로 크게 개선할 수 있다. 일반적인 HTML 테이블은 다음과 같다.

```html
<body>
  <table>
    <tr>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Age</th>
    </tr>
    <tr>
      <td>Jill</td>
      <td>Smith</td>
      <td>50</td>
    </tr>
    <tr>
      <td>Eve</td>
      <td>Jackson</td>
      <td>94</td>
    </tr>
    <tr>
      <td>John</td>
      <td>Doe</td>
      <td>80</td>
    </tr>
  </table>
</body>
```

```css

table, td, th {
  border: 1px solid black;
}
```

![table-border](images/css-basics/table-border.png)

가장 먼저 알아차린 것은 테이블이 이중 테두리를 가지고 있다는 것이다. 그 이유는 테이블 자신(`<table>`)과 `<th>`와 `<td>` 요소 모두 별도의 경계를 가지고 있기 때문이다. `border-collapse` 속성을 사용하여 단일 경계로 축소할 수 있다.

```css
table {
  border-collapse: collapse;
}
```

![border-collapse](images/css-basics/border-collapse.png)

너비와 높이 속성을 사용하여 열/행 또는 전체 테이블의 크기를 설정할 수 있다.

```css
table {
  border-collapse: collapse;
  width: 100%;
}

th {
  height: 70px;
}
```

![border-height](images/css-basics/border-height.png)

테이블 셀의 얼라인먼트를 다음과 같이 정의할 수 있다.

```css
td {
  text-align: center;
}
```

![table-text-align](images/css-basics/table-text-align.png)

마지막으로 테이블의 배경색을 설정할 수 있다.

```css
th {
  height: 70px;
  background-color: coral;
  color: white;
}

td {
  text-align: center;
  background-color: gainsboro;
}
```

![table-background-color](images/css-basics/table-background-color.png)

### 양식과 버튼
이 섹션에서는 HTML 양식을 사용자 지정하는 방법에 대해 설명한다. 다음은 그 예이다.

```html
<body>
  <form>
    <label for="firstname">First Name:</label><br />
    <input type="text" id="firstname" /><br />
    <label for="lastname">Last Name:</label><br />
    <input type="text" id="lastname" /><br />
    <button>Submit</button>
  </form>
</body>
```

![form](images/css-basics/form.png)

이제 몇 가지 CSS 코드를 추가하여 보자.

```css
input[type=text] {
  width: 20%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}
```

![form-with-style](images/css-basics/form-with-style.png)

이 버튼은 여전히 좋아 보이지 않sms다. 다음과 같이 수정하여 보자.

```css
button {
  background-color: #4CAF50;
  color: white;
  border: none;
  padding: 10px 20px;
  text-align: center;
  font-size: 16px;
}
```

![button-with-style](images/css-basics/button-with-style.png)

마지막으로 한 단계 더 나아가 버튼에 테두리를 추가하고 의사 클래스를 사용하여 호버링할 수 있도록 설정한다.

```css
button {
  background-color: white;
  color: #4CAF50;
  border: solid #4CAF50;
  padding: 10px 20px;
  text-align: center;
  font-size: 16px;
  transition-duration: 0.2s;
}

button:hover {
  background-color: #4CAF50;
  color: white;
  border: solid #4CAF50;
  padding: 10px 20px;
  text-align: center;
  font-size: 16px;
}
```

![button-hover](images/css-basics/button-hover.png)

**주** 이 포스팅은 [CSS Basics #1: Getting Started](https://www.ericsdevblog.com/posts/css-basics-1/)를 편역한 것임.