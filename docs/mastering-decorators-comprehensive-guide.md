> Python 데코레이터로 코드를 장식하여 기능성, 재사용 가능성과 미관을 향상시킬 수 있다

## 데코레이터 소개
Python에서 데코레이터(decorator)는 소스 코드를 변경하지 않고 함수의 동작을 수정하거나 향상시키는 방법이다. 이들은 함수를 인수로 받아 원래 함수를 감싸는 새로운 함수를 반환하는 함수로 구현된다.

데코레이터는 로깅, 타이밍, 캐싱, 권한 부여와 유효성 검사같은 기능을 함수에 추가하는 데 사용할 수 있다. 그들은 비즈니스 로직과 교차 관심사를 분리함으로써 코드를 단순화하는 데 도움을 줄 수 있다.

```python
def my_decorator(func):
    def wrapper():
        print("Before function is called.")
        func()
        print("After function is called.")
    return wrapper

@my_decorator
def my_function():
    print("Hello, World!")

my_function()
```

위 예에서 `my_decorator`는 함수를 인수로 받아 원래 함수가 호출되기 전후에 일부 함수를 추가한 새 함수 `wrapper`를 반환하는 함수이다. `@my_decorator` 구문은 `my_function`에 데코레이터를 적용하는 데 사용된다.

## 함수 데코레이터
함수 데코레이터는 Python에서 가장 일반적인 데코레이터 타입이다. 이들은 다른 함수로 감싸서 함수의 동작을 수정하는 데 사용된다.

```python
import time

def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f"Function {func.__name__} took {end - start:.2f} seconds to run.")
        return result
    return wrapper

@timing_decorator
def my_function():
    time.sleep(2)

my_function()
```

위의 예에서 `timing_decorator`는 함수가 실행되는 데 걸리는 시간을 계산하는 함수 데코레이터이다. `wrapper` 함수는 시작 시간과 종료 시간을 얻어 함수의 수행 시간을 계산하고 인쇄한다. `@timing_decorator` 구문은 데코레이터를 `my_function`에 적용하는 데 사용된다.

## 클래스 데코레이터
클래스 데코레이터는 함수 데코레이터와 비슷하지만 함수 대신 클래스에 적용된다. 클래스를 다른 클래스로 묶어서 클래스의 동작을 수정하는 데 사용된다.

```python
def my_decorator(cls):
    class NewClass(cls):
        def new_method(self):
            print("New method added.")
    return NewClass

@my_decorator
class MyClass:
    def original_method(self):
        print("Original method called.")

obj = MyClass()
obj.original_method()
obj.new_method()
```

위의 예에서 `my_decorator`는 클래스에 새 메서드를 추가하는 클래스 데코레이터이다. `NewClass` 클래스는 새 메서드를 정의하며 원래 클래스 cls의 서브클래스이다. `@my_decorator` 구문은 데코레이터를 MyClass에 적용하는 데 사용된다.

## `__call__` 메소드를 갖는 데코레이터
데코레이터는 `__call__` 메서드가 있는 클래스를 사용하여 만들 수도 있다. 이 메서드를 사용하면 클래스를 함수처럼 호출할 수 있으며 인수를 사용하는 데코레이터를 만들 수 있다.

```python
class my_decorator:
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2
        
    def __call__(self, func):
        def wrapper(*args, **kwargs):
            print(f"Arguments passed to decorator: {self.arg1}, {self.arg2}")
            result = func(*args, **kwargs)
            return result
        return wrapper

@my_decorator("Hello", "World")
def my_function():
    print("Hello, World!")

my_function()
```

위의 예에서 `my_decorator`는 생성자에서 두 개의 인수를 사용하고 `__call__` 메서드를 구현하여 데코레이터를 만드는 클래스이다. `wrapper` 함수는 원래 함수를 호출하기 전에 데코레이터에게 전달된 인수를 인쇄한다. `@my_decorator("Hello", "World")` 구문을 사용하여 `my_function`에 데코레이터를 적용한다.

## `wraps`로 메타데이터 보존
데코레이터를 만들 때 이름, 문서 문자열과 매개 변수같은 원래 함수의 메타데이터를 보존하는 것이 중요하다. 이 작업은 `functools` 모듈의 `wraps` 함수를 사용하여 수행할 수 있다.

```python
from functools import wraps

def my_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print("Before function is called.")
        result = func(*args, **kwargs)
        print("After function is called.")
        return result
    return wrapper

@my_decorator
def my_function():
    """This is the docstring of my_function."""
    print("Hello, World!")

print(my_function.__name__)
print(my_function.__doc__)
```

위의 예에서 `@wraps(func)` 구문은 원래 함수 `func`의 메타데이터를 보존하기 위해 사용된다. `my_function.__name__`과 `my_function.__doc__`문은 원래 함수의 이름과 docstring을 인쇄한다.

## 인수가 지정된 데코레이터 만들기
데코레이터는 또한 그들의 동작을 수정하기 위해 인수를 사용할 수 있다. 이것은 다양한 사용 사례에 맞게 커스터마이징할 수 있는 일반 데코레이터를 만드는 데 유용할 수 있다.

```python
def repeat(num):
    def my_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for i in range(num):
                print(f"Function {func.__name__} called {i+1} times.")
                result = func(*args, **kwargs)
            return result
        return wrapper
    return my_decorator

@repeat(num=3)
def my_function():
    print("Hello, World!")

my_function()
```

위의 예에서 `repeat` 함수는 함수가 호출되어야 하는 횟수를 지정하는 인수 `num`을 사용한다. `my_decorator` 함수는 `repeat` 함수 내부에 정의되며 `wrapper` 함수를 사용하여 원래 함수를 `num`번 실행한다.

## 함수와 클래스 모두에서 작동하는 데코레이터 만들기
대부분의 데코레이터는 함수을 사용하도록 설계되었지만 클래스를 사용하는 데코레이터도 만들 수 있다. 클래스의 동작이나 클래스 메소드를 수정하려는 경우 유용하다.

또한 데토레이트할 객체의 타입을 확인하여 함수와 클래스를 모두 사용하는 데코레이터를 작성할 수 있다.

```python
import time

def timer(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Function {func.__name__} took {end_time - start_time:.2f} seconds to run.")
        return result
    if isinstance(func, type):
        # If the decorated object is a class, decorate all its methods
        for attr_name in dir(func):
            attr = getattr(func, attr_name)
            if callable(attr):
                setattr(func, attr_name, timer(attr))
        return func
    else:
        # If the decorated object is a function, decorate it directly
        return wrapper

@timer
class TestClass:
    def method1(self):
        time.sleep(1)

    def method2(self):
        time.sleep(2)

@timer
def my_function():
    time.sleep(3)

TestClass().method1()
TestClass().method2()
my_function()
```

위의 예에서 `timer` 데코레이터를 사용하여 함수와 클래스 메소드를 모두 데코레이트할 수 있다. 데코레이트된 객체가 클래스일 경우, 루프를 사용하여 데코레이터는 모든 메소드에 적용한다.  데코레이트된 객체가 함수라면, 데코레이터는 `wrapper` 함수를 이용해 직접 적용한다.

## 데코레이터들을 순서로 연결
또한 데코레이터들을 체인으로 연결하여 함수 또는 클래스에 여러 수정사항을 적용할 수 있다. 그러나 각 데코레이터가 이전 데코레이터의 동작을 수정할 수 있으므로 데코레이터를 적용하는 순서가 중요하다.

```python
def decorator1(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print("Decorator 1")
        result = func(*args, **kwargs)
        return result
    return wrapper

def decorator2(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print("Decorator 2")
        result = func(*args, **kwargs)
        return result
    return wrapper

@decorator1
@decorator2
def my_function():
    print("Hello, World!")

my_function()
```

위의 예에서 `decorator2` 데코레이터를 먼저 적용한 후 `decorator1` 데코레이터를 적용한다. `my_function` 함수가 호출될 때.

이러한 데코레이터 적용 순서는 *데코레이터 순서*로 알려져 있으며, 데코레이터는 코드에 나타나는 순서의 역순으로 적용된다는 점을 기억해야 한다.

이것은 다음과 같은 함수 호출 형식으로 표현될 수 있다.

```python
    decorator1(decorator2(my_function))()
```

이것은 `my_function`을 인수로 하여 `decorator2` 함수를 실행한 다음, 결과를 `decorator1` 함수에 전달한다. 최종 결과는 빈 괄호 `()` 구문으로 호출할 수 있는 데코레이트된 함수이다.


**(주)** 위 내용은 [Mastering Decorators in Python: A Comprehensive Guide with Examples](https://satish-p.medium.com/mastering-decorators-in-python-a-comprehensive-guide-with-examples-165f46006351)을 편역한 것이다. 
