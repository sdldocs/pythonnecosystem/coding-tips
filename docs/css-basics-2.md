이 포스팅에서는 CSS를 사용하여 다양한 HTML 요소를 배치하는 방법과 웹 페이지에 대한 응답 레이아웃을 만드는 방법에 초점을 맞춘다.

## CSS에서 박스 이해
CSS는 모든 HTML 요소를 상자처럼 처리하며 각 상자에는 테두리(border), 여백(margin)과 패딩의 세 구성 요소가 있다. 테두리는 상자 자체, 여백은 상자 주위의 공간, 패딩은 상자와 내용물 사이의 공간으로 볼 수 있다.

![css-margin-and-padding](images/css-basics/css-margin-and-padding.png)

예를 들어보자.

### 테두리(border)
```html

<body>
  <div class="box1">
    <p>...</p>
  </div>

  <div class="box2">
    <p>...</p>
  </div>
</body>
```

```css
.box1 {
  border-style: double;
}

.box2 {
  border-style: dashed;
} 
```

![css-border](images/css-basics/css-border.png)
여기에서 사용할 수 있는 다른 테두리 속성들이  있다. 예를 들어 `border-width`는 테두리 두께를 정의하고 `border-color`은 테두리 색상을 정의할 수 있다. 이러한 테두리 속성은 `border-style` 속성을 설정하지 않으면 적용되지 않는다.

### 여백(margin)
```css
.box1 {
  border-style: double;
  margin: 10px;
}

.box2 {
  border-style: dashed;
  margin: 20px;
} 
```

![css-margin](images/css-basics/css-margin.png)

### 패딩(padding)
```css
.box1 {
  border-style: double;
  margin: 10px;
  padding: 20px;
}

.box2 {
  border-style: dashed;
  margin: 20px;
  padding: 40px;
} 
```

![css-padding](images/css-basics/css-padding.png)

아마 알고 있겠지만, 상자에 패딩을 추가한 후 상자와 내용물 사이의 수직 공간이 수평 공간보다 큽니다. 이는 모든 인라인 요소(이 경우에는 `<p>` 요소)를 정의하지 않았더라도 기본 수직 여백이 있기 때문이다.

## CSS에서의 위치, 오버플로우와 정렬

### `position` 속성
`position` 속성은 요소에 사용되는 위치 지정 메서드의 타입을 지정한다. 5 종류 위치 메서드을 사용할 수 있다.

- `static`
- `relative`
- `fixed`
- `absolute`
- `sticky`

`top`, `bottom`, `left` 및 `right` 속성을 사용하여 요소를 배치한다. 그러나 위치 메서드를 먼저 설정하지 않으면 이러한 속성이 작동하지 않는다. 또한 `position` 값에 따라 다르게 작동한다.

`static` 메서드는 기본 옵션이며 `static` 메서드가 있는 요소는 특별한 방식으로 배치되지 않는다.

`relative` 방법은 정상(`static`) 위치를 기준으로 상대적 위치에 요소를 배치한다. 즉, `top`, `bottom`, `left` 및 `right` 속성을 추가하여 해당 위치를 조정할 수 있다.

```html
<body>
  <div class="div-static">This div element has position: static;</div>
  <div class="div-relative">This div element has position: relative;</div>
</body>
```

```css
.div-static {
  border: solid;
  position: static;
}

.div-relative {
  border: solid;
  position: relative;
  left: 5px;
  top: 10px;
} 
```

![relative-position](images/css-basics/relative-position.png)

`fixed` 메서드는 뷰포트에 상대적인 고정 위치에 요소를 배치한다. 예를 들어, 스크린/브라우저의 오른쪽 하단 모서리에 나타나는 광고는 모두 `fixed` 위치를 갖는다. 왜냐하면 페이지를 스크롤하더라도 항상 오른쪽 하단 모서리에 항상 머물기기 때문이다.

`sticky` 메서드는 `relative`와 `fixed` 메서드의 조합이다. 기본적으로 스티키 요소는 뷰포트에서 지정된 간격이 충족될 때까지 상대적으로 위치한다. 그러면 해당 요소는 그 위치에 "붙을" 것이다.

여기서 `fiexed`와 `sticky` 메서드를 시연하는 것은 어렵지만, 관심이 있다면 [w3schools](https://www.w3schools.com/css/css_positioning.asp)에서 예를 찾을 수 있다.

마지막으로, `absolute` 메서드가 있다. 절대 요소는 가장 가까운 위치에 있는 요소(앵커)에 상대적으로 위치한다. 위치한 요소는 `static` 위치가 아닌 요소이다. 예를 들어, 다음 예에서 `absolute` 요소는 `relative` 요소에 상대적으로 위치한다.

```html
<body>
  <div class="relative">
    This div element has position: relative;
    <div class="absolute">This div element has position: absolute;</div>
  </div>
</body>
```

```css
.relative {
  border: solid;
  position: relative;
  width: 400px;
  height: 200px;
}

.absolute {
  border: solid;
  position: absolute;
  top: 80px;
  right: 0;
}
```

![absolute-position](images/css-basics/absolute-position.png)

### `overflow` 속성
`overflow` 속성은 내용이 너무 커서 상자에 들어가지 않을 때 발생하는 작업을 제어한다. 기본값은 `visible` 이다. 오버플로우는 상자 밖에 렌더링한다.

```css
div {
  border-style: solid;
  width: 200px;
  height: 50px;
  overflow: visible;
}
```

![scroll-overflow](images/css-basics/scroll-overflow.png)

`hidden` 옵션의 경우 내용이 잘리고 오버플로우는 디스플레이되지 않는다.

```css
div {
  border-style: solid;
  width: 200px;
  height: 50px;
  overflow: hidden;
}
```

![hidden-overflow](images/css-basics/hidden-overflow.png)

값을 `scroll`로 설정하면 오버플로우가 잘리고 스크롤 바가 추가되어 상자 안에서 스크롤링할 수 있다. 이렇게 하면 (필요하지 않은 경우에도) 스크롤 바가 수평과 수직으로 추가된다.

```css
div {
  border-style: solid;
  width: 200px;
  height: 50px;
  overflow: scroll;
} 
```

![scroll-overflow](images/css-basics/scroll-overflow.png)

마지막으로 `auto` 옵션은 `scroll`과 매우 유사하지만 필요한 경우에만 스크롤 바를 추가한다.

```css
div {
  border-style: solid;
  width: 200px;
  height: 50px;
  overflow: auto;
} 
```

![auto-overflow](images/css-basics/auto-overflow.png)

### CSS에서 정렬

- 가운데 정렬 박스

`margin: auto;`를 사용하여 다음과 같이 블록 요소를 수평으로 중앙에 배치할 수 있다.

```css
div {
  border-style: solid;
  width: 200px;
  margin: auto;
} 
```

![centered-div](images/css-basics/centered-div.png)

먼저 `<div>` 요소의 폭을 설정하여 박스 가장자리까지 확장되지 않도록 한다. 그러면 요소가 지정된 너비를 차지하고 나머지 공간은 두 여백 사이에 균등하게 분할된다.

- 가운데 정렬 텍스트

상자가 가장자리까지 뻗어 내용물/텍스트 센터를 상자 안에 넣으려면 어떻게 해야 할까? 텍스트 중심을 수평으로 맞추려면 `text-align: center;`를 사용할 수 있다. 예를 들어 보겠다.

```css
div {
  border-style: solid;
  text-align: center;
}
```

![center-text](images/css-basics/center-text.png)

텍스트의 중심을 수직으로 맞추려면 가장 쉬운 방법은 위쪽과 아래쪽 패딩/마진을 사용하는 것이다.

```css
div {
  border-style: solid;
  text-align: center;
  padding-top: 50px;
  padding-bottom: 50px;
}
```

![vertical-align](images/css-basics/vertical-align.png)

또한 위치와 변환 또는 플렉스박스를 사용하여 요소를 위치시킬 수 있지만, 그것은 CSS에서 약간 더 발전된 주제이다. 다음 포스팅에서 어떻게 작동하는지 살펴보겠다.

- 왼쪽과 오른쪽 정렬

때때로 상자를 가운데에 맞추는 대신 페이지의 왼쪽이나 오른쪽에 붙이기를 원할 수 있다. 이를 위해 절대 위치를 사용할 수 있다.

```html
<body>
  <div class="left">
    This div block should stick to the left.
  </div>

  <div class="right">
    This div block should stick to the right.
  </div>
</body>
```

```css
div {
  border-style: solid;
  width: 300px;
}

.left {
  position: absolute;
  left: 0px;
}

.right {
  position: absolute;
  right: 0px;
}
```

![left-right-align](images/css-basics/left-right-align.png)

## 그리드 레이아웃 탐사
그리드 시스템은 CSS에서 매우 중요한 개념으로 플로트나 위치를 사용하지 않고도 웹 페이지를 설계할 수 있도록 도와준다. 그리드 시스템은 하나의 상위 요소와 다음과 같은 여러 하위 요소로 구성된다.

```html
<div class="grid-container">
  <div class="grid-item">1</div>
  <div class="grid-item">2</div>
  <div class="grid-item">3</div>
  . . .
</div> 
```

```css
.grid-container {
  display: grid;
}
```

`class="grid-container"`를 갖는 모든 요소를 **그리드 컨테이너**로 설정하고 그리드 컨테이너의 모든 직접 자식 요소는 자동으로 **그리드 항목**이 된다.

그리드 항목의 수직선을 *열*이라고 하며, 그리드 항목의 수평선을 *행*이라고 합니다. 각 열/행 사이의 공백을 *간격*이라고 한다.

![grid](images/css-basics/grid.png)

그리드 간격은 다음 속성 중 하나를 사용하여 조정할 수 있다.

- `grid-column-gap`
- `grid-row-gap`
- `grid-gap`

```css
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 50px 100px;
} 
```

예를 들어, 이 경우 열 간격은 100px이고 행 간격은 50px이다. 이 예에서는 그리드 템플릿 열 속성을 추가해야 한다. 이 항목의 의미에 대해 나중에 설명하겠다.

여러 열 또는 행에 걸쳐 한 그리드 항목만 설정할 수도 있다.

```html
<div class="grid-container">
  <div class="grid-item1">1</div>
  <div class="grid-item2">2</div>
	. . .
</div> 
```

```css
.grid-item1 {
  grid-column-start: 1;
  grid-column-end: 3;
}

.grid-item2 {
  grid-column-start: 3;
  grid-column-end: 5;
  grid-row-start: 1;
  grid-row-end: 3;
}
```

![col-row](images/css-basics/col-row.png)

![grid-span](images/css-basics/grid-span.png)

위 그림의 숫자에 주목하여라. 그리드 항목 1의 경우, 1행에서 시작하여 3행 이전에 끝난다.

### 그리드 컨테이너

#### `grid-template-columns`
`grid-template-columns` 속성은 그리드 레이아웃의 열 갯수를 정의하며, 이 속성을 사용하여 각 열의 너비를 정의할 수도 있다.

```html
<div class="grid-container">
  <div class="grid-item">1</div>
  <div class="grid-item">2</div>
</div>
```

```css
.grid-container {
  display: grid;
  grid-template-columns: 150px 100px;
} 
```

![grid-template-columns-150px-100px](images/css-basics/grid-template-columns-150px-100px.png)

이 예에서는 두 개의 열을 정의했는데, 첫 번째 열은 `150px`이고 두 번째 열은 `100px`이다. 이 두 열 그리드에 두 개 이상의 항목이 있는 경우 항목을 넣을 새 행이 자동으로 추가된다.

![grid-template-columns-multi-row](images/css-basics/grid-template-columns-multi-row.png)

열의 너비를 자동으로 설정할 수도 있다.

```css
.grid-container {
  display: grid;
  grid-template-columns: 150px auto;
}
```

![grid-template-columns-auto](images/css-basics/grid-template-columns-auto.png)

두 열 모두 `width="auto"`이면 너비가 같아야 한다.

```css
.grid-container {
  display: grid;
  grid-template-columns: auto auto;
} 
```

![grid-template-columns-auto-auto](images/css-basics/grid-template-columns-auto-auto.png)

#### `grid-template-rows`
`grid-template-rows` 속성은 각 행의 높이를 설정한다.

```css
.grid-container {
  display: grid;
  grid-template-columns: auto auto;
  grid-template-rows: 100px 200px;
}
```

![](images/css-basics/grid-template-rows.png)

#### `justify-content`
`justify-content` 속성은 컨테이너 내부의 전체 그리드를 수평으로 정렬하는 데 사용된다. 이 속성을 적용하려면 그리드의 총 너비(모든 열 결합)가 다음과 같이 컨테이너의 너비보다 작아야 한다,

![justify-content](images/css-basics/justify-content.png)

`justify-content` 속성을 사용하여 열을 다르게 정렬할 수 있다.

```css
.grid-container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  justify-content: center;
}
```

![justify-content-center](images/css-basics/justify-content-center.png)

```css
.grid-container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  justify-content: space-evenly;
} 
```

![justify-content-even](images/css-basics/justify-content-even.png)

```css
.grid-container {
  display: grid;
  grid-template-columns: 100px 100px 100px;
  justify-content: end;
} 
```

![justify-content-end](images/css-basics/justify-content-end.png)

#### `align-content`
`align-content` 속성은 컨테이너 내부의 전체 그리드를 수직으로 정렬하는 데 사용되며 `justify-content` 속성과 동일하게 작동한다.

```css
.grid-container {
  display: grid;
  align-content: space-evenly;
} 
```

![align-content](images/css-basics/align-content.png)

### 그리드 항목

#### `grid-column`
`grid-column` 속성은 `grid-column-start` 속성과 `grid-column-end` 속성의 단축형 속성이다. CSS가 항목을 배치하는 데 사용할 열을 정의한다.

```css
.grid-item1 {
  grid-column: 1 / 3;
} 
```

![grid-column](images/css-basics/grid-column.png)

이 예에서 첫 번째 항목은 열 1에서 시작하여 열 3보다 먼저 끝난다.

#### `grid-row`
`grid-row` 속성은 `grid-row-start` 속성과 `grid-row-end` 속성의 단축형 속성이다. 항목을 배치하는 데 사용할 CSS 행을 정의한다.

```css
.grid-item1 {
  grid-column: 1 / 3;
  grid-row: 1 / 3;
}
```

![grid-row](images/css-basics/grid-row.png)

#### `grid-area`
`grid-area` 속성은 `grid-row-start`, `grid-column-start`, `grid-row-end`와 `grid-column-end` 속성을 결합한 간단한 특성이다.

.grid-item1 {
  grid-area: 1 / 1 / 3 / 3;
}

이 CSS 규칙은 항목 1이 행 1과 열 1에서 시작하여 행 3과 열 3보다 먼저 끝나도록 지정한다.

## 응답형 레이아웃 만들기
아시다시피 디자인하는 웹 페이지는 모든 종류의 기기, 컴퓨터, 태블릿, 휴대폰에서 접근할 수 있어야 하는데, 크기가 모두 다르다. 이는 한 장치에서 잘 보이는 레이아웃이 다른 장치에 비해 너무 크거나 작다는 것을 의미한다.

응답형 레이아웃은 이 문제를 해결하기 위해 설계되었다. 다양한 화면에 적응할 수 있는 단일 레이아웃을 만들 수 있는 웹 디자인 개념이다. 응답형 레이아웃을 이해하려면 먼저 그리드 뷰와 미디어 쿼리에 대해 이야기해야 한다.

### 그리드 뷰
그리드 뷰의 개념은 웹 페이지를 설계할 때 항상 전체 페이지를 열(일반적으로 12열)로 나눈다는 것을 의미한다. 이러한 열은 전체 너비가 100%이며 브라우저 창 크기를 조정할 때 축소 또는 확장된다.

먼저 모든 HTML 요소에 `box-sizing` 속성이 `border-box`로 설정되어 있는지 확인해야 한다. 이는 패딩과 테두리가 요소의 총 너비 및 높이 계산에 포함되는지 확인하기 위한 것이다.

```css
* {
  box-sizing: border-box;
}
```

페이지의 전체 너비가 100%이고 페이지에 열이 12개인 것으로 알고 있다. 한 열의 백분율을 계산한다. `100% / 12 = 8.33%`.

그런 다음 이 페이지의 레이아웃을 정의할 수 있다.

```css
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;} 
```

`col-1`은 이 요소가 첫번째 열을 사용하고, `col-2`는 두번째 열을 사용한다는 것을 의미한다.

이 모든 열은 왼쪽으로 이동해야 하며(다음 포스팅에서 부동에 대해 설명함) 패딩이 있어야 한다.

```css
[class*="col-"] {
  float: left;
  padding: 15px;
} 
```

다음 레이아웃을 사용한 예를 살펴보자.

```html
<div class="row">
  <div class="col-3">Menu</div> <!-- 25% -->
  <div class="col-9">Main</div> <!-- 75% -->
</div>
```

![responsive-layout](images/css-basics/responsive-layout.png)

### 미디어 쿼리
미디어 쿼리는 응답형 설계의 또 다른 핵심 개념이다. `@media` 규칙을 사용하여 특정 조건이 충족되는 경우에만 CSS 속성 블록을 포함한다. 이제 데스크탑, 태블릿 및 휴대폰을 위한 다양한 레이아웃을 설계할 수 있다. 가장 쉬운 휴대폰부터 시작해 보겠다.

```css
/* For mobile phones: */
[class*="col-"] {
  width: 100%;
} 
```

휴대폰의 화면이 매우 작기 때문에, 모든 요소가 화면 너비의 100% 차지하기를 원한다. 그런 다음 태블릿의 레이아웃을 추가할 수 있지만, 이번에는 중단점(breakpoint)을 설정해야 한다.

```css
/* For mobile phones: */
[class*="col-"] {
  width: 100%;
}

@media only screen and (min-width: 600px) {
  /* For tablets: */
  .col-s-1 {width: 8.33%;}
  .col-s-2 {width: 16.66%;}
  .col-s-3 {width: 25%;}
  .col-s-4 {width: 33.33%;}
  .col-s-5 {width: 41.66%;}
  .col-s-6 {width: 50%;}
  .col-s-7 {width: 58.33%;}
  .col-s-8 {width: 66.66%;}
  .col-s-9 {width: 75%;}
  .col-s-10 {width: 83.33%;}
  .col-s-11 {width: 91.66%;}
  .col-s-12 {width: 100%;}
} 
```

즉, 화면 너비가 `600px`보다 크면 두 번째 규칙 집합이 활성화된다.

마지막으로 동일한 방법으로 데스크탑 레이아웃을 정의할 수 있다.

```css
/* For mobile phones: */
[class*="col-"] {
  width: 100%;
}

@media only screen and (min-width: 600px) {
  /* For tablets: */
  .col-s-1 {width: 8.33%;}
  .col-s-2 {width: 16.66%;}
  .col-s-3 {width: 25%;}
  .col-s-4 {width: 33.33%;}
  .col-s-5 {width: 41.66%;}
  .col-s-6 {width: 50%;}
  .col-s-7 {width: 58.33%;}
  .col-s-8 {width: 66.66%;}
  .col-s-9 {width: 75%;}
  .col-s-10 {width: 83.33%;}
  .col-s-11 {width: 91.66%;}
  .col-s-12 {width: 100%;}
}

@media only screen and (min-width: 768px) {
  /* For desktop: */
  .col-1 {width: 8.33%;}
  .col-2 {width: 16.66%;}
  .col-3 {width: 25%;}
  .col-4 {width: 33.33%;}
  .col-5 {width: 41.66%;}
  .col-6 {width: 50%;}
  .col-7 {width: 58.33%;}
  .col-8 {width: 66.66%;}
  .col-9 {width: 75%;}
  .col-10 {width: 83.33%;}
  .col-11 {width: 91.66%;}
  .col-12 {width: 100%;}
} 
```

마지막 두 규칙 집합은 거의 동일하며, 유일한 차이점은 이름이다. 하나는 `col`로 시작하고 다른 하나는 `col-s`로 시작한다. 만약 그들이 같다면 왜 이것을 할까? 각 요소가 각 중단점에서 사용할 열 수를 결정할 수 있기 때문이다.

```html
<div class="row">
  <div class="col-3 col-s-6">...</div>
</div> 
```

이 `<div>` 요소는 데스크탑에서 3열을 사용하지만 태블릿 모드에서는 6열을 사용한다.

**주** 이 포스팅은 [CSS Basics #2](https://www.ericsdevblog.com/posts/css-basics-2/)를 편역한 것임.