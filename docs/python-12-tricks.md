12 Python 팁과 트릭을 소개한다. 이 트릭들로 여러분의 귀중한 시간을 절약해 줄 수 있을 것이다.

## 1. Get 메소드
이를 사용하면 딕셔너리에서 키가 없는 경우 키 오류로부터 사용자를 보호할 수 있다. Bracket 메서드 `[]` 대신 get 메서드를 사용하면 KeyError 대신 None을 반환한다.

```python
mydict = {1: "Python", 2: "JavaScript", 3: "Dart"}
# Default Method
mydict[4] # KeyError
# Get Method
mydict.get(4) # None
```

## 2. Zip 함수
`zip()` 함수는 Python의 또 다른 중요한 기능이다. 다음 예와 같이 동일 수준에서 여러 리스트 반복하는 것은 유용하다. 

```python
firstname = ["John", "Diana", "Christopher"]
lastname = ["David", "Logan", "Nolan"]
#z ip function
for f, l in zip(firstname, lastname):
    print(f, l)

# output:
John David
Diana Logan
Christopher Nolan
```

또한 zip 함수는 두 리스트를 딕셔너리 형식으로 변환하는 데 매우 유용하다.

```python
firstname = ["John", "Diana", "Christopher"]
lastname = ["David", "Logan", "Nolan"]
mydict = dict(zip(firstname, lastname))
print(mydict) # {'John': 'David', 'Diana': 'Logan', 'Christopher': 'Nolan'}
```

## 3. 리스트 슬라이싱
이 트릭은 `[]`으로 리스트를 슬라이싱하는 방법을 보인다. 아래 코드처럼 세 파라미터 `[Start: Stop: Step]`을 사용한다.

```python
mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9]

# example 1
print(mylist[1: 9: 2]) # [2, 4, 6, 8]

# example 2
print(mylist[3: 9: 3]) # [4, 7]
```

## 4. 딕셔너리 병합
이 트릭은 두 딕셔너리를 하나로 통합하는 중요한 방법이다. 딕셔너리의 `update` 메소드를 사용하여 두 번째 딕셔너리을 첫 번째 딕셔너리에 병합할 수 있다.
```python
def merge_dict(dict1, dict2):
    return(dict1.update(dict2))

mydict1= {1: "Python", 2: "JavaScript"}
mydict2= {3: "C++", 4: "Dart"}
merge_dict(mydict1, mydict2)
print(mydict1)

#output: 
{1: 'Python', 2: 'JavaScript', 3: 'C++', 4: 'Dart'}
```

## 5. Lambda 함수
lambda는 또 다른 중요한 방법이다. 이것을 사용하면 아래와 같이 한 라인 코드 함수를 만들 수 있다. 

```python
#example 1 -- multiple of 2 
num = 4
data = lambda x : x * 2
print(data(2)) # 4

#example 2 -- Check even
data = lambda x : x % 2 == 0
print(data(4)) #True
```

## 6. Passeord 입력
이 방법을 사용하면 암호 형식으로 사용자 입력을 받을 수 있다. Python은 getpass라는 이름의 내장 라이브러리를 갖고 있었다. 이것을 사용하면 echo없이 입력을 받을 수 있다.

```python
import getpass
userinput = getpass.getpass("Enter the Password: ")
print(userinput)
```

## 7. 빠른 리스트 뒤집기
이 방법을 사용하면 짧은 코드 수로 함수를 사용하지 않고 리스트를 빠르게 되돌릴 수 있다. 리스트에서 `[]` 사용법을 기억하는가? 이를 사용하여 리스트를 뒤집을 수 있다.

```python
mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#reversing list
print(mylist[::-1]) # [9, 8, 7, 6, 5, 4, 3, 2, 1]
```

## 8. Temp없이 변수 교환(sewapping)
변수 교환은 모든 프로그램에서 사용하는 것으로, 이 트릭은 변수를 임시 변수나 세 번째 변수 없이 교환하는 방법을 보여준다. 

```python
# Default method
a = 1
b = 2
temp = a
a = b
b = temp
print(a, b) #2 1

# Good method
a,b = b,a
print(a, b) #2 1
```

## 9. 빠른 팰린드롬 확인
이는 팰린드롬을 빨리 확인할 수 있는 방법이다. 일반적으로 팰린드롬을 확인하기 위해 루프를 사용한다. 아래 방법은 루프 없이 동작한다.

```python
def isPalindrome(string):
    return string == string[::-1]

print(isPalindrome("maham")) # True
print(isPalindrome("echo")) # False
```

## 10. 데이터 필터링
필터링 내장 함수는 변수 또는 모든 데이터 구조에서 데이터를 필터링하는 데 유용하다.

```python
mylist = [1, 2, 3, 4, 5, 6]
newlist = list(filter(lambda x: (x % 2 == 0), mylist))
print(newlist) # [2, 4, 6]
```

## 11. 다중 결과 반환
C++, Java와 같은 많은 프로그래밍 언어에서 함수는 하나의 결과만 반환할 수 있다. 그러나 Python은 여러 결과를 한번에 반환할 수 있다.

```python
def fun():
    return 1, 2, 3
print(fun()) # (1, 2, 3)
```

## 12. 쉬운 문자열 결합
문자열 연결에 루프를 사용하지 말고, 이 방법을 사용하여 문자열 리스트를 쉽게 연결할 수 있다.

```python
mylist = ["Learn", "Python", "Programming Language"]
#concatenate the list into one string
print(" ".join(mylist)) # Learn Python Programming Language
```

## 마치며
위 팁과 트릭들이 프로그래밍에 도움이 되고 재미 있기를 바란다. 더 많은 방법을 계속 탐구해여 코드 작업 시간을 단축하고 코드를 짧은 시간에 수행할 수 있도록 하여야 한다.


**(주)** 위 내용은 [12 Python Tricks To Make Your Life Easier](https://levelup.gitconnected.com/12-python-tricks-to-make-your-life-easier-b4a88e4c6767)을 편역한 것이다.
