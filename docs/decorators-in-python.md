## 객체 지향 프로그래밍의 데코레이터 패턴 대 파이썬의 데코레이터 패턴:
이름을 보면 Python의 데코레이터들이 데코레이터 패턴의 구현이라고 헷갈릴 수 있다. 하지만, 그렇지 않다. 

**먼저 데코레이터 패턴이 무엇인지 정의해 보자** 

데코레이터 패턴은 aggregation/composition을 사용하여 런타임에 클래스 인스턴스에 동작을 추가하는 구조적 패턴이다. 클래스를 새로 작성할 필요 없이 한 계층의 동작을 다른 계층 위에 추가하는 것으로 간주할 수 있다. 간단한 예는 [이 리포지터리](https://github.com/VarshaChahal/Java-Design-Patterns)를 참조하시오.

파이썬의 데코레이터는 데코레이트된 기능이나 방법을 수정하지 않고 동작을 추가하는 동일한 개념을 사용하지만 다른 방식으로 수행한다. 그들은 데코레이터 패턴의 구현이 아니라 어플리케이션이다. 데코레이터는 Python에서 한 줄의 코드만으로 함수에 동작을 추가할 수 있는 **syntatic sugar**이다. 이들은 꼭 필요한 것은 아니나 동일한 것을 달성하기 위한 여러 가지 방법이 있다. 그러나 데코레이터 구문을 사용하면 코드 가독성이 향상되고 결합이 감소된 표준 변환 방법을 사용할 수 있다. 나중에 추가하기보다 선언과 동시에 변환논리를 묶는 것이다. 이 글에서 데코레이터를 함수에만 사용하는 것을 다루고자 한다.

다음은 데토레이터를 사용핮 않고 함수를 실행하는 방법의 예이다.

```python
def func():
  print("just a random func")
 
func=transform_1(func)
func=transform_2(func)
```

위의 예에서는 변환 횟수가 증가함에 따라 읽기가 어려워진다.

데코레이터와 동일한 함수의 예:

```python
@transform_1
@transform_2
def func():
  print("just a random func")
```

깔끔하지요!!!

## 언제 데코레이터를 사용해야 할까?
데코레이터는 함수 선언 시 함수의 동작을 변경하려는 경우 유용하다. 동기화, 타이밍, 로깅, 권한 부여 등 여러 함수에 동일한 동작을 추가하는 것이 몇 가지 예이다.

```python
#An example of adding the same behaviour to multiple functions.
#the function below raises a TypeError
#WITHOUT DECORATORS
def func_1():
  try:
    return "Hi"+5
  except TypeError as te:
    #add a message to TypeError
    print("TypeError here!!") #For simplicity, let's just print the error

def func_2():
  try:
    return "Wi" + 5
  except TypeError as te:
    print("TypeError here!!") #For simplicity, let's just print the error


# WITH DECORATORS:
#decorator
def type_error_with_message(func):
  def wrapped_func(): #this function will replace the decorated function
    try:
      func()
    except TypeError as te:
      print("TypeError here!!") #For simplicity, let's just print the error
  return wrapped_func

@type_error_with_message
def func_1():
  return "Hi"+5

@type_error_with_message
def func_2():
  return "Wi"+5

func_1() 
func_2()

'''
Result:
TypeError here!!
TypeError here!!
'''
```

위 예의 코드에서 데코레이터는 Python에서 일반 함수처럼 정의되고 함수를 인수로 사용하는 것을 볼 수 있다. `wrapd_func`는 데커레이트된 함수를 대체하는 것이다. 모든 변환은 데코레이터로부터 최종적으로 반환될 `wrapd_func` 내에 적용된다. 이 모든 것은 Python에서 함수가 first-class 객체이기 때문에 가능하다. `'@'`로 접두사를 붙이고 함수 선언 위에 배치함으로써 데코레이터를 사용한다. 이제 데코레이트된 함수를 호출할 때마다 데코레이터로부터 반환된 레퍼(wrapper)가 실행되어 함수를 실행한다.

## 데코레이터 내에서 데코레이트된 함수 인수를 사용하는 방법
데코레이트된 함수의 인수는 데코레이터의 래퍼 함수(`wrapd_func`)로 전달된다.

```python
def type_error_with_message(func):
  def wrapped_func(*args, **kwargs):
    try:
      func(*args, **kwargs)
    except TypeError as te:
      print("TypeError here!!") #For simplicity, let's print the error
  return wrapped_func

@type_error_with_message
def dummy_func(*args,**kwargs)
  pass
```

데코레이터 구문을 사용하지 않고 이 작업을 수행할 수 있다:

```python
decorated_func = type_error_with_message(dummy_func)("Hi there!")
```

여전히 데코레이터를 일반적인 함수처럼 사용할 수 있다.

## 인수를 갖는 데코레이터:
TypeError 예제로 돌아가서, 모든 함수에 대해 오류에 다른 메시지를 추가하려면 어떻게 해야 할까? 어떻게 그 메시지를 데코레이터에게 전달할까?

메시지를 데코레이터의 인수로 전달할 수 있다. 하지만, 우리는 초기의 예에서 데코레이트된 함수가 데코레이터에게 전달되는 것을 보았다. 그러면 우리는 어떻게 인수를 전달할 수 있을까?

인수가 있는 데코레이터의 구문을 변경한다. 내부 래퍼를 이전 데코레이터가 담당했던 함수 객체를 수락하도록 정의한다. 데코레이터에서 반환되는 값은 데코레이트된 함수를 감싸는 기능이다. 예:

```python
def type_error_with_message(decoratorArg):
  def wrapper(func): #Acting as the actual decorator now
    def wrapped_func(funcArg): #this function will replace the decorated function
      try:
        func(funcArg)
      except TypeError as te:
        print("TypeError here with message: "+decoratorArg) #For simplicity, let's print the error
    return wrapped_func
  return wrapper

@type_error_with_message("Cannot perform a Hi5")
def func_1(funcArg):
  print(funcArg)
  return "Hi"+5

@type_error_with_message("Your Wi5 failed")
def func_2(funcArg):
  print(funcArg)
  return "Wi"+5
 
func_1("Performing a Hi5") 
func_2("Testing the Wi5")

'''
Result: 
Performing a Hi5
TypeError here with message: Cannot perform a Hi5
Testing Wi5
TypeError here with message: Your Wi5 failed
'''
```

데코레이터 구문을 사용하지 않는다면,

```python
type_error_with_message("Cannot perform a Hi5")(func_1)("Performing a Hi5")
type_error_with_message("Your Wi5 failed")(func_1)("Testing Wi5")

'''
Result:
Performing a Hi5
TypeError here with message: Cannot perform a Hi5Testing Wi5
TypeError here with message: Your Wi5 failed
'''
```

## 하나의 함수에 여러 데코레이터 추가:
하나의 함수에 여러 데코레이터를 차례로 추가할 수 있으며, 데코레이터는 아래에서 위로 실행된다.

```python
@decorator_2
@decorator_1
def func():
  pass
```

데코레이터 구문을 사용하지 않는다면,

```python
decorated_func=decorator_2(decorator_1(func))
```

**(주)** 위 내용은 [Decorators in Python](https://medium.com/@VarshaChahal/decorators-in-python-b43e4322dd93)을 편역한 것이다. 