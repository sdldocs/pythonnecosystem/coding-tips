이 글에서는 Django Admin 패널의 인터페이스를 변경하는 방법에 대해 설명한다.

Django Admin은 어플리케이션을 관리하기 위한 강력한 도구이다. 하지만 때로는 커스터마이즈하여 완전히 다르게 만들고 싶을 때가 있다.

저자의 블로그에서 django Admin 패널을 머스터마이즈하는 여러 방법을 공유했다. 예를 들어, [Django Admin 패널에 차트](https://sevdimali.medium.com/how-to-add-a-chart-in-django-admin-3baf901da160?source=user_profile---------3----------------------------)를 추가하거나 [커스텀 관리자 작업을 추가](https://sevdimali.medium.com/django-admin-custom-actions-71872bc9afd2?source=user_profile---------26----------------------------)하는 등의 방법이 있다. 하지만 이 글에서는 주로 기본 모양을 변경하는 방법에 대해 이야기하고자 한다.

Django Admin 패널을 커스터마이징하기 위해 [django-jazzmin](https://django-jazzmin.readthedocs.io/) 패키지를 사용하겠다.

## django-jazzmin 설치

```bash
$ pip install django-jazzmin
```

패키지를 설치한 후 이를 **INSTALLED_APPS**내 `django.contrib.admin` 앞에 추가한다.

```python
INSTALLED_APPS = [
    'jazzmin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'core',
]
```

이제 설치가 완료되었다!

개발 서버를 실행하고 `http://127.0.0.1:8000/admin/` 으로 이동한다.

아래와 같은 멋진 로그인 화면이 표시될 것이다.

![](images/rebranding-django-admin-interface/login.webp)

## Jazzmin 커스텀 옵션
Jazzmin은 Django Admin 인터페이스의 모양을 수정할 수 있는 다양한 커스텀 옵션을 제공한다.

이러한 옵션은 `settings.py`에 **JAZZMIN_SETTINGS**으로 다음과 같이 추가할 수 있다.

몇 가지 예를 보이겠다.

```python
JAZZMIN_SETTINGS = {
    # title of the window (Will default to current_admin_site.site_title if absent or None)
    "site_title": "My Cool Admin",

    # Title on the login screen (19 chars max) (defaults to current_admin_site.site_header if absent or None)
    "site_header": "My Cool Site",

    # Title on the brand (19 chars max) (defaults to current_admin_site.site_header if absent or None)
    "site_brand": "My Cool Site",

    # Logo to use for your site, must be present in static files, used for brand on top left
    "site_logo": "images/logo.jpg",

    # Logo to use for your site, must be present in static files, used for login form logo (defaults to site_logo)
    "login_logo": None,

    # Logo to use for login form in dark themes (defaults to login_logo)
    "login_logo_dark": None,

    # CSS classes that are applied to the logo above
    "site_logo_classes": "img-circle",

    # Welcome text on the login screen
    "welcome_sign": "Welcome to the My Cool Admin Page",
}
```

또 다른 멋진 옵션은 Admin 패널에서 앱과 모델에 커스텀 아이콘을 사용할 수 있다는 것이다. 현재 `Category`와 `Article`이라는 두 가지 모델이 있다.

이 두 모델에 대한 아이콘을 설정해 보겠다.

```python
    "icons": {
        "auth": "fas fa-users-cog",
        "auth.user": "fas fa-user",
        "auth.Group": "fas fa-users",
        "core.Category": "fas fa-bookmark",
        "core.Article": "fas fa-newspaper",
    }
```

위의 내용을 `JAZZMIN_SETTINGS`에 추가하기만 하면 된다.

![](images/rebranding-django-admin-interface/jazzmin_settings.webp)

또한 jazzmin에는 프로젝트에서 바로 사용할 수 있는 기본 제공 테마가 있다. 전체 목록은 [jazzmin 문서](https://django-jazzmin.readthedocs.io/ui_customisation/#themes)에서 확인할 수 있다.

`settings.py`에 이러한 설정을 추가하여 활성화할 수 있다.

```python
JAZZMIN_UI_TWEAKS = {
    "theme": "darkly",
}
```

![](images/rebranding-django-admin-interface/setting_ui.webp)

Jazzmin에는 Admin 인터페이스의 일부를 대화식으로 커스텀마이즈할 수 있는 UI 커스터마이저가 내장되어 있다. `JAZZMIN_SETTINGS`에서 `"show_ui_builder":True`로 설정하여 활성화할 수 있다.


**(주)** 위 내용은 [Rebranding Django Admin Interface](https://towardsdev.com/rebranding-django-admin-interface-94232ddfd637)을 편역한 것이다. 