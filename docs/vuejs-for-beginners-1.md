Vue는 프런트엔드 JavaScript 프레임워크로 사용자 인터페이스를 신속하게 만들 수 있다. Reactor나 Angular같은 다른 프레임워크에 비해 더 가볍고 초보자에게 친화적이다. Vue의 핵심 라이브러리는 사용자가 볼 수 있는 부분인 뷰 계층에만 초점을 맞춘다. 그것이 프레임워크를 뷰(view로 발음됨)라고 이름 지은 이유이다<sup>[1](#footnote_1)</sup>.

## 설치 <sup>[2](#footnote_2)</sup>
새 Vue 어플리케이션 프로그램을 만들려면 터미널로 이동하여 다음 명령을 실행한다.

```bash
$ npm init vue@latest
```

or

```bash
$ npm install --global @vue/cli
```

이 명령을 실행하면 TypeScript, Vue Router와 Pinia와 같은 여러 옵션이 표시ehls다. 이 포스팅에서는 이러한 기능이 필요하지 않지만 원하는 경우 이 기능을 사용하여 재생할 수 있다.

```
✔ Project name: … <your-project-name>
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit testing? … No / Yes
✔ Add Cypress for both Unit and End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in ./<your-project-name>...
Done.
```


다음 프로젝트 폴더로 이동하여 필요한 패키지를 설치하고 개발 서버를 실행한다.

```bash
$ cd <your-project-name>
$ npm install
$ npm run dev
```

브라우저를 열고 http://localhost:3000/으로 이동하면 Vue의 시작 페이지가 나타납니다.

![vue-welcome](images/vuejs-for-beginners/vue-welcome.webp)

<!--  참고 

https://developer.mozilla.org/ko/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_getting_started

-->

## 소개
시작하기 전에 프로젝트 폴더에 설치된 항목을 살펴보겠다.

![vue-directory-structure](images/vuejs-for-beginners/vue-directory-structure.png)

이미 알고 있는 것들이 몇 가지 있다. `node_modules`에는 설치한 패키지가 포함되어 있다. `public` 폴더에는 공용으로 사용하려는 파일과 리소스가 들어 있다. `package-lock.json`과 `package.json` 파일은 모두 이전에 [JavaScript Basics]() 에서 언급한 패키지 관리용이며 `index.html` 파일이 프로젝트의 시작점이다.

이 튜토리얼에서는 `src` 디렉토리 내의 파일에만 초점을 맞춘다. `assets` 폴더에는 이미지, CSS 파일과 기타 리소스가 저장된다. `main.js` 파일은 프로젝트의 모든 Vue 앱을 마운트하고 구성하며, `index.html` 파일로 가져오는 스크립트이기도 하다.

`App.vue`는 실제 Vue 앱이며 대부분의 코딩을 여기서 수행한다. 그러나 앱이 너무 커질 때가 있다. 앱을 여러 구성 요소로 분할하면 이러한 구성 요소를 `components` 폴더에 저장할 수 있다.

http://localhost:3000/ 으로 이동하면 어떻게 되는지 자세히 알아보겠다. `index.html`부터 시작해서 `<body>` 태그 안에 무엇이 있는지 알아보자.

```html
<body>
  <div id="app"></div>
</body>
```

중요한 코드의 유일한 행은 `<div id="app"></div>` 이다. 왜 일까? `main.js`로 가보겠다.

```js
import { createApp } from "vue";
import App from "./App.vue";

createApp(App).mount("#app");
```

이 파일은 Vue 앱을 가져오고 HTML 요소 `id="app"`으로 해당 앱을 마운트한다. `#`은 `id`를 나타내고 `.`은 클래스를 나타낸다. 이것이 바로 `<div>` 요소가 비록 비어 있더라도 매우 중요한 이유이다.

다음 `App.vue` 파일로 이동한다.

```js
<script setup>
import HelloWorld from "./components/HelloWorld.vue";
</script>

<template>
  <img alt="Vue logo" src="./assets/logo.png" />
  <HelloWorld msg="Hello Vue 3 + Vite" />
</template>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  ...;
}
</style>
```

파일이 세 개의 섹션으로 나누어 있는 것을 즉시 확인할 수 있다. `<script>` 섹션에는 JavaScript 코드가 포함되어 있고, `<template>` 섹션에는 HTML 요소가 포함되어 있으며, `<style>` 섹션에는 CSS 코드가 포함되어 있다.

`<script>` 섹션에서는 구성 요소 폴더에서 구성 요소를 가져와 `<template>` 섹션에서 사용했다.

마지막으로 HelloWorld 구성 요소로 이동한다. 그것이 `App.vue`와 정확히 같은 구조를 가지고 있다는 것을 알 수 있다. 또한 이 파일 내부의 내용을 편집하여 웹 페이지가 변경되는지 확인할 수 있다.

## 기초
이제 `App.vue` 파일로 돌아가서 불필요한 모든 항목을 삭제하여 깨끗하고 비어 있는 vue 문서를 만들겠다.

```js
<script></script>

<template></template>

<style></style>
```

### 메서드와 속성
알다시피 `<script>` 섹션은 자바스크립트 코드를 작성하는 부분이지만, Vue는 프레임워크이기 때문에 몇 가지 제약사항과 요구사항이 있다. 이 섹션의 구조는 일반적으로 다음과 같다.

#### `App.vue`

```js
<script>
export default {
  data() {
    return {
      name: "value"
    }
  },

  methods: {
    xxx() {...}
  },
  ...
}
</script>
```

즉, 이 Vue 앱을 `main.js`로 가져올 때 실제로 여러 메서드와 속성을 가져오는 것이다. 각 속성/메소드는 서로 다른 용도로 사용된다.

예를 들어 `data()` 메서드는 앱에서 사용되는 모든 변수를 포함하는 객체를 반환한다. `data`는 단순한 속성이 아니라 메서드여야 한다. 이것이 Vue를 반응적으로 만드는 이유이다. 즉, 변수 값이 변경되면 웹 페이지를 다시 로드할 필요 없이 변경된다. `methods` 속성은 코더인 사용자가 생성한 모든 메소드를 포함한다. 물론 `props`, `computed`, `inject`와 `setup`과 같은 다른 속성도 허용된다. 그것들에 대해 나중에 자세히 논의할 것이다.

### 간단한 counter 앱
이 두 가지 간단한 개념인 `data` 메서드와 `methods` 속성만 알면 앱을 만들기 시작할 수 있다. 예를 들어 버튼을 클릭한 횟수를 세는 앱을 만든다.

```js
<script>
export default {
  data() {
    return {
      count: 0,
    };
  },
};
</script>

<template>
  <button v-on:click="count++">click me</button>
  <p>count = {{ count }}</p>
</template>
```

먼저 초기 값이 0인 변수 `count`를 선언하고, `<template>` 섹션에서 event listner (`v-on:click`)를 설정하여 버튼을 클릭할 때마다 `count`가 1씩 증가한다. 그런 다음 변수는 이중 중괄호(`{{ }}`)를 사용하여 렌더링된다. 이러한 구문에 대해서는 나중에 설명하겠다.

`count` 값을 재설정하는 다른 버튼이 필요하면 어떻게 해야 할까? 다음과 같이 할 수 있다.

#### App.vue

```js
<script>
export default {
  data() {
    return {
      count: 0,
    };
  },

  methods: {
    clear() {
      this.count = 0;
    },
  },
};
</script>

<template>
  <button v-on:click="count++">click me</button>
  <button v-on:click="clear()">clear</button>
  <p>count = {{ count }}</p>
</template>
```

이 어플리케이션 프로그램 인스턴스에 속하는 변수를 참조할 때는 `this` 키워드를 사용해야 한다. 데이터 메서드에서 정의한 변수는 이 인스턴스에 고유하며, 이는 다른 인스턴스나 구성 요소에서 액세스할 수 없음을 의미한다. 예를 들어 다른 counter를 생성하여 구성 요소로 `App.vue`로 가져올 수 있다.

#### `components/Counter.vue`

```js
<script>
export default {
  data() {
    return {
      count: 0,
    };
  },
  methods: {
    clear() {
      this.count = 0;
    },
  },
  components: { Counter },
};
</script>

<template>
  <button v-on:click="count++">click me</button>
  <button v-on:click="clear()">clear</button>
  <p>count = {{ count }}</p>
</template>

<style></style>
```

#### `App.vue`

```js
<script>
import Counter from "./components/Counter.vue";
export default {
  data() {
    return {
      count: 0,
    };
  },
  methods: {
    clear() {
      this.count = 0;
    },
  },
  components: { Counter },
};
</script>

<template>
  <button v-on:click="count++">click me</button>
  <button v-on:click="clear()">clear</button>
  <p>count = {{ count }}</p>

  <Counter />
</template>

<style></style>
```
![counter](images/vuejs-for-beginners/counter.webp)

브라우저에서 이것을 시도해 보면, `Counter.vue`와 `App.vue`를 위하여 정의한 변수가 모두 `counter`임에도 불구하고 그들이 서로 영향을 미치지 않는 것으로 보이며 변수 값을 재설정하면 동일한 인스턴스에 있는 `count`만 0가 된다.

### 생명 주기(lifecycle)
마지막으로 Vue의 또 다른 중요한 개념인 라이프사이클을 소개하고자 한다.

앱 인스턴스가 생성되면 데이터 초기화, 템플릿 컴파일, DOM에 템플릿 마운트, 데이터 변경에 따른 템플릿 업데이트 등 일련의 프로세스를 거친다. 이를 통해 어플리케이션 인스턴스의 수명을 여러 단계로 나눌 수 있으며, Vue는 다양한 단계에서 자체 코드를 추가할 수 있는 몇 가지 라이프사이클 후크를 제공한다.

예를 들어, `created()` 함수를 사용하면 인스턴스가 생성된 직후에 실행되어야 하는 코드를 추가할 수 있다.

```js
<script>
export default {
  data() {
    return { count: 1 };
  },
  created() {
    console.log("initial count is: " + this.count);
  },
};
</script>
```

사용할 수 있는 다른 라이프사이클 훅들이 있다. 다음은 모든 훅과 훅들이 라이프사이클에서 어느 위치에 있는지 보여주는 다이어그램이다.

![](images/vuejs-for-beginners/lifecycle.webp)

## 보간(interpolation)

이 포스팅의 첫 번째 섹션에서, 우리는 Vue 파일이 `<template>`, `<script>`과 `<style>`의 세 섹션으로 나뉘어진다는 것을 설며했다. 그러나 지난 번에는 Vue.js의 표면만 만져 보았는데, 지금부터 각 섹션의 세부 사항에 대해 설명하겠다. 가장 쉬운 템플릿 섹션부터 시작한다.

템플릿 섹션에는 HTML 코드만 있으며 Vue 파일이 결국 렌더링할 항목을 보인다. 그러나 페이지가 반응적이기를 원하기 때문에 데이터가 변경되면 또한 페이지가 변경되기를 원한다. 그러기 위해서는 Vue.js에게 데이터를 어디에 둘지 알려줘야 한다.

### 텍스트
텍스트 보간은 데이터 바인딩의 가장 기본적인 형태로, 다음과 같은 이중 중괄호를 사용한다.

```js
<script>
export default {
  data() {
    return { msg: "This is a message." };
  },
};
</script>

<template>
  <p>Message: {{ msg }}</p>
</template>
```

`msg`의 값을 변경하면 페이지를 새로 고칠 필요 없이 변경되는 것을 볼 수 있다

### 원시 HTML(raw HTML)
하지만 데이터가 더 복잡해지면 어떻게 해야 할까요? 예를 들어 HTML 코드를 변수에 바인딩하려고 하면 중괄호로 HTML을 출력하려고 하면 어떻게 되는지 확인한다.

```js
<script>
export default {
  data() {
    return { msg: '<span style="color: red">This is a message.</span>' };
  },
};
</script>

<template>
  <p>Message: {{ msg }}</p>
</template>
```

데이터는 HTML 코드 대신 일반 텍스트로 처리된다. 이 문제를 해결하려면 Vue.js에 HTML 지시어를 사용하여 렌더링하려는 데이터가 HTML임을 알려야 한다.

```html
<p>Message: <span v-html="msg"></span></p>
```

이번에는 데이터가 렌더링될 때 원본 `<span>` 태그가 교체된다.

### 애트리뷰트(attributes)
때때로 애트리뷰트를 변수에 바인딩하면 유용할 수 있다. 예를 들어 사용자가 확인되면 버튼을 활성화하고 사용자가 확인되지 않으면 비활성화한다. `v-bind` 지시문을 사용하여 `disabled` 매트리뷰트를 `verified` 변수에 바인딩할 수 있다.

```js
<script>
export default {
  data() {
    return { verified: false };
  },
};
</script>

<template>
  <button v-bind:disabled="!verified">Button</button>
</template>
```

느낌표(`!`)는 `verified` 값을 반전시킨다.

### JavaScript 표현식
템플릿 내에서 간단한 JavaScript 표현식을 사용할 수도 있다. 사실, 방금 본 `!verified`는 매우 간단한 예이다. 다음과 같이 더 복잡한 작업도 수행할 수 있다.

```js
{{ number + 1 }}

{{ ok ? "YES" : "NO" }}

{{ message.split("").reverse().join("") }}

<div v-bind:id="'list-' + id"></div>
```

그러나 몇 가지 제한 사항이 있다. 예를 들어 새 변수가 작동하지 않는다고 선언하는 문이 있다. 루프와 흐름 제어(`if`문인 경우) 문에서도 작동하지 않는다.

## 지시어(directives)
Vue.js에서 지시어는 접두사 `v-`를 사용하는 특수 애트리뷰트이다. 이들의 주요 기능은 사이드 이펙트를 DOM 노드에 바인딩하는 것이다.

예를 들어 다음 예제에서는 `v-if` 지시문을 사용하여 변수를 `<p>` 요소에 바인딩한다. 그것은 일반적인 `if` 문처럼 작동한다. `verified`가 `true`이면 첫 번째 `<p>` 요소가 렌더링되고, `verified`가 `false`이면 두 번째 `<p>` 요소가 렌더링된다.

```js
<p v-if="verified">You are verified.</p>
<p v-if="!verified">You are NOT verified.</p>
```

### 인수(arguments)
일부 지시어는 추가 인수를 사용할 수 있다. 예를 들어, 앞서 살펴본 `v-bind` 지시어는 HTML 애트리뷰트를 변수에 바인딩하는 데 사용되며, 이 지시어는 해당 매트리뷰트의 이름을 인수로 사용한다.

```js
<p v-bind:color="colorName">...</p>
<button v-bind:class="className">click me</button>
```

다른 예로는 `v-on` 지시문이 있다. Vue.js의 event listner이다.

```js
<a v-on:click="action">...</a>
```

이 링크를 클릭하면 변수 `action`에 연결된 함수가 실행된다.

인수 자체를 변수에 바인딩할 수도 있다. 예를 들면,

```html
<a v-on:[event]="action">...</a>
```

이 경우 `var event = "click"`이면 이 예는 `v-on:click="action"`과 동일하다.

실제로 `v-bind`와 `v-on`은 가장 일반적으로 사용되는 두 지시어이므로 Vue.js는 이러한 지시어를 위해 특별한 바로 가기를 만들었다. `v-bind`는 콜론(`:`)으로 단축할 수 있으며 `v-on`은 `@`으로 사용하여 나타낼 수 있다.

다음 코드는 동일하다.

```html
<a v-bind:href="url">...</a>
<a :href="url">...</a>
```

```html
<a v-on:click="action">...</a>
<a @click="action">...</a>
```

### 흐름 제어(flow control)
다음으로, Vue의 `if` 문에 대해 이야기하겠다. 앞에서 살펴본 것처럼 `v-if` 지시문은 요소를 부울 값으로 바인딩한다. 부울 값이 `true`이면 요소가 렌더링되고 `false`이면 요소가 Vue에 의해 무시된다.

`v-if` 이외에도 `v-else` 지시문이 있으며, `v-if` 지시문과 함께 작동된다.

```js
<p v-if="verified">You are verified.</p>
<p v-else>You are NOT verified.</p>
```

두 가지 이상의 조건이 필요하다면 어떻게 해야 할까? `v-else-if` 지시문은 이름에서 알 수 있듯이 `else if` 블록을 생성합니다. 여러 번 연결할 수 있으므로 여러 조건을 만들 수 있다.

```js
<p v-if="num === 1">The number is 1.</p>
<p v-else-if="num === 2">The number is 2.</p>
<p v-else-if="num === 3">The number is 3.</p>
<p v-else-if="num === 4">The number is 4.</p>
<p v-else>The number is 5.</p>
```

### 루프
마지막으로, `if` 문을 제외하고, Vue는 또한 템플릿 내부의 간단한 `for` 루프를 만들 수 있다. 언어에 익숙한 경우 구문은 실제로 Python의 `for` 루프와 유사하다.

다음과 같은 배열로 항목 목록을 렌더링할 수 있다.

```js
<script>
export default {
  data() {
    return {
      items: [{ num: 1 }, { num: 2 }, { num: 3 }, { num: 4 }, { num: 5 }],
    };
  },
};
</script>

<template>
  <ul>
    <li v-for="item in items">The number is {{ item.num }}.</li>
  </ul>
</template>
```

Vue는 인덱스 번호에 대한 두 번째 인수(선택 사항)도 지원한다.

```js
<template>
  <ul>
    <li v-for="(item, index) in items">
      #{{ index }} - The number is {{ item.num }}.
    </li>
  </ul>
</template>
```

---
<a name="footnote_1">1</a>: 이 포스팅은 [Vue.js for Beginners #1](https://www.ericsdevblog.com/posts/vuejs-for-beginners-1/)를 편역한 것이다.

<a name="footnote_2">2</a>: npm을 실행하여 "permissions issue"로 오류가 발생하면 `sudo npm ...`으로 명령을 바꾸어 실행한다.
