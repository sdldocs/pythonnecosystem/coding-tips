파이썬 데코레이터의 가장 유용한 기능 중 하나에 대해 이야기하려고 한다. 데코레이터는 실제로 소스 코드를 변경하지 않고 함수나 메서드의 기능을 수정하거나 향상시키는 방법이다.

이 글에서는 Python 데코레이터의 작동 방식, 데코레이터를 만드는 방법, 효과적으로 사용하는 방법을 포함하여 데코레이터의 기본적인 사항에 대해 설명한다.

## 데코레이터란?
- 데코레이터는 함수의 기본 동작을 변경할 수 있는 특수한 유형의 함수이다.
- 데코레이터를 사용하여 원래 코드를 실제로 변경하지 않고 캐시, 로깅 또는 인증과 같은 기능을 추가할 수 있다.
- 데코레이터는 기본적으로 다른 함수를 인수로 받아 원래 함수에 추가 기능을 수행할 수 있는 새로운 함수를 반환하는 래퍼 함수이다. 데코레이터는 일반적으로 코드를 단순화하고 모듈화하며 자주 사용되는 코드의 양을 줄일 수 있다.

## 데코레이터 동작 방법
Python에서 데코레이터가 작동하는 방식을 이해하려면 데코레이터를 2단계 프로세스로 생각하는 것이 바람직 하다:

- 데코레이터 함수가 정의되어 있으며, 이 함수는 다른 함수를 인수로 사용한다.
- 데코레이터 함수는 원래 함수에 추가 기능을 실행할 수 있는 새 함수를 반환한다.

다음은 이 기능을 보여주는 간단한 예이다:

```python
    def my_decorator(func):
        def wrapper():
            print("Before the function is called.")
            func()
            print("After the function is called.")
        return wrapper
```

```python
    @my_decorator
    def say_hello():
        print("Hello, world!")
```

- 위의 예에서 `my_decorator`는 다른 함수를 인수(`func`)로 사용하는 함수이다.
- 그런 다음 `func` 호출 전후에 메시지를 출력하는 새로운 함수(`wrapper`)를 정의한다.
- `@my_decorator` 구문은 `say_hello` 함수에 데코레이터를 적용하는 간단한 방법이다. 이는 `say_hello = my_creator(say_hello)`를 호출하는 것과 같다.
- `say_hello`를 호출하면 데코레이터가 자동으로 적용되며 아래와 같이 출력된다.

```
Before the function is called.
Hello, world!
After the function is called.
```

이제 Python 데코레이터를 사용해야 하는 몇 가지 시나리오를 살펴보자.

### 1. 수행시간 측정
함수를 만들고 그 함수 수행 시간이 얼마나 걸리는지 확인해야 할 상황을 가정해 보자.

```python
import time

def measure_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Execution time: {execution_time} seconds")
        return result
    return wrapper

@measure_time
def my_function():
    time.sleep(2)

my_function()
```

### 2. 로깅
아래 코드를 사용하여 함수를 로깅하고 싶다면 이를 아래와 같이 쉽게 할 수 있다.

```python
def log_function(func):
    def wrapper(*args, **kwargs):
        print(f"Calling function: {func.__name__}")
        print(f"Arguments: {args}")
        print(f"Keyword Arguments: {kwargs}")
        result = func(*args, **kwargs)
        return result
    return wrapper

@log_function
def my_function(x, y):
    return x + y

print(my_function(2, 3))
```

### 3. 입력 검증
아래 스크립트는 함수를 호출할 때 전달된 인수의 유효성을 검사하는 데 사용할 수 있다.

```python
def validate_input(func):
    def wrapper(*args, **kwargs):
        for arg in args:
            if not isinstance(arg, int):
                raise TypeError("Input arguments must be integers")
        for value in kwargs.values():
            if not isinstance(value, int):
                raise TypeError("Keyword arguments must be integers")
        return func(*args, **kwargs)
    return wrapper

@validate_input
def add_numbers(a, b):
    return a + b

print(add_numbers(2, 3))
```


**(주)** 위 내용은 [Mastering Python Decorators](https://rajansahu713.medium.com/mastering-python-decorators-72005d2352be)을 편역한 것이다.
