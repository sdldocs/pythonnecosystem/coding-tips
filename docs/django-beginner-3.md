이전 포스팅에서 많은 새로운 개념을 소개했는데, 여러분은 아마도 약간 길을 잃었을 것이다. 그러나 이 포스팅<sup>[1](#footnote_1)</sup>에서는 URL 디스패처, 모델, 뷰와 템플릿이 함께 작동하여 기능적인 Django 어플리케이션을 만드는 방법에 대해 자세히 알아볼 것이다.

이해하기 쉽게 하기 위해, 이 포스팅에서는 카테고리, 태그 등을 포함한 모든 기능을 갖춘 블로그 어플리케이션을 만들지 않을 것이다. 대신 게시물을 표시하는 게시 페이지, 모든 글 목록을 표시하는 홈 페이지, 게시물을 수정하는 생성/업데이트/삭제 페이지만 생성할 것이다.

## 데이터베이스 구조 설계
모델 레이어부터 시작한다. 먼저 데이터베이스 구조를 설계해야 한다. 지금은 게시물만 처리하고 있으므로 `models.py` 파일로 이동하여 새 게시물 모델을 만들 수 있다.

#### `blog/models.py`

```python
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
```

이 `Post`에는 최대 100자의 CharField인 `title`과 텍스트 필드인 `contens` 두 필드만 있다.

다음 명령을 사용하여 해당 마이그레이션 파일을 생성한다.

```bash
$ python manage.py makemigrations
```

마이그레이션을 적용한다.

```bash
$ python manage.py migrate
```

## CRUD 동작
이제는 어플리케이션 자체를 들여다볼 때이다. 실제 어플리케이션 프로그램을 작성할 때 모든 컨트롤러를 먼저 만든 다음, 템플릿을 설계한 다음, 라우터로 이동하는 경우는 거의 없다. 대신 사용자의 관점에서 생각하고 사용자가 어떤 조치를 취할 것인지에 대해 생각해야 한다.

일반적으로 사용자는 각 리소스에 대해 4개의 작업을 수행할 수 있어야 하며, 이 경우는 `Post`이다.

- Create: 데이터베이스에 새 데이터를 삽입하는 데 사용한다.
- Read: 이 동작은 데이터베이스에서 데이터를 검색하는 데 사용된다.
- Update: 이 동작은 데이터베이스의 기존 데이터를 수정하는 데 사용된다.
- Delete: 데이터베이스에서 데이터를 제거하는 데 사용된다

### 생성(create action)
먼저 생성 작업부터 시작한다. 현재 데이터베이스는 여전히 비어 있으므로 사용자는 새 게시물을 작성해야 한다. 이 생성 작업을 완료하려면 URL 패턴 `/post/create/`를 뷰 함수 `post_create()`를 가리키는 URL 디스패처가 필요하다.

`post_create()` 보기 함수는 흐름 제어 기능(`if` 문)을 가지고 있어야 하며, 요청 메서드가 `GET`인 경우 HTML 양식이 포함된 템플릿을 반환하여 백엔드에게 사용자의 정보를 전달할 수 있도록 한다(양식 제출은 `POST` 요청이어야 한다). 요청 방법이 `POST`인 경우 새 `Post` 리소스를 생성하고 저장해야 한다.

> **다음은 HTTP 메서드에 대한 간략하게 다시 설명한다.**
>
> - `GET` 메서드는 가장 일반적으로 사용되는 HTTP 요청 메서드입니다. 서버에서 데이터와 리소스를 요청하는 데 사용된다.
> - `POST` 메서드는 리소스를 생성/업데이트하는 데 사용되는 데이터를 서버로 보내는 데 사용된다.
> - `HEAD` 메서드은 `GET` 방법과 동일하게 작동한다. HTTP 응답에는 본문이 아닌 머리만 포함된다. 이 메서드는 일반적으로 개발자가 디버깅 목적으로 사용한다.
> - `PUT` 메서드은 `POST`와 비슷하지만 한 가지 작은 차이가 있다. 서버에 이미 있는 리소스를 게시할 때 이 작업을 수행해도 차이가 발생하지 않는다. 그러나 `PUT` 메서드는 요청할 때마다 해당 리소스를 복제한다.
> - `DELETE` 메서드는 서버에서 리소스를 제거한다.

`urls.py`로 이동하여 URL 디스패처와 다시 시작하자.

#### `djangoBlog/urls.py`

```python
from django.urls import path
from blog import views

urlpatterns = [
    path("post/create/", views.post_create, name="create"),
]
```

그러면 뷰 함수 `post_create()`가 필요하다. `views.py`로 이동하여 다음 코드를 추가한다.

#### `blog/view.py`

```python
from django.shortcuts import redirect, render
from .models import Post

def post_create(request):
    if request.method == "GET":
        return render(request, "post/create.html")
    elif request.method == "POST":
        post = Post(title=request.POST["title"], content=request.POST["content"])
        post.save()
        return redirect("home")
```

`post_create()` 함수는 먼저 HTTP 요청의 메서드를 검토하고, `GET` 메서드인 경우 `create.html` 템플릿을 반환하고, `POST`인 경우 해당 `POST` 요청을 통해 전달된 정보를 사용하여 새 `Post` 인스턴스를 생성한 후 홈 페이지로 리디렉션한다(다음 단계에서 이 페이지를 생성한다).

다음으로 `create.html` 템플릿을 만들 시간이다. 우선 `layout.html`이 필요하다.

#### `template/layout.html`

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    {% block title %}{% endblock %}
</head>

<body class="container mx-auto font-serif">
    <div class="bg-white text-black font-serif">
        <div id="nav">
            <nav class="flex flex-row justify-between h-16 items-center shadow-md">
                <div class="px-5 text-2xl">
                    <a href="/">
                        My Blog
                    </a>
                </div>
                <div class="hidden lg:flex content-between space-x-10 px-10 text-lg">
                    <a href="{% url 'create' %}" class="hover:underline hover:underline-offset-1">New Post</a>
                    <a href="https://github.com/ericnanhu" class="hover:underline hover:underline-offset-1">GitHub</a>
                </div>
            </nav>
        </div>

        {% block content %}{% endblock %}

        <footer class="bg-gray-700 text-white">
            <div
                class="flex justify-center items-center sm:justify-between flex-wrap lg:max-w-screen-2xl mx-auto px-4 sm:px-8 py-10">
                <p class="font-serif text-center mb-3 sm:mb-0">Copyright © <a href="https://www.ericsdevblog.com/"
                        class="hover:underline">Eric Hu</a></p>

                <div class="flex justify-center space-x-4">
                    . . .
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
```

위의 코드 중간쯤(22행) `{% url 'create' %}`을(를) 확인하자. 이렇게 하면 이름으로부터 생성된 URL을 되돌릴 수 있다. `create`라는 이름은 `post/create` 디스패처에서 지정한 이름과 일치한다.

또한 이 페이지를 보기 좋게 만들기 위해 8번째 줄의 CDN을 통해 `TailwindCSS`를 추가했지만, 여러분은은 생산 환경에서 이것을 해서는 안 된다.

그런 다음 `create.html` 템플릿을 만든다. 이 템플릿이 게시물을 만들기 위한 것임을 분명히 하기 위해 `post` 디렉토리를 만들기로 했지만, 여러분이 이해할 수 있는 한 이 작업은 다르게 수행할 수 있다.

#### `template/post/cretae.html`

```html
% extends 'layout.html' %}

{% block title %}
<title>Create</title>
{% endblock %}

{% block content %}
<div class="w-96 mx-auto my-8">
    <h2 class="text-2xl font-semibold underline mb-4">Create new post</h2>
    <form action="{% url 'create' %}" method="POST">
        {% csrf_token %}
        <label for="title">Title:</label><br>
        <input type="text" id="title" name="title"
            class="p-2 w-full bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"><br>
        <br>
        <label for="content">Content:</label><br>
        <textarea type="text" id="content" name="content" rows="15"
            class="p-2 w-full bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"></textarea><br>
        <br>
        <button type="submit"
            class="font-sans text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-md text-sm w-full px-5 py-2.5 text-center">Submit</button>
    </form>
</div>
{% endblock %}
```

10번째 줄은 양식이 제출될 때 수행할 작업과 양식이 사용할 요청 메서드을 지정한다.

11번째 줄은 보안 목적으로 양식에 CSRF 보호를 추가한다.

13과 14번째 줄의 `<input>` 필드에도 주의하시오. 그것의 `name` 속성은 매우 중요하다. 양식이 제출되면 사용자 입력이 이 이름 속성에 연결되며, 다음과 같이 뷰 함수에서 입력을 검색할 수 있다.

```python
title=request.POST["title"]
```

17번과 18번 째 줄의 `testarea`도 마찬가지다.

그리고 마지막으로 버튼이 작동하려면 `type="submit"`가 있어야 한다.

### list action
이제 모든 게시물 목록을 표시할 수 있는 홈 페이지를 만들어 보겠다. 다음 URL로 다시 시작할 수 있다.

#### `djangoBlog/urls.py`

```python
path("", views.post_list, name="home"),
```

그리고 뷰 함수

#### `blog/views.py`


```python
def post_list(request):
    posts = Post.objects.all()
    return render(request, "post/list.html", {"posts": posts})
```

템플릿 `list.html`

```
{% extends 'layout.html' %}

{% block title %}
<title>My Blog</title>
{% endblock %}

{% block content %}
<div class="max-w-screen-lg mx-auto my-8">
    {% for post in posts %}
    <h2 class="text-2xl font-semibold underline mb-2"><a href="{% url 'show' post.pk %}">{{ post.title }}</a></h2>
    <p class="mb-4">{{ post.content | truncatewords:50 }}</p>
    {% endfor %}
</div>
{% endblock %}
```

`{% for post in posts %}`은(는) 모든 `posts`에 대해 반복되며 각 항목은 변수 `post`에 할당된다.

`{% url 'show' post.pk %}`은(는) `post`의 기본 키를 `show` URL 디스패처에 전달하며, 이는 나중에 생성한다.

마지막으로 `{{post.content | truncatewords:50}}`는 필터 `truncatewords`를 사용하여 처음 50개 단어로 내용을 자른다.

#### show action
다음으로 show action은 특정 게시물의 내용을 표시해야 하며, 이는 해당 URL에 Django가 고유한 하나의 `Post` 인스턴스만 찾을 수 있도록 하는 것을 포함해야 한다는 것을 의미한다. 이는 보통 무엇인가가 주 키라는 것이다.

#### `djangoBlog/urls.py`

```python
path("post/<int:id>", views.post_show, name="show"),
```

`post/` 뒤에 오는 정수는 변수 `id`에 할당되어 view 함수에 전달된다.

#### `blog/views.py`

```python
def post_show(request, id):
    post = Post.objects.get(pk=id)
    return render(request, "post/show.html", {"post": post})
```

그리고 해당 템플릿은

#### `templates/post/show.html`

```html
{% extends 'layout.html' %}

{% block title %}
<title>{{ post.title }}</title>
{% endblock %}

{% block content %}
<div class="max-w-screen-lg mx-auto my-8">

    <h2 class="text-2xl font-semibold underline mb-2">{{ post.title }}</h2>
    <p class="mb-4">{{ post.content }}</p>

    <a href="{% url 'update' post.pk %}" class="font-sans text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-md text-sm w-full px-5 py-2.5 text-center">Update</a>
</div>
{% endblock %}
```

### update action
URL 디스패처

#### `django/urls.py`

```python
path("post/update/<int:id>", views.post_update, name="update"),
```

뷰 함수:

#### `blog/views.py`

```python
def post_update(request, id):
    if request.method == "GET":
        post = Post.objects.get(pk=id)
        return render(request, "post/update.html", {"post": post})
    elif request.method == "POST":
        post = Post.objects.update_or_create(
            pk=id,
            defaults={
                "title": request.POST["title"],
                "content": request.POST["content"],
            },
        )
        return redirect("home")
```

[update_or_create()](https://docs.djangoproject.com/en/4.1/ref/models/querysets/#update-or-create) 메서드는 장고 4.1에 추가된 새로운 메서드이다.

해당 템플릿

#### `templates/post/update.html`

```html
{% extends 'layout.html' %}

{% block title %}
<title>Update</title>
{% endblock %}

{% block content %}
<div class="w-96 mx-auto my-8">
    <h2 class="text-2xl font-semibold underline mb-4">Update post</h2>
    <form action="{% url 'update' post.pk %}" method="POST">
        {% csrf_token %}
        <label for="title">Title:</label><br>
        <input type="text" id="title" name="title" value="{{ post.title }}"
            class="p-2 w-full bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300"><br>
        <br>
        <label for="content">Content:</label><br>
        <textarea type="text" id="content" name="content" rows="15"
            class="p-2 w-full bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300">{{ post.content }}</textarea><br>
        <br>
        <div class="grid grid-cols-2 gap-x-2">
            <button type="submit"
            class="font-sans text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-md text-sm w-full px-5 py-2.5 text-center">Submit</button>
        <a href="{% url 'delete' post.pk %}"
            class="font-sans text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-md text-sm w-full px-5 py-2.5 text-center">Delete</a>
        </div>

    </form>
</div>
{% endblock %}
```

### delete action
마지막으로 delete action

#### `django/urls.py`

```python
path("post/delete/<int:id>", views.post_delete, name="delete"),
```

#### `blog/views.py`

```python
def post_delete(request, id):
    post = Post.objects.get(pk=id)
    post.delete()
    return redirect("home")
```

이 작업은 작업이 완료된 후 홈 페이지로 리디렉션되므로 템플릿이 필요하지 않다.

### 서버 시작
마지막으로, dev 서버를 시작하고 결과를 보자.

```python
$ python manage.py runserver
```

home 페이지
![home](images/django_beginner/home.webp)

create 페이지
![create](images/django_beginner/create.webp)

show 페이지
![show](images/django_beginner/show.webp)

update 페이지
![update](images/django_beginner/update.webp)

---
<a name="footnote_1">1</a>: 이 포스팅은 [Django for Beginners #3 - The CRUD Operations](https://www.ericsdevblog.com/posts/django-for-beginners-3/)를 편역한 것이다.
