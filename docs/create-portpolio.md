이 포스팅<sup>[1](#footnote_1)</sup>에서는 지금까지 HTML, CSS, JavaScript와 Vue.js에 대해 배운 내용을 사용하여 포트폴리오 웹 사이트를 만들어 여러분과 여러분의 프로젝트에 대해 세상에 알릴 수 있다. 시작해 보자.

## HTML 파일 만들기
우선 작업 디렉토리를 생성해 보겠다. 그러면 `portpolio`라고 할 수 있다. 포트폴리오 프로젝트와 관련된 모든 것은 `portpolio` 디렉토리 안에 저장되어야 한다. 다르게 할 수도 있지만, 나중에 몇 가지를 추가할 예정이기 때문에 정리해야 한다.

그런 다음 `portpolio`로 이동하여 HTML 파일인 `portpolio.html`을 만듭니다. 텍스트 편집기를 사용하여 열면 운영 체제와 함께 제공되는 메모장을 사용할 수 있지만 [Visual Studio Code](https://code.visualstudio.com/)와 같은 적절한 코드 편집기를 사용하는 것이 좋다.

이전에 설명했듯이 HTML 파일의 구조는 다음과 같다.

```html
<!DOCTYPE html>
<html>
  <head>
    . . .
  </head>

  <body>
    . . .
  </body>
</html>
```

이 HTML 파일을 열려면 파일을 두 번 클릭하고 브라우저로 열기만 하면 된다. 이제 `<head>` 섹션부터 시작하겠다.

```html
<head>
  <meta charset="utf-8" />
  <meta name="description" content="My Portfolio" />
  <meta name="keywords" content="HTML, CSS, JavaScript" />
  <meta name="author" content="Eric Hu" />
  <title>My Portfolio</title>
</head>
```

이 부분은 꽤 간단하다. 먼저 이 파일에서 사용할 문자 집합을 정의했으며, 이 정보는 브라우저에서 문자와 기호가 정확히 표시되도록 하는 데 사용된다. 다음으로, 이 웹 페이지의 설명, 키워드와 작성자를 명시했다. 이 정보는 표시되지 않지만 SEO(Search Engine Optimization) 목적으로 중요하다. 그리고 마지막으로, 이 웹 페이지의 제목도 선언되었다.

### 헤더와 탐색 바(navigation bar)

```html
<!--Header and Navigation Bar-->
<header>
  <h1>Eric's Portfolio</h1>
</header>
<nav>
  <ul>
    <li><a href="/">Root</a></li>
    <li><a href="portfolio.html">Home1</a></li>
    <li><a href="/portfolio.html">Home2</a></li>
    <li><a href="https://www.ericsdevblog.com">Blog</a></li>
    <li><a href="#project">Projects</a></li>
  </ul>
</nav>
```

7본째 줄부터 11줄까지 5가지 타입의 링크를 정의한 것을 볼 수 있다. 처음 셋은 상대 URL이고, 네 번째는 이미 숙지하고 있어야 할 절대 URL이며, 마지막 하나는 앵커 링크로, 나중에 설명할 것이다.

첫 번째 링크인 `Root`(`/`)는 서버 루트로 이동한다. 웹 서버를 사용하지 않는 경우 이 링크는 HTML 파일이 저장된 디스크 루트로 이동한다.

두 번째 링크 `Home1`(`portfolio.html`)의 경우 브라우저는 현재 디렉터리에서 `portfolio.html`이라는 파일을 찾는다. 웹 사이트가 커질수록 현재 어떤 디렉터리에 있는지 모를 수 있기 때문에 문제가 발생한다.

세 번째 링크 `Home2`(`/portfolio.html`)는 기본적으로 처음 두 링크의 조합이다. 브라우저는 항상 루트 디렉터리로 돌아간 다음 `portolio.html` 파일을 찾는다. 이렇게 하면 예기치 않은 오류가 발생할 가능성이 없어진다.

### 자기 소개 섹션

```html
<!--Self Introduction Section-->
<section>
  <h2>Hello, I am Eric. Welcome to My Portfolio.</h2>
  <p>. . .</p>
</section>
```

### 소식지 등록 섹션

```html
<!--Newsletter Sign Up Section-->
<section>
  <h2>Would you like to see more tips and tutorials on web development?</h2>
  <form>
    <label for="firstname">First Name:</label>
    <input type="text" id="firstname" />
    <br /><br />
    <label for="lastname">Last Name:</label>
    <input type="text" id="lastname" />
    <br /><br />
    <label for="email">Enter your email:</label>
    <input type="email" id="email" name="email" />
  </form>
</section>
```

### 기술 섹션

```html
<!--Skills Section-->
<section>
  <h2>My Skills</h2>
  <ul>
    <li>HTML (100%)</li>
    <li>CSS (90%)</li>
    <li>JavaScript (90%)</li>
    <li>Python (90%)</li>
    <li>PHP (100&#37;)</li>
    <li>Java (90&#37;)</li>
    <li>Vue.js (80&percnt;)</li>
    <li>Django (90&percnt;)</li>
    <li>Laravel (90&percnt;)</li>
  </ul>
</section>
```

백분율 기호(`%`)를 나타내기 위해 세 가지 다른 방법을 사용했는데 모두 동일한 결과를 얻을 수 있다.

### 프로젝트 섹션

```html
<!--Projects Section-->
<section id="project">
  <h2>My Projects</h2>
  <div>
    <h3>First Project</h3>
    <img src="/images/first project.jpg" width="500" height="300" />
    <p>. . .</p>
  </div>

  <div>
    <h3>Second Project</h3>
    <img src="/images/second project.jpeg" width="500" height="300" />
    <p>. . .</p>
  </div>

  <div>
    <h3>Third Project</h3>
    <img src="/images/third project.jpeg" width="500" height="300" />
    <p>. . .</p>
  </div>
</section>
```

`<section>` 요소에 `id` 애트리뷰트에 `project`라는 이름을 부여한다. 이것은 이전에 언급했던 앵커 링크와 함께 작업하기 위한 것이다.

```html
<a href="#project">Projects</a>
```

이 링크는 ID가 `project`인 요소로 이동시킬 수 있다. 또한 ID를 사용하여 앵커(anchor)할 요소를 지정하여야 하므로 전체 웹 페이지에서 고유해야 한다.

폴더 포트폴리오 아래의 이미지라는 폴더에 이 섹션의 이미지를 모두 저장한다.

![images-folder](images/create-portpolio/images-folder.webp)

### 각주(footer) 섹션

```html
<!--Footer-->
<footer>
  <p>Created by Eric Hu</p>
  <p><a href="mailto:huericnan@gmail.com">huericnan@gmail.com</a></p>
</footer>
```

![portfolio](images/create-portpolio/portfolio.webp)

이것이 최종 결과이다. 하지만, 여러분은 새로운 문제를 가지고 있다. 끔찍해 보인다. 그리하여 여러분에게 이와 같은 직업을 주지 않을 것이다. 하지만 너무 걱정하지 말자, 이 포스팅의 다음 부분에서 여러분은 CSS를 사용하여 이 웹 페이지를 더 매력적으로 만드는 방법을 찾을 수 있을 것이다.

## HTML 문서에 CSS 추가
이제 CSS로 portpolio 웹사이트를 더 좋게 만들어야 할 때이다. 저자가 전문 디자이너가 아니므로, 디자인이 형편없더라도 양해 바란다. 하지만 가능한 한 기본적인 개념을 명확하게 설명하려고 노력할 것이다.

먼저 `style.css` 파일을 만든 다음 HTML 문서로 가져온다.

![stylecss](images/create-portpolio/stylecss.webp)

### 반응형 레이아웃 만들기
먼저 포트폴리오에 대한 반응형 레이아웃을 설계하는 것부터 시작하자. 반응형 레이아웃에는 컨테이너와 그리드 시스템이라는 두 가지 주요 구성 요소가 있어야 한다.

먼저 브라우저 창 크기를 조정할 때 너비를 변경하는 유동 컨테이너가 있어야 한다. 보통 컨테이너가 가장자리까지 늘어나지 않도록 큰 화면에 최대 폭을 설정한다. 그러나 작은 화면에서 가능한 한 많은 공간을 활용하기 위해 보통 너비를 100%로 설정한다.

여러분이 할 수 있는 일은 다음과 같다.

```html
<body>
  <div class="container">. . .</div>
</body>
```

```css
/* Small Screen */
.container {
  max-width: 100%;
  display: flex;
  flex-wrap: wrap;
}

/* Large Screen */
@media only screen and (min-width: 1200px) {
  .container {
    max-width: 1140px;
    margin: auto;
    display: flex;
    flex-wrap: wrap;
  }
}
```

간단히 말하자면, 여기에는 단 하나의 중단점이 있다. 화면 너비가 1200px보다 작으면 컨테이너는 가장자리까지 늘어난다. 화면이 1200px보다 크면 용기의 최대 너비가 1140px로 설정된다. 두 번째 시나리오에서는 `margin: auto;`를 정의했다. 이는 컨테이너가 항상 입력되었는지 확인하기 위한 것이다.

![screen-1](images/create-portpolio/screen-1.png)

둘째로, small screen(휴대전화), medium screen(태블릿), large screen(데스크탑)에 대해 서로 다른 그리드 시스템을 갖춰야 한다. 각 그리드 시스템에는 12개의 열이 포함되어 있으며, 서로 다른 요소가 서로 다른 화면에서 서로 다른 열을 차지할 수 있다. 이는 [regilar grid system](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-2/) 또는 [flexbox](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-3/)를 통해 실현될 수 있다. 이 포스팅에서는 flexbox를 사용한다.

```html
<body>
  <div class="container">
    <div class="col-6 col-s-8">Div1</div>
    <div class="col-6 col-s-4">Div2</div>

    <div class="col-2 col-s-10">Div3</div>
    <div class="col-4 col-s-2">Div4</div>
  </div>
</body>
```

```css
/* For mobile phones: */
[class*="col-"] {
  width: 100%;
}

/* For tablets: */
@media only screen and (min-width: 600px) {
  .col-s-1 {
    width: 8.33%;
  }
  .col-s-2 {
    width: 16.66%;
  }
  .col-s-3 {
    width: 25%;
  }
  .col-s-4 {
    width: 33.33%;
  }
  .col-s-5 {
    width: 41.66%;
  }
  .col-s-6 {
    width: 50%;
  }
  .col-s-7 {
    width: 58.33%;
  }
  .col-s-8 {
    width: 66.66%;
  }
  .col-s-9 {
    width: 75%;
  }
  .col-s-10 {
    width: 83.33%;
  }
  .col-s-11 {
    width: 91.66%;
  }
  .col-s-12 {
    width: 100%;
  }
}

/* For desktop: */
@media only screen and (min-width: 768px) {
  .col-1 {
    width: 8.33%;
  }
  .col-2 {
    width: 16.66%;
  }
  .col-3 {
    width: 25%;
  }
  .col-4 {
    width: 33.33%;
  }
  .col-5 {
    width: 41.66%;
  }
  .col-6 {
    width: 50%;
  }
  .col-7 {
    width: 58.33%;
  }
  .col-8 {
    width: 66.66%;
  }
  .col-9 {
    width: 75%;
  }
  .col-10 {
    width: 83.33%;
  }
  .col-11 {
    width: 91.66%;
  }
  .col-12 {
    width: 100%;
  }
}
```

위의 코드는 [CSS Basics #2](https://sdldocs.gitlab.io/pythonnecosystem/coding-tips/css-basics-2/)에서 설명했던 것과 정확히 같은 코드이다. 어떻게 작동하는지 보자.

![screen-2](images/create-portpolio/screen-2.png)

이제 일반적인 반응형 레이아웃 설계를 마쳤으므로 세부적인 내용으로 들어갈 차례이다. 가이드로서 설계한 portpolio 페이지는 다음과 같다.

- [Small Screen Design](images/create-portpolio/small-screen-layout.png)
- [Medium Screen Design](images/create-portpolio/medium-screen-layout.png)
- [Large Screen Design](images/create-portpolio/large-screen-layout.png)

이미지가 너무 커서 여기에 표시할 수 없지만 직접 다운로드할 수 있다.

다음으로 웹 페이지 설계를 시작하기 전에 먼저 사용할 색 구성표와 글꼴을 선택한다. 전문 디자이너가 아니라면 어려울 수도 있지만 걱정하지 마시오. 도움이 될 수 있는 몇 가지 도구가 있다.

보기 좋은 색 구성을 찾으려면 다음 웹 사이트를 사용할 수 있다. [Cooler](https://coolors.co/)는 색상 코드가 있는 수백 가지의 다양한 색상 팔레트를 제공한다. 글꼴에 대해서는 [Google Fonts](https://fonts.google.com/)를 사용하는 것이 일반적이다. 어떤 글꼴이든 선택하여 CSS 파일에 사용할 수 있다.

![google-fonts](images/create-portpolio/google-fonts.webp)

선택한 글꼴은 다음과 같다.

```css
@import url("https://fonts.googleapis.com/css2?family=Comfortaa:wght@700&family=Courier+Prime:ital,wght@0,400;0,700;1,400;1,700&family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=Poppins:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700&display=swap");
```

위의 코드는 사용할 모든 필수 글꼴을 가져온다. 그것을 CSS 파일의 **맨 위**에 두어야 한다.

```css
h1 {
  font-family: "Comfortaa", cursive;
}

h2,
h3,
h4 {
  font-family: "Crimson Text", serif;
}

p,
a {
  font-family: "Poppins", sans-serif;
}
```

### 각 섹션 스타일 지정

#### 탐색 바

웹 페이지를 설계할 때, 항상 "모바일 우선"의 규칙을 따라야 한다. 이는 항상 작은 화면 레이아웃을 디자인하는 것으로 시작해야 한다는 것을 의미한다.

탐색 바부터 시작하겠다. 먼저 `<header>`와 `<nav>` 블록을 `<div>` 블록 안에 넣는다. `<div>` 블록은 모든 장치에서 12개의 열을 사용한다.

```html
<div class="col-12 col-s-12 header-and-nav">
  <header>. . .</header>
  <nav>. . .</nav>
</div>
```

다음으로, 작은 기기에서는 `<header>`를 `<nav>` 위에 두고, 큰 화면에서는 `<header>`를 왼쪽에 두고, `<nav>`를 오른쪽에 두고 싶다.

이를 위해서 `<div>` 요소도 플렉스박스 컨테이너인지 확인해야 한다. 코드는 다음과 같다.

```css
.header-and-nav {
  display: flex;
  flex-wrap: wrap;
}
```

```html
<div class="col-12 col-s-12 header-and-nav">
  <header class="col-4 col-s-4">. . .</header>

  <nav class="col-8 col-s-8">. . .</nav>
</div>
```

![nav-1](images/create-portpolio/nav-1.webp)

이것이 가지고 있는 탐색 바이다. 다음으로, 지금 작은 기기에만 관심이 있기 때문에, 헤더와 탐색 목록을 중앙에 두어야 한다. 코드는 다음과 같다.

```html
<!--Header and Navigation Bar-->
<div class="col-12 col-s-12 header-and-nav">
  <header class="col-4 col-s-4 header">
    <h1>Eric's Portfolio</h1>
  </header>
  <nav class="col-8 col-s-8">
    <ul class="nav-list">
      <li class="nav-item">
        <a href="/portfolio.html">Home</a>
      </li>
      . . .
    </ul>
  </nav>
</div>
```

```css
.header {
  text-align: center;
}

.nav-list {
  text-align: center;
}

.nav-item {
  display: inline-block;
}

.nav-item a {
  display: inline-block;
}
```

![nav-2](images/create-portpolio/nav-2.webp)

마지막으로 색상, 패딩, 여백, 텍스트 장식 등을 추가해 보겠다.

```css
.header {
  color: white;
  text-align: center;
}

.nav-list {
  list-style-type: none;
  margin: 0px;
  padding: 0px;

  text-align: center;
}

.nav-item {
  display: inline-block;
}

.nav-item a {
  display: inline-block;
  color: white;
  text-decoration: none;
  padding: 27px 20px;
}

.nav-item a:hover {
  background-color: #457b9d;
  text-decoration: underline;
}
```

![nav-3](images/create-portpolio/nav-3.webp)

마지막으로, 데스크톱에서 Navbar의 모양을 변경해야 하며, 모든 것이 중앙 집중화되는 것을 원하지 않는다. 대신 링크가 오른쪽으로 정렬되기를 원한다.

```css
@media only screen and (min-width: 768px) {
  .nav-list {
    list-style-type: none;
    margin: 0px;
    padding: 0px;

    text-align: right;
  }
}
```
![nav-4](images/create-portpolio/nav-4.png)

![self-intro](images/create-portpolio/self-intro.webp)

```html
<!--Self Introduction Section-->
<section class="col-12 col-s-12 self-intro">
  <img src="./images/profile-image.jpg" class="col-4 col-s-4 profile-image" />
  <div class="col-8 col-s-8 self-intro-text">
    <h2>Hello, I am XXXX. Welcome to My Portfolio.</h2>
    <p>. . .</p>
  </div>
</section>
```

```css
/* Self Introduction Section */
.self-intro {
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
}

.profile-image {
  object-fit: cover;
}

.self-intro-text h2 {
  text-decoration: underline;
}

@media only screen and (min-width: 768px) {
  .self-intro-text {
    padding: 0px 10px;
  }
}
```

#### newletter 섹션

![newsletter-sm](images/create-portpolio/newsletter-sm.webp)

*small screen에서*

![newsletter-lg](images/create-portpolio/newsletter-lg.webp)

*large screen에서*

```html
<!--Newsletter Sign up Section-->

<section class="col-12 col-s-12 newsletter-signup">
  <h2 class="col-6 col-s-6">
    Would you like to see more tips and tutorials on web development?
  </h2>
  <form class="col-6 col-s-6 newsletter-signup-form">
    <label for="firstname">First Name:</label><br />
    <input type="text" id="firstname" />
    <br />
    <label for="lastname">Last Name:</label><br />
    <input type="text" id="lastname" />
    <br />
    <label for="email">Enter your email:</label><br />
    <input type="email" id="email" name="email" />
    <button type="submit">Submit</button>
  </form>
</section>
```

```css
/* Newsletter Signup Section */
.newsletter-signup {
  background-color: #a8dadc;
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
}

.newsletter-signup h2 {
  text-align: center;
}

.newsletter-signup-form {
  margin: 0 auto;
}

.newsletter-signup-form label {
  font-family: "Poppins", sans-serif;
}

.newsletter-signup-form input {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}

.newsletter-signup-form button {
  background-color: #a8dadc;
  border: solid 3px #e63946;
  color: #e63946;
  width: 100%;
  padding: 10px 0;
  margin: 10px 0;
  cursor: pointer;
  font-family: "Poppins", sans-serif;
  font-size: 1em;
  font-weight: 600;
}

.newsletter-signup-form button:hover {
  background-color: #e63946;
  border: solid 3px #e63946;
  color: white;
}

@media only screen and (min-width: 768px) {
  .newsletter-signup {
    background-color: #a8dadc;
    padding: 10px;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
  .newsletter-signup h2 {
    text-align: center;
    padding: 10px;
  }
}
```

#### Skills 섹션

![skills](images/create-portpolio/skills.webp)

```html
<!--Skills Section-->
<section class="col-12 col-s-12 skills-section">
  <h2 class="col-12 col-s-12">My Skills</h2>
  <ul class="col-8 col-s-8 skills-list">
    <li>HTML (100%)</li>
    . . .
  </ul>
</section>
```

```css
/* Skills Section */
.skills-section {
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
}

.skills-section h2 {
  text-decoration: underline;
}

.skills-list {
  column-count: 2;
}

@media only screen and (min-width: 768px) {
  .skills-list {
    column-count: 2;
    margin: 0 auto;
  }
}
```

여기서 언급할 필요가 있는 한 가지 작은 트릭은 목록을 두 개의 열로 나누기 위해 `column-count: 2;`를 사용했다는 것이다.

#### projects 섹션

![projects-sm](images/create-portpolio/projects-sm.webp)
*small screen에서*

![projects-lg](images/create-portpolio/projects-lg.webp)
*large screen에서*

```html
<!--Projects Section-->

<section class="col-12 col-s-12 porject-section" id="project">
  <h2 class="col-12 col-s-12">My Projects</h2>
  <div class="col-4 col-s-4">
    <div class="project-card">
      <h3>First Project</h3>
      <img src="/frontend/images/p1.jpg" />
      <p>. . .</p>
    </div>
  </div>
  . . .
</section>
```

```css
/* Project Section */
.porject-section {
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
  background-color: #a8dadc;
}

.porject-section h2 {
  text-decoration: underline;
}

.project-card {
  border: solid 2px gray;
  border-radius: 5px;
  padding: 5px;
  margin: 5px;
}

.project-card img {
  max-width: 100%;
  object-fit: cover;
}
```

#### Footer

![footer](images/create-portpolio/footer.webp)

```html
<!--Footer-->

<footer class="col-12 col-s-12 footer">
  <p>Created by Eric Hu</p>
  <p><a href="mailto:huericnan@gmail.com">huericnan@gmail.com</a></p>
</footer>
```

```css
/* Footer */
.footer {
  background-color: #1d3557;
  color: white;
  text-align: center;
}

.footer a {
  color: white;
}
```

## Vue.js를 사용하여 반응형 애플리케이션 생성
마지막으로 이 섹션에서는 Vue.js를 사용하여 portpolio 웹 사이트를 업그레이드하려고 한다. 먼저 새 작업 폴더를 만들고 다음 명령을 실행하여 새 Vue.js 프로젝트를 설정한다.

```bash
npm init vue@latest
cd <your-project-name>
npm install
npm run dev
```

### 프로젝트 구조
이 프로젝트를 위해 다음과 같은 구조를 만들었다.

![portfolio-vue-component-structure](images/create-portpolio/portfolio-vue-component-structure.webp)

`App.vue`는 루트 구성 요소로, 5 하위 구성 요소가 있다. 이러한 하위 구성 요소 중 하나인 P`rojectListComponent.vue`, 다른 자식 `ProjectComponen.vuet`가 있다. 매우 단순한 중첩 구조를 형성하고 있다.

물론, 여러분이 원하는 구조물을 사용할 수 있다. 예를 들어, 자기소개, 기술, 뉴스레터와 프로젝트 목록이 포함된 `MainComponent`를 하위 구성요소로 지정할 수 있으며, 이는 약간 더 복잡한 구조를 만든다. 또는 많은 구성요소를 원하지 않는 경우 자기소개서, 기술과 뉴스레터 섹션을 하나의 구성요소로 결합할 수 있다. Vue.js는 프로젝트를 원하는 대로 구성할 수 있는 기능을 제공한다.

먼저 프로젝트의 진입점인 `index.html` 파일을 편집해야 한다. 메타 정보를 편집해야 한다.

#### `index.html`

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="description" content="My Portfolio" />
    <meta name="keywords" content="HTML, CSS, JavaScript" />
    <meta name="author" content="Eric Hu" />

    <title>My Portfolio</title>
  </head>
  <body>
    <div id="app"></div>
    <script type="module" src="/src/main.js"></script>
  </body>
</html>
```

`<div id="app"></div>`에서 루트 구성 요소(`App.vue`)를 마운트한다.

### 루트와 헤더 구성 요소 생성
이제 첫 번째 구성 요소인 `HeaderComponent.vue`를 만들 수 있다, 그리고 해당 HTML 코드를 복사하여 `<template>` 섹션에 붙여넣는다.

#### `src/components/HeaderComponent.vue`

```html
<script>
export default {
  props: ["websiteName"],
};
</script>

<template>
  <!--Header and Navigation Bar-->
  <div class="col-12 col-s-12 header-and-nav">
    <header class="col-4 col-s-4 header">
      <h1>{{ websiteName }}</h1>
    </header>
    <nav class="col-8 col-s-8">
      <ul class="nav-list">
        <li><a href="/">Root</a></li>
        <li><a href="portfolio.html">Home1</a></li>
        <li><a href="/portfolio.html">Home2</a></li>
        <li><a href="https://www.ericsdevblog.com">Blog</a></li>
        <li><a href="#project">Projects</a></li>
      </ul>
    </nav>
  </div>
</template>
```

그런 다음 방금 만든 구성 요소를 가져와 루트 구성 요소(`App.vue`)에 저장한다.

#### `src/App.vue`

```html
<script>
import HeaderComponent from "@/components/HeaderComponent.vue";

export default {
  data() {
    return {
      websiteName: "My Portfolio",
    };
  },
  components: {
    HeaderComponent,
  },
};
</script>

<template>
  <div class="container">
    <HeaderComponent></HeaderComponent>
  </div>
</template>

<style>
@import "./assets/style.css";
</style>
```

루트 구성 요소 내에서 CSS 코드를 가져와 다른 모든 구성 요소에도 적용해야 한다. 이 포스팅에서, CSS 파일을 `/src/assets/` 디렉토리 아래에 놓았다. 브라우저로 이동하면 탐색 바가 나타난다.

모든 것이 작동하는 것처럼 보이지만, 이제 새로운 문제가 생겼다. 모든 것이 하드 코딩되어 있기 때문에, 무언가를 바꾸고 싶다면, 코드 자체를 바꿔야 한다. 그러나 여러분의 목표는 웹 페이지를 동적이고 반응적으로 만드는 것이다. 즉, 무언가를 변경하려면 데이터베이스에 저장된 해당 데이터를 변경하기만 하면 된다.

이를 위해서는 변수로 정보를 묶어야 한다. navbar의 경우, 웹사이트 이름뿐만 아니라 내비게이션 링크도 바인딩할 수 있다.

#### `HeaderComponent.vue`

```html
<script>
export default {
  data() {
    return {
      websiteName: "My Portfolio"
      navLinks: [
        { id: 1, name: "Home", link: "#" },
        { id: 2, name: "Features", link: "#" },
        { id: 3, name: "Pricing", link: "#" },
        { id: 4, name: "FAQs", link: "#" },
        { id: 5, name: "About", link: "#" },
      ],
    };
  },
};
</script>

<template>
  <!--Header and Navigation Bar-->
  <div class="col-12 col-s-12 header-and-nav">
    <header class="col-4 col-s-4 header">
      <h1>{{ websiteName }}</h1>
    </header>
    <nav class="col-8 col-s-8">
      <ul class="nav-list">
        <li
          class="nav-item"
          v-for="navLink in navLinks"
          v-bind:key="navLink.id"
        >
          <a href="/portfolio.html" v-bind:href="navLink.link">{{
            navLink.name
          }}</a>
        </li>
      </ul>
    </nav>
  </div>
</template>
```

보시다시피, 여기서는 `websiteName`과 `navLinks`를 `HeaderComponent`에 모두 넣었다. 하지만 그것이 최선의 해결책일까? `websiteName`이 `FooterComponent` 구성요소에 다시 표시된다는 것은 이미 알고 있으며, 서로 다른 구성요소가 별도로 렌더링되므로 데이터베이스에서 `websiteName`을 두 번 검색해야 하므로 귀중한 리소스가 낭비된다.

대체 솔루션은 `websiteName`을 루트 구성 요소(App.vue) 내부에 배치하는 것이며, 여기서 데이터를 사용해야 하는 하위 구성 요소로 전달할 수 있다.

#### `App.vue`

```html
<script>
import HeaderComponent from "@/components/HeaderComponent.vue";

export default {
  data() {
    return {
      websiteName: "My Portfolio",
    };
  },
  components: {
    HeaderComponent,
  },
};
</script>

<template>
  <div class="container">
    <HeaderComponent v-bind:websiteName="websiteName"></HeaderComponent>
  </div>
</template>
```

#### `HeaderComponent.vue`

```html
<script>
export default {
  data() {
    return {
      navLinks: [
        . . .
      ],
    };
  },

  props: ["websiteName"],
};
</script>

<template>
  <!--Header and Navigation Bar-->
  <div class="col-12 col-s-12 header-and-nav">
    <header class="col-4 col-s-4 header">
      <h1>{{ websiteName }}</h1>
    </header>
    . . .
  </div>
</template>
```

시간을 절약하기 위해 이 포스팅에 다른 구성 요소는 포함하지 않지만 [여기](https://github.com/ericsdevblog/portfolio)에서 소스 코드를 다운로드할 수 있다. 그러나 소스 코드를 보기 전에 직접 사용해 보는 것이 좋다.

![portfolio-vue](images/create-portpolio/portfolio-vue.webp)

---
<a name="footnote_1">1</a>: 이 포스팅은 [Create a portfolio Website](https://www.ericsdevblog.com/posts/create-a-portfolio-website/)를 편역한 것이다.