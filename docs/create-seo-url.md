이 섹션에서는 기사에 대한 SEO 친화적인 URL을 만들 것이다. 기사 URL은 `http://127.0.0.1:8000/blog/PUBLISH-YEAR/PUBLISH-MONTH/PUBLISH-DATE/SLUG/`일 것이다.

## SEO 친화적인 URL이란?
SEO 친화적인 콘텐츠는 검색 엔진이 높은 순위를 매기는 데 도움이 되는 방식으로 제작된 콘텐츠 타입이다.

사이트의 URL 구조는 가능한 한 단순해야 한다. URL을 긴 ID 번호가 아닌 읽기 쉬운 단어로 구성한다.

## MySQL 시작과 가상 환경 활성화
Apache와 MySQL이 실행 중이 아니라면, 이들을 시작합니다. 또한 가상 환경을 활성화한다.

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## 기사의 SEO 친화적인 URL 가져오기 위한 메서드 생성

### `django.urls` 유틸리티 함수

<i>`reverse()`

코드에서 URL 템플릿 태그와 유사한 것을 사용해야 하는 경우, Django는 다음 함수를 제공한다.

`reverse(viewname, urlconf=None, args=None, kwargs=None, current_app=None)`: `viewname`은 URL 패턴 이름 또는 호출 가능한 뷰 객체일 수 있다.</i>

### `django.urls`에서 `reverse()` 메서드 가져오기 django.urls
`blog/models.py` 파일에 있는 `django.urls`에서 `reverse()` 메서드를 가져온다.

```python
from django.urls import reverse
```

![](images/create-seo-url/screenshot_06_01.webp)

### 기사 객체에 대한 표준 URL을 반환하는 메서드 작성
`get_canonical_url()`이라는 메서드를 만든다. 기사의 고유 URL이 필요하므로 게시된 기사의 연도, 월, 일, slug를 인자로 전달해야 한다.

```python
    def get_canonical_url(self):
        return reverse('blog:article_details', 
                        args=[
                                self.publish.year,
                                self.publish.month,
                                self.publish.day,
                                self.slug
                            ]
                        )
        pass
```

![](images/create-seo-url/screenshot_06_02.webp)

### 기사 모델 변경
기사 모델에서 슬러그 필드를 수정하고 `unique_for_date` 매개 변수를 추가해야 한다.

```python
    slug= models.SlugField(max_length=250, unique_for_date='publish')
```

![](images/create-seo-url/screenshot_06_03.webp)

### 뷰 변경
`blog/views.py` 파일에서 `article_details()` 메서드를 수정해야 한다. 입력 매개변수에 게시된 기사의 슬러그, 연도, 월, 날짜를 추가해야 한다. 기사 모델의 슬러그 필드에 `unique_for_date='publish'`를 추가하여 슬러그와 지정된 날짜가 있는 가사는 하나만 존재하도록 했다.

```python
    def article_details(request, year, month, day, article):
        try:
            article = get_object_or_404(Article, status=Article.Status.PUBLISHED, 
                        slug=article,
                        publish__year=year,
                        publish__month=month,
                        publish__day=day
                    )
        except Article.DoesNotExist:
            raise Http404("No article found.")
        
        return render(request, 'blog/detail.html', {'article': article})
        pass
```

![](images/create-seo-url/screenshot_06_04.webp)

### 기사에 대한 상세 정보의 url 변경
`blog/views.py`에서 `article_details()` 메서드의 입력 매개변수를 변경한다. 이제 `article_details` 뷰의 URL 패턴을 변경해야 한다.

```python
    # Change or comment following url 
    # path('<int:id>/', views.article_details, name='article_details'),
    # with
    path('<int:year>/<int:month>/<int:day>/<slug:article>/', views.article_details, name='article_details'),
```

![](images/create-seo-url/screenshot_06_05.webp)

### `blog/templates/blog/list.html`에 기사에 대한 상세 정보의 url 변경

```
Change the following line 
<a href="{% url 'blog:article_details' article.id %}"> 
with 
<a href="{{ article.get_canonical_url }}"> in blog/templates/blog/list.html file.
```

![](images/create-seo-url/screenshot_06_06.webp)

### `makemigrations`과 `migrate` 명령 실행

```bash
$ python manage.py makemigrations blog

$ python manage.py migrate
```

![](images/create-seo-url/cli_06_01.png)

![](images/create-seo-url/cli_06_02.png)

## 개발 서버 실행

개발 서버를 실행한 후 브라우저에서 다음 URL을 입력한다.

`http://127.0.0.1:8000/blog/`

기사 제목을 클릭하면 아래와 같은 URL이 표시된다.

`http://127.0.0.1:8000/blog/2023/7/4/draw-types-of-triangles-using-matplotlib-module/`

![](images/create-seo-url/screenshot_06_07.png)

`http://127.0.0.1:8000/blog/2023/7/4/most-useful-linux-commands/`

![](images/create-seo-url/screenshot_06_08.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Created SEO friendly url for article details"
$ git push origin main
```

![](images/create-seo-url/cli_06_03.png)

이 파트는 여기까지 ...

**(주)** 위 내용은 [Creating SEO-friendly URLs for articles — Part 6](https://medium.com/@nutanbhogendrasharma/creating-seo-friendly-urls-for-the-articles-in-django-part-6-2b4602aa8491)을 편역한 것이다. 
