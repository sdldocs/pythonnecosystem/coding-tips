마지막으로, Django를 사용하여 완벽한 블로그 어플리케이션을 만들 시간이다. 이전 포스팅에서 모델, 뷰와 템플릿이 어떻게 함께 작동하여 Django 어플리케이션을 만들 수 있는지 살펴보았지만, 솔직히 각 기능에 대해 최소 5개의 동작을 작성해야 하고 대부분의 코드가 반복적으로 느껴지기 때문에 지루한 과정이다.

그래서 이 포스팅<sup>[1](#footnote_1)</sup>에서 Django의 가장 좋은 기능 중 하나인 내장 관리 패널을 활용할 것이다. 어플리케이션 프로그램에 대해 생성하려는 대부분의 기능은 show/list action만 작성하면 되며 나머지는 Django가 자동으로 처리한다.

## 모델 레이어 생성
다시 데이터베이스 구조 설계를 시작하자.

### 데이터베이스 구조 설계
기본 블로그 시스템의 경우 최소 4개의 모델이 필요하다. `User`, `Category`, `Tag`와 `Post`. 다음 포스팅에서는 몇 가지 고급 기능을 추가하겠지만, 지금은 이 네 가지 모델만 있으면 된다.

#### `User` 모델

| key | type | info |
|-----|------|------|
| id | integer | auto increment |
| name | string | |
|email | string | unique |
| passwd | string | |

`User` 모델은 이미 장고에 포함되어 있고, 여러분은 그것에 대해 아무것도 할 필요가 없다. 내장된 `User` 모델은 비밀번호 해싱, 사용자 인증과 같은 몇 가지 기본 기능과 Django admin과 통합된 내장된 권한 시스템을 제공한다. 이것이 어떻게 작동하는지 나중에 알게 될 것이다.

#### `Tag` 모델

| key | type | info |
|-----|------|------|
| id | integer | auto increment |
| name | string | |	
| slug | string | unique |
| description | text | |

#### `Post` 모델

| key | type | info |
|-----|------|------|
| id | integer | auto increment |
| title | string | |
| slug | string | unique |
| content | text | | 
| featured_image | string | |
| is_published | boolean | |
| is_featured | boolean | |
| created_at | date | |

#### `Site` 모델
물론 이름, 설명, 로고와 같은 전체 웹 사이트의 기본 정보를 저장하는 다른 테이블이 필요하다.

| key | type | info |
|-----|------|------|
| name | string | |
| description | text | |
| logo | string | |

#### `relations`

이 블로그 어플리케이션의 경우, 처리해야 할 관계가 여섯 가지 있다.

- 각 사용자는 여러 개의 게시물을 갖는다
- 각 카테고리에는 많은 게시물이 있다
- 각 태그는 많은 게시물에 속한다
- 각 게시물은 한 사용자가 작성한 것이다
- 각 게시물은 한 범주에 속한다
- 각 게시물은 많은 태그에 속합한다

### 설계 구현
이 디자인을 구현할 때이다.

#### `Site` 모델

먼저,  `Site`모델이 필요하다.

```python
class Site(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    logo = models.ImageField(upload_to="logo/")

    class Meta:
        verbose_name_plural = "sites"

    def __str__(self):
        return self.name
```

`ImageField()`에 주의하시오. 이 필드는 실제로 `string` 타입이다. 데이터베이스는 이미지를 저장할 수 없으므로, 대신 이미지는 서버의 파일 시스템에 저장되고, 이 필드는 이미지의 위치를 가리키는 경로를 유지한다.

이 예에서는 이미지가 `mediafiles/logo/` 디렉토리에 업로드된다. `settings.py` 파일에서 `MEDIA_ROOT = "mediafiles/"`를 정의했다.

이 `ImageField()`가 작동하려면 컴퓨터에 `Pillow`를 설치해야 한다.

```bash
$ pip install Pillow
```

#### `Category` 모델

```python
class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    description = models.TextField()

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name
```

`Category` 모델은 이해하기 쉬워야 한다. 말하고 싶은 것은 `Meta` 클래스이다. 이렇게 하면 메타데이터를 모델에 추가할 수 있다.

모델의 메타데이터는 주문 옵션, 데이터베이스 테이블 이름 등 필드가 아닌 모든 항목이다. 이 경우 단어 범주의 복수 형식을 정의하기 위해 `verbose_name_plural`을 사용한다. 불행히도 Django는 이 특정 측면에서 Laravel만큼 "smart"하지 않다. 만약 Django에게 정확한 복수 형태를 제공하지 않는다면 대신 범주를 사용할 것이다.

그리고 `__str__(self)` 함수는 Django가 특정 범주를 언급할 때 어떤 필드를 사용할지 정의하는데, 이 경우에는 `name` 필드를 사용하고 있다. django admin 섹션에 가면 왜 이것이 필요한지 분명해질 것이다.

#### `Tag` 모델

```python
class Tag(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name
```

#### `Post` 모델

```python
from ckeditor.fields import RichTextField

. . .

class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    content = RichTextField()
    featured_image = models.ImageField(upload_to="images/")
    is_published = models.BooleanField(default=False)
    is_featured = models.BooleanField(default=False)
    created_at = models.DateField(auto_now=True)

    # Define relations
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tag = models.ManyToManyField(Tag)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
```

첫 줄의 코드를 복사하여 붙여넣기만 하면 편집기에서 `RichTextField`와 ckeditor를 찾을 수 없다고 한다. 이는 서드파티 패키지이며, Django 프레임워크에 포함되지 않기 때문이다.

이전 포스팅에서는 게시물을 만들 때 일반 텍스트만 추가할 수 있으므로 블로그에 적합하지 않다. rich text 편집기 또는 WYSIWYG HTML 편집기를 사용하면 코드를 작성하지 않고 HTML 페이지를 직접 편집할 수 있다. 이 포스팅에서는 [CKEditor](https://ckeditor.com/)를 예로 사용한다.

![ckeditor](images/django_beginner/ckeditor.webp)

CKEDitor를 설치하려면 다음 명령을 실행한다.

```bash
$ pip install django-ckeditor
```

이후, `ckeditor`를 `settings.py`에 등록한다.

```python
INSTALLED_APPS = [
    "blog",
    "ckeditor",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]
```

#### relation 정의
마지막으로 모델에 relation을 추가할 수 있다. `Post` 모델에 코드 세 줄만 추가하면 된다.

```python
category = models.ForeignKey(Category, on_delete=models.CASCADE)
tag = models.ManyToManyField(Tag)
user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
```

그리고 내장된 `User` 모델(`settings.AUTH_USER_MODEL`)을 사용하고 있기 때문에 `settings` 모듈을 import해야 한다.

```python
from django.conf import settings
```

마지막으로 마이그레이션 파일을 생성하여 데이터베이스에 적용한다.

```bash
$ python manage.py makemigration
```

```bash
$ python manage.py migrate
```

## `admin` 패널 설정
다음 단계는 admin 패널을 설정하는 것이다. Django는 admin 시스템이 내장되어 있으며, 이를 사용하기 위해서는 다음 명령을 실행하여 슈퍼 유저를 등록하기만 하면 된다.

```bash
$ python manage.py createsuperuser
```

![superuser](images/django_beginner/superuser.webp)

http://127.0.0.1:8000/admin/ 으로 이동하여 admin 패널을 액세스할 수 있다.

![django-login](images/django_beginner/django-login.webp)

![admin](images/django_beginner/admin.webp)

현재 admin 패널은 여전히 비어 있으며 인증 탭만 있으며 이 탭을 사용하여 사용자들에게 각각 다른 역할을 할당할 수 있다. 이는 다른 튜토리얼이 필요한 다소 복잡한 주제이므로 지금은 다루지 않을 것이다. 대신 블로그 앱을 admin 시스템에 연결하는 방법에 중점을 둔다.

블로그 앱 안에서 `admin.py` 라는 파일을 찾을 수 있을 것이다. 여기에 다음 코드를 추가한다.

#### `blog/admin.py`

```python
from django.contrib import admin
from .models import Site, Category, Tag, Post


# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Site)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Post, PostAdmin)
```

2째 줄에서 방금 만든 모델을 가져온 다음 `admin.site.register()`를 사용하여 가져온 모델을 등록한다. 그러나 `Category` 모델을 등록할 때 `CategoryAdmin`이라는 추가 기능이 있는데, 이는 6째 줄에 정의된 클래스이다. 이렇게 하면 Django admin 시스템에 추가 정보를 전달할 수 있다.

여기서 `prepopulated_fields`를 사용하여 모든 범주, 태그와 게시물에 대한 slugs를 생성할 수 있다. `slug`의 값은 `name`에 따라 달라질 것이다. 새 범주를 만들어 테스트해 보겠다.

`http://127.0.0.1:8000/admin/`으로 이동한다. **`Categories`**를 클릭하고 새 카테고리를 추가한다. 모델에서 카테고리의 복수 형태를 정의한 것을 기억하는가? 이것이 그렇게 하지 않으면 Django가 대신 카테고리를 사용할 필요가 있는 이유이다.

![admin-home](images/django_beginner/admin-home.webp)

![category](images/django_beginner/category.webp)

이름을 입력하면 슬러그가 자동으로 생성된다. 더미 데이터를 추가해 보자, 모든 것이 원활하게 작동할 것이다.

### 선택적 구성
하지만, 아직 끝나지 않았다. 카테고리 패널을 열면 게시 페이지에서 카테고리를 액세스할 수 있지만 카테고리 페이지에서 해당 게시물을 액세스할 수 있는 방법이 없다. 그럴 필요가 없다고 생각되면 다음 섹션으로 넘어가세요. 그러나 이 문제를 해결하려면 `InlineModelAdmin`을 사용해야 한다.

#### `blog/admin.py`

```python
class PostInlineCategory(admin.StackedInline):
    model = Post
    max_num = 2


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    inlines = [
        PostInlineCategory
    ]
```

먼저 `PostInlineCategory` 클래스를 만든 다음 `CategoryAdmin`에서 사용한다. `max_num = 2`는 두 개의 게시물만 카테고리 페이지에 표시된다는 것을 의미한다. 다음과 같이 표시된다.

![category-inline](images/django_beginner/category-inline.webp)

다음, `TagAdmin`에 대하여도 동일하게 수행한다.

#### `blog/admin.py`

```python
class PostInlineTag(admin.TabularInline):
    model = Post.tag.through
    max_num = 5


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    inlines = [
        PostInlineTag
    ]
```

코드는 매우 유사하지만 `model`은 `Post`뿐만 아니라 `Post.tag.through`이다. 그것은 `Post`와 `Tag`의 관계가 many-to-many 관계이기 때문이다. 이것이 최종 결과이다.

![tag-inline](images/django_beginner/tag-inline.webp)

## 뷰 레이어 구축
이전 섹션에서는 주로 Django 어플리케이션의 백엔드와 관리 부분에 중점을 두었다. 이제 사용자가 볼 수 있는 부분인 프런트 엔드에 집중해야 할 때이다. 뷰 함수부터 시작하겠다.

블로그 어플리케이션 프로그램에 대한 admin 패널이 설정되어 있으므로 전체 CRUD 작업을 직접 작성할 필요가 없다. 대신 데이터베이스에서 정보를 검색하는 방법만 걱정하면 된다. 홈, 카테고리, 태그, 게시물 등 4페이지가 필요하며, 각각 하나의 뷰 기능이 필요하다.

### 홈 뷰

#### `blog/views.py`

```python
from .models import Site, Category, Tag, Post

def home(request):
    site = Site.objects.first()
    posts = Post.objects.all().filter(is_published=True)
    categories = Category.objects.all()
    tags = Tag.objects.all()

    return render(request, 'home.html', {
        'site': site,
        'posts': posts,
        'categories': categories,
        'tags': tags,
    })
```

첫째 줄, 여기서는 이전 포스팅에서 작성한 모델을 import한다.

네째 줄 site에는 웹 사이트의 기본 정보가 포함되어 있으며 데이터베이스에서 항상 첫 번째 레코드를 검색한다.

다섯 째 `filter(is_published=True)`는 게시된 블로그만 표시되도록 한다.

다음으로 해당 URL 디스패처를 잊지 마시오.

#### `djangoBlog/urls.py`

```python
path('', views.home, name='home'),
```

### category 뷰

#### blog/views.py

```python
def category(request, slug):
    site = Site.objects.first()
    posts = Post.objects.filter(category__slug=slug).filter(is_published=True)
    requested_category = Category.objects.get(slug=slug)
    categories = Category.objects.all()
    tags = Tag.objects.all()

    return render(request, 'category.html', {
        'site': site,
        'posts': posts,
        'category': requested_category,
        'categories': categories,
        'tags': tags,
    })
```

#### `djangoBlog/urls.py`

```python
path('category/<slug:slug>', views.category, name='category'),
```

여기서 URL에서 view 함수로 추가 변수인 `slug`를 전달했고, 3째 줄과 4째 줄에서는 그 변수를 사용하여 올바른 범주와 게시물을 찾았다.

### 태그 뷰

#### `blog/views.py`

```python
def tag(request, slug):
    site = Site.objects.first()
    posts = Post.objects.filter(tag__slug=slug).filter(is_published=True)
    requested_tag = Tag.objects.get(slug=slug)
    categories = Category.objects.all()
    tags = Tag.objects.all()

    return render(request, 'tag.html', {
        'site': site,
        'posts': posts,
        'tag': requested_tag,
        'categories': categories,
        'tags': tags,
    })
```

#### djangoBlog/urls.py`

```python
path('tag/<slug:slug>', views.tag, name='tag'),
```

### post 뷰

#### `blog/viewss.py`

```python
def post(request, slug):
    site = Site.objects.first()
    requested_post = Post.objects.get(slug=slug)
    categories = Category.objects.all()
    tags = Tag.objects.all()

    return render(request, 'post.html', {
        'site': site,
        'post': requested_post,
        'categories': categories,
        'tags': tags,
    })
```

#### djangoBlog/urls.py`

```python
path('post/<slug:slug>', views.post, name='post'),
```

## 탬플릿 레이어 생성
템플릿의 경우, HTML과 CSS는 실제로 이 포스팅의 초점이 아니기 때문에 HTML과 CSS 코드를 직접 작성하는 대신 [저자가 만든 템플릿](https://github.com/ericsdevblog/blog-template)을 사용한다.

![blog-template](images/django_beginner/blog-template.webp)

탬플릿 구조는 다음과 같다.

```
templates
├── category.html
├── home.html
├── layout.html
├── post.html
├── search.html
├── tag.html
└── vendor
    ├── list.html
    └── sidebar.html
```

`layout.html`에는 머리글과 바닥글이 포함되어 있으며 CSS와 JavaScript 파일을 import한다.`home`, `category`, `tag`와 `post`는 view 함수가 가리키는 템플릿이며 모두 레이아웃으로 확장된다. 마지막으로 `vendor` 디렉터리 내부에는 서로 다른 템플릿에 여러 번 나타나는 구성 요소가 있으며 `include` 태그를 사용하여 이 구성 요소를 import할 수 있다.

### layout

#### `layout.html`

```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {% load static %}
    <link rel="stylesheet" href="{% static 'style.css' %}" />
    {% block title %}{% endblock %}
  </head>

  <body class="container mx-auto font-serif">
    <nav class="flex flex-row justify-between h-16 items-center border-b-2">
      <div class="px-5 text-2xl">
        <a href="/"> My Blog </a>
      </div>
      <div class="hidden lg:flex content-between space-x-10 px-10 text-lg">
        <a
          href="https://github.com/ericnanhu"
          class="hover:underline hover:underline-offset-1"
          >GitHub</a
        >
        <a href="#" class="hover:underline hover:underline-offset-1">Link</a>
        <a href="#" class="hover:underline hover:underline-offset-1">Link</a>
      </div>
    </nav>

    {% block content %}{% endblock %}

    <footer class="bg-gray-700 text-white">
      <div
        class="flex justify-center items-center sm:justify-between flex-wrap lg:max-w-screen-2xl mx-auto px-4 sm:px-8 py-10"
      >
        <p class="font-serif text-center mb-3 sm:mb-0">
          Copyright ©
          <a href="https://www.ericsdevblog.com/" class="hover:underline"
            >Eric Hu</a
          >
        </p>

        <div class="flex justify-center space-x-4">
          . . .
        </div>
      </div>
    </footer>
  </body>
</html>
```

이 파일에서 설명해야 할 것이 하나 있다. 7째 불부터 8째 줄까지, 이것이 Django에서 정적 파일(CSS 및 자바스크립트 파일)을 import할 수 있는 방법이다. 물론 이 포스팅에서 CSS에 대해 논의하는 것은 아니지만, 추가적인 CSS 파일을 가져와야 한다면 어떻게 해야 하는지에 대해 보여주고 있다.

기본적으로 Django는 개별 앱 폴더에서 정적 파일을 검색한다. blog 앱의 경우, Django는 `/blog`로 이동하여 `static`이라는 폴더를 검색하고, 그 `static` 폴더 안에서 Django는 템플릿에 정의된 `style.css` 파일을 찾는다.

```
blog
├── admin.py
├── apps.py
├── __init__.py
├── migrations
├── models.py
├── static
│   ├── input.css
│   └── style.css
├── tests.py
└── views.py
```

### Home

![home-2](images/django_beginner/home-2.webp)

#### `home.html`

```html
{% extends 'layout.html' %} {% block title %}
<title>Page Title</title>
{% endblock %} {% block content %}
<div class="grid grid-cols-4 gap-4 py-10">
  <div class="col-span-3 grid grid-cols-1">
    <!-- Featured post -->
    <div class="mb-4 ring-1 ring-slate-200 rounded-md hover:shadow-md">
      <a href="{% url 'post' featured_post.slug %}"
        ><img
          class="float-left mr-4 rounded-l-md object-cover h-full w-1/3"
          src="{{ featured_post.featured_image.url }}"
          alt="..."
      /></a>
      <div class="my-4 mr-4 grid gap-2">
        <div class="text-sm text-gray-500">
          {{ featured_post.created_at|date:"F j, o" }}
        </div>
        <h2 class="text-lg font-bold">{{ featured_post.title }}</h2>
        <p class="text-base">
          {{ featured_post.content|striptags|truncatewords:80 }}
        </p>
        <a
          class="bg-blue-500 hover:bg-blue-700 rounded-md p-2 text-white uppercase text-sm font-semibold font-sans w-fit focus:ring"
          href="{% url 'post' featured_post.slug %}"
          >Read more →</a
        >
      </div>
    </div>

    {% include "vendor/list.html" %}
  </div>
  {% include "vendor/sidebar.html" %}
</div>
{% endblock %}
```

사이드바와 게시물 목록을 하드코딩하는 대신 카테고리와 태그 페이지에서 동일한 구성 요소를 사용할 예정이므로 이를 분리하여 `vendor` 디렉토리에 배치했다.

### posts 목록

#### `vendor/list.html`

```html
<!-- List of posts -->
<div class="grid grid-cols-3 gap-4">
  {% for post in posts %}
  <!-- post -->
  <div class="mb-4 ring-1 ring-slate-200 rounded-md h-fit hover:shadow-md">
    <a href="{% url 'post' post.slug %}"
      ><img
        class="rounded-t-md object-cover h-60 w-full"
        src="{{ post.featured_image.url }}"
        alt="..."
    /></a>
    <div class="m-4 grid gap-2">
      <div class="text-sm text-gray-500">
        {{ post.created_at|date:"F j, o" }}
      </div>
      <h2 class="text-lg font-bold">{{ post.title }}</h2>
      <p class="text-base">
        {{ post.content|striptags|truncatewords:30 }}
      </p>
      <a
        class="bg-blue-500 hover:bg-blue-700 rounded-md p-2 text-white uppercase text-sm font-semibold font-sans w-fit focus:ring"
        href="{% url 'post' post.slug %}"
        >Read more →</a
      >
    </div>
  </div>
  {% endfor %}
</div>
```

셋째 줄부터 27째 줄까지, 뷰에서 템플릿으로 변수 `posts`을 전달했다는 것을 기억하자. `posts`에는 게시물 모음이 포함되어 있으며, 여기서 템플릿 내부에서는 `for` 루프를 사용하여 해당 컬렉션의 모든 항목을 반복한다.

6째 줄, 다음과 같은 URL 디스패처를 만들었다는 것을 기억하시오.

```python
path('post/<slug:slug>', views.post, name='post'),
```

템플릿에서 `{% url 'post' post.slug %}이(가) 'posts'라는 이름의 URL 디스패처를 찾고 `post.slug` 값을 변수 <slug:slug>에 할당하며, 이를 해당 view 함수로 전달한다.

14째 줄, 기본값이 사용자 친화적이지 않기 때문에 `date` 필터는 템플릿으로 전달되는 날짜 데이터를 포맷한다. 다른 날짜 형식은 [여기](https://docs.djangoproject.com/en/4.0/ref/templates/builtins/#date)에서 확인할 수 있다.

18째 줄, 여기 두 필터를 `post.content`로 연결한다. 첫 번째 것은 HTML 태그를 제거하고 두 번째 것은 처음 30개의 단어를 가져오고 나머지를 잘라낸다.

### sidebar

#### `vendor/sidebar.html`

```html
<div class="col-span-1">
  <div class="border rounded-md mb-4">
    <div class="bg-slate-200 p-4">Search</div>
    <div class="p-4">
      <form action="" method="get">
        <input type="text" name="search" id="search" class="border rounded-md w-44 focus:ring p-2" placeholder="Search something...">
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 rounded-md p-2 text-white uppercase font-semibold font-sans w-fit focus:ring">Search</button>
      </form>
    </div>
  </div>
  <div class="border rounded-md mb-4">
    <div class="bg-slate-200 p-4">Categories</div>
    <div class="p-4">
      <ul class="list-none list-inside">
        {% for category in categories %}
        <li>
          <a
            href="{% url 'category' category.slug %}"
            class="text-blue-500 hover:underline"
            >{{ category.name }}</a
          >
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="border rounded-md mb-4">
    <div class="bg-slate-200 p-4">Tags</div>
    <div class="p-4">
      {% for tag in tags %}
      <span class="mr-2"
        ><a
          href="{% url 'tag' tag.slug %}"
          class="text-blue-500 hover:underline"
          >{{ tag.name }}</a
        ></span
      >
      {% endfor %}
    </div>
  </div>
  <div class="border rounded-md mb-4">
    <div class="bg-slate-200 p-4">More Card</div>
    <div class="p-4">
      <p>
        . . .
      </p>
    </div>
  </div>
</div>
```

### Category

#### `category.html`

```html
{% extends 'layout.html' %}

{% block title %}
<title>Page Title</title>
{% endblock %}

{% block content %}
<div class="grid grid-cols-4 gap-4 py-10">
  <div class="col-span-3 grid grid-cols-1">

    {% include "vendor/list.html" %}

  </div>
  {% include "vendor/sidebar.html" %}
</div>
{% endblock %}
```

### Tag

#### `tag.html`

```html
{% extends 'layout.html' %}

{% block title %}
<title>Page Title</title>
{% endblock %}

{% block content %}
<div class="grid grid-cols-4 gap-4 py-10">
  <div class="col-span-3 grid grid-cols-1">

    {% include "vendor/list.html" %}

  </div>
  {% include "vendor/sidebar.html" %}
</div>
{% endblock %}
```

### Post

![](images/django_beginner/post.webp)

```html
{% extends 'layout.html' %}

{% block title %}
<title>Page Title</title>
{% endblock %}

{% block content %}
<div class="grid grid-cols-4 gap-4 py-10">
  <div class="col-span-3">

    <img
        class="rounded-md object-cover h-96 w-full"
        src="{{ post.featured_image.url }}"
        alt="..."
    />
    <h2 class="mt-5 mb-2 text-center text-2xl font-bold">{{ post.title }}</h2>
    <p class="mb-5 text-center text-sm text-slate-500 italic">By {{ post.user|capfirst }} | {{ post.created_at }}</p>

    <div>{{ post.content|safe }}</div>

    <div class="my-5">
        {% for tag in post.tag.all %}
        <a href="{% url 'tag' tag.slug %}" class="text-blue-500 hover:underline" mr-3">#{{ tag.name }}</a>
        {% endfor %}
    </div>

  </div>
  {% include "vendor/sidebar.html" %}
</div>
{% endblock %}
```

마지막으로 설명할 내용은 19째 줄에 관한 것이다. `safe` 필터를 추가했다. 기본적으로 Django는 보안상의 이유로 HTML 코드를 일반 텍스트로 렌더링하기 때문에 Django에게 HTML 코드를 HTML로 렌더링해도 괜찮다고 알려줘야 한다.

마지막으로, 개발 서버를 시작하고 첫 번째 Django 앱을 방문해 보자.

---
<a name="footnote_1">1</a>: 이 포스팅은 [Django for Beginners #4 - The Blog App](https://www.ericsdevblog.com/posts/django-for-beginners-4/)를 편역한 것이다.
