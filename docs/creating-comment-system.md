이 섹션에서는 사용자가 기사에 댓글을 달 수 있도록 댓글 시스템을 추가한다. 댓글을 위해 댓글 모델과 댓글 양식을 만들 것이다. 사용자는 댓글 양식을 통해 댓글을 제출할 수 있다.

## `Xampp` 시작과 가상 환경 활성화
모든 작업을 수행하기 전에 `Xampp`(아파치 및 mysql)를 시작하고 가상 환경을 활성화해야 한다.

## `blog/models.py`에 `Comment` 모델 생성
댓글 모델을 만들려고 한다. 여기에는 `article`, `name`, `email`, `body`, `created`, `updated`과 `active` 필드를 추가한다. 댓글이 하나의 기사에 대한 댓글이 되도록 `ForeignKey` 필드를 추가한다. 하나의 기사에 대해 여러 댓글이 작성될 수 있으므로 다대일 관계가 될 것dl다.

```python
class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments')
    name = models.CharField(max_length=100)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['created']
        indexes = [
            models.Index(fields=['created']),
        ]
    
    def __str__(self):
        return f'Comment by {self.name} on {self.article}'
        pass
    
    pass
```

![](images/creating-comment-system/screenshot_08_01.webp)

## `makemigrations`과 `migrate` 명령 수행

`Comment` 모델을 만들었으니 이제 메이크 `makemigrations`과 `migrate` 명령을 실행해야 한다.

```bash
$ python manage.py makemigrations blog
```

![](images/creating-comment-system/cli_08_01.png)

```bash
$ python manage.py migrate
```

![](images/creating-comment-system/cli_08_02.png)

## Add comment to the `admin` 패널에서 댓글 추가
`blog/admin.py` 파일을 열고 댓글 모델을 가져와서 다음 코드를 작성한다.

## 개발 서버 수행

```python
$ python manage.py runserver
```

개발 서버를 실행한 후 `admin` URL을 브라우징한다.

`http://127.0.0.1:8000/admin/`

로그인 후 아래와 같이 관리 섹션에서 댓글을 확인할 수 있다.

![](images/creating-comment-system/screenshot_08_02.png)

"`+ ADD`"를 클릭하면 아래와 같은 다음 화면이 나타난다.

![](images/creating-comment-system/screenshot_08_03.png)

모든 기사에 댓글을 추가할 수 있다. 하나의 기사에 대해 하나의 댓글을 추가한다. 저장 후 관리 섹션에서 댓글을 볼 수 있다.

![](images/creating-comment-system/screenshot_08_04.png)

## 댓글 폼 생성
관리 섹션에서 댓글을 추가할 수 있다. 이제 사용자가 댓글을 제출할 수 있는 댓글 양식을 제공해야 한다. `blog` 폴더에 아래와 같이 `forms.py` 파일을 만든다.

### `blog/forms.py` 파일 생성

![](images/creating-comment-system/screenshot_08_05.webp)

### `blog/forms.py` 파일에 `Comment` 모델 가져오기

```python
from .models import Comment
```

![](images/creating-comment-system/screenshot_08_06.webp)

### 댓글 폼 생성
`CommentForm`이라는 클래스를 생성한다. 부모 클래스는 ModelForm이다. 모델에서 댓글 양식을 만들려면 `Meta` 클래스에서 model을 언급해야 한다. Django는 모델에 따라 자동으로 폼을 빌드한다. 모든 필드에 대한 양식을 만들고 싶지 않으므로 `name`, `email`과 `body`만 포함시킨다.

```python
from .models import Comment
from django import forms

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['name', 'email', 'body']
        pass
    
    pass
```

![](images/creating-comment-system/screenshot_08_07.webp)

## 댓글 폼을 위한 뷰 만들기
Django는 다양한 HTTP 기능을 지원하기 위해 뷰에 적용할 수 있는 여러 데코레이터를 제공한다.

### `@require_POST()`
데코레이터를 추가하여 뷰가 `POST` 메서드만 허용하도록 한다.

`blog/views.py` 파일을 열고 기사에 댓글을 작성하는 메서드를 작성한다.

```python
@require_POST
def comment_for_article(request, article_id):

    # get the article by article_id
    article = get_object_or_404(Article, id = article_id, status=Article.Status.PUBLISHED)
    comment = None
    
    # A comment form
    form = CommentForm(data=request.POST)

    if form.is_valid():
        # Create a Comment object before saving it to the database
        comment = form.save(commit=False)

        # Assign the article to the comment
        comment.article = article
        # Save the comment to the database
        comment.save()
        pass

    return render(request, 'blog/comment.html', {'article': article, 'form': form, 'comment': comment})

    pass
```

![](images/creating-comment-system/screenshot_08_08.webp)

## `comment_for_article()` 메서드를 위한 url 패턴 생성

```python
path('<int:article_id>/comment/', views.comment_for_article, name='comment_for_article'),
```

![](images/creating-comment-system/screenshot_08_09.webp)

## 댓글 폼을 위한 탬플릿 만들기
`blog/template/blog` 폴더에 `comment_form.html`을 생성한다.

```html
<h2>Add a new comment</h2>
<form action="{% url "blog:comment_for_article" article.id %}" method="post">
    {{ form.as_p }}
    {% csrf_token %}
    <p><input type="submit" value="Add comment"></p>
</form>
```

![](images/creating-comment-system/screenshot_08_10.webp)

## `comment.html` 생성

```html
{% extends "blog/base.html" %}

{% block title %}Add a comment{% endblock %}

{% block content %}
    {% if comment %}
        <h2>Your comment has been added.</h2>
        <p><a href="{{ article.get_canonical_url }}">Back to the article</a></p>
    {% else %}
        {% include "blog/comment_form.html" %}
    {% endif %}

{% endblock %}
```

![](images/creating-comment-system/screenshot_08_11.webp)

## 기사 상세 페이지에 댓글 추가

### `blog/views.py` 파일에 `Comment` 모델과 `CommentForm` 가져오기

![](images/creating-comment-system/screenshot_08_12.webp)


### 기사 상세 페이지에 댓글 추가
`blog/views.py` 파일을 열고 `article_details()` 메서드를 수정한다.

```python
def article_details(request, year, month, day, article):
    try:
        article = get_object_or_404(Article, status=Article.Status.PUBLISHED, 
                    slug=article,
                    publish__year=year,
                    publish__month=month,
                    publish__day=day
                )

        # List of active comments for this article
        comments = article.comments.filter(active=True)
        
        # Form for users to write comment
        form = CommentForm()

    except Article.DoesNotExist:
        raise Http404("No article found.")
    
    return render(request, 'blog/detail.html', {
            'article': articlet,
            'comments': comments,
            'form': form}
        )
    pass
```

![](images/creating-comment-system/screenshot_08_13.webp)

### 기사 상세 탬플릿에 댓글 모두 추가

```html
{% with comments.count as total_comments %}
    <h2>
        {{ total_comments }} comment{{ total_comments|pluralize }}
    </h2>
{% endwith %}

<a href = '/blog/'>
    Go back to the Articles
</a>
```

![](images/creating-comment-system/screenshot_08_14.webp)

### 기사 상세 탬플릿에 연관된 모든 댓글 보여주기

```python
{% for comment in comments %}
    <div>
        <p>
            Comment {{ forloop.counter }} by {{ comment.name }} on {{ comment.created }}
        </p>
        {{ comment.body|linebreaks }}
    </div>
{% empty %}
    <div>
        <p>There are no comments.</p>
    </div>
{% endfor %}
```

![](images/creating-comment-system/screenshot_08_15.webp)

### 기사 상세 탬플릿에 댓글 폼 추가

```html
{% include "blog/comment_form.html" %}
```

![](images/creating-comment-system/screenshot_08_16.webp)

### `blog/detail.html`의 완성 코드

```python
{% extends "blog/base.html" %}

{% block title %}{{ article.title }}{% endblock %}

{% block content %}
    <h1>{{ article.title }}</h1>
    <p>
        Published {{ article.publish }} by {{ article.author }}
    </p>
    {{ article.body|linebreaks }}
    
    {% with comments.count as total_comments %}
        <h2>
            {{ total_comments }} comment{{ total_comments|pluralize }}
        </h2>
    {% endwith %}

    {% for comment in comments %}
        <div>
            <p>
                Comment {{ forloop.counter }} by {{ comment.name }} on {{ comment.created }}
            </p>
            {{ comment.body|linebreaks }}
        </div>
    {% empty %}
        <div>
            <p>There are no comments.</p>
        </div>
    {% endfor %}

    {% include "blog/comment_form.html" %}
    
    <a href="/blog/">
        Go back to the Articles
    </a>

{% endblock %}
```

## 만약 실행되지 않는다면, 개발 서버 재실행
개발 서버를 실행한 후 브라우저에서 URL을 확인한다.

`http://127.0.0.1:8000/blog/`

`http://127.0.0.1:8000/blog/2023/7/4/draw-types-of-triangles-using-matplotlib-module/`

두 번째 URL에서는 아래와 같이 글 내용 뒤에 댓글과 댓글 양식을 확인할 수 있다.

![](images/creating-comment-system/screenshot_08_17.png)

이제 댓글을 추가합니다. 이름, 이메일, 텍스트를 입력하고 제출을 클릭합니다.

![](images/creating-comment-system/screenshot_08_18.png)

저장 후 다음 화면으로 이동한다.

![](images/creating-comment-system/screenshot_08_19.png)

기사 링크로 돌아가기를 클릭한다.

![](images/creating-comment-system/screenshot_08_20.png)

관리 섹션에서도 댓글을 볼 수 있다.

![](images/creating-comment-system/screenshot_08_21.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Created SEO friendly url for article details"
$ git push origin main
```

![](images/creating-comment-system/cli_08_03.png)

이 파트는 여기까지 ...

**(주)** 위 내용은 [Creating a comment system to the article — Part 8](https://medium.com/@nutanbhogendrasharma/creating-a-comment-system-to-the-article-in-django-part-8-ba9d8067bb2)을 편역한 것이다.
