
개발자 또는 관리자로서 서버에 로그인하고 로그를 스크롤하여 오류를 확인하는 것이 얼마나 어려운지 알고 있지만, **더 이상** 그렇지 않을 수 있다. slack 채널로 직접 예외를 보내는 파이썬 라이브러리가 있다.

## 무엇으로
라이브러리의 이름은 **Slack Exception Logger**이다. 개발자들이 직면한 주요 과제 중 하나는 어플리케이션에서 발생하는 예외를 추적하는 것이다. 이는 크고 복잡한 프로젝트에서 작업할 때 특히 어려울 수 있다. slack exception logger 라이브러리는 Slack에 예외 로깅을 위한 간단하고 사용하기 쉬운 인터페이스를 제공하여 이 문제를 해결한다. 자, 이제 시작해 보자.

## 시작하기
다음 명령을 실행하여 라이브러리를 설치한다.

```bash
$ pip install slack_exception_logger
```

설치가 완료되면 아래 코드를 추가하여 코드로 가져온다.

```python
from slack_exception_logger import SlackExceptionLogger
slack_logger = SlackExceptionLogger()
```

다음은 사용 방법의 샘플 사용 사례이다.

```python
try:
    # Your code that may throw an exception
    1/0
except Exception as e:
    slack_logger.push_to_slack(e)
```

마지막 단계인 환경에 필요한 설정을 추가한다.

```python
SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/your-webhook-url'
SLACK_CHANNEL = '#exceptions'
```

매개 변수:
SLACK_WEBHOOK_URL: slack webhook URL

SLACK_CHANNEL: slack 채널의 이름

## webhook URL을 가져오는 방법?
slack 채널의 webhook URL을 가져오려면 다음 단계를 수행하여야 한다.

1. slack 작업영역으로 이동하여 사이드바의 "**Apps**" 버튼을 클릭한다.
2. "**Incoming Webhook**" 검색하여 선택한다.
3. "**Add Configuration**" 버튼을 클릭한다.
4. 수신 메시지를 수신할 채널을 선택하거나 새 채널를 만든다.
5. "**Add Incoming Webhooks integration**" 버튼을 클릭한다.
6. **webhook URL**이 제공된다. 이 URL을 복사하여 코드에 사용한다.
7. 또한 채널에 메시지를 게시할 봇의 이름과 아이콘을 커스터마이즈할 수 있다.

2줄의 코드를 사용하여 exception를 slack 채널로 직접 가져올 수 있다. 다음은 slack 메시지 스크린샷 샘플이다.

![slack-message-screenshot](images/slack-message.webp)


**(주)** 위 내용은 [Easy Error Tracking: A Python library that sends exceptions directly to your slack channel](https://towardsdev.com/easy-error-tracking-a-python-library-that-sends-exceptions-directly-to-your-slack-channel-450ee9944b0c)을 편역한 것이다. 
