[월드 와이드 웹(World Wide Web)](https://en.wikipedia.org/wiki/World_Wide_Web)은 하이퍼링크로 연결된 서버와 클라이언트로 구성된 정보 및 자원 공유 시스템이다. 브라우저에서 웹의 문서와 리소스를 요청한 다음 HTTP(Hypertext Transfer Protocol)를 통해 전송되며, 이들이 브라우저에 출력될 수 있다.

웹 개발 과정을 시작하기 전에 웹에 대해 이해해야 할 몇 가지 기본 개념이 있다. 먼저, 서버란 무엇이고 클라이언트는 무엇이며 서로 통신할 수 있는 방법에 대해 이야기하여 본다.

## 클라이언트와 서버
클라이언트와 서버는 서로 다른 기능을 가진 컴퓨터의 다른 이름일 뿐이다. 컴퓨터를 켜고 브라우저를 수행하여 문서를 요청하는 컴퓨터를 클라이언트라고 하고 문서를 보내는 컴퓨터를 서버라고 한다.

문서를 요청하려면 브라우저에 URL(Uniform Resource Locator)을 입력하고 Enter 키를 눌러야 한다. 그 이후에 정확히 무슨 일이 일어날까?

### URL과 DNS
URL은 서버에 있는 리소스(문서, 이미지, 비디오 등)의 위치를 지정하는 참조(reference)이다. 일반적인 URL은 "`http://www.example.com/index.html`"같은 형식을 갖는다. 이는 전송 프로토콜(`http`), 호스트 이름(`www.example.com`)과 파일 이름(`index.html`)을 나타낸다. 호스트 이름은 서버의 IP 주소에 해당한다.

문제는 당신이 IP 주소에 호스트 이름만 주었을 때 브라우저가 어떻게 IP 주소를 알 수 있는가 하는 것이다? 세계에는 약 10억 개의 호스트 이름이 있으며, 컴퓨터가 모든 호스트 이름을 저장할 수는 없다. 대신 해당 정보를 저장하고 있는 DNS(Domain Name Servers)가 있다. 세계에는 13개의 DNS 루트 서버가 있으며, 이들은 웹의 레지스트리 북 역할을 한다.

따라서 Enter 키를 누르면 브라우저가 먼저 DNS 서버에게 요청하여 호스트 이름을 기반으로 서버의 올바른 IP 주소를 찾은 다음 브라우저가 해당 서버로 HTTP 요청을 전송한다.

### HTTP
HTTP는 클라이언트와 서버 간의 통신을 지원하는 TCP/IP(Transmission Control Protocol/Internet Protocol)의 한 유형이다. 클라이언트는 서버에 요청을 발송한 후 서버는 클라이언트에 응답을 반환한다.

HTTP에 정의된 여러 요청 메서드가 있으며, 각 요청 메서드는 지정된 리소스에 대해 수행할 작업을 나타낸다.

예를 들어 서버에서 무언가를 검색하려는 경우 `GET` 메서드를 사용한다.

원하는 리소스을 검색하지 않고 리소스가 있는지 확인하려면 `HEAD` 메서드를 사용한다.

서버로 무언가를 보내려면 `POST` 또는 `PUT` 메서드를 사용한다. `POST`와 `PUT` 메서드의 차이점은 리소스가 이미 존재하는 경우 `PUT`은 이를 새 리소스로 대체한다.

클라이언트가 HTTP 요청을 서버로 발송한 후, 서버가 해당 요청에 대해 OK이면, "200 OK" 응답을 클라이언트에 다시 반환하고 요청된 자원의 전송을 시작한다.

여기서 자원은 보통 HTML, CSS, 자바스크립트 파일이다. 그들에 대해서는 다음에 얘기하자.

![http_pic](images/http_pic.webp)

## 정적 웹 페이지
[HTML(HyperText Markup Language)](https://www.ericsdevblog.com/posts/html-basics-1/)은 웹의 가장 기본적인 구성 요소이다. 이는 모든 웹 페이지의 구조와 내용을 정의한다.

[CSS(Cascading Style Sheets)](https://www.ericsdevblog.com/posts/css-basics-1/)는 HTML 문서의 모양을 설명하는 데 사용되는 스타일시트 언어이다. HTML 요소의 색상, 모양과 스타일을 정의하는 데 사용한다.

[JavaScript](https://www.ericsdevblog.com/posts/javascript-basics-1/)는 웹 페이지가 사용자와 상호 작용하는 방식을 정의하는 데 사용된다. 예를 들어 HTML 문서에 단추가 있는 경우 사용자가 누르면 이미지가 팝업될 수 있다. 이 경우 JavaScript를 사용하여 이 작업을 정의할 수 있다.

이러한 기술은 정적 웹 페이지를 만드는 데 사용될 수 있다. 정적 웹 페이지는 서버에 저장된 것과 동일하게 브라우저에 전달되는 웹 페이지이다. 즉, 그 페이지에서 무언가를 변경하려면 HTML 코드를 직접 변경해야 한다. 이는 몇 페이지만 있다면 별 문제가 되지 않는다. 그러나 웹사이트가 성장함에 따라 점점 더 지루해진다. 이 문제를 처리하기 위해 더 효율적인 방법이 필요하게 되었다.

## 동적 윕 페이지
동적 웹 페이지는 서버에서 생성되어 요청 시 클라이언트로 전송되는 웹 페이지이다. 데이터베이스에 저장된 정보를 기반으로 페이지를 생성한다. 따라서 변경할 내용이 있으면 데이터베이스의 정보를 변경하기만 하면 된다.

JavaScript는 Node.js를 백엔드에서도 사용할 수 있지만, 지금은 그것에 대해 다루지 않을 것이다. 이 과정에서, 우리는 좀 더 전통적인 방법으로 가고 백엔드에서 사용할 수 있는 두 가지 다른 언어인 [PHP](https://www.ericsdevblog.com/posts/laravel-for-beginners-1/)와 [Python](https://www.ericsdevblog.com/posts/django-for-beginners-1/)에 대하여 설명할 것이다.

**주** 이 포스팅은 [Introducing the World Wide Web](https://www.ericsdevblog.com/posts/introducing-the-world-wide-web/)를 편역한 것임.

