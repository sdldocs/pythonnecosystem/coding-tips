[Django 튜토리얼 시리즈](https://www.ericsdevblog.com/posts/django-for-beginners-1/)에서는 기본 Django 애플리케이션을 만드는 방법에 대해 설명했다. 그러나 튜토리얼에서 누락된 중요한 부분이 있는데, 바로 일반인이 액세스할 수 있도록 앱을 배포하는 방법이다.

이 포스팅에서는 uWSGI 및 Nginx를 사용하여 Linux 서버에 Django 프로젝트를 수동으로 배포하는 방법에 대해 설명한다. Ubuntu 22.4 LTS를 사용하고 있다고 가정한다.

우선, 이 문서에서 설명한 것처럼 서버를 안전하게 사용할 수 있도록 준비해야 한다. 또한 다음 명령을 실행하여 소프트웨어가 최신 상태인지 확인한다.

```bash
$ sudo apt upgrade
```

```bash
$ sudo apt update
```

## Git를 사용하여 프로젝트 복제
서버를 설치한 후에는 Django 프로젝트를 서버에 업로드해야 한다. Git은 이러한 목적에 이상적인 도구이다. Git는 소스 코드의 변경 사항을 추적할 수 있는 오픈 소스 버전 제어 소프트웨어이다. 그것은 프로그래머들 간의 협업을 위해 설계되었다.

Git이 기본적으로 시스템에 미리 설치되어 있어야 하지만 Git이 없는 경우 다음 명령을 사용하여 설치할 수 있다.

```bash
$ sudo apt install git
```

그런 다음 `home` 디렉토리로 이동하여 GitHub에서 서버로 프로젝트를 복제한다.

```bash
$ cd ~
```

```bash
$ git clone https://github.com/ericsdevblog/django-tutorial.git
```

이 명령어는 `django-tutorial` 디렉토리를 만들고 우리의 프로젝트를 그 디렉토리에 복제할 것이다. 다음은 작은 Linux 트릭이다. 다음 명령을 사용하여 디렉터리의 내용을 확인할 수 있다.

```bash
$ ls
```

숨겨진 항목도 나열해야 하면,

```bash
$ ls -a
```

다음에 프로젝트가 변경되었다면 전체 프로젝트를 다시 업로드하는 대신 git pull 명령만 실행하면 된다.

## 가상 환경 생성 및 요구 사항(?) 설치
이제 Django 프로젝트를 복제했으므로 이를 위한 격리된 가상 파이썬 환경을 만들어야 한다. 그러나 Debian 기반 시스템에서는 가상 환경을 생성하는 데 필요한 venv 패키지가 기본적으로 포함되어 있지 않지만 다음 명령을 사용하여 설치할 수 있다.

```bash
$ sudo apt install python3.10-venv
```

애플리케이션의 루트 디렉터리로 이동한다.

```bash
$ cd <path_to_root_directory>
```

가상 환경을 생성한다.

```bash
$ python3 -m venv env
```

`ls` 명령을 다시 실행하면 `env` 디렉토리가 나타난다. 가상 환경은 내부에 생성된다. 이제 환경을 활성화하고 이 프로젝트에 필요한 모든 요구 사항을 설치하기만 하면 된다.

가상 환경 활성화한다.

```bash
$ source env/bin/activate
```

가상 환경을 활성화하지 않고 필요한 패키지를 설치하면 패키지가 방금 생성한 가상 환경이 아닌 Python 전체적으로 설치된다.

활성화에 성공하면 터미널 프롬프트가 다음과 같이 디스플레이된다.

```bash
(env) eric@djangoDeployDemo:~/django-tutorial/djangoTutorial$
```

`(env)`는 현재 `env`라는 가상 환경 내에서 작업 중임을 의미한다.

요구 사항을 설치한다.

```bash
(env) eric@djangoDeployDemo:~/django-tutorial/djangoTutorial$ pip install requirements
```

다음 명령을 사용하여 가상 환경을 비활성화할 수 있다.

```bash
(env) eric@djangoDeployDemo:~/django-tutorial/djangoTutorial$ deactivate
```

## uWSGI 설치
uWSGI는 "호스팅 서비스 구축을 위한 전체 스택 개발을 목표로 하는" 소프트웨어 애플리케이션이다. 이는 웹 서버 게이트웨이 인터페이스(WSGI)의 이름을 따서 명명되었으며, 프로젝트가 지원하는 첫 번째 플러그인이다.

uWSGI는 uWSGI의 기본 uWSGI 프로토콜을 직접 지원하는 Cherokee 또는 Nginx와 같은 웹 서버와 함께 Python 웹 애플리케이션을 제공하는 데 종종 사용된다.

uWSGI를 설치하려면 먼저 Python 개발 패키지를 설치해야 한다.

```bash
$ sudo yum groupinstall "Development Tools"
$ sudo yum install python-devel
```

그런 다음 최신 안정 버전의 uWSGI를 설치한다.

```bash
$ pip install uwsgi
```

### Django 프로젝트 테스트
이제 uWSGI로 Django 프로젝트를 테스트할 수 있다. 먼저, 해당 사이트가 실제로 작동하는지 확인한다.

```bash
$ python manage.py runserver 0.0.0.0:8000
```

작동하면 uWSGI 또한 실행되는 것이다.

```bash
$ uwsgi --http :8000 --module django_blog.wsgi
```

`--module django_bolg.wsgi`: 지정한 `wsgi` 모듈을 로드한다.

브라우저에서 IP로 서버를 방문한다. 사이트가 나타나면 uWSGI가 `virtualenv`에서 Django 애플리케이션을 서비스할 수 있으며 이 스택은 올바르게 작동하는 것이다.

```
the web client <-> uWSGI <-> Django
```

일반적으로 브라우저가 uWSGI와 직접 대화하지 않는다. 그것은 웹 서버를 위한 일이고, uWSGI는 중개 역할을 한다.

## Nginx 설치
Nginx는 오픈 소스 고성능 웹 서버이다. 아파치에 비해 훨씬 유연하고 가볍다.

먼저 Nginx 저장소를 추가해야 한다.

```bash
$ sudo yum install epel-release
```

`yum` 명령을 사용하여 Ngnix를 설치한다.

```bash
$ sudo yum install nginx
```

다음을 입력하여 Nginx 서버를 시작할 수 있다.

```bash
$ sudo systemctl start ngnix
```

이제 포트 80의 웹 브라우저에서 Nginx 서버가 제대로 작동하는지 확인해야 한다. Nginx는 "Welcome to Nginx!"라는 메시지를 디스플레이해야 한다. 그렇지 않은 경우 포트 80에서 다른 것이 실행되고 있기 때문일 수 있으며 다른 포트에서 작동하도록 Nginx를 재구성해야 할 수 있다. 이 포스팅에서는 포트 8000을 사용한다.

### 사이트를 위한 Nginx 설정
Django 사이트가 작동하도록 Nginx를 설정하려면 uWSGI 배포의 nginx 디렉터리 또는 GitHub에서 사용할 수 있는 `uwsgi_params` 파일을 편집해야 한다.

이 파일을 Django 프로젝트 디렉터리에 복사해야 하고, 나중에 Nginx에게 이 파일을 사용하라고 알려줘야 한다.

이제 `/etc/nginx/sites-available/` 디렉토리에 `django_blog_nginx.conf`라는 파일을 생성하고 다음 설정을 추가한다.

```python
# django_blog_nginx.conf

# the upstream component nginx needs to connect to
upstream django {
    # server unix:///<path_to_your_site>/mysite.sock; # for a file socket
    server 127.0.0.1:8001; # for a web port socket (we'll use this first)
}
# configuration of the server
server {
    # the port your site will be served on
    listen      8000;
    # the domain name it will serve for
    server_name example.com; # substitute your machine's IP address or FQDN
    charset     utf-8;
    # max upload size
    client_max_body_size 75M;   # adjust to taste
    # Django media
    location /media  {
        alias <path_to_your_site>/media;  # your Django project's media files - amend as required
    }
    location /static {
        alias <path_to_your_site>/static; # your Django project's static files - amend as required
    }
    # Finally, send all non-media requests to the Django server.
    location / {
        uwsgi_pass  django;
        include     <path_to_your_site>/uwsgi_params; # the uwsgi_params file you installed
    }
}
```

이 설정 파일은 Nginx가 포트 8000에서 사이트를 서비스하도록 지시한다. Nginx는 정적 파일과 미디어 파일뿐만 아니라 Django의 개입이 필요한 요청도 처리한다. 대규모 프로젝트의 경우 한 서버가 정적/미디어 파일을 처리하고 다른 서버가 Django 애플리케이션을 처리하도록 하는 것이 좋다. 하지만 지금 해야 할 일은 이뿐이다.

Nginx가 사용할 수 있도록 `/etc/nginx/sites`에서 이 파일에 대한 심볼릭 링크를 설정한다.

```bash
$ sudo ln -s /etc/nginx/sites-available/django_blog_nginx.conf /etc/nginx/sites-enabled/
```

### 정적 파일 배포
Nginx를 실행하기 전에 정적 폴더에 있는 모든 Django 정적 파일을 수집해야 한다. 우선 다음을 추가하도록 settings.py 을 편집 한다.

```python
# settings.py

STATIC_ROOT = os.path.join(BASE_DIR, "static/")
```

그리고 아래를 수행한다.

```bash
$ python manage.py collectstatic
```

### Nginx 서버 테스트
Nginx 서버 설정을 변경했으므로 변경 사항을 적용하려면 서버를 다시 시작해야 한다.

```bash
$ sudo systemctl restart nginx
```

새로운 이미지(`media.png`)를 `/media` 디렉토리에 업로드하면 Nginx가 미디어 파일을 제공하는지 확인하기 위해 브라우저에서 `http://<your_domain>:8000/media/media`를 방문할 수 있다. 작동하지 않으면 Nginx 서버를 중지했다가 다시 시작해 본다. 그러면 문제가 있는지 그리고 문제가 어디에 발생했는지 알려준다.

## Nginx와 uWSGI를 사용하여 Django 애플리케이션 실행
이제 uWSGI와 Nginx가 적절히 설치되고 설정되었으므로 Django 애플리케이션을 시작할 수 있다.

```bash
$ uwsgi --socket django_blog.sock --module django_blog.wsgi --chmod-socket=664
```

### `.ini` 파일로 실행하도록 uWSGI 설정
관련된 uWSGI 설정을 파일에 작성한 다음 시작 시 설정을 읽어 설정하도록 uWSGI에게 요청할 수 있다.

`django_blog_uwsgi.ini` 파일을 작성한다.

```
# django_blog_uwsgi.ini file

[uwsgi]
# Django-related settings
# the base directory (full path)
chdir           = /path/to/your/project
# Django's wsgi file
module          = project.wsgi
# the virtualenv (full path)
home            = /path/to/virtualenv
# process-related settings
# master
master          = true
# maximum number of worker processes
processes       = 10
# the socket (use the full path to be safe
socket          = /path/to/your/project/mysite.sock
# ... with appropriate permissions - may be needed
# chmod-socket    = 664
# clear environment on exit
vacuum          = true
```

위의 파일을 사용하여 `uwsgi`를 실행한다.

```bash
$ uwsgi --ini mysite_uwsgi.ini # the --ini option is used to specify a file
```

### 시스템 전체에 uWSGI 설치
지금까지 uWSGI는 가상 환경에 설치되었으며, 구축을 위해 시스템 전체에 설치해야 한다.

가상 환경을 비활성화한다.

```bash
$ deactivate
```

시스템 전체에 uWSGI를 설치한다.

```bash
$ sudo pip3 install uwsgi
```

uWSGI Wiki에서 몇 가지 [설치 절차](https://projects.unbit.it/uwsgi/wiki/Install)를 설명하고 있다. 시스템 전체에 uWSGI를 설치하기 전에 버전을 선택하고 가장 적합한 설치 방법을 고려해 볼 필요가 있다.

이전과 마찬가지로 uWSGI를 계속 실행할 수 있는지 다시 확인한다.

```bash
$ uwsgi --ini mysite_uwsgi.ini # the --ini option is used to specify a file
```

**주** 이 포스팅은 [How to Deploy a Django Project](https://towardsdev.com/how-to-deploy-a-django-project-563ce50c3365)를 편역한 것임.
