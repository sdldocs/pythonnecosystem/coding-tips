이번 파트에서는 기사 상세 페이지에서 유사한 기사를 보일 수 있도록 한다.

## ## `Xampp` 시작과 가상 환경 활성화

```bash
$ cd ~/myproject/blog_app
$ source my_blog_env/bin/activate
```

## 태그로 유사한 기사 찾기
이 파트에서는 객체 컬렉션을 요약하거나 집계하여 파생된 값을 검색해 보겠다. [Aggregation Docs](https://docs.djangoproject.com/en/4.2/topics/db/aggregation/)에서 집계에 대해 자세히 알아볼 수 있다.

### `Count` 가져오기
`blog/views.py` 파일을 열고 `django.db.models`에서 `Count`를 가져온다.

```python
from django.db.models import Count
```

![](images/getting-similar-articles/screenshot_10_01.webp)

### `blog/views.py` 파일의 `article_details()` 메서드에서 유사한 기사 검색
현재 기사에 대한 모든 태그를 가져온다. 다음 동일한 태그가 있는 모든 기사를 검색한고, 결과에서 현재 기사를 제거한다. 다음 단계에서는 게시된 글의 개수를 `same_tags_in_article` 속성에 저장한다. 내림차순으로 세(?) 개의 기사를 디스플레이해야 한다.

마지막으로 유사한 기사 객체를 상세 템플릿에 전달해야 한다.

```python
# Retrieving list of similar articles
article_tags_ids = article.tags.values_list('id', flat=True)
similar_published_articles = Article.publishedArticles.filter(tags__in=article_tags_ids)\
                                .exclude(id=article.id)
similar_articles = similar_published_articles.annotate(same_tags_in_article=Count('tags'))\
                                .order_by('-same_tags_in_article','-publish')[:3]
```

![](images/getting-similar-articles/screenshot_10_02.webp)

### `blog/templates/blog/detail.html`에 유사한 기사 디스플레이
아래와 같이 기사 상세 페이지에 유사한 기사를 디스플레이한다.

```html
<h2>Similar articles</h2>
{% for article in similar_articles %}
    <p>
        <a href="{{ article.get_canonical_url }}">{{ article.title }}</a>
    </p>
{% empty %}
    There are no similar articles.
{% endfor %}
```

![](images/getting-similar-articles/screenshot_10_03.webp)

## 개발 서버 실행

```python
$ python manage.py runserver
```

개발 서버를 실행한 후 브라우저에서 URL을 확인한다.

http://127.0.0.1:8000/blog/

화면이 아래와 같이 디스플레이된다.

![](images/getting-similar-articles/screenshot_10_04.png)

임의의 기사를 클릭한다. "Transfer Learning Using Feature Extraction In Deep Learning"을 클릭했는데 비슷한 기사가 없다는 것을 볼 수 있다.

![](images/getting-similar-articles/screenshot_10_05.png)

그리하여 다른 기사를 클릭했더니 비슷한 기사가 있는 것을 확인할 수 있다.

![](images/getting-similar-articles/screenshot_10_06.png)

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Displayed similar articles in article detail page"
$ git push origin main
```

![](images/getting-similar-articles/cli_10_01.png)

이 파트는 여기까지 ...


**(주)** 위 내용은 [Getting similar articles — Part 10](https://medium.com/@nutanbhogendrasharma/getting-similar-articles-in-django-part-10-e4c33e93e2fe)을 편역한 것이다.
