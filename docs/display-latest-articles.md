이 파트에서는 웹사이트에 게시된 기사의 수와 사이드바에 최신 기사를 보여주고자 한다.

가상 환경을 활성화하고 Visual studio 코드에서 프로젝트를 연다. 커스텀 태그를 구현할 것이다.

## 커스텀 템플릿 태그와 필터를 만드는 방법
Django의 템플릿에는 어플리케이션의 프레젠테이션 로직 요구 사항을 해결하도록 설계된 다양한 기본 제공 태그와 필터가 포함되어 있다. 그럼에도 불구하고 템플릿 기본 요소의 핵심 집합에서 다루지 않는 기능이 필요할 수 있다. Python을 사용하여 커스텀 태그와 필터를 정의하여 템플릿 엔진을 확장한 다음 `{% load %}` 태그를 사용하여 템플릿에서 사용할 수 있도록 할 수 있다.

커스텀 템플릿 태그와 필터를 지정하는 가장 일반적인 위치는 Django 앱 내부이다. 기존 앱과 관련된 경우 여기에 번들로 제공하는 것이 좋으며, 그렇지 않은 경우 새 앱으로 추가할 수 있다. Django 앱이 INSTALLED_APPS에 추가되면 아래에 설명된 기존 위치에 정의된 모든 태그가 자동으로 템플릿 내에서 로드할 수 있게 된다.

앱에는 `models.py`, `views.py` 등과 같은 수준의 `templatetags` 디렉터리가 포함되어야 한다. 이 디렉터리가 아직 존재하지 않는 경우 생성하고, 디렉터리가 Python 패키지로 취급되도록 `__init__.py` 파일을 잊지 말자. (참고: [How to create custom template tags and filters](https://docs.djangoproject.com/en/4.2/howto/custom-template-tags/))

Django는 태그 생성을 위하여 일반적으로 사용되는 두 가지 헬퍼 함수를 제공한다.

1. `simple_tag`: 주어진 데이터를 처리하고 문자열을 반환한다.
2. `inclusion_tag`: 주어진 데이터를 처리하고 렌더링된 템플릿을 반환한다.

두 도우미 함수를 모두 사용하여 태그를 생성하여 보도록 한다.

## 커스텀 태그 구현

### 필요한 폴더와 파일을 생성
커스텀 태그와 필터는 Django 앱의 `templatetags/` 디렉터리에 있다. 따라서 `templatetags` 디렉터리를 만들고 그 안에 `__init__.py`와 `article_tags.py` 파일을 생성한다.

![](images/display-latest-articles/screenshot_11_01.webp)

이 디렉터리는 다른 Python 코드와 마찬가지로 모듈이므로 `__init__.py` 파일이 필요하다. `article_tags.py` 파일에는 커스텀 태그와 필터에 대한 모든 코드가 저장된다.

### Simple 태그
**`django.template.Library.simple_tag()`**

템플릿 태그는 문자열 또는 템플릿 변수 등을 인수로 받아 입력 인자와 일부 외부 정보를 기반으로 처리를 수행한 후 결과를 반환한다. 예를 들어 `current_time` 태그는 포맷 문자열을 받아 그 형식을 따르는 문자열로 시간을 반환한다.

이러한 타입의 태그를 쉽게 생성할 수 있도록 Django는 도우미 함수인 `simple_tag`를 제공한다. `django.template.Library`의 메서드인, 이 함수는 인수를 받는 함수 받아, `render` 함수와 앞서 언급한 기타 필요한 비트로 래핑한 후 템플릿 시스템에 등록한다. ([Simple tags](https://docs.djangoproject.com/en/4.2/howto/custom-template-tags/#simple-tags) 참조)

### `article_tags.py`에 simple tag 파일 생성
Django와 Article 모델에서 템플릿을 가져온다. 그런 다음 템플릿 라이브러리 클래스를 인스턴스화한다. 유효한 태그 라이브러리가 되려면 모듈에 템플릿인 `register`라는 모듈 수준 변수가 포함되어야 한다. 모든 태그와 필터가 등록되어 있는 라이브러리 인스턴스이다. 여기서는 일반 Python 함수인 `total_articles`라는 simple_tag를 정의하고 있으며, 이 함수는 게시된 기사의 총 개수를 반환한다.

```python
from django import template
from ..models import Article

register = template.Library()

@register.simple_tag
def total_articles():
    return Article.publishedArticles.count()
    pass
```

![](images/display-latest-articles/screenshot_11_02.webp)

게시된 기사의 수를 반환하는 간단한 템플릿 태그를 만들었다.

### `blog/templates/base.html`에 게시된 총 기사 디스플레이
`base.html`에 `article_tags`를 로드한다.

```html
{% load article_tags %}
```

![](images/display-latest-articles/screenshot_11_03.webp)

`blog/templates/base.html`에 게시된 기사 총 갯수를 디스플레이

```html
I have published {% total_articles %} articles till now.
```

![](images/display-latest-articles/screenshot_11_04.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

브라우저에서 아래 URL을 입력한다.

`http://127.0.0.1:8000/blog/`

![](images/display-latest-articles/screenshot_11_05.png)

이제 4개의 기사가 게시되었다고 표시되었다.

## 최신 기사 태그 만들기

### Inclusion 태그

**`django.template.Library.inclusion_tag()`**

또 다른 타입의 템플릿 태그는 다른 템플릿을 렌더링하여 일부 데이터를 표시하는 유형이다. 예를 들어, Django의 관리자 인터페이스는 커스텀 템플릿 태그를 사용하여 '`add/chang`' 양식 페이지의 하단에 버튼을 표시한다. 이러한 버튼은 항상 동일하게 보이지만 링크 대상은 편집 중인 객체에 따라 변경되므로 현재 객체의 세부 정보로 채워진 작은 템플릿을 사용하기에 완벽한 경우이다. (관리자의 경우에는 `submit_row` 태그이다.)

이러한 종류의 태그를 "inclusion 태그"라고 한다. ([Inclusion tags](https://docs.djangoproject.com/en/4.2/howto/custom-template-tags/#inclusion-tags) 참고)

### article_tags.py 파일에 최신 기사를 보여주기 위한 태그 만들기
`inclusion_tag`를 생성하고 `blog/latest_articles.html`을 사용하여 반환된 값을 렌더링할 템플릿을 지정하고 한다. 게시된 최신 기사 세 개를 반환하는 `show_latest_articles()`라는 함수를 정의한다.

```python
@register.inclusion_tag('blog/latest_articles.html')
def show_latest_articles(count=3):
    latest_articles = Article.publishedArticles.order_by('-publish')[:count]
    return {'latest_articles': latest_articles}
    pass
```

![](images/display-latest-articles/screenshot_11_06.webp)

### `blog/templates/blog` 폴더에 `latest_articles.html` 파일 만들기
inclusion 태그가 렌더링할 `blog/latest_articles.html` 파일을 만든다. 해당 파일에 다음과 같은 코드를 작성한다.

```html
<ul>
    {% for latest_article in latest_articles %}
        <li>
            <a href="{{ latest_article.get_canonical_url }}">{{ latest_article.title }}</a>
        </li>
    {% endfor %}
</ul>
```

![](images/display-latest-articles/screenshot_11_07.webp)

### `base.html`에서 커스텀 태그 호출

```html
{% show_latest_articles 3 %}
```

URL `http://127.0.0.1:8000/blog/`을 새로 고침.

![](images/display-latest-articles/screenshot_11_08.png)

오른쪽 패널에서 최신 기사 세 개를 확인할 수 있다.

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Displayed latest articles"
$ git push origin main
```

![](images/display-latest-articles/cli_11_01.png)

이 파트는 여기까지 ...

**(주)** 위 내용은 [Display the latest articles in the sidebar of the blog website — Part 11](https://medium.com/@nutanbhogendrasharma/display-the-latest-articles-in-the-sidebar-of-the-blog-website-part-11-59c02a6cf119)을 편역한 것이다.
