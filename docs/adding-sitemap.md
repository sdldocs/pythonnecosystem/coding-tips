이 파트에서는 블로그 웹사이트에 사이트맵을 추가하려고 한다. 사이트맵을 생성하기 위해 Django "`site`" 및 "`sitemap`" 프레임워크를 사용할 것이다.

## 사이트 맵이란?
사이트맵은 웹사이트의 웹페이지에 대한 정보를 제공하는 XML 파일이다. Google/Bing과 같은 검색 엔진은 XML 파일을 읽고 웹사이트를 크롤링한다. 사이트맵은 페이지가 마지막으로 업데이트된 시기, 해당 페이지의 중요도 등과 같은 웹페이지에 대한 중요한 정보를 제공한다.

## Django 사이트맵 프레임워크
Django에는 사이트맵 XML 파일을 생성하기 위한 높은 수준의 사이트맵 생성 프레임워크를 함께 제공한다.

프레임워크는 이 정보를 Python 코드로 표현할 수 있게 함으로써 이 XML 파일의 생성을 자동화한다. 사이트맵을 생성하려면 사이트맵 클래스를 작성하고 `URLconf`에서 이를 가리키면 된다.

### 설치
다음 단계를 따라 사이트맵 앱을 설치한다.

1. `INSTALLED_APPS` 설정에 `'django.contrib.sitemaps'`을 추가한다.
2. `TEMPLATES` 설정에 `APP_DIRS` 옵션이 `True`로 설정된 `DjangoTemplates` 백엔드가 포함되어 있는지 확인한다. 이 설정은 기본적으로 포함되어 있으므로 해당 설정을 변경한 경우에만 변경한다.
3. 사이트 프레임워크가 설치되었는지 확인한다.

> **Note**
>
>사이트맵 어플리케이션은 데이터베이스 테이블을 설치하지 않는다. `INSTALLED_APPS`에 들어가야 하는 유일한 이유는 템플릿 로더 `Loader()`가 기본 템플릿을 찾을 수 있도록 하기 위해서이다.)

## Django `sites` 프레임워크
Django는 선택 사항인 `site` 프레임워크를 함께 제공한다. 이 프레임워크는 객체와 기능을 특정 웹사이트에 연결하기 위한 후크이며, Django로 구동되는 사이트의 도메인 이름과 "자세한" 이름을 보관하는 장소이다.

단일 Django 설치가 둘 이상의 사이트를 지원하며 이러한 사이트를 어떤 식으로든 구분해야 하는 경우에 사용한다.

사이트(site) 프레임워크는 주로 이 모델을 기반으로 한다.

**`model.Site`** 클래스

웹사이트의 도메인과 이름 속성을 저장하는 모델이다.

**domain****: 웹사이트와 연결된 정규화된 도메인 이름이다. 예: `www.example.com`.

**name**: 웹사이트의 사람이 읽을 수 있는 "자세한" 이름이다.

`SITE_ID` 설정은 해당 특정 설정 파일과 연결된 사이트 객체의 데이터베이스 ID를 지정한다. 이 설정을 생략하면 `get_current_site()` 함수는 도메인을 `request.get_host()` 메서드의 호스트 이름과 비교하여 현재 사이트를 가져오려고 시도한다.

사이트 프레임워크 활성화

**다음 단계를 따라 사이트 프레임워크를 사용 설정을 할 수 있다.**

1. `INSTALLED_APPS` 설정에 '`django.contrib.sites`'를 추가한다.
2. `SITE_ID` 설정을 정의한다: `SITE_ID = 1`
3. 마이그레이션을 실행한다.

`django.contrib.sites`는 `example.com` 도메인을 가진 `example.com`이라는 기본 사이트를 생성하는 `post_migrate` 시그널 핸들러를 등록한다. 이 사이트는 Django가 테스트 데이터베이스를 생성한 후에도 생성된다. 프로젝트에 정확한 이름과 도메인을 설정하려면 데이터 마이그레이션을 사용하여야 한다.

프로덕션 환경에서 다른 사이트를 서비스하려면 각 `SITE_ID`를 사용하여 별도의 설정 파일을 만든 다음(공유 설정 중복을 피하기 위해 공통 설정 파일에서 가져올 수도 있음) 각 사이트에 대해 적절한 `DJANGO_SETTINGS_MODULE`을 지정해야 한다.

### 초기화
**`views.sitemap(request, sitemaps, section=None, template_name=’sitemap.xml’, content_type=’application/xml’)`**

Django 사이트에서 사이트맵 생성을 활성화하려면 `URLconf`에 아래 코드를 추가한다.

```python
from django.contrib.sitemaps.views import sitemap

path(
“sitemap.xml”,
sitemap,
{“sitemaps”: sitemaps},
name=”django.contrib.sitemaps.views.sitemap”,
)
```

이는 클라이언트가 `/sitemap.xml`을 액세스할 때 사이트맵을 빌드하도록 Django에 지시합니다.

사이트맵 파일의 이름은 중요하지 않지만 위치는 중요하다. 검색 엔진은 사이트맵에서 현재 URL 수준 이하의 링크만 색인화한다. 예를 들어 `sitemap.xml`이 루트 디렉터리에 있는 경우 사이트 내 모든 URL을 참조할 수 있다. 그러나 사이트맵이 `/content/sitemap.xml`에 있는 경우 `/content/`로 시작하는 URL만 참조할 수 있다.

사이트맵 뷰에는 필수 인수 `{‘sitemaps’: sitemaps}`가 추가로 필요하다. `sitemap`은 짧은 섹션 레이블(예: 블로그 또는 뉴스)을 해당 `Sitemap` 클래스(예: BlogSitemap 또는 NewsSitemap)로 매핑하는 딕셔너리여야 한다. `sitemaps`는 `Sitemap` 클래스의 인스턴스(예: `BlogSitemap(some_var)`)에 매핑할 수도 있다.

## `settings.py` file에 Django `sites`와 `sitemap` 프레임워크 추가
Django 설정 파일을 열고 `SITE_ID = 1`과 '`django.contrib.sites`'와 '`django.contrib.sitemaps`'를 `INSTALLED_APPS` 목록에 추가한다.

```python
SITE_ID = 1

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog.apps.BlogConfig',
    'taggit',
    'django.contrib.sites',
    'django.contrib.sitemaps',
]
```

![](images/adding-sitemap/screenshot_13_01.webp)

![](images/adding-sitemap/screenshot_13_02.webp)

## 데이터베이스 테이블 보기
데이터베이스에 테이블을 생성하는 마이그레이션 명령을 실행해야 한다. 마이그레이션 명령을 실행하기 전에 데이터베이스의 테이블을 확인해 보자.

![](images/adding-sitemap/screenshot_13_03.png)

데이터베이스에는 14개의 테이블이 있다.

## `migrate` 명령 실행

```bash
$ python manage.py migrate
```

![](images/adding-sitemap/cli_13_01.png)

## `migrate` 명령 실행 후 테이블 보기

![](images/adding-sitemap/screenshot_13_04.png)

`django_site`라는 테이블이 하나 더 생성된 것을 볼 수 있다.

### `django_site` table의 데이터

![](images/adding-sitemap/screenshot_13_05.png)

`django_site` 테이블 도메인 이름은 "`example.com`"이며, 나중에 요구 사항에 따라 수정할 수 있다.

## 사이트에 대한 사이트맵 생성

### `blog` 디렉토리에 `sitemaps.py` 파일 생성

![](images/adding-sitemap/screenshot_13_06.webp)

### `BlogSitemap` 클래스 만들기
임의로 클래스 이름을 명명할 수 있다. 여기서는 "BlogSitemap"이라고 클래스 이름을 작성했고, 부모 클래스는 `Sitemap`이다. 클래스에 속성 `changefreq`와 `priority`를 추가했다.

1. `changefreq`와 `priority`는 각각 `<changefreq>`와 `<priority>` 요소에 해당하는 클래스 애트리뷰트이다. 예에서 `lastmod`가 그랬던 것처럼 함수로 호출 가능하게 만들 수 있다.

2. `items()`는 객체의 시퀀스 또는 쿼리셋을 반환하는 메서드이다. 반환된 객체는 사이트맵 속성(`location`, `lastmod`, `changefreq`와 `priority`)에 해당하는 호출 가능한 메서드로 전달된다.

3. `lastmod`는 날짜 시간을 반환해야 한다.

```python
from django.contrib.sitemaps import Sitemap
from .models import Article

class BlogSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        return Article.publishedArticles.all()
        pass

    def lastmod(self, obj):
        return obj.updated
        pass
    pass
```

![](images/adding-sitemap/screenshot_13_07.webp)

## `my_blog/urls.py`에 sitemap url 추가

```python
from django.contrib import admin
from django.urls import path, include
from django.contrib.sitemaps.views import sitemap
from blog.sitemaps import BlogSitemap

sitemaps = {
    'articles': BlogSitemap,
}
urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls', namespace='blog')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]
```

![](images/adding-sitemap/screenshot_13_08.webp)

## 개발 서버 실행

```bash
$ python manage.py runserver
```

### 브라우저에서 url 열기

`http://127.0.0.1:8000/sitemap.xml`

![](images/adding-sitemap/screenshot_13_09.png)

사이트맵이 기사 모델에서 `get_absolute_url()` 메서드를 검색하는 것을 볼 수 있다. 기본적으로 `location()`은 각 객체에서 `get_absolute_url()`을 호출하고 결과를 반환한다. 위치가 제공되지 않으면 프레임워크는 `items()`가 반환한 대로 각 객체에서 `get_absolute_url()` 메서드를 호출한다.

하지만 우리는 기사 모델에 다른 메서드 이름인 `get_canonical_url()`을 사용했다. 이것이 바로 오류가 발생하는 이유이다.

## `get_canonical_url()` 메서드 이름 변경
`blog/models.py` 파일을 열고 메서드 이름을 `get_canonical_url()`을 `get_absolute_url()`로 변경한다.

![](images/adding-sitemap/screenshot_13_10.webp)

브라우저에서 URL `http://127.0.0.1:8000/sitemap.xml`을 새로 고침한다.

![](images/adding-sitemap/screenshot_13_11.png)

이제 `sitemap.xml`이 생성되었다. 하지만 `Article` 모델에서 메서드 이름을 변경했기 때문에 코드가 손상되었다. 따라서 프로젝트에서 메서드 이름을 전역적으로 변경해야 한다. Visual Studio Code에서 전역 찾기와 바꾸기를 사용할 수 있다.

![](images/adding-sitemap/screenshot_13_12.png)

전체 프로젝트에서 메서드 이름을 바꾼 후 모든 파일을 저장한다. 그런 다음 모든 URL을 확인한다.

`http://127.0.0.1:8000/blog/`

![](images/adding-sitemap/screenshot_13_13.png)

모든 링크가 정상적으로 작동하는지 확인했다.

## sitemap의 도메인 이름 변경
Django의 관리 로그인을 실행한다.

`http://127.0.0.1:8000/admin/`

그런 다음 `Sites`를 클릭

![](images/adding-sitemap/screenshot_13_14.png)

아래와 같이 다음 화면이 나타난다.

![](images/adding-sitemap/screenshot_13_15.png)

`example.com`을 `localhost:800`으로 변경한 다음 `Save`을 클릭한다.

![](images/adding-sitemap/screenshot_13_16.png)

저장 후 다음과 같은 화면이 디스플레이된다.

![](images/adding-sitemap/screenshot_13_17.png)

### 개발 서버 실행을 중지하고 개발 서버를 다시 시작
개발 서버를 중지하려면 `Ctrl + c`를 누른 다음 개발 서버를 다시 실행한다.

```bash
$ python msnage.py runserver
```

### sitemap url을 다시 확인

`http://127.0.0.1:8000/sitemap.xml`

![](images/adding-sitemap/screenshot_13_18.png)

이제 `sitemap.xml`이 정상적으로 표시된다. 최신 변경 사항을 github 리포지토리에 푸시하자.

## 최신 변경을 Github 레포지터리에 푸시

```bash
$ git add --all
$ git commit -m "Sitemap added to the website"
$ git push origin main
```

![](images/adding-sitemap/cli_13_02.png)

이 파트는 여기까지 ...

**(주)** 위 내용은 [Adding a sitemap to the blog site — Part 13](https://medium.com/@nutanbhogendrasharma/adding-a-sitemap-to-the-blog-site-in-django-part-13-1f69ab43f6c)을 편역한 것이다.
