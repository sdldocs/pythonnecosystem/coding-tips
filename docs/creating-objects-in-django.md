Django는 웹 어플리케이션을 쉽게 만들 수 있는 Python 웹 프레임워크이다. Django의 가장 중요한 기능 중 하나는 Python 코드를 사용하여 데이터베이스에서 객체를 만들고 관리할 수 있는 ORM(객체 릴레이셔날 매퍼)이다.

Django에서 객체를 만들려면 먼저 model을 정의해야 한다. Model은 데이터베이스의 테이블을 나타내는 Python 클래스이다. Model은 테이블에 저장될 필드와 테이블 간의 관계를 정의한다.

Model을 정의한 다음 model 클래스를 인스턴스화하여 객체를 만들 수 있다. `Model()` 클래스는 입력으로 키워드 인수 딕셔너리을 사용한다. 키워드 인수는 객체의 필드에 저장될 값을 나타낸다.

예를 들어, 다음 코드는 `Book` model의 객체를 만든다:

```python
from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)

book = Book(title="The Hitchhiker's Guide to the Galaxy", author='Douglas Adams')
```

이제 `book` 객체는 데이터베이스의 `books` 테이블에 있는 레코드를 나타낸다. 객체의 `title`과 `author` 필드는 `Book()` 생성자에게 전달된 값으로 채워진다.

객체를 만든 후 `save()` 메서드를 호출하여 객체를 데이터베이스에 저장할 수 있다. `save()` 메서드는 객체의 데이터를 데이터베이스에 저장한다.

예를 들어, 다음 코드는 `book` 객체를 데이터베이스에 저장한다:

```python
    book.save()
```

이제 `book` 객체를 데이터베이스에 저장하고 그 결과는 지속된다. `objects.get()` 메서드를 호출하여 데이터베이스에서 객체를 검색할 수 있다. `objects.get()` 메서드는 객체의 주 키를 입력 인수로 사용한다.

예를 들어, 다음 코드는 데이터베이스에서 `book` 객체를 검색한다:

```python
    book = Book.objects.get(pk=1)
```

이제 데이터베이스의 데이터로 `book` 객체가 채워진다.

Django에서 객체를 만드는 것은 매우 간단한 과정이다. 여기서 설명된 단계를 따라 Django 어플리케이션에서 객체를 쉽게 만들고 관리할 수 있다.


**(주)** 위 내용은 [Creating Objects in Django with Python](https://medium.com/@andiksyldnata/creating-objects-in-django-with-python-11dbff833ff4)을 편역한 것이다.
