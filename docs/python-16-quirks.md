**초급에서 중급으로 가는 길**

## 1. 참과 거짓 값
```python
if 100:
  print('apple')
else:
  print('orange')

# 'apple' is printed
```

`if` 문에서 적절한 부울식 대신 100을 사용하면 실제로 True로 평가된다. 이러한 값을 *참(truthy)* 값이라고 한다.

반대로 `if` 문에서 적절한 부울식 대신 0을 사용하면 False로 평가됩니다. 이러한 값을 *거짓(falsy)* 값이라고 한다.

다른 유형에 대한 참/거짓 값.

- int/float — 0은 False이고 다른 모든 숫자는 True이다
- str — 빈 문자열 `''`은(는) False이고, 길이가 1보다 긴 문자열은 True이다
- list - 빈 리스트 `[]`은(는) False이고, 길이가 ≥ 1인 리스트는 True이다
- dict - 빈 dict `{}`은(는) False이고 길이가 1보다 긴 모든 dict는 True이다
- 튜플 — 빈 튜플 `()`은 False이고 길이가 ≥ 1인 모든 튜플은 True이다
- object — `None`은 False이고, 다른 모든 객체는 True이다 (특별한 경우 제외)

예:

```python
if -1: print('apple')  # prints
if 0: print('apple')   # doesn't print
if -4: print('apple')  # prints

if '': print('apple')  # doesn't print
if 'a': print('apple') # prints

if []: print('apple')  # doesn't print
if [1]: print('apple') # prints
```

숙련된 Python 프로그래머들은 이것을 자주 사용하는 경향이 있기 때문에 이것에 익숙해져야 한다.

## 2. 메소드 체인
함축된 코드을 위해 메소드를 차례로 연결할 수 있다.

```python
string = ' Apple Orange Pear  '
string = string.strip()    # string = 'Apple Orange Pear'
string = string.lower()    # string = 'apple orange pear'
string = string.split()    # string = ['apple', 'orange', 'pear']

# string is now ['apple', 'orange', 'pear']
```

위에서 문자열에 여러 방법을 적용하였지만, 그것들을 사슬로 묶지는 않았다.

```python
string = ' Apple Orange Pear  '
string = string.strip().lower().split()
# string is now ['apple', 'orange', 'pear']
```

메서드 체인을 사용한 후. 얼마나 코드를 줄였는지 살펴보자. 

## 3. 리스트 컴프리헨션
```python
lis = []
for i in range(5):
  lis.append(i)

# lis = [0,1,2,3,4]
```

for 루프를 사용하여 기본적인 리스트를 만들었다.

```python
lis = [i for i in range(5)]
```

리스트 컴프리헨션을 사용하면 위의 작업을 한 줄로 수행할 수 있다.

```python
# general list comprehension syntax
lis = [expression for i in iterable if condition]

# basic list comprehension
lis = [i for i in range(5)]             # [0,1,2,3,4]

# when we transform the expression
lis = [i*2 for i in range(5)]           # [0,2,4,6,8]

# when we add a condition
lis = [i for i in range(5) if i>2]      # [3,4]
```

## 4. 딕셔너이와 세트 컴프리헨션
딕셔너리 컴프리헨션은 리스트 컴프리헨션과 같지만 딕셔너리를 위한 것이다.

세트 컴프리핸션도 리스트 컴프리핸션과 비슷하지만 새트를 위한 것이다.

```python
# basic dictionary comprehension
d = {i:i*2 for i in range(3)}      # {0:0, 1:2, 2:4}

# basic set comprehension
s = {i for i in range(3)}          # {0,1,2}
```

더 많은 딕셔너리 컴프리헨션 예:

```python
# general dictionary comprehension syntax
d = {key:value for i in iterable if condition}

# basic dictionary comprehension
d = {i:i*2 for i in [1,2,3]}                    # {1:2, 2:4, 3:6}

# when we do stuff to key/value
d = {-i:i*10 for i in [1,2,3]}                  # {-1:10, -2:20, -3:30}

# when we add a condition
d = {i:i for i in [1,2,3] if i!=2}              # {1:1, 3:3}
```

더 많은 tpxm 컴프리헨션 (값이 없는 딕셔너리 컴프리헨션) 예:

```python
# general set comprehension syntax
s = {expression for i in iterable if condition}

# basic set comprehension
s = {i for i in [1,2,3]}          # {1,2,3}

# when we do stuff to the expression
s = {i*100 for i in [1,2,3]}      # {100,200,300}

# when we add a condition
s = {i for i in [1,2,3] if i>1}   # {2,3}
```

## 5. 삼항 연산자
```python
score = 76

if score > 50:
  grade = 'pass'
else:
  grade = 'fail'
```

기본 if-else 문,

```python
score = 76

grade = 'pass' if score>50 else 'fail'
```

3진 연산자를 사용하여 위와 동일한 작업을 수행할 수 있다.

```python
# basic ternary operator syntax
x = A if condition else B

# if condition is True, x = A
# if condition if False, x = B
```

```python
# multiple ternary operator syntax
x = A if condition1 else B if condition2 else C if condition3 else D

# if condition1 is True, x = A
# elif condition2 is True, x = B
# elif condition3 is True, x = C
# else, x = D
```

## 6. DIct.get

```python
fruits = {'apple':4, 'orange':5, 'pear':6}

a = fruits['apple']     # a = 4
b = fruits['orange']    # b = 5
c = fruits['pear']      # c = 6
d = fruits['banana']    # KeyError raised, whole program crashes
```

`dict[key]`를 사용하여 사전의 값에 액세스gks다. 존재하지 않는 키를 액세스하려고 하면 프로그램 예외가 발생하고 종료한다.

```python
fruits = {'apple':4, 'orange':5, 'pear':6}

a = fruits.get('apple')     # a = 4
b = fruits.get('orange')    # b = 5
c = fruits.get('pear')      # c = 6
d = fruits.get('banana')    # None
```

`dict[key]` 대신 `dict.get(key)`을 사용하면, 키가 없을 때 오류로 프로그램 이 종료되는 대신 `None`을  반환한다.

```python
fruits = {'apple':4, 'orange':5, 'pear':6}

a = fruits.get('apple', 0)     # a = 4
b = fruits.get('orange', 0)    # b = 5
c = fruits.get('pear', 0)      # c = 6
d = fruits.get('banana', 0)    # 0
```

`dict.get(key)`에 기본 반환 값 0을 추가하면, 키가 없으면 `None`` 대신 이 기본값을 반환한다.

## 7. 튜플 언팩킹

```python
person = ['bob', 25, 'm']

name = person[0]      # name = 'bob'
age = person[1]       # age = 25
gender = person[2]    # gender = 'm'
```

인덱스를 사용하여 리스트에서 값 추출

```python
person = ['bob', 25, 'm']

name, age, gender = person    # name='bob', age=25, gender='m'
```

같은 작업을 하지만 튜플 언책킹을 사용한다. 코드가 얼마나 줄었는지 살펴보자.

```python
# more examples

person = ['rock', 'lee']
firstname, lastname = person    # firstname='rock', lastname='lee'

person = ['rock', 'lee', 16, 'm', 'black']
firstname, lastname, age, gender, hair = person
# firstname='rock', lastname='lee', age=16, gender='m', hair='black'
```

> **IMPORTANT** — 왼쪽의 변수 수는 오른쪽의 변수 수와 일치해야 한다. 그렇지 않으면 오류가 발생한다. 

## 8. \*args와 \*\*kwargs
함수에서 `*args`는 위치 인수(positional arguments)들을 전달할 수 있는 반면 `**kgwargs`는 키워드 인수(keyword arguments)들을 전달할 수 있다.

```python
def test(*args):
  print(args)

test()             # ()
test(1)            # (1,)
test(1,2)          # (1,2)
test(1,2,3,4,5)    # (1,2,3,4,5)

def test(**kwargs):
  print(kwargs)

test()             # {}
test(a=1)          # {'a':1}
test(a=1, b=2)     # {'a':1, 'b':2}
```

`*args`와 `**kwargs`를 사용하여 리스트와 딕셔너리를 언팩할 수도 있다.

```
*args/**kwargs           Equivalent to

func(*[1,2])             func(1,2)
func(*[1,2,3])           func(1,2,3)

func(**{'a':1})          func(a=1)
func(**{'a':1, 'b':2})   func(a=1, b=2) 
```

## 9. \*으로 튜플 언팩킹
이전 튜플 언팩킹에서 왼쪽의 변수 수가 오른쪽의 요소 수와 일치해야 한다고 하였다. 이 문은 튜플 언팩킹에서 \*를 사용하는 경우에는 적용되지 않는다.

```python
person = ['bob', 15, 'm', 'blue', 1.75]

name, age, *others = person
# name = 'bob'
# age = 15
# others = ['m', 'black', 1.75]
```

다른 변수 앞에 \*를 표시하면, 할당되지 않은 모든 변수를 다른 변수가 '받기' 때문이다.

이는 리스트에 불필요한 데이터가 많은 경우 특히 유용하다.

## 10. `break` 키워드
for 루프 내부에서 `break` 키워드를 사용하면 for 루프를 조기에 중단할 수 있다.

```python
for i in range(1000):
  print(i)
```

위 루프의 경우 0에서 999까지의 숫자를 출혁한다.

```python
for i in range(1000):
  print(i)
  
  if i==10:
    break
```

`break`문을 사용한 루프는 0에서 10 사이의 숫자만 인쇄한다. `i==10` 순간, `break` 문이 실행되고 for 루프에서 더 이상 아무 일도 일어나지 않는다.

## 11. `continue` 키워드
for 루프 내에서 `continue` 키워드를 사용하면 현재 반복을 무시하고 다음 반복으로 바로 건너뛸 수 있다.

> Note - `continue` 문이 실행될 때마다 하나의 반복에만 영향을 미친다.

```python
for i in [1,2,3,4,5]:
  print('top', i)

  if i==3 or i==4:
    continue

  print('bottom', i)

# top 1
# bottom 1
# top 2
# bottom 2
# top 3
# top 4
# top 5
# bottom 5
```

`continue` 문은 `i==3`과 `i==4`일 때 실행된다. `continue` 문이 실행되면 `continue` 문 아래의 모든 문장은 실행되지 않는다. 그러나 다음 반복에는 영향이 없다.

## 12. `enuerate` 함수
내장 함수 `enuerate(list)`는 기본적으로 리스트의 한 번의 반복으로 INDEX와 ELEMENT를 모두 생성한다.

```python
fruits = ['apple', 'orange', 'pear']

for i in range(len(fruits)):
  fruit = fruits[i]
  print(i, fruit)

# 0 apple
# 1 orange
# 2 pear
```

리스트의 INDEX와 ELEMENT를 모두 인쇄하는 단순 루프이다.

```python
fruits = ['apple', 'orange', 'pear']

for i, fruit in enumerate(fruits):
  print(i, fruit)

# 0 apple
# 1 orange
# 2 pear
```

내장 `enuerate` 함수를 사용하여 동일한 작업을 수행하여 한 줄 적게 작성할 수 있다.

## 13. `zip` 함수
`zip` 함수가 내장되어 있어 2개 이상의 리스트를 동시에 반복할 수 있다.

```python
fruits = ['apple', 'orange', 'pear']
prices = [4,5,6]

for i in range(len(fruits)):
  fruit = fruits[i]
  price = prices[i]
  print(fruit, price)

# apple 4
# orange 5
# pear 6
```

위의 코드는 두 개의 리스트를 한 번에 반복하는 일반적인 방법이다.

```python
fruits = ['apple', 'orange', 'pear']
prices = [4,5,6]

for fruit, price in zip(fruits, prices):
  print(fruit, price)

# apple 4
# orange 5
# pear 6
```

`zip` 함수를 사용하여 한 번에 두 개의 리스트를 반복한다.

3개 이상의 리스트에서도 같은 작업을 수행할 수 있다.

```python
fruits = ['apple', 'orange', 'pear']
prices = [4,5,6]
quantities = [100,200,300]

for fruit, price, qty in zip(fruits, prices, quantities):
  print(fruit, price, qty)

# apple 4 100
# orange 5 200
# pear 6 300
```

## 14. Generator 함수와 `yield` 키워드
generator 함수는 `return` 키워드 대신 `yield` 키워드를 사용한다.

```python
def add10(x):
  return x + 10

n = add10(2)      # n = 12
```

보통 함수에서 `return` 문이 실행되면 이후에는 아무 일도 일어나지 않는다. (`finally` 블럭 제외)

```python
def test():
  yield 1
  yield 3
  yield 5

for i in test():
  print(i)

# 1
# 3
# 5
```

`test`는 숫자 1, 3 및 5를 생성하는 generator 함수이다. `yield`를 여러 번 할 수 있는 `return`이라고 생각할 수 있다.

## 15. `pprint` 함수
`pprint`(깔끔한 프린트)를 사용하면 데이터 구조를 보기좋게 인쇄할 수 있다.

```python
d = {
  'a': [1,2,3,4,5,6,7,8,9,10],
  'b': [1,2,3,4,5,6,7,8,9,10],
  'c': [1,2,3,4,5,6,7,8,9,10],
}

print(d)
```
```
{'a': [1,2,3,4,5,6,7,8,9,10], 'b': [1,2,3,4,5,6,7,8,9,10], 'c': [1,2,3,4,5,6,7,8,9,10]}
```

큰 딕셔너리가 있다면, 정상적으로 인쇄하면 위와 같이 출력된다 (읽기 쉽지 않음).

```python
d = {
  'a': [1,2,3,4,5,6,7,8,9,10],
  'b': [1,2,3,4,5,6,7,8,9,10],
  'c': [1,2,3,4,5,6,7,8,9,10],
}

from pprint import pprint
pprint(d)
```

```
{'a': [1,2,3,4,5,6,7,8,9,10], 
 'b': [1,2,3,4,5,6,7,8,9,10],
 'c': [1,2,3,4,5,6,7,8,9,10]}
```

`pprint`를 사용하면, 읽기 쉬운 출력을 얻을 수 있다.

> Note - Python과 함께 설치되므로 pprint를 별도로 설치할 필요는 없다. 그러나 `pprint import pprint`같이 `import`문이 필요하다.


## 16. `if __name__ = ‘__main__’`
이것은 둘 이상의 .py 파일으로 작업할 경우 매우 중요하다.

```python
# a.py
from b import *
from c import *

b_function()
c_function()
```
```python
# b.py
def b_function():
  print('from b_function')

print('hello from b.py')
```
```python
# c.py
def c_function():
  print('from c_function')

print('hello from c.py')
```
```python
# WHAT HAPPENS WHEN WE RUN a.py

hello from b.py
hello from c.py
from b_function
from c_function
```

`a.py`가 `b.py`와 `c.py`를 import할 때 `print('hello from b.py')`와 `print('hello from c.py')` 문도 실행한다.

이 문장을 실행하지 않으려면 `if __name__ == '__main__'` 문이 필요하다.

```python
# a.py
from b import *
from c import *

b_function()
c_function()
```
```python
# b.py
def b_function():
  print('from b_function')

if __name__ == '__main__':
  print('hello from b.py')
```
```python
# c.py
def c_function():
  print('from c_function')

if __name__ == '__main__':
  print('hello from c.py')
```
```python
# WHAT HAPPENS WHEN WE RUN a.py

from b_function
from c_function
```

위의 코드에 `__name__ == '__main__'` 문을 `b.py`와 `c.py` 에 모두 추가했다. 파일을 직접 실행하는 경우에만 이 if 블록 아래의 모든 코드 행이 실행된다.

- 만약 `b.py` 를 직접 실행하면, b.py 의 `hello from b.py`가 인쇄될 것이다
- 만약 `c.py` 를 직접 실행하면, b.py 의 `hello from c.py`가 인쇄될 것이다

파일을 직접 실행하면 내장된 특수 변수 `__name__`에 문자열 값 '__main__'이 자동으로 할당된다. import된 다른 파일의 경우 `__name__`은 (예: `b` 또는 `c`) 단순히 파일 이름이다.

따라서 `if __name__ == '__main_'`문은 불필요한 코드가 실수로 실행되지 않도록 하기 위하여 매우 중요하다. 더 복잡한 코드에서 이것을 많이 찾아 수 있다. 


**(주)** 위 내용은 [16 Python Quirks Beginners Need To Know To Level Up](https://python.plainenglish.io/16-python-quirks-beginners-need-to-know-to-level-up-5e0469244a95)을 편역한 것이다.
